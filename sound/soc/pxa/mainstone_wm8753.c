/*
 * mainstone.c  --  SoC audio for Mainstone
 *
 * Copyright 2005 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  Mainstone audio amplifier code taken from arch/arm/mach-pxa/mainstone.c
 *  Copyright:	MontaVista Software Inc.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    30th Oct 2005   Initial version.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/mainstone.h>
#include <asm/arch/audio.h>

#include "../codecs/wm8753.h"
#include "pxa2xx-pcm.h"

static struct snd_soc_machine mainstone;

static int mainstone_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if(rtd->cpu_dai->type == SND_SOC_DAI_PCM && rtd->cpu_dai->id == 1) {
		/* enable USB on the go MUX so we can use SSPFRM2 */
		MST_MSCWR2 |= MST_MSCWR2_USB_OTG_SEL;
		MST_MSCWR2 &= ~MST_MSCWR2_USB_OTG_RST;
	}
	return 0;
}

static void mainstone_shutdown(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if(rtd->cpu_dai->type == SND_SOC_DAI_PCM && rtd->cpu_dai->id == 1) {
		/* disable USB on the go MUX so we can use ttyS0 */
		MST_MSCWR2 &= ~MST_MSCWR2_USB_OTG_SEL;
		MST_MSCWR2 |= MST_MSCWR2_USB_OTG_RST;
	}
}

static struct snd_soc_ops mainstone_ops = {
	.startup = mainstone_startup,
	.shutdown = mainstone_shutdown,
};

static long mst_audio_suspend_mask;

static int mainstone_suspend(struct platform_device *pdev, pm_message_t state)
{
	mst_audio_suspend_mask = MST_MSCWR2;
	MST_MSCWR2 |= MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_resume(struct platform_device *pdev)
{
	MST_MSCWR2 &= mst_audio_suspend_mask | ~MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_probe(struct platform_device *pdev)
{
	MST_MSCWR2 &= ~MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_remove(struct platform_device *pdev)
{
	MST_MSCWR2 |= MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

/* example machine audio_mapnections */
static const char* audio_map[][3] = {

	/* mic is connected to mic1 - with bias */
	{"MIC1", NULL, "Mic Bias"},
	{"MIC1N", NULL, "Mic Bias"},
	{"Mic Bias", NULL, "Mic1 Jack"},
	{"Mic Bias", NULL, "Mic1 Jack"},

	{"ACIN", NULL, "ACOP"},
	{NULL, NULL, NULL},
};

/* headphone detect support on my board */
static const char * hp_pol[] = {"Headphone", "Speaker"};
static const struct soc_enum wm8753_enum =
	SOC_ENUM_SINGLE(WM8753_OUTCTL, 1, 2, hp_pol);

static const struct snd_kcontrol_new wm8753_mainstone_controls[] = {
	SOC_SINGLE("Headphone Detect Switch", WM8753_OUTCTL, 6, 1, 0),
	SOC_ENUM("Headphone Detect Polarity", wm8753_enum),
};

/*
 * This is an example machine initialisation for a wm8753 connected to a
 * Mainstone II. It is missing logic to detect hp/mic insertions and logic
 * to re-route the audio in such an event.
 */
static int mainstone_wm8753_init(struct snd_soc_codec *codec)
{
	int i, err;

	/* set up mainstone codec pins */
	snd_soc_dapm_set_endpoint(codec, "RXP", 0);
	snd_soc_dapm_set_endpoint(codec, "RXN", 0);
	snd_soc_dapm_set_endpoint(codec, "MIC2", 0);

	/* add mainstone specific controls */
	for (i = 0; i < ARRAY_SIZE(wm8753_mainstone_controls); i++) {
		if ((err = snd_ctl_add(codec->card,
				snd_soc_cnew(&wm8753_mainstone_controls[i],codec, NULL))) < 0)
			return err;
	}

	/* set up mainstone specific audio path audio_mapnects */
	for(i = 0; audio_map[i][0] != NULL; i++) {
		snd_soc_dapm_connect_input(codec, audio_map[i][0], audio_map[i][1], audio_map[i][2]);
	}

	snd_soc_dapm_sync_endpoints(codec);
	return 0;
}

unsigned int mainstone_config_sysclk(struct snd_soc_pcm_runtime *rtd,
	struct snd_soc_clock_info *info)
{
	/* wm8753 has pll that generates mclk from 13MHz xtal */
	return rtd->codec_dai->config_sysclk(rtd->codec_dai, info, 13000000);
}

static struct snd_soc_dai_link mainstone_dai[] = {
{
	.name = "WM8753",
	.stream_name = "WM8753 HiFi 1",
	.cpu_dai = &pxa_i2s_dai,
	.codec_dai = &wm8753_dai[WM8753_DAI_MODE1_HIFI],
	.init = mainstone_wm8753_init,
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "WM8753",
	.stream_name = "WM8753 Voice 1",
	.cpu_dai = &pxa_ssp_dai[PXA2XX_DAI_SSP2],
	.codec_dai = &wm8753_dai[WM8753_DAI_MODE1_VOICE],
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "WM8753",
	.stream_name = "WM8753 Voice 2",
	.cpu_dai = &pxa_ssp_dai[PXA2XX_DAI_SSP2],
	.codec_dai = &wm8753_dai[WM8753_DAI_MODE2_VOICE],
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "WM8753",
	.stream_name = "WM8753 HiFi 3",
	.cpu_dai = &pxa_i2s_dai,
	.codec_dai = &wm8753_dai[WM8753_DAI_MODE3_HIFI],
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "WM8753",
	.stream_name = "WM8753 HiFi 4",
	.cpu_dai = &pxa_i2s_dai,
	.codec_dai = &wm8753_dai[WM8753_DAI_MODE4_HIFI],
	.config_sysclk = mainstone_config_sysclk,
},
};

static struct snd_soc_machine mainstone = {
	.name = "Mainstone",
	.probe = mainstone_probe,
	.remove = mainstone_remove,
	.suspend_pre = mainstone_suspend,
	.resume_post = mainstone_resume,
	.ops = &mainstone_ops,
	.dai_link = mainstone_dai,
	.num_links = ARRAY_SIZE(mainstone_dai),
};

static struct wm8753_setup_data mainstone_wm8753_setup = {
	.i2c_address = 0x1a,
};

static struct snd_soc_device mainstone_snd_devdata = {
	.machine = &mainstone,
	.platform = &pxa2xx_soc_platform,
	.codec_dev = &soc_codec_dev_wm8753,
	.codec_data = &mainstone_wm8753_setup,
};

static struct platform_device *mainstone_snd_device;

static int __init mainstone_init(void)
{
	int ret;

	mainstone_snd_device = platform_device_alloc("soc-audio", -1);
	if (!mainstone_snd_device)
		return -ENOMEM;

	platform_set_drvdata(mainstone_snd_device, &mainstone_snd_devdata);
	mainstone_snd_devdata.dev = &mainstone_snd_device->dev;
	ret = platform_device_add(mainstone_snd_device);

	if (ret)
		platform_device_put(mainstone_snd_device);

	return ret;
}

static void __exit mainstone_exit(void)
{
	platform_device_unregister(mainstone_snd_device);
}

module_init(mainstone_init);
module_exit(mainstone_exit);

/* Module information */
MODULE_AUTHOR("Liam Girdwood, liam.girdwood@wolfsonmicro.com, www.wolfsonmicro.com");
MODULE_DESCRIPTION("ALSA SoC WM8753 Mainstone");
MODULE_LICENSE("GPL");
