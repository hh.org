/*
 * mainstone.c  --  SoC audio for Mainstone
 *
 * Copyright 2005 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  Mainstone audio amplifier code taken from arch/arm/mach-pxa/mainstone.c
 *  Copyright:	MontaVista Software Inc.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    5th June 2006   Initial version.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/mainstone.h>
#include <asm/arch/audio.h>

#include "../codecs/wm8731.h"
#include "pxa2xx-pcm.h"

static struct snd_soc_machine mainstone;


static const struct snd_soc_dapm_widget dapm_widgets[] = {
	SND_SOC_DAPM_MIC("Int Mic", NULL),
	SND_SOC_DAPM_SPK("Ext Spk", NULL),
};

static const char* intercon[][3] = {

	/* speaker connected to LHPOUT */
	{"Ext Spk", NULL, "LHPOUT"},

	/* mic is connected to Mic Jack, with WM8731 Mic Bias */
	{"MICIN", NULL, "Mic Bias"},
	{"Mic Bias", NULL, "Int Mic"},

	/* terminator */
	{NULL, NULL, NULL},
};

/*
 * Logic for a wm8731 as connected on a Endrelia ETI-B1 board.
 */
static int mainstone_wm8731_init(struct snd_soc_codec *codec)
{
	int i;


	/* Add specific widgets */
	for(i = 0; i < ARRAY_SIZE(dapm_widgets); i++) {
		snd_soc_dapm_new_control(codec, &dapm_widgets[i]);
	}

	/* Set up specific audio path interconnects */
	for(i = 0; intercon[i][0] != NULL; i++) {
		snd_soc_dapm_connect_input(codec, intercon[i][0], intercon[i][1], intercon[i][2]);
	}

	/* not connected */
	snd_soc_dapm_set_endpoint(codec, "RLINEIN", 0);
	snd_soc_dapm_set_endpoint(codec, "LLINEIN", 0);

	/* always connected */
	snd_soc_dapm_set_endpoint(codec, "Int Mic", 1);
	snd_soc_dapm_set_endpoint(codec, "Ext Spk", 1);

	snd_soc_dapm_sync_endpoints(codec);

	return 0;
}

unsigned int mainstone_config_sysclk(struct snd_soc_pcm_runtime *rtd,
	struct snd_soc_clock_info *info)
{
	/* we have a 12.288MHz crystal */
	return rtd->codec_dai->config_sysclk(rtd->codec_dai, info, 12288000);
}

static struct snd_soc_dai_link mainstone_dai[] = {
{
	.name = "WM8731",
	.stream_name = "WM8731 HiFi",
	.cpu_dai = &pxa_i2s_dai,
	.codec_dai = &wm8731_dai,
	.init = mainstone_wm8731_init,
	.config_sysclk = mainstone_config_sysclk,
},
};

static struct snd_soc_machine mainstone = {
	.name = "Mainstone",
	.dai_link = mainstone_dai,
	.num_links = ARRAY_SIZE(mainstone_dai),
};

static struct wm8731_setup_data corgi_wm8731_setup = {
	.i2c_address = 0x1b,
};

static struct snd_soc_device mainstone_snd_devdata = {
	.machine = &mainstone,
	.platform = &pxa2xx_soc_platform,
	.codec_dev = &soc_codec_dev_wm8731,
	.codec_data = &corgi_wm8731_setup,
};

static struct platform_device *mainstone_snd_device;

static int __init mainstone_init(void)
{
	int ret;

	mainstone_snd_device = platform_device_alloc("soc-audio", -1);
	if (!mainstone_snd_device)
		return -ENOMEM;

	platform_set_drvdata(mainstone_snd_device, &mainstone_snd_devdata);
	mainstone_snd_devdata.dev = &mainstone_snd_device->dev;
	ret = platform_device_add(mainstone_snd_device);

	if (ret)
		platform_device_put(mainstone_snd_device);

	return ret;
}

static void __exit mainstone_exit(void)
{
	platform_device_unregister(mainstone_snd_device);
}

module_init(mainstone_init);
module_exit(mainstone_exit);

/* Module information */
MODULE_AUTHOR("Liam Girdwood, liam.girdwood@wolfsonmicro.com, www.wolfsonmicro.com");
MODULE_DESCRIPTION("ALSA SoC WM8731 Mainstone");
MODULE_LICENSE("GPL");
