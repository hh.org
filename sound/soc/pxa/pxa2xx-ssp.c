/*
 * pxa2xx-ssp.c  --  ALSA Soc Audio Layer
 *
 * Copyright 2005 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    12th Aug 2005   Initial version.
 *
 * TODO:
 *  o Fix master mode (bug)
 *  o Fix resume (bug)
 *  o Add support for other clocks
 *  o Test network mode for > 16bit sample size
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>

#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/initval.h>
#include <sound/soc.h>

#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/audio.h>
#include <asm/arch/ssp.h>

#include "pxa2xx-pcm.h"

/*
 * SSP sysclock frequency in Hz
 * Neither default pxa2xx PLL clocks are good for audio, hence pxa27x
 * has audio clock. I would recommend using the pxa27x audio clock or an
 * external clock or making the codec master to gurantee better sample rates.
 */
#ifdef CONFIG_PXA27x
static int sysclk[3] = {13000000, 13000000, 13000000};
#else
static int sysclk[3] = {1843200, 1843200, 1843200};
#endif
module_param_array(sysclk, int, NULL, 0);
MODULE_PARM_DESC(sysclk, "sysclk frequency in Hz");

/*
 * SSP sysclock source.
 * sysclk is ignored if audio clock is used
 */
#ifdef CONFIG_PXA27x
static int clksrc[3] = {0, 0, 0};
#else
static int clksrc[3] = {0, 0, 0};
#endif
module_param_array(clksrc, int, NULL, 0);
MODULE_PARM_DESC(clksrc,
	"sysclk source, 0 = internal PLL, 1 = ext, 2 = network, 3 = audio clock");

/*
 * SSP GPIO's
 */
#define GPIO26_SSP1RX_MD	(26 | GPIO_ALT_FN_1_IN)
#define GPIO25_SSP1TX_MD	(25 | GPIO_ALT_FN_2_OUT)
#define GPIO23_SSP1CLKS_MD	(23 | GPIO_ALT_FN_2_IN)
#define GPIO24_SSP1FRMS_MD	(24 | GPIO_ALT_FN_2_IN)
#define GPIO23_SSP1CLKM_MD	(23 | GPIO_ALT_FN_2_OUT)
#define GPIO24_SSP1FRMM_MD	(24 | GPIO_ALT_FN_2_OUT)

#define GPIO11_SSP2RX_MD	(11 | GPIO_ALT_FN_2_IN)
#define GPIO13_SSP2TX_MD	(13 | GPIO_ALT_FN_1_OUT)
#define GPIO22_SSP2CLKS_MD	(22 | GPIO_ALT_FN_3_IN)
#define GPIO88_SSP2FRMS_MD	(88 | GPIO_ALT_FN_3_IN)
#define GPIO22_SSP2CLKM_MD	(22 | GPIO_ALT_FN_3_OUT)
#define GPIO88_SSP2FRMM_MD	(88 | GPIO_ALT_FN_3_OUT)

#define GPIO82_SSP3RX_MD	(82 | GPIO_ALT_FN_1_IN)
#define GPIO81_SSP3TX_MD	(81 | GPIO_ALT_FN_1_OUT)
#define GPIO84_SSP3CLKS_MD	(84 | GPIO_ALT_FN_1_IN)
#define GPIO83_SSP3FRMS_MD	(83 | GPIO_ALT_FN_1_IN)
#define GPIO84_SSP3CLKM_MD	(84 | GPIO_ALT_FN_1_OUT)
#define GPIO83_SSP3FRMM_MD	(83 | GPIO_ALT_FN_1_OUT)

#define PXA_SSP_MDAIFMT \
	(SND_SOC_DAIFMT_DSP_B  |SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBS_CFS | SND_SOC_DAIFMT_CBM_CFS | \
	SND_SOC_DAIFMT_CBS_CFM | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_NB_IF)

#define PXA_SSP_SDAIFMT \
	(SND_SOC_DAIFMT_DSP_B  |SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS | \
	SND_SOC_DAIFMT_CBS_CFM | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_NB_IF)

#define PXA_SSP_DIR \
	(SND_SOC_DAIDIR_PLAYBACK | SND_SOC_DAIDIR_CAPTURE)

#define PXA_SSP_RATES \
	(SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_11025 | SNDRV_PCM_RATE_16000 | \
	SNDRV_PCM_RATE_22050 | SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 | \
	SNDRV_PCM_RATE_48000 | SNDRV_PCM_RATE_88200 | SNDRV_PCM_RATE_96000 | \
	SNDRV_PCM_RATE_176400 | SNDRV_PCM_RATE_192000)

#define PXA_SSP_BITS \
	(SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE | \
	SNDRV_PCM_FMTBIT_S24_LE | SNDRV_PCM_FMTBIT_S32_LE)

/*
 * SSP modes
 */
static struct snd_soc_dai_mode pxa2xx_ssp_modes[] = {
	/* port slave clk & frame modes */
	{
		.fmt = PXA_SSP_SDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = PXA_SSP_RATES,
		.pcmdir = PXA_SSP_DIR,
		.fs = SND_SOC_FS_ALL,
		.bfs = SND_SOC_FSB_ALL,
	},

	/* port master clk & frame modes */
#ifdef CONFIG_PXA27x
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_11025,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_16000,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_22050,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_32000,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_44100,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_48000,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 256,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_88200,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 128,
		.bfs = SND_SOC_FSBW(1),
	},
	{
		.fmt = PXA_SSP_MDAIFMT,
		.pcmfmt = PXA_SSP_BITS,
		.pcmrate = SNDRV_PCM_RATE_96000,
		.pcmdir = PXA_SSP_DIR,
		.flags = SND_SOC_DAI_BFS_RCW,
		.fs = 128,
		.bfs = SND_SOC_FSBW(1),
	},
#endif
};

static struct ssp_dev ssp[3];
#ifdef CONFIG_PM
static struct ssp_state ssp_state[3];
#endif

static struct pxa2xx_pcm_dma_params pxa2xx_ssp1_pcm_mono_out = {
	.name			= "SSP1 PCM Mono out",
	.dev_addr		= __PREG(SSDR_P1),
	.drcmr			= &DRCMRTXSSDR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp1_pcm_mono_in = {
	.name			= "SSP1 PCM Mono in",
	.dev_addr		= __PREG(SSDR_P1),
	.drcmr			= &DRCMRRXSSDR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp1_pcm_stereo_out = {
	.name			= "SSP1 PCM Stereo out",
	.dev_addr		= __PREG(SSDR_P1),
	.drcmr			= &DRCMRTXSSDR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp1_pcm_stereo_in = {
	.name			= "SSP1 PCM Stereo in",
	.dev_addr		= __PREG(SSDR_P1),
	.drcmr			= &DRCMRRXSSDR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp2_pcm_mono_out = {
	.name			= "SSP2 PCM Mono out",
	.dev_addr		= __PREG(SSDR_P2),
	.drcmr			= &DRCMRTXSS2DR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp2_pcm_mono_in = {
	.name			= "SSP2 PCM Mono in",
	.dev_addr		= __PREG(SSDR_P2),
	.drcmr			= &DRCMRRXSS2DR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp2_pcm_stereo_out = {
	.name			= "SSP2 PCM Stereo out",
	.dev_addr		= __PREG(SSDR_P2),
	.drcmr			= &DRCMRTXSS2DR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp2_pcm_stereo_in = {
	.name			= "SSP2 PCM Stereo in",
	.dev_addr		= __PREG(SSDR_P2),
	.drcmr			= &DRCMRRXSS2DR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp3_pcm_mono_out = {
	.name			= "SSP3 PCM Mono out",
	.dev_addr		= __PREG(SSDR_P3),
	.drcmr			= &DRCMRTXSS3DR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp3_pcm_mono_in = {
	.name			= "SSP3 PCM Mono in",
	.dev_addr		= __PREG(SSDR_P3),
	.drcmr			= &DRCMRRXSS3DR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH2,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp3_pcm_stereo_out = {
	.name			= "SSP3 PCM Stereo out",
	.dev_addr		= __PREG(SSDR_P3),
	.drcmr			= &DRCMRTXSS3DR,
	.dcmd			= DCMD_INCSRCADDR | DCMD_FLOWTRG |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params pxa2xx_ssp3_pcm_stereo_in = {
	.name			= "SSP3 PCM Stereo in",
	.dev_addr		= __PREG(SSDR_P3),
	.drcmr			= &DRCMRRXSS3DR,
	.dcmd			= DCMD_INCTRGADDR | DCMD_FLOWSRC |
				  DCMD_BURST16 | DCMD_WIDTH4,
};

static struct pxa2xx_pcm_dma_params *ssp_dma_params[3][4] = {
	{&pxa2xx_ssp1_pcm_mono_out, &pxa2xx_ssp1_pcm_mono_in,
	&pxa2xx_ssp1_pcm_stereo_out,&pxa2xx_ssp1_pcm_stereo_in,},
	{&pxa2xx_ssp2_pcm_mono_out, &pxa2xx_ssp2_pcm_mono_in,
	&pxa2xx_ssp2_pcm_stereo_out, &pxa2xx_ssp2_pcm_stereo_in,},
	{&pxa2xx_ssp3_pcm_mono_out, &pxa2xx_ssp3_pcm_mono_in,
	&pxa2xx_ssp3_pcm_stereo_out,&pxa2xx_ssp3_pcm_stereo_in,},
};

static struct pxa2xx_gpio ssp_gpios[3][4] = {
	{{ /* SSP1 SND_SOC_DAIFMT_CBM_CFM */
		.rx = GPIO26_SSP1RX_MD,
		.tx = GPIO25_SSP1TX_MD,
		.clk = (23 | GPIO_ALT_FN_2_IN),
		.frm = (24 | GPIO_ALT_FN_2_IN),
	},
	{ /* SSP1 SND_SOC_DAIFMT_CBS_CFS */
		.rx = GPIO26_SSP1RX_MD,
		.tx = GPIO25_SSP1TX_MD,
		.clk = (23 | GPIO_ALT_FN_2_OUT),
		.frm = (24 | GPIO_ALT_FN_2_OUT),
	},
	{ /* SSP1 SND_SOC_DAIFMT_CBS_CFM */
		.rx = GPIO26_SSP1RX_MD,
		.tx = GPIO25_SSP1TX_MD,
		.clk = (23 | GPIO_ALT_FN_2_OUT),
		.frm = (24 | GPIO_ALT_FN_2_IN),
	},
	{ /* SSP1 SND_SOC_DAIFMT_CBM_CFS */
		.rx = GPIO26_SSP1RX_MD,
		.tx = GPIO25_SSP1TX_MD,
		.clk = (23 | GPIO_ALT_FN_2_IN),
		.frm = (24 | GPIO_ALT_FN_2_OUT),
	}},
	{{ /* SSP2 SND_SOC_DAIFMT_CBM_CFM */
		.rx = GPIO11_SSP2RX_MD,
		.tx = GPIO13_SSP2TX_MD,
		.clk = (22 | GPIO_ALT_FN_3_IN),
		.frm = (88 | GPIO_ALT_FN_3_IN),
	},
	{ /* SSP2 SND_SOC_DAIFMT_CBS_CFS */
		.rx = GPIO11_SSP2RX_MD,
		.tx = GPIO13_SSP2TX_MD,
		.clk = (22 | GPIO_ALT_FN_3_OUT),
		.frm = (88 | GPIO_ALT_FN_3_OUT),
	},
	{ /* SSP2 SND_SOC_DAIFMT_CBS_CFM */
		.rx = GPIO11_SSP2RX_MD,
		.tx = GPIO13_SSP2TX_MD,
		.clk = (22 | GPIO_ALT_FN_3_OUT),
		.frm = (88 | GPIO_ALT_FN_3_IN),
	},
	{ /* SSP2 SND_SOC_DAIFMT_CBM_CFS */
		.rx = GPIO11_SSP2RX_MD,
		.tx = GPIO13_SSP2TX_MD,
		.clk = (22 | GPIO_ALT_FN_3_IN),
		.frm = (88 | GPIO_ALT_FN_3_OUT),
	}},
	{{ /* SSP3 SND_SOC_DAIFMT_CBM_CFM */
		.rx = GPIO82_SSP3RX_MD,
		.tx = GPIO81_SSP3TX_MD,
		.clk = (84 | GPIO_ALT_FN_3_IN),
		.frm = (83 | GPIO_ALT_FN_3_IN),
	},
	{ /* SSP3 SND_SOC_DAIFMT_CBS_CFS */
		.rx = GPIO82_SSP3RX_MD,
		.tx = GPIO81_SSP3TX_MD,
		.clk = (84 | GPIO_ALT_FN_3_OUT),
		.frm = (83 | GPIO_ALT_FN_3_OUT),
	},
	{ /* SSP3 SND_SOC_DAIFMT_CBS_CFM */
		.rx = GPIO82_SSP3RX_MD,
		.tx = GPIO81_SSP3TX_MD,
		.clk = (84 | GPIO_ALT_FN_3_OUT),
		.frm = (83 | GPIO_ALT_FN_3_IN),
	},
	{ /* SSP3 SND_SOC_DAIFMT_CBM_CFS */
		.rx = GPIO82_SSP3RX_MD,
		.tx = GPIO81_SSP3TX_MD,
		.clk = (84 | GPIO_ALT_FN_3_IN),
		.frm = (83 | GPIO_ALT_FN_3_OUT),
	}},
};

static int pxa2xx_ssp_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	int ret = 0;

	if (!rtd->cpu_dai->active) {
		ret = ssp_init (&ssp[rtd->cpu_dai->id], rtd->cpu_dai->id + 1,
			SSP_NO_IRQ);
		if (ret < 0)
			return ret;
		ssp_disable(&ssp[rtd->cpu_dai->id]);
	}
	return ret;
}

static void pxa2xx_ssp_shutdown(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if (!rtd->cpu_dai->active) {
		ssp_disable(&ssp[rtd->cpu_dai->id]);
		ssp_exit(&ssp[rtd->cpu_dai->id]);
	}
}

#ifdef CONFIG_PM

#if defined (CONFIG_PXA27x)
static int cken[3] = {CKEN23_SSP1, CKEN3_SSP2, CKEN4_SSP3};
#else
static int cken[3] = {CKEN3_SSP, CKEN9_NSSP, CKEN10_ASSP};
#endif

static int pxa2xx_ssp_suspend(struct platform_device *pdev,
	struct snd_soc_cpu_dai *dai)
{
	if (!dai->active)
		return 0;

	ssp_save_state(&ssp[dai->id], &ssp_state[dai->id]);
	pxa_set_cken(cken[dai->id], 0);
	return 0;
}

static int pxa2xx_ssp_resume(struct platform_device *pdev,
	struct snd_soc_cpu_dai *dai)
{
	if (!dai->active)
		return 0;

	pxa_set_cken(cken[dai->id], 1);
	ssp_restore_state(&ssp[dai->id], &ssp_state[dai->id]);
	ssp_enable(&ssp[dai->id]);

	return 0;
}

#else
#define pxa2xx_ssp_suspend	NULL
#define pxa2xx_ssp_resume	NULL
#endif

/* todo - check clk source and PLL before returning clock rate */
static unsigned int pxa_ssp_config_sysclk(struct snd_soc_cpu_dai *dai,
	struct snd_soc_clock_info *info, unsigned int clk)
{
	/* audio clock ? (divide by 1) */
	if (clksrc[dai->id] == 3) {
		switch(info->rate){
		case 8000:
		case 16000:
		case 32000:
		case 48000:
		case 96000:
			return 12288000;
			break;
		case 11025:
		case 22050:
		case 44100:
		case 88200:
			return 11289600;
		break;
		}
	}

	/* pll */
	return sysclk[dai->id];
}

#ifdef CONFIG_PXA27x
static u32 pxa27x_set_audio_clk(unsigned int rate, unsigned int fs)
{
	u32 aclk = 0, div = 0;

	if (rate == 0 || fs == 0)
		return 0;

	switch(rate){
	case 8000:
	case 16000:
	case 32000:
	case 48000:
	case 96000:
		aclk = 0x2 << 4;
		div = 12288000 / (rate * fs);
		break;
	case 11025:
	case 22050:
	case 44100:
	case 88200:
		aclk = 0x1 << 4;
		div = 11289600 / (rate * fs);
		break;
	}

	aclk |= ffs(div) - 1;
	return aclk;
}
#endif

static inline int get_scr(int srate, int id)
{
	if (srate == 0)
		return 0;
	return (sysclk[id] / srate) - 1;
}

static int pxa2xx_ssp_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	int fmt = 0, dma = 0, fs, chn = params_channels(params);
	u32 ssp_mode = 0, ssp_setup = 0, psp_mode = 0, rate = 0;

	fs = rtd->cpu_dai->dai_runtime.fs;

	/* select correct DMA params */
	if (substream->stream != SNDRV_PCM_STREAM_PLAYBACK)
		dma = 1;
	if (chn == 2 || rtd->cpu_dai->dai_runtime.pcmfmt != PXA_SSP_BITS)
		dma += 2;
	rtd->cpu_dai->dma_data = ssp_dma_params[rtd->cpu_dai->id][dma];

	/* is port used by another stream */
	if (SSCR0 & SSCR0_SSE)
		return 0;

	/* bit size */
	switch(rtd->cpu_dai->dai_runtime.pcmfmt) {
	case SNDRV_PCM_FMTBIT_S16_LE:
		ssp_mode |=SSCR0_DataSize(16);
		break;
	case SNDRV_PCM_FMTBIT_S24_LE:
		ssp_mode |=(SSCR0_EDSS | SSCR0_DataSize(8));
		/* use network mode for stereo samples > 16 bits */
		if (chn == 2) {
			ssp_mode |= (SSCR0_MOD | SSCR0_SlotsPerFrm(2) << 24);
			/* active slots 0,1 */
			SSTSA_P(rtd->cpu_dai->id +1) = 0x3;
			SSRSA_P(rtd->cpu_dai->id +1) = 0x3;
		}
		break;
	case SNDRV_PCM_FMTBIT_S32_LE:
		ssp_mode |= (SSCR0_EDSS | SSCR0_DataSize(16));
		/* use network mode for stereo samples > 16 bits */
		if (chn == 2) {
			ssp_mode |= (SSCR0_MOD | SSCR0_SlotsPerFrm(2) << 24);
			/* active slots 0,1 */
			SSTSA_P(rtd->cpu_dai->id +1) = 0x3;
			SSRSA_P(rtd->cpu_dai->id +1) = 0x3;
		}
		break;
	}

	ssp_mode |= SSCR0_PSP;
	ssp_setup = SSCR1_RxTresh(14) | SSCR1_TxTresh(1) |
		SSCR1_TRAIL | SSCR1_RWOT;

	switch(rtd->cpu_dai->dai_runtime.fmt & SND_SOC_DAIFMT_CLOCK_MASK) {
	case SND_SOC_DAIFMT_CBM_CFM:
		ssp_setup |= (SSCR1_SCLKDIR | SSCR1_SFRMDIR);
		break;
	case SND_SOC_DAIFMT_CBM_CFS:
		ssp_setup |= SSCR1_SCLKDIR;
		break;
	case SND_SOC_DAIFMT_CBS_CFM:
		ssp_setup |= SSCR1_SFRMDIR;
		break;
	}

	switch(rtd->cpu_dai->dai_runtime.fmt) {
	case SND_SOC_DAIFMT_CBS_CFS:
		fmt = 1;
		break;
	case SND_SOC_DAIFMT_CBS_CFM:
		fmt = 2;
		break;
	case SND_SOC_DAIFMT_CBM_CFS:
		fmt = 3;
		break;
	}

	pxa_gpio_mode(ssp_gpios[rtd->cpu_dai->id][fmt].rx);
	pxa_gpio_mode(ssp_gpios[rtd->cpu_dai->id][fmt].tx);
	pxa_gpio_mode(ssp_gpios[rtd->cpu_dai->id][fmt].frm);
	pxa_gpio_mode(ssp_gpios[rtd->cpu_dai->id][fmt].clk);

	switch (rtd->cpu_dai->dai_runtime.fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
		psp_mode |= SSPSP_SFRMP | SSPSP_FSRT;
		break;
	}

	if (rtd->cpu_dai->dai_runtime.fmt & SND_SOC_DAIFMT_DSP_A)
		psp_mode |= SSPSP_SCMODE(2);
	if (rtd->cpu_dai->dai_runtime.fmt & SND_SOC_DAIFMT_DSP_B)
		psp_mode |= SSPSP_SCMODE(3);

	switch(clksrc[rtd->cpu_dai->id]) {
	case 2: /* network clock */
		ssp_mode |= SSCR0_NCS | SSCR0_MOD;
	case 1: /* external clock */
		ssp_mode |= SSCR0_ECS;
	case 0: /* internal clock */
		rate = get_scr(snd_soc_get_rate(rtd->cpu_dai->dai_runtime.pcmrate),
			rtd->cpu_dai->id);
		break;
#ifdef CONFIG_PXA27x
	case 3: /* audio clock */
		ssp_mode |= (1 << 30);
		SSACD_P(rtd->cpu_dai->id) = (0x1 << 3) |
			pxa27x_set_audio_clk(
				snd_soc_get_rate(rtd->cpu_dai->dai_runtime.pcmrate), fs);
	 	break;
#endif
	}

	ssp_config(&ssp[rtd->cpu_dai->id], ssp_mode, ssp_setup, psp_mode,
		SSCR0_SerClkDiv(rate));
#if 0
	printk("SSCR0 %x SSCR1 %x SSTO %x SSPSP %x SSSR %x\n",
		SSCR0_P(rtd->cpu_dai->id+1), SSCR1_P(rtd->cpu_dai->id+1),
		SSTO_P(rtd->cpu_dai->id+1), SSPSP_P(rtd->cpu_dai->id+1),
		SSSR_P(rtd->cpu_dai->id+1));
#endif
	return 0;
}

static int pxa2xx_ssp_trigger(struct snd_pcm_substream *substream, int cmd)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	int ret = 0;

	switch (cmd) {
	case SNDRV_PCM_TRIGGER_RESUME:
		ssp_enable(&ssp[rtd->cpu_dai->id]);
		break;
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSCR1_P(rtd->cpu_dai->id+1) |= SSCR1_TSRE;
		else
			SSCR1_P(rtd->cpu_dai->id+1) |= SSCR1_RSRE;
		SSSR_P(rtd->cpu_dai->id+1) |= SSSR_P(rtd->cpu_dai->id+1);
		break;
	case SNDRV_PCM_TRIGGER_START:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSCR1_P(rtd->cpu_dai->id+1) |= SSCR1_TSRE;
		else
			SSCR1_P(rtd->cpu_dai->id+1) |= SSCR1_RSRE;
		ssp_enable(&ssp[rtd->cpu_dai->id]);
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSCR1_P(rtd->cpu_dai->id+1) &= ~SSCR1_TSRE;
		else
			SSCR1_P(rtd->cpu_dai->id+1) &= ~SSCR1_RSRE;
		break;
	case SNDRV_PCM_TRIGGER_SUSPEND:
		ssp_disable(&ssp[rtd->cpu_dai->id]);
		break;
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSCR1_P(rtd->cpu_dai->id+1) &= ~SSCR1_TSRE;
		else
			SSCR1_P(rtd->cpu_dai->id+1) &= ~SSCR1_RSRE;
		break;

	default:
		ret = -EINVAL;
	}
#if 0
	printk("SSCR0 %x SSCR1 %x SSTO %x SSPSP %x SSSR %x\n",
		SSCR0_P(rtd->cpu_dai->id+1), SSCR1_P(rtd->cpu_dai->id+1),
		SSTO_P(rtd->cpu_dai->id+1), SSPSP_P(rtd->cpu_dai->id+1),
		SSSR_P(rtd->cpu_dai->id+1));
#endif
	return ret;
}

struct snd_soc_cpu_dai pxa_ssp_dai[] = {
	{	.name = "pxa2xx-ssp1",
		.id = 0,
		.type = SND_SOC_DAI_PCM,
		.suspend = pxa2xx_ssp_suspend,
		.resume = pxa2xx_ssp_resume,
		.config_sysclk = pxa_ssp_config_sysclk,
		.playback = {
			.channels_min = 1,
			.channels_max = 2,},
		.capture = {
			.channels_min = 1,
			.channels_max = 2,},
		.ops = {
			.startup = pxa2xx_ssp_startup,
			.shutdown = pxa2xx_ssp_shutdown,
			.trigger = pxa2xx_ssp_trigger,
			.hw_params = pxa2xx_ssp_hw_params,},
		.caps = {
			.mode = pxa2xx_ssp_modes,
			.num_modes = ARRAY_SIZE(pxa2xx_ssp_modes),},
	},
	{	.name = "pxa2xx-ssp2",
		.id = 1,
		.type = SND_SOC_DAI_PCM,
		.suspend = pxa2xx_ssp_suspend,
		.resume = pxa2xx_ssp_resume,
		.config_sysclk = pxa_ssp_config_sysclk,
		.playback = {
			.channels_min = 1,
			.channels_max = 2,},
		.capture = {
			.channels_min = 1,
			.channels_max = 2,},
		.ops = {
			.startup = pxa2xx_ssp_startup,
			.shutdown = pxa2xx_ssp_shutdown,
			.trigger = pxa2xx_ssp_trigger,
			.hw_params = pxa2xx_ssp_hw_params,},
		.caps = {
			.mode = pxa2xx_ssp_modes,
			.num_modes = ARRAY_SIZE(pxa2xx_ssp_modes),},
	},
	{	.name = "pxa2xx-ssp3",
		.id = 2,
		.type = SND_SOC_DAI_PCM,
		.suspend = pxa2xx_ssp_suspend,
		.resume = pxa2xx_ssp_resume,
		.config_sysclk = pxa_ssp_config_sysclk,
		.playback = {
			.channels_min = 1,
			.channels_max = 2,},
		.capture = {
			.channels_min = 1,
			.channels_max = 2,},
		.ops = {
			.startup = pxa2xx_ssp_startup,
			.shutdown = pxa2xx_ssp_shutdown,
			.trigger = pxa2xx_ssp_trigger,
			.hw_params = pxa2xx_ssp_hw_params,},
		.caps = {
			.mode = pxa2xx_ssp_modes,
			.num_modes = ARRAY_SIZE(pxa2xx_ssp_modes),},
	},
};

EXPORT_SYMBOL_GPL(pxa_ssp_dai);

/* Module information */
MODULE_AUTHOR("Liam Girdwood, liam.girdwood@wolfsonmicro.com, www.wolfsonmicro.com");
MODULE_DESCRIPTION("pxa2xx SSP/PCM SoC Interface");
MODULE_LICENSE("GPL");
