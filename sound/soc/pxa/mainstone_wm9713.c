/*
 * mainstone.c  --  SoC audio for Mainstone
 *
 * Copyright 2006 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  Mainstone audio amplifier code taken from arch/arm/mach-pxa/mainstone.c
 *  Copyright:	MontaVista Software Inc.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    29th Jan 2006   Initial version.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/mainstone.h>
#include <asm/arch/audio.h>

#include "../codecs/wm9713.h"
#include "pxa2xx-pcm.h"

static struct snd_soc_machine mainstone;

static int mainstone_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if(rtd->cpu_dai->type == SND_SOC_DAI_PCM && rtd->cpu_dai->id == 1) {
		/* enable USB on the go MUX so we can use SSPFRM2 */
		MST_MSCWR2 |= MST_MSCWR2_USB_OTG_SEL;
		MST_MSCWR2 &= ~MST_MSCWR2_USB_OTG_RST;
	}
	return 0;
}

static void mainstone_shutdown(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if(rtd->cpu_dai->type == SND_SOC_DAI_PCM && rtd->cpu_dai->id == 1) {
		/* disable USB on the go MUX so we can use ttyS0 */
		MST_MSCWR2 &= ~MST_MSCWR2_USB_OTG_SEL;
		MST_MSCWR2 |= MST_MSCWR2_USB_OTG_RST;
	}
}

static struct snd_soc_ops mainstone_ops = {
	.startup = mainstone_startup,
	.shutdown = mainstone_shutdown,
};

static int test = 0;
static int get_test(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = test;
	return 0;
}

static int set_test(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);

	test = ucontrol->value.integer.value[0];
	if(test) {

	} else {

	}
	return 0;
}

static long mst_audio_suspend_mask;

static int mainstone_suspend(struct platform_device *pdev, pm_message_t state)
{
	mst_audio_suspend_mask = MST_MSCWR2;
	MST_MSCWR2 |= MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_resume(struct platform_device *pdev)
{
	MST_MSCWR2 &= mst_audio_suspend_mask | ~MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_probe(struct platform_device *pdev)
{
	MST_MSCWR2 &= ~MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static int mainstone_remove(struct platform_device *pdev)
{
	MST_MSCWR2 |= MST_MSCWR2_AC97_SPKROFF;
	return 0;
}

static const char* test_function[] = {"Off", "On"};
static const struct soc_enum mainstone_enum[] = {
	SOC_ENUM_SINGLE_EXT(2, test_function),
};

static const struct snd_kcontrol_new mainstone_controls[] = {
	SOC_ENUM_EXT("ATest Function", mainstone_enum[0], get_test, set_test),
};

/* mainstone machine dapm widgets */
static const struct snd_soc_dapm_widget mainstone_dapm_widgets[] = {
	SND_SOC_DAPM_MIC("Mic 1", NULL),
	SND_SOC_DAPM_MIC("Mic 2", NULL),
	SND_SOC_DAPM_MIC("Mic 3", NULL),
};

/* example machine audio_mapnections */
static const char* audio_map[][3] = {

	/* mic is connected to mic1 - with bias */
	{"MIC1", NULL, "Mic Bias"},
	{"Mic Bias", NULL, "Mic 1"},
	/* mic is connected to mic2A - with bias */
	{"MIC2A", NULL, "Mic Bias"},
	{"Mic Bias", NULL, "Mic 2"},
	/* mic is connected to mic2B - with bias */
	{"MIC2B", NULL, "Mic Bias"},
	{"Mic Bias", NULL, "Mic 3"},

	{NULL, NULL, NULL},
};

/*
 * This is an example machine initialisation for a wm9713 connected to a
 * Mainstone II. It is missing logic to detect hp/mic insertions and logic
 * to re-route the audio in such an event.
 */
static int mainstone_wm9713_init(struct snd_soc_codec *codec)
{
	int i, err;

	/* set up mainstone codec pins */
	snd_soc_dapm_set_endpoint(codec, "RXP", 0);
	snd_soc_dapm_set_endpoint(codec, "RXN", 0);
	//snd_soc_dapm_set_endpoint(codec, "MIC2", 0);

	/* Add test specific controls */
	for (i = 0; i < ARRAY_SIZE(mainstone_controls); i++) {
		if ((err = snd_ctl_add(codec->card,
				snd_soc_cnew(&mainstone_controls[i],codec, NULL))) < 0)
			return err;
	}

	/* Add mainstone specific widgets */
	for(i = 0; i < ARRAY_SIZE(mainstone_dapm_widgets); i++) {
		snd_soc_dapm_new_control(codec, &mainstone_dapm_widgets[i]);
	}

	/* set up mainstone specific audio path audio_mapnects */
	for(i = 0; audio_map[i][0] != NULL; i++) {
		snd_soc_dapm_connect_input(codec, audio_map[i][0], audio_map[i][1], audio_map[i][2]);
	}

	snd_soc_dapm_sync_endpoints(codec);
	return 0;
}

/* configure the system audio clock */
unsigned int mainstone_config_sysclk(struct snd_soc_pcm_runtime *rtd,
	struct snd_soc_clock_info *info)
{
	return rtd->codec_dai->config_sysclk(rtd->codec_dai, info, 24576000);
}

static struct snd_soc_dai_link mainstone_dai[] = {
{
	.name = "AC97",
	.stream_name = "AC97 HiFi",
	.cpu_dai = &pxa_ac97_dai[PXA2XX_DAI_AC97_HIFI],
	.codec_dai = &wm9713_dai[WM9713_DAI_AC97_HIFI],
	.init = mainstone_wm9713_init,
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "AC97 Aux",
	.stream_name = "AC97 Aux",
	.cpu_dai = &pxa_ac97_dai[PXA2XX_DAI_AC97_AUX],
	.codec_dai = &wm9713_dai[WM9713_DAI_AC97_AUX],
	.config_sysclk = mainstone_config_sysclk,
},
{
	.name = "WM9713",
	.stream_name = "WM9713 Voice",
	.cpu_dai = &pxa_ssp_dai[PXA2XX_DAI_SSP2],
	.codec_dai = &wm9713_dai[WM9713_DAI_PCM_VOICE],
	.config_sysclk = mainstone_config_sysclk,
},
};

static struct snd_soc_machine mainstone = {
	.name = "Mainstone",
	.probe = mainstone_probe,
	.remove = mainstone_remove,
	.suspend_pre = mainstone_suspend,
	.resume_post = mainstone_resume,
	.ops = &mainstone_ops,
	.dai_link = mainstone_dai,
	.num_links = ARRAY_SIZE(mainstone_dai),
};

static struct snd_soc_device mainstone_snd_ac97_devdata = {
	.machine = &mainstone,
	.platform = &pxa2xx_soc_platform,
	.codec_dev = &soc_codec_dev_wm9713,
};

static struct platform_device *mainstone_snd_ac97_device;

static int __init mainstone_init(void)
{
	int ret;

	mainstone_snd_ac97_device = platform_device_alloc("soc-audio", -1);
	if (!mainstone_snd_ac97_device)
		return -ENOMEM;

	platform_set_drvdata(mainstone_snd_ac97_device, &mainstone_snd_ac97_devdata);
	mainstone_snd_ac97_devdata.dev = &mainstone_snd_ac97_device->dev;

	if((ret = platform_device_add(mainstone_snd_ac97_device)) != 0)
		platform_device_put(mainstone_snd_ac97_device);

	return ret;
}

static void __exit mainstone_exit(void)
{
	platform_device_unregister(mainstone_snd_ac97_device);
}

module_init(mainstone_init);
module_exit(mainstone_exit);

/* Module information */
MODULE_AUTHOR("Liam Girdwood, liam.girdwood@wolfsonmicro.com, www.wolfsonmicro.com");
MODULE_DESCRIPTION("ALSA SoC WM9713 Mainstone");
MODULE_LICENSE("GPL");
