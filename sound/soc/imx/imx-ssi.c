/*
 * imx-ssi.c  --  SSI driver for Freescale IMX
 *
 * Copyright 2006 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  Based on mxc-alsa-mc13783 (C) 2006 Freescale.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  Revision history
 *    29th Aug 2006   Initial version.
 *
 */

#define IMX_DSP_DAIFMT \
	( SND_SOC_DAIFMT_DSP__A |SND_SOC_DAIFMT_DSP_B  | \
	SND_SOC_DAIFMT_CBS_CFS |SND_SOC_DAIFMT_CBM_CFS | \
	SND_SOC_DAIFMT_CBS_CFM |SND_SOC_DAIFMT_NB_NF |\
	SND_SOC_DAIFMT_NB_IF)

#define IMX_DSP_DIR \
	(SND_SOC_DAIDIR_PLAYBACK | SND_SOC_DAIDIR_CAPTURE)

#define IMX_DSP_RATES \
	(SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_11025 | \
	SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_22050 | \
	SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 | \
	SNDRV_PCM_RATE_48000 | SNDRV_PCM_RATE_88200 | \
	SNDRV_PCM_RATE_96000)

#define IMX_DSP_BITS \
	(SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE | \
	SNDRV_PCM_FMTBIT_S24_LE)

static struct snd_soc_dai_mode imx_dsp_pcm_modes[] = {

	/* frame master and clock slave mode */
	{IMX_DSP_DAIFMT | SND_SOC_DAIFMT_CBM_CFS,
		SND_SOC_DAITDM_LRDW(0,0), IMX_DSP_BITS, IMX_DSP_RATES,
		 IMX_DSP_DIR, 0, SND_SOC_FS_ALL,
		 SND_SOC_FSB(32) | SND_SOC_FSB(32) | SND_SOC_FSB(16)},

};

static imx_pcm_dma_params_t imx_ssi1_pcm_stereo_out = {
	.name			= "SSI1 PCM Stereo out",
	.params = {
		.bd_number = 1,
		.transfer_type = emi_2_per,
		.watermark_level = SDMA_TXFIFO_WATERMARK,
		.word_size = TRANSFER_16BIT, // maybe add this in setup func
		.per_address = SSI1_STX0,
		.event_id = DMA_REQ_SSI1_TX1,
		.peripheral_type = SSI,
	},
};

static imx_pcm_dma_params_t imx_ssi1_pcm_stereo_in = {
	.name			= "SSI1 PCM Stereo in",
	.params = {
		.bd_number = 1,
		.transfer_type = per_2_emi,
		.watermark_level = SDMA_RXFIFO_WATERMARK,
		.word_size = TRANSFER_16BIT, // maybe add this in setup func
		.per_address = SSI1_SRX0,
		.event_id = DMA_REQ_SSI1_RX1,
		.peripheral_type = SSI,
	},
};

static imx_pcm_dma_params_t imx_ssi2_pcm_stereo_out = {
	.name			= "SSI2 PCM Stereo out",
	.params = {
		.bd_number = 1,
		.transfer_type = per_2_emi,
		.watermark_level = SDMA_TXFIFO_WATERMARK,
		.word_size = TRANSFER_16BIT, // maybe add this in setup func
		.per_address = SSI2_STX0,
		.event_id = DMA_REQ_SSI2_TX1,
		.peripheral_type = SSI,
	},
};

static imx_pcm_dma_params_t imx_ssi2_pcm_stereo_in = {
	.name			= "SSI2 PCM Stereo in",
	.params = {
		.bd_number = 1,
		.transfer_type = per_2_emi,
		.watermark_level = SDMA_RXFIFO_WATERMARK,
		.word_size = TRANSFER_16BIT, // maybe add this in setup func
		.per_address = SSI2_SRX0,
		.event_id = DMA_REQ_SSI2_RX1,
		.peripheral_type = SSI,
	},
};

static int imx_dsp_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	if (!rtd->cpu_dai->active) {

	}

	return 0;
}

static int imx_ssi1_hw_tx_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 bfs, div;

	bfs = SND_SOC_FSBD_REAL(rtd->cpu_dai->dai_runtime.bfs);

	SSI1_STCR = 0;
	SSI1_STCCR = 0;

	/* DAI mode */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_DSP_B:
		SSI1_STCR |= SSI_STCR_TEFS; // data 1 bit after sync
	case SND_SOC_DAIFMT_DSP_A:
		SSI1_STCR |= SSI_STCR_TFSL; // frame is 1 bclk long
		break;
	}

	/* DAI clock inversion */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_IB_IF:
		SSI1_STCR |= SSI_STCR_TFSI | SSI_STCR_TSCKP;
		break;
	case SND_SOC_DAIFMT_IB_NF:
		SSI1_STCR |= SSI_STCR_TSCKP;
		break;
	case SND_SOC_DAIFMT_NB_IF:
		SSI1_STCR |= SSI_STCR_TFSI;
		break;
	}

	/* DAI data (word) size */
	switch(rtd->codec_dai->dai_runtime.pcmfmt) {
	case SNDRV_PCM_FMTBIT_S16_LE:
		SSI1_STCCR |= SSI_STCCR_WL(16);
		break;
	case SNDRV_PCM_FMTBIT_S20_3LE:
		SSI1_STCCR |= SSI_STCCR_WL(20);
		break;
	case SNDRV_PCM_FMTBIT_S24_LE:
		SSI1_STCCR |= SSI_STCCR_WL(24);
		break;
	}

	/* DAI clock master masks */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_CLOCK_MASK){
	case SND_SOC_DAIFMT_CBM_CFM:
		SSI1_STCR |= SSI_STCR_TFDIR | SSI_STCR_TXDIR;
		break;
	case SND_SOC_DAIFMT_CBS_CFM:
		SSI1_STCR |= SSI_STCR_TFDIR;
		break;
	case SND_SOC_DAIFMT_CBM_CFS:
		SSI1_STCR |= SSI_STCR_TXDIR;
		break;
	}

	/* DAI BCLK ratio to SYSCLK / MCLK */
	/* prescaler modulus - todo */
	switch (bfs) {
	case 2:
		break;
	case 4:
		break;
	case 8:
		break;
	case 16:
		break;
	}

	/* TDM - todo, only fifo 0 atm */
	SSI1_STCR |= SSI_STCR_TFEN0;
	SSI1_STCCR |= SSI_STCCR_DC(params_channels(params));

	return 0;
}

static int imx_ssi1_hw_rx_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 bfs, div;

	bfs = SND_SOC_FSBD_REAL(rtd->cpu_dai->dai_runtime.bfs);

	SSI1_SRCR = 0;
	SSI1_SRCCR = 0;

	/* DAI mode */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_DSP_B:
		SSI1_SRCR |= SSI_SRCR_REFS; // data 1 bit after sync
	case SND_SOC_DAIFMT_DSP_A:
		SSI1_SRCR |= SSI_SRCR_RFSL; // frame is 1 bclk long
		break;
	}

	/* DAI clock inversion */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_IB_IF:
		SSI1_SRCR |= SSI_SRCR_TFSI | SSI_SRCR_TSCKP;
		break;
	case SND_SOC_DAIFMT_IB_NF:
		SSI1_SRCR |= SSI_SRCR_RSCKP;
		break;
	case SND_SOC_DAIFMT_NB_IF:
		SSI1_SRCR |= SSI_SRCR_RFSI;
		break;
	}

	/* DAI data (word) size */
	switch(rtd->codec_dai->dai_runtime.pcmfmt) {
	case SNDRV_PCM_FMTBIT_S16_LE:
		SSI1_SRCCR |= SSI_SRCCR_WL(16);
		break;
	case SNDRV_PCM_FMTBIT_S20_3LE:
		SSI1_SRCCR |= SSI_SRCCR_WL(20);
		break;
	case SNDRV_PCM_FMTBIT_S24_LE:
		SSI1_SRCCR |= SSI_SRCCR_WL(24);
		break;
	}

	/* DAI clock master masks */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_CLOCK_MASK){
	case SND_SOC_DAIFMT_CBM_CFM:
		SSI1_SRCR |= SSI_SRCR_RFDIR | SSI_SRCR_RXDIR;
		break;
	case SND_SOC_DAIFMT_CBS_CFM:
		SSI1_SRCR |= SSI_SRCR_RFDIR;
		break;
	case SND_SOC_DAIFMT_CBM_CFS:
		SSI1_SRCR |= SSI_SRCR_RXDIR;
		break;
	}

	/* DAI BCLK ratio to SYSCLK / MCLK */
	/* prescaler modulus - todo */
	switch (bfs) {
	case 2:
		break;
	case 4:
		break;
	case 8:
		break;
	case 16:
		break;
	}

	/* TDM - todo, only fifo 0 atm */
	SSI1_SRCR |= SSI_SRCR_RFEN0;
	SSI1_SRCCR |= SSI_SRCCR_DC(params_channels(params));

	return 0;
}

static int imx_ssi_dsp_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	/* clear register if not enabled */
	if(!(SSI1_SCR & SSI_SCR_SSIEN))
		SSI1_SCR = 0;

	/* async */
	if (rtd->cpu_dai->flags & SND_SOC_DAI_ASYNC)
		SSI1_SCR |= SSI_SCR_SYN;

	/* DAI mode */
	switch(rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
	case SND_SOC_DAIFMT_LEFT_J:
		SSI1_SCR |= SSI_SCR_NET;
		break;
	}

	/* TDM  - to complete */

	/* Tx/Rx config */
	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		return imx_ssi1_dsp_hw_tx_params(substream, params);
	} else {
		return imx_ssi1_dsp_hw_rx_params(substream, params);
	}
}



static int imx_ssi_dsp_trigger(struct snd_pcm_substream *substream, int cmd)
{
	int ret = 0;

	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			SSI1_SCR |= SSI_SCR_TE;
			SSI1_SIER |= SSI_SIER_TDMAE;
		} else {
			SSI1_SCR |= SSI_SCR_RE;
			SSI1_SIER |= SSI_SIER_RDMAE;
		}
		SSI1_SCR |= SSI_SCR_SSIEN;

		break;
	case SNDRV_PCM_TRIGGER_RESUME:
	break;
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSI1_SCR |= SSI_SCR_TE;
		else
			SSI1_SCR |= SSI_SCR_RE;
	break
	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_SUSPEND:
	break;
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			SSI1_SCR &= ~SSI_SCR_TE;
		else
			SSI1_SCR &= ~SSI_SCR_RE;
	break;
	default:
		ret = -EINVAL;
	}

	return ret;
}

static void imx_ssi_dsp_shutdown(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;

	/* shutdown SSI */
	if (!rtd->cpu_dai->active) {
		if(rtd->cpu_dai->id == 0)
			SSI1_SCR &= ~SSI_SCR_SSIEN;
		else
			SSI2_SCR &= ~SSI_SCR_SSIEN;
	}
}

#ifdef CONFIG_PM
static int imx_ssi_dsp_suspend(struct platform_device *dev,
	struct snd_soc_cpu_dai *dai)
{
	if(!dai->active)
		return 0;

	if(rtd->cpu_dai->id == 0)
		SSI1_SCR &= ~SSI_SCR_SSIEN;
	else
		SSI2_SCR &= ~SSI_SCR_SSIEN;

	return 0;
}

static int imx_ssi_dsp_resume(struct platform_device *pdev,
	struct snd_soc_cpu_dai *dai)
{
	if(!dai->active)
		return 0;

	if(rtd->cpu_dai->id == 0)
		SSI1_SCR |= SSI_SCR_SSIEN;
	else
		SSI2_SCR |= SSI_SCR_SSIEN;

	return 0;
}

#else
#define imx_ssi_dsp_suspend	NULL
#define imx_ssi_dsp_resume	NULL
#endif

static unsigned int imx_ssi_config_dsp_sysclk(struct snd_soc_cpu_dai *iface,
	struct snd_soc_clock_info *info, unsigned int clk)
{
	return clk;
}

struct snd_soc_cpu_dai imx_ssi_dsp_dai = {
	.name = "imx-dsp-1",
	.id = 0,
	.type = SND_SOC_DAI_PCM,
	.suspend = imx_ssi_dsp_suspend,
	.resume = imx_ssi_dsp_resume,
	.config_sysclk = imx_ssi_config_dsp_sysclk,
	.playback = {
		.channels_min = 1,
		.channels_max = 2,},
	.capture = {
		.channels_min = 1,
		.channels_max = 2,},
	.ops = {
		.startup = imx_ssi_dsp_startup,
		.shutdown = imx_ssi_dsp_shutdown,
		.trigger = imx_ssi_trigger,
		.hw_params = imx_ssi_dsp_hw_params,},
	.caps = {
		.num_modes = ARRAY_SIZE(imx_dsp_modes),
		.mode = imx_dsp_modes,},
},
{
	.name = "imx-dsp-2",
	.id = 1,
	.type = SND_SOC_DAI_PCM,
	.suspend = imx_ssi_dsp_suspend,
	.resume = imx_ssi_dsp_resume,
	.config_sysclk = imx_ssi_config_dsp_sysclk,
	.playback = {
		.channels_min = 1,
		.channels_max = 2,},
	.capture = {
		.channels_min = 1,
		.channels_max = 2,},
	.ops = {
		.startup = imx_dsp_startup,
		.shutdown = imx_dsp_shutdown,
		.trigger = imx_ssi1_trigger,
		.hw_params = imx_ssi1_pcm_hw_params,},
	.caps = {
		.num_modes = ARRAY_SIZE(imx_dsp_modes),
		.mode = imx_dsp_modes,},
};


EXPORT_SYMBOL_GPL(imx_ssi_dsp_dai);

/* Module information */
MODULE_AUTHOR("Liam Girdwood, liam.girdwood@wolfsonmicro.com, www.wolfsonmicro.com");
MODULE_DESCRIPTION("i.MX ASoC SSI driver");
MODULE_LICENSE("GPL");
