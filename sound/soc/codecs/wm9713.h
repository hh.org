/*
 * wm9713.h  --  WM9713 Soc Audio driver
 */

#ifndef _WM9713_H
#define _WM9713_H

#define WM9713_DAI_AC97_HIFI	0
#define WM9713_DAI_AC97_AUX		1
#define WM9713_DAI_PCM_VOICE	2

extern struct snd_soc_codec_device soc_codec_dev_wm9713;
extern struct snd_soc_codec_dai wm9713_dai[3];

u32 wm9713_set_pll(struct snd_soc_codec *codec, u32 in);
int wm9713_reset(struct snd_soc_codec *codec,  int try_warm);

#endif
