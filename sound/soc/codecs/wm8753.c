/*
 * wm8753.c  --  WM8753 ALSA Soc Audio driver
 *
 * Copyright 2003 Wolfson Microelectronics PLC.
 * Author: Liam Girdwood
 *         liam.girdwood@wolfsonmicro.com or linux@wolfsonmicro.com
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 * Notes:
 *  The WM8753 is a low power, high quality stereo codec with integrated PCM
 *  codec designed for portable digital telephony applications.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>

#include "wm8753.h"

#define AUDIO_NAME "wm8753"
#define WM8753_VERSION "0.14"

/*
 * Debug
 */

#define WM8753_DEBUG 0

#ifdef WM8753_DEBUG
#define dbg(format, arg...) \
	printk(KERN_DEBUG AUDIO_NAME ": " format "\n" , ## arg)
#else
#define dbg(format, arg...) do {} while (0)
#endif
#define err(format, arg...) \
	printk(KERN_ERR AUDIO_NAME ": " format "\n" , ## arg)
#define info(format, arg...) \
	printk(KERN_INFO AUDIO_NAME ": " format "\n" , ## arg)
#define warn(format, arg...) \
	printk(KERN_WARNING AUDIO_NAME ": " format "\n" , ## arg)

static int caps_charge = 2000;
module_param(caps_charge, int, 0);
MODULE_PARM_DESC(caps_charge, "WM8753 cap charge time (msecs)");

static struct workqueue_struct *wm8753_workq = NULL;
static struct work_struct wm8753_dapm_work;

/*
 * wm8753 register cache
 * We can't read the WM8753 register space when we
 * are using 2 wire for device control, so we cache them instead.
 */
static const u16 wm8753_reg[] = {
	0x0008, 0x0000, 0x000a, 0x000a,
	0x0033, 0x0000, 0x0007, 0x00ff,
	0x00ff, 0x000f, 0x000f, 0x007b,
	0x0000, 0x0032, 0x0000, 0x00c3,
	0x00c3, 0x00c0, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0055,
	0x0005, 0x0050, 0x0055, 0x0050,
	0x0055, 0x0050, 0x0055, 0x0079,
	0x0079, 0x0079, 0x0079, 0x0079,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0097, 0x0097, 0x0000, 0x0004,
	0x0000, 0x0083, 0x0024, 0x01ba,
	0x0000, 0x0083, 0x0024, 0x01ba,
	0x0000, 0x0000, 0x0000, 0x0000,// virtual reg to handle Vmid Mux
};

#define VMID_MUX	63

#define WM8753_DAIFMT \
	(SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_LEFT_J | SND_SOC_DAIFMT_RIGHT_J | \
	SND_SOC_DAIFMT_DSP_A | SND_SOC_DAIFMT_DSP_B |  SND_SOC_DAIFMT_NB_NF | \
	SND_SOC_DAIFMT_NB_IF | SND_SOC_DAIFMT_IB_NF | SND_SOC_DAIFMT_IB_IF)

#define WM8753_DIR \
	(SND_SOC_DAIDIR_PLAYBACK | SND_SOC_DAIDIR_CAPTURE)

#define WM8753_HIFI_FSB \
	(SND_SOC_FSBD(1) | SND_SOC_FSBD(2) | SND_SOC_FSBD(4) | \
	SND_SOC_FSBD(8) | SND_SOC_FSBD(16))

#define WM8753_HIFI_RATES \
	(SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_11025 | SNDRV_PCM_RATE_16000 | \
	SNDRV_PCM_RATE_22050 | SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 | \
	SNDRV_PCM_RATE_48000 | SNDRV_PCM_RATE_88200 | SNDRV_PCM_RATE_96000)

#define WM8753_HIFI_BITS \
	(SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE | \
	SNDRV_PCM_FMTBIT_S24_LE | SNDRV_PCM_FMTBIT_S32_LE)

/*
 * HiFi modes
 */
static struct snd_soc_dai_mode wm8753_hifi_modes[] = {
	/* codec frame and clock master modes */
	/* 8k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1536,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1408,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 2304,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 2112,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_8000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1500,
		.bfs = WM8753_HIFI_FSB,
	},

	/* 11.025k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_11025,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1024,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_11025,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1536,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_11025,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1088,
		.bfs = WM8753_HIFI_FSB,
		},

	/* 16k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_16000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 768,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt= WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_16000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 1152,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_16000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 750,
		.bfs = WM8753_HIFI_FSB,
	},

	/* 22.05k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_22050,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 512,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_22050,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 768,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_22050,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 544,
		.bfs = WM8753_HIFI_FSB,
	},

	/* 32k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_32000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 384,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_32000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 576,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_32000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 375,
		.bfs = WM8753_HIFI_FSB,
	},

	/* 44.1k & 48k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_44100 | SNDRV_PCM_RATE_48000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 256,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_44100 | SNDRV_PCM_RATE_48000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 384,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_48000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 250,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_44100,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 272,
		.bfs = WM8753_HIFI_FSB,
	},

	/* 88.2k & 96k */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_88200 | SNDRV_PCM_RATE_96000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 128,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_88200 | SNDRV_PCM_RATE_96000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 192,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_88200,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 136,
		.bfs = WM8753_HIFI_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = SNDRV_PCM_RATE_96000,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 125,
		.bfs = WM8753_HIFI_FSB,
	},

	/* codec frame and clock slave modes */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBS_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = WM8753_HIFI_RATES,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = SND_SOC_FS_ALL,
		.bfs = SND_SOC_FSB_ALL,
	},
};

#define WM8753_VOICE_FSB \
	(SND_SOC_FSBD(1) | SND_SOC_FSBD(2) | SND_SOC_FSBD(4) | \
	SND_SOC_FSBD(8) | SND_SOC_FSBD(16))

#define WM8753_VOICE_RATES \
	(SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_11025 | SNDRV_PCM_RATE_16000 | \
	SNDRV_PCM_RATE_22050 | SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 | \
	SNDRV_PCM_RATE_48000)

#define WM8753_VOICE_BITS \
	(SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE | \
	SNDRV_PCM_FMTBIT_S24_LE | SNDRV_PCM_FMTBIT_S32_LE)

/*
 * Voice modes
 */
static struct snd_soc_dai_mode wm8753_voice_modes[] = {

	/* master modes */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_VOICE_BITS,
		.pcmrate = WM8753_VOICE_RATES,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 256,
		.bfs = WM8753_VOICE_FSB,
	},
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS,
		.pcmfmt = WM8753_VOICE_BITS,
		.pcmrate = WM8753_VOICE_RATES,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = 384,
		.bfs = WM8753_VOICE_FSB,
	},

	/* slave modes */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBS_CFS,
		.pcmfmt = WM8753_VOICE_BITS,
		.pcmrate = WM8753_VOICE_RATES,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = SND_SOC_FS_ALL,
		.bfs = SND_SOC_FSB_ALL,
	},
};


/*
 * Mode 4
 */
static struct snd_soc_dai_mode wm8753_mixed_modes[] = {
	/* slave modes */
	{
		.fmt = WM8753_DAIFMT | SND_SOC_DAIFMT_CBS_CFS,
		.pcmfmt = WM8753_HIFI_BITS,
		.pcmrate = WM8753_HIFI_RATES,
		.pcmdir = WM8753_DIR,
		.flags = SND_SOC_DAI_BFS_DIV,
		.fs = SND_SOC_FS_ALL,
		.bfs = SND_SOC_FSB_ALL,
	},
};

/*
 * read wm8753 register cache
 */
static inline unsigned int wm8753_read_reg_cache(struct snd_soc_codec *codec,
	unsigned int reg)
{
	u16 *cache = codec->reg_cache;
	if (reg < 1 || reg > (ARRAY_SIZE(wm8753_reg) + 1))
		return -1;
	return cache[reg - 1];
}

/*
 * write wm8753 register cache
 */
static inline void wm8753_write_reg_cache(struct snd_soc_codec *codec,
	unsigned int reg, unsigned int value)
{
	u16 *cache = codec->reg_cache;
	if (reg < 1 || reg > 0x3f)
		return;
	cache[reg - 1] = value;
}

/*
 * write to the WM8753 register space
 */
static int wm8753_write(struct snd_soc_codec *codec, unsigned int reg,
	unsigned int value)
{
	u8 data[2];

	/* data is
	 *   D15..D9 WM8753 register offset
	 *   D8...D0 register data
	 */
	data[0] = (reg << 1) | ((value >> 8) & 0x0001);
	data[1] = value & 0x00ff;

	wm8753_write_reg_cache (codec, reg, value);
	if (codec->hw_write(codec->control_data, data, 2) == 2)
		return 0;
	else
		return -EIO;
}

#define wm8753_reset(c) wm8753_write(c, WM8753_RESET, 0)

/*
 * WM8753 Controls
 */
static const char *wm8753_base[] = {"Linear Control", "Adaptive Boost"};
static const char *wm8753_base_filter[] =
	{"130Hz @ 48kHz", "200Hz @ 48kHz", "100Hz @ 16kHz", "400Hz @ 48kHz",
	"100Hz @ 8kHz", "200Hz @ 8kHz"};
static const char *wm8753_treble[] = {"8kHz", "4kHz"};
static const char *wm8753_alc_func[] = {"Off", "Right", "Left", "Stereo"};
static const char *wm8753_ng_type[] = {"Constant PGA Gain", "Mute ADC Output"};
static const char *wm8753_3d_func[] = {"Capture", "Playback"};
static const char *wm8753_3d_uc[] = {"2.2kHz", "1.5kHz"};
static const char *wm8753_3d_lc[] = {"200Hz", "500Hz"};
static const char *wm8753_deemp[] = {"None", "32kHz", "44.1kHz", "48kHz"};
static const char *wm8753_mono_mix[] = {"Stereo", "Left", "Right", "Mono"};
static const char *wm8753_dac_phase[] = {"Non Inverted", "Inverted"};
static const char *wm8753_line_mix[] = {"Line 1 + 2", "Line 1 - 2",
	"Line 1", "Line 2"};
static const char *wm8753_mono_mux[] = {"Line Mix", "Rx Mix"};
static const char *wm8753_right_mux[] = {"Line 2", "Rx Mix"};
static const char *wm8753_left_mux[] = {"Line 1", "Rx Mix"};
static const char *wm8753_rxmsel[] = {"RXP - RXN", "RXP + RXN", "RXP", "RXN"};
static const char *wm8753_sidetone_mux[] = {"Left PGA", "Mic 1", "Mic 2",
	"Right PGA"};
static const char *wm8753_mono2_src[] = {"Inverted Mono 1", "Left", "Right",
	"Left + Right"};
static const char *wm8753_out3[] = {"VREF", "ROUT2", "Left + Right",
	"ROUT2 VREF", "Left + Right VREF"};
static const char *wm8753_out4[] = {"VREF", "Capture ST", "LOUT2",
	"Capture ST VREF", "LOUT2 VREF"};
static const char *wm8753_radcsel[] = {"PGA", "Line or RXP-RXN", "Sidetone"};
static const char *wm8753_ladcsel[] = {"PGA", "Line or RXP-RXN", "Line"};
static const char *wm8753_mono_adc[] = {"Stereo", "Analogue Mix Left",
	"Analogue Mix Right", "Digital Mono Mix"};
static const char *wm8753_adc_hp[] = {"3.4Hz @ 48kHz", "82Hz @ 16k",
	"82Hz @ 8kHz", "170Hz @ 8kHz"};
static const char *wm8753_adc_filter[] = {"HiFi", "Voice"};
static const char *wm8753_mic_sel[] = {"Mic 1", "Mic 2", "Mic 3"};

static const struct soc_enum wm8753_enum[] = {
SOC_ENUM_SINGLE(WM8753_BASS, 7, 2, wm8753_base),		// 0
SOC_ENUM_SINGLE(WM8753_BASS, 4, 6, wm8753_base_filter),	// 1
SOC_ENUM_SINGLE(WM8753_TREBLE, 6, 2, wm8753_treble), 	// 2
SOC_ENUM_SINGLE(WM8753_ALC1, 7, 4, wm8753_alc_func),	// 3
SOC_ENUM_SINGLE(WM8753_NGATE, 1, 2, wm8753_ng_type),	// 4
SOC_ENUM_SINGLE(WM8753_3D, 7, 2, wm8753_3d_func),		// 5
SOC_ENUM_SINGLE(WM8753_3D, 6, 2, wm8753_3d_uc),			// 6
SOC_ENUM_SINGLE(WM8753_3D, 5, 2, wm8753_3d_lc),			// 7
SOC_ENUM_SINGLE(WM8753_DAC, 1, 4, wm8753_deemp),		// 8
SOC_ENUM_SINGLE(WM8753_DAC, 4, 4, wm8753_mono_mix),		// 9
SOC_ENUM_SINGLE(WM8753_DAC, 6, 2, wm8753_dac_phase),	// 10
SOC_ENUM_SINGLE(WM8753_INCTL1, 3, 4, wm8753_line_mix),	// 11
SOC_ENUM_SINGLE(WM8753_INCTL1, 2, 2, wm8753_mono_mux),	// 12
SOC_ENUM_SINGLE(WM8753_INCTL1, 1, 2, wm8753_right_mux),	// 13
SOC_ENUM_SINGLE(WM8753_INCTL1, 0, 2, wm8753_left_mux),	// 14
SOC_ENUM_SINGLE(WM8753_INCTL2, 6, 4, wm8753_rxmsel),	// 15
SOC_ENUM_SINGLE(WM8753_INCTL2, 4, 4, wm8753_sidetone_mux),// 16
SOC_ENUM_SINGLE(WM8753_OUTCTL, 7, 4, wm8753_mono2_src),	// 17
SOC_ENUM_SINGLE(VMID_MUX, 0, 5, wm8753_out3),		// 18
SOC_ENUM_SINGLE(VMID_MUX, 7, 5, wm8753_out4),		// 19
SOC_ENUM_SINGLE(WM8753_ADCIN, 2, 3, wm8753_radcsel),	// 20
SOC_ENUM_SINGLE(WM8753_ADCIN, 0, 3, wm8753_ladcsel),	// 21
SOC_ENUM_SINGLE(WM8753_ADCIN, 4, 4, wm8753_mono_adc),	// 22
SOC_ENUM_SINGLE(WM8753_ADC, 2, 4, wm8753_adc_hp),		// 23
SOC_ENUM_SINGLE(WM8753_ADC, 4, 2, wm8753_adc_filter),	// 24
SOC_ENUM_SINGLE(WM8753_MICBIAS, 6, 3, wm8753_mic_sel),	// 25
};

static const struct snd_kcontrol_new wm8753_snd_controls[] = {
SOC_DOUBLE_R("PCM Volume", WM8753_LDAC, WM8753_RDAC, 0, 255, 0),

SOC_DOUBLE_R("ADC Capture Volume", WM8753_LADC, WM8753_RADC, 0, 63, 0),
SOC_DOUBLE_R("ADC Capture Switch", WM8753_LADC, WM8753_RADC, 7, 1, 0),
SOC_DOUBLE_R("ADC Capture ZC Switch", WM8753_LADC, WM8753_RADC, 6, 1, 0),

SOC_DOUBLE_R("Out1 Playback Volume", WM8753_LOUT1V, WM8753_ROUT1V, 0, 127, 0),
SOC_DOUBLE_R("Out2 Playback Volume", WM8753_LOUT2V, WM8753_ROUT2V, 0, 127, 0),

SOC_SINGLE("Mono Playback Volume", WM8753_MOUTV, 0, 127, 0),

SOC_DOUBLE_R("Bypass Playback Volume", WM8753_LOUTM1, WM8753_ROUTM1, 4, 7, 1),
SOC_DOUBLE_R("Sidetone Playback Volume", WM8753_LOUTM2, WM8753_ROUTM2, 4, 7, 1),
SOC_DOUBLE_R("Voice Playback Volume", WM8753_LOUTM2, WM8753_ROUTM2, 0, 7, 1),

SOC_DOUBLE_R("Out1 Playback ZC Switch", WM8753_LOUT1V, WM8753_ROUT1V, 7, 1, 0),
SOC_DOUBLE_R("Out2 Playback ZC Switch", WM8753_LOUT2V, WM8753_ROUT2V, 7, 1, 0),

SOC_SINGLE("Mono Bypass Playback Volume", WM8753_MOUTM1, 4, 7, 1),
SOC_SINGLE("Mono Sidetone Playback Volume", WM8753_MOUTM2, 4, 7, 1),
SOC_SINGLE("Mono Voice Playback Volume", WM8753_MOUTM2, 4, 7, 1),
SOC_SINGLE("Mono Playback ZC Switch", WM8753_MOUTV, 7, 1, 0),

SOC_ENUM("Bass Boost", wm8753_enum[0]),
SOC_ENUM("Bass Filter", wm8753_enum[1]),
SOC_SINGLE("Bass Volume", WM8753_BASS, 0, 7, 1),

SOC_SINGLE("Treble Volume", WM8753_TREBLE, 0, 7, 0),
SOC_ENUM("Treble Cut-off", wm8753_enum[2]),

SOC_DOUBLE("Sidetone Capture Volume", WM8753_RECMIX1, 0, 4, 7, 1),
SOC_SINGLE("Voice Sidetone Capture Volume", WM8753_RECMIX2, 0, 7, 1),

SOC_DOUBLE_R("Capture Volume", WM8753_LINVOL, WM8753_RINVOL, 0, 63, 0),
SOC_DOUBLE_R("Capture ZC Switch", WM8753_LINVOL, WM8753_RINVOL, 6, 1, 0),
SOC_DOUBLE_R("Capture Switch", WM8753_LINVOL, WM8753_RINVOL, 7, 1, 0),

SOC_ENUM("Capture Filter Select", wm8753_enum[23]),
SOC_ENUM("Capture Filter Cut-off", wm8753_enum[24]),
SOC_SINGLE("Capture Filter Switch", WM8753_ADC, 0, 1, 1),

SOC_SINGLE("ALC Capture Target Volume", WM8753_ALC1, 0, 7, 0),
SOC_SINGLE("ALC Capture Max Volume", WM8753_ALC1, 4, 7, 0),
SOC_ENUM("ALC Capture Function", wm8753_enum[3]),
SOC_SINGLE("ALC Capture ZC Switch", WM8753_ALC2, 8, 1, 0),
SOC_SINGLE("ALC Capture Hold Time", WM8753_ALC2, 0, 15, 1),
SOC_SINGLE("ALC Capture Decay Time", WM8753_ALC3, 4, 15, 1),
SOC_SINGLE("ALC Capture Attack Time", WM8753_ALC3, 0, 15, 0),
SOC_SINGLE("ALC Capture NG Threshold", WM8753_NGATE, 3, 31, 0),
SOC_ENUM("ALC Capture NG Type", wm8753_enum[4]),
SOC_SINGLE("ALC Capture NG Switch", WM8753_NGATE, 0, 1, 0),

SOC_ENUM("3D Function", wm8753_enum[5]),
SOC_ENUM("3D Upper Cut-off", wm8753_enum[6]),
SOC_ENUM("3D Lower Cut-off", wm8753_enum[7]),
SOC_SINGLE("3D Volume", WM8753_3D, 1, 15, 0),
SOC_SINGLE("3D Switch", WM8753_3D, 0, 1, 0),

SOC_SINGLE("Capture 6dB Attenuate", WM8753_ADCTL1, 2, 1, 0),
SOC_SINGLE("Playback 6dB Attenuate", WM8753_ADCTL1, 1, 1, 0),

SOC_ENUM("De-emphasis", wm8753_enum[8]),
SOC_ENUM("Playback Mono Mix", wm8753_enum[9]),
SOC_ENUM("Playback Phase", wm8753_enum[10]),

SOC_SINGLE("Mic2 Capture Volume", WM8753_INCTL1, 7, 3, 0),
SOC_SINGLE("Mic1 Capture Volume", WM8753_INCTL1, 5, 3, 0),
};

/* Virtual Mux control maps onto real mux control.
 * This is to allow the automatic switching between Vmid and the
 * real signal into the Mux (minimising pops) at stream startup and
 * stream shutdown.
 */
static int vmid_mux_change(struct snd_soc_dapm_widget *w, int event)
{
	u16 vmux, mux;

	if (event != SND_SOC_DAPM_POST_REG)
		return 0;

	vmux = wm8753_read_reg_cache(w->codec, VMID_MUX);

	/* out 4 */
	mux = wm8753_read_reg_cache(w->codec, WM8753_ADCTL2) & 0xfc7f;
	if ((vmux & 0x380) <= 0x180)
		mux |= (vmux & 0x380);
	else
		mux |= ((vmux & 0x380) - 0x180);
	wm8753_write(w->codec, WM8753_ADCTL2, mux);

	/* out 3 */
	mux = wm8753_read_reg_cache(w->codec, WM8753_OUTCTL) & 0xfffc;
	if ((vmux & 0x7) <= 0x3)
		mux |= (vmux & 0x3);
	else
		mux |= ((vmux & 0x7) - 0x3);
	wm8753_write(w->codec, WM8753_OUTCTL, mux);

	return 0;
}


/*
 * Do Vmid Mux switching at stream startup.
 * Switches Mux from vmid -> audio signal
 */
static int vref_mux_stream_on (struct snd_soc_dapm_widget *w, int event)
{
	u16 vmux, mux = 0;

	if (event != SND_SOC_DAPM_PRE_REG)
		return 0;

	vmux = wm8753_read_reg_cache(w->codec, VMID_MUX);

	/* out 4 */
	mux = wm8753_read_reg_cache(w->codec, WM8753_ADCTL2) & 0xfc7f;
	if ((vmux & 0x380) > 0x180)
		mux |= ((vmux & 0x380) - 0x20);
	else
		mux |= (vmux & 0x380);
	wm8753_write(w->codec, WM8753_ADCTL2, mux);

	/* out 3 */
	mux = wm8753_read_reg_cache(w->codec, WM8753_OUTCTL) & 0xfffc;
	if ((vmux & 0x7) > 0x3)
		mux |= ((vmux & 0x7) - 0x2);
	else
		mux |= (vmux & 0x7);
	wm8753_write(w->codec, WM8753_OUTCTL, mux);

	return 0;
}

/*
 * Do Vmid Mux switching at stream shutdown.
 * Switches Mux audio signal --> Vmid.
 */
static int vref_mux_stream_off (struct snd_soc_dapm_widget *w, int event)
{
	if (event != SND_SOC_DAPM_PRE_PMD)
		return 0;
	return vmid_mux_change(w, SND_SOC_DAPM_PRE_REG);
}

/* add non dapm controls */
static int wm8753_add_controls(struct snd_soc_codec *codec)
{
	int err, i;

	for (i = 0; i < ARRAY_SIZE(wm8753_snd_controls); i++) {
		err = snd_ctl_add(codec->card,
				snd_soc_cnew(&wm8753_snd_controls[i],codec, NULL));
		if (err < 0)
			return err;
	}
	return 0;
}

/*
 * _DAPM_ Controls
 */

/* Left Mixer */
static const struct snd_kcontrol_new wm8753_left_mixer_controls[] = {
SOC_DAPM_SINGLE("Voice Playback Switch", WM8753_LOUTM2, 8, 1, 0),
SOC_DAPM_SINGLE("Sidetone Playback Switch", WM8753_LOUTM2, 7, 1, 0),
SOC_DAPM_SINGLE("Left Playback Switch", WM8753_LOUTM1, 8, 1, 0),
SOC_DAPM_SINGLE("Bypass Playback Switch", WM8753_LOUTM1, 7, 1, 0),
};

/* Right mixer */
static const struct snd_kcontrol_new wm8753_right_mixer_controls[] = {
SOC_DAPM_SINGLE("Voice Playback Switch", WM8753_ROUTM2, 8, 1, 0),
SOC_DAPM_SINGLE("Sidetone Playback Switch", WM8753_ROUTM2, 7, 1, 0),
SOC_DAPM_SINGLE("Right Playback Switch", WM8753_ROUTM1, 8, 1, 0),
SOC_DAPM_SINGLE("Bypass Playback Switch", WM8753_ROUTM1, 7, 1, 0),
};

/* Mono mixer */
static const struct snd_kcontrol_new wm8753_mono_mixer_controls[] = {
SOC_DAPM_SINGLE("Left Playback Switch", WM8753_MOUTM1, 8, 1, 0),
SOC_DAPM_SINGLE("Right Playback Switch", WM8753_MOUTM2, 8, 1, 0),
SOC_DAPM_SINGLE("Voice Playback Switch", WM8753_MOUTM2, 4, 1, 0),
SOC_DAPM_SINGLE("Sidetone Playback Switch", WM8753_MOUTM2, 7, 1, 0),
SOC_DAPM_SINGLE("Bypass Playback Switch", WM8753_MOUTM1, 7, 1, 0),
};

/* Mono 2 Mux */
static const struct snd_kcontrol_new wm8753_mono2_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[17]);

/* Out 3 Mux */
static const struct snd_kcontrol_new wm8753_out3_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[18]);

/* Out 4 Mux */
static const struct snd_kcontrol_new wm8753_out4_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[19]);

/* ADC Mono Mix */
static const struct snd_kcontrol_new wm8753_adc_mono_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[22]);

/* Record mixer */
static const struct snd_kcontrol_new wm8753_record_mixer_controls[] = {
SOC_DAPM_SINGLE("Voice Capture Switch", WM8753_RECMIX2, 3, 1, 0),
SOC_DAPM_SINGLE("Left Capture Switch", WM8753_RECMIX1, 3, 1, 0),
SOC_DAPM_SINGLE("Right Capture Switch", WM8753_RECMIX1, 7, 1, 0),
};

/* Left ADC mux */
static const struct snd_kcontrol_new wm8753_adc_left_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[21]);

/* Right ADC mux */
static const struct snd_kcontrol_new wm8753_adc_right_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[20]);

/* MIC mux */
static const struct snd_kcontrol_new wm8753_mic_mux_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[16]);

/* ALC mixer */
static const struct snd_kcontrol_new wm8753_alc_mixer_controls[] = {
SOC_DAPM_SINGLE("Line Capture Switch", WM8753_INCTL2, 3, 1, 0),
SOC_DAPM_SINGLE("Mic2 Capture Switch", WM8753_INCTL2, 2, 1, 0),
SOC_DAPM_SINGLE("Mic1 Capture Switch", WM8753_INCTL2, 1, 1, 0),
SOC_DAPM_SINGLE("Rx Capture Switch", WM8753_INCTL2, 0, 1, 0),
};

/* Left Line mux */
static const struct snd_kcontrol_new wm8753_line_left_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[14]);

/* Right Line mux */
static const struct snd_kcontrol_new wm8753_line_right_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[13]);

/* Mono Line mux */
static const struct snd_kcontrol_new wm8753_line_mono_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[12]);

/* Line mux and mixer */
static const struct snd_kcontrol_new wm8753_line_mux_mix_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[11]);

/* Rx mux and mixer */
static const struct snd_kcontrol_new wm8753_rx_mux_mix_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[15]);

/* Mic Selector Mux */
static const struct snd_kcontrol_new wm8753_mic_sel_mux_controls =
SOC_DAPM_ENUM("Route", wm8753_enum[25]);

static const struct snd_soc_dapm_widget wm8753_dapm_widgets[] = {
SND_SOC_DAPM_MICBIAS("Mic Bias", WM8753_PWR1, 5, 0),
SND_SOC_DAPM_MIXER("Left Mixer", WM8753_PWR4, 0, 0,
	&wm8753_left_mixer_controls[0], ARRAY_SIZE(wm8753_left_mixer_controls)),
SND_SOC_DAPM_PGA("Left Out 1", WM8753_PWR3, 8, 0, NULL, 0),
SND_SOC_DAPM_PGA("Left Out 2", WM8753_PWR3, 6, 0, NULL, 0),
SND_SOC_DAPM_DAC("Left DAC", "Left HiFi Playback", WM8753_PWR1, 3, 0),
SND_SOC_DAPM_OUTPUT("LOUT1"),
SND_SOC_DAPM_OUTPUT("LOUT2"),
SND_SOC_DAPM_MIXER("Right Mixer", WM8753_PWR4, 1, 0,
	&wm8753_right_mixer_controls[0], ARRAY_SIZE(wm8753_right_mixer_controls)),
SND_SOC_DAPM_PGA("Right Out 1", WM8753_PWR3, 7, 0, NULL, 0),
SND_SOC_DAPM_PGA("Right Out 2", WM8753_PWR3, 5, 0, NULL, 0),
SND_SOC_DAPM_DAC("Right DAC", "Right HiFi Playback", WM8753_PWR1, 2, 0),
SND_SOC_DAPM_OUTPUT("ROUT1"),
SND_SOC_DAPM_OUTPUT("ROUT2"),
SND_SOC_DAPM_MIXER("Mono Mixer", WM8753_PWR4, 2, 0,
	&wm8753_mono_mixer_controls[0], ARRAY_SIZE(wm8753_mono_mixer_controls)),
SND_SOC_DAPM_PGA("Mono Out 1", WM8753_PWR3, 2, 0, NULL, 0),
SND_SOC_DAPM_PGA("Mono Out 2", WM8753_PWR3, 1, 0, NULL, 0),
SND_SOC_DAPM_DAC("Voice DAC", "Voice Playback", WM8753_PWR1, 4, 0),
SND_SOC_DAPM_OUTPUT("MONO1"),
SND_SOC_DAPM_MUX("Mono 2 Mux", SND_SOC_NOPM, 0, 0, &wm8753_mono2_controls),
SND_SOC_DAPM_OUTPUT("MONO2"),
SND_SOC_DAPM_MIXER("Out3 Left + Right", -1, 0, 0, NULL, 0),
SND_SOC_DAPM_MUX_E("Out3 Mux", SND_SOC_NOPM, 0, 0, &wm8753_out3_controls,
	vmid_mux_change, SND_SOC_DAPM_POST_REG),
SND_SOC_DAPM_PGA("Out 3", WM8753_PWR3, 4, 0, NULL, 0),
SND_SOC_DAPM_OUTPUT("OUT3"),
SND_SOC_DAPM_MUX_E("Out4 Mux", SND_SOC_NOPM, 0, 0, &wm8753_out4_controls,
	vmid_mux_change, SND_SOC_DAPM_POST_REG),
SND_SOC_DAPM_PGA("Out 4", WM8753_PWR3, 3, 0, NULL, 0),
SND_SOC_DAPM_OUTPUT("OUT4"),
SND_SOC_DAPM_MIXER("Playback Mixer", WM8753_PWR4, 3, 0,
	&wm8753_record_mixer_controls[0],
	ARRAY_SIZE(wm8753_record_mixer_controls)),
SND_SOC_DAPM_ADC("Left ADC", "Left Voice Capture", WM8753_PWR2, 3, 0),
SND_SOC_DAPM_ADC("Right ADC", "Right Voice Capture", WM8753_PWR2, 2, 0),
SND_SOC_DAPM_MUX("Capture Left Mixer", SND_SOC_NOPM, 0, 0,
	&wm8753_adc_mono_controls),
SND_SOC_DAPM_MUX("Capture Right Mixer", SND_SOC_NOPM, 0, 0,
	&wm8753_adc_mono_controls),
SND_SOC_DAPM_MUX("Capture Left Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_adc_left_controls),
SND_SOC_DAPM_MUX("Capture Right Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_adc_right_controls),
SND_SOC_DAPM_MUX("Mic Sidetone Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_mic_mux_controls),
SND_SOC_DAPM_PGA("Left Capture Volume", WM8753_PWR2, 5, 0, NULL, 0),
SND_SOC_DAPM_PGA("Right Capture Volume", WM8753_PWR2, 4, 0, NULL, 0),
SND_SOC_DAPM_MIXER("ALC Mixer", WM8753_PWR2, 6, 0,
	&wm8753_alc_mixer_controls[0], ARRAY_SIZE(wm8753_alc_mixer_controls)),
SND_SOC_DAPM_MUX("Line Left Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_line_left_controls),
SND_SOC_DAPM_MUX("Line Right Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_line_right_controls),
SND_SOC_DAPM_MUX("Line Mono Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_line_mono_controls),
SND_SOC_DAPM_MUX("Line Mixer", SND_SOC_NOPM, 0, 0,
	&wm8753_line_mux_mix_controls),
SND_SOC_DAPM_MUX("Rx Mixer", SND_SOC_NOPM, 0, 0,
	&wm8753_rx_mux_mix_controls),
SND_SOC_DAPM_PGA("Mic 1 Volume", WM8753_PWR2, 8, 0, NULL, 0),
SND_SOC_DAPM_PGA("Mic 2 Volume", WM8753_PWR2, 7, 0, NULL, 0),
SND_SOC_DAPM_MUX("Mic Selection Mux", SND_SOC_NOPM, 0, 0,
	&wm8753_mic_sel_mux_controls),
SND_SOC_DAPM_INPUT("LINE1"),
SND_SOC_DAPM_INPUT("LINE2"),
SND_SOC_DAPM_INPUT("RXP"),
SND_SOC_DAPM_INPUT("RXN"),
SND_SOC_DAPM_INPUT("ACIN"),
SND_SOC_DAPM_INPUT("ACOP"),
SND_SOC_DAPM_INPUT("MIC1N"),
SND_SOC_DAPM_INPUT("MIC1"),
SND_SOC_DAPM_INPUT("MIC2N"),
SND_SOC_DAPM_INPUT("MIC2"),
SND_SOC_DAPM_VMID("VREF"),
SND_SOC_DAPM_PRE("VREF Mux Off", vref_mux_stream_off),
SND_SOC_DAPM_POST("VREF Mux On", vref_mux_stream_on),
};

static const char *audio_map[][3] = {
	/* left mixer */
	{"Left Mixer", "Left Playback Switch", "Left DAC"},
	{"Left Mixer", "Voice Playback Switch", "Voice DAC"},
	{"Left Mixer", "Sidetone Playback Switch", "Mic Sidetone Mux"},
	{"Left Mixer", "Bypass Playback Switch", "Line Left Mux"},

	/* right mixer */
	{"Right Mixer", "Right Playback Switch", "Right DAC"},
	{"Right Mixer", "Voice Playback Switch", "Voice DAC"},
	{"Right Mixer", "Sidetone Playback Switch", "Mic Sidetone Mux"},
	{"Right Mixer", "Bypass Playback Switch", "Line Right Mux"},

	/* mono mixer */
	{"Mono Mixer", "Voice Playback Switch", "Voice DAC"},
	{"Mono Mixer", "Left Playback Switch", "Left DAC"},
	{"Mono Mixer", "Right Playback Switch", "Right DAC"},
	{"Mono Mixer", "Sidetone Playback Switch", "Mic Sidetone Mux"},
	{"Mono Mixer", "Bypass Playback Switch", "Line Mono Mux"},

	/* left out */
	{"Left Out 1", NULL, "Left Mixer"},
	{"Left Out 2", NULL, "Left Mixer"},
	{"LOUT1", NULL, "Left Out 1"},
	{"LOUT2", NULL, "Left Out 2"},

	/* right out */
	{"Right Out 1", NULL, "Right Mixer"},
	{"Right Out 2", NULL, "Right Mixer"},
	{"ROUT1", NULL, "Right Out 1"},
	{"ROUT2", NULL, "Right Out 2"},

	/* mono 1 out */
	{"Mono Out 1", NULL, "Mono Mixer"},
	{"MONO1", NULL, "Mono Out 1"},

	/* mono 2 out */
	{"Mono 2 Mux", "Left + Right", "Out3 Left + Right"},
	{"Mono 2 Mux", "Inverted Mono 1", "MONO1"},
	{"Mono 2 Mux", "Left", "Left Mixer"},
	{"Mono 2 Mux", "Right", "Right Mixer"},
	{"Mono Out 2", NULL, "Mono 2 Mux"},
	{"MONO2", NULL, "Mono Out 2"},

	/* out 3 */
	{"Out3 Left + Right", NULL, "Left Mixer"},
	{"Out3 Left + Right", NULL, "Right Mixer"},
	{"Out3 Mux", "VREF", "VREF"},
	{"Out3 Mux", "Left + Right", "Out3 Left + Right"},
	{"Out3 Mux", "ROUT2", "ROUT2"},
	{"Out 3", NULL, "Out3 Mux"},
	{"OUT3", NULL, "Out 3"},

	/* out 4 */
	{"Out4 Mux", "VREF", "VREF"},
	{"Out4 Mux", "Capture ST", "Capture ST Mixer"},
	{"Out4 Mux", "LOUT2", "LOUT2"},
	{"Out 4", NULL, "Out4 Mux"},
	{"OUT4", NULL, "Out 4"},

	/* record mixer  */
	{"Playback Mixer", "Left Capture Switch", "Left Mixer"},
	{"Playback Mixer", "Voice Capture Switch", "Mono Mixer"},
	{"Playback Mixer", "Right Capture Switch", "Right Mixer"},

	/* Mic/SideTone Mux */
	{"Mic Sidetone Mux", "Left PGA", "Left Capture Volume"},
	{"Mic Sidetone Mux", "Right PGA", "Right Capture Volume"},
	{"Mic Sidetone Mux", "Mic 1", "Mic 1 Volume"},
	{"Mic Sidetone Mux", "Mic 2", "Mic 2 Volume"},

	/* Capture Left Mux */
	{"Capture Left Mux", "PGA", "Left Capture Volume"},
	{"Capture Left Mux", "Line or RXP-RXN", "Line Left Mux"},
	{"Capture Left Mux", "Line", "LINE1"},

	/* Capture Right Mux */
	{"Capture Right Mux", "PGA", "Right Capture Volume"},
	{"Capture Right Mux", "Line or RXP-RXN", "Line Right Mux"},
	{"Capture Right Mux", "Sidetone", "Capture ST Mixer"},

	/* Mono Capture mixer-mux */
	{"Capture Right Mixer", "Stereo", "Capture Right Mux"},
	{"Capture Left Mixer", "Analogue Mix Left", "Capture Left Mux"},
	{"Capture Left Mixer", "Analogue Mix Left", "Capture Right Mux"},
	{"Capture Right Mixer", "Analogue Mix Right", "Capture Left Mux"},
	{"Capture Right Mixer", "Analogue Mix Right", "Capture Right Mux"},
	{"Capture Left Mixer", "Digital Mono Mix", "Capture Left Mux"},
	{"Capture Left Mixer", "Digital Mono Mix", "Capture Right Mux"},
	{"Capture Right Mixer", "Digital Mono Mix", "Capture Left Mux"},
	{"Capture Right Mixer", "Digital Mono Mix", "Capture Right Mux"},

	/* ADC */
	{"Left ADC", NULL, "Capture Left Mixer"},
	{"Right ADC", NULL, "Capture Right Mixer"},

	/* Left Capture Volume */
	{"Left Capture Volume", NULL, "ACIN"},

	/* Right Capture Volume */
	{"Right Capture Volume", NULL, "Mic 2 Volume"},

	/* ALC Mixer */
	{"ALC Mixer", "Line Capture Switch", "Line Mixer"},
	{"ALC Mixer", "Mic2 Capture Switch", "Mic 2 Volume"},
	{"ALC Mixer", "Mic1 Capture Switch", "Mic 1 Volume"},
	{"ALC Mixer", "Rx Capture Switch", "Rx Mixer"},

	/* Line Left Mux */
	{"Line Left Mux", "Line 1", "LINE1"},
	{"Line Left Mux", "Rx Mix", "Rx Mixer"},

	/* Line Right Mux */
	{"Line Right Mux", "Line 2", "LINE2"},
	{"Line Right Mux", "Rx Mix", "Rx Mixer"},

	/* Line Mono Mux */
	{"Line Mono Mux", "Line Mix", "Line Mixer"},
	{"Line Mono Mux", "Rx Mix", "Rx Mixer"},

	/* Line Mixer/Mux */
	{"Line Mixer", "Line 1 + 2", "LINE1"},
	{"Line Mixer", "Line 1 - 2", "LINE1"},
	{"Line Mixer", "Line 1 + 2", "LINE2"},
	{"Line Mixer", "Line 1 - 2", "LINE2"},
	{"Line Mixer", "Line 1", "LINE1"},
	{"Line Mixer", "Line 2", "LINE2"},

	/* Rx Mixer/Mux */
	{"Rx Mixer", "RXP - RXN", "RXP"},
	{"Rx Mixer", "RXP + RXN", "RXP"},
	{"Rx Mixer", "RXP - RXN", "RXN"},
	{"Rx Mixer", "RXP + RXN", "RXN"},
	{"Rx Mixer", "RXP", "RXP"},
	{"Rx Mixer", "RXN", "RXN"},

	/* Mic 1 Volume */
	{"Mic 1 Volume", NULL, "MIC1N"},
	{"Mic 1 Volume", NULL, "Mic Selection Mux"},

	/* Mic 2 Volume */
	{"Mic 2 Volume", NULL, "MIC2N"},
	{"Mic 2 Volume", NULL, "MIC2"},

	/* Mic Selector Mux */
	{"Mic Selection Mux", "Mic 1", "MIC1"},
	{"Mic Selection Mux", "Mic 2", "MIC2N"},
	{"Mic Selection Mux", "Mic 3", "MIC2"},

	/* ACOP */
	{"ACOP", NULL, "ALC Mixer"},

	/* terminator */
	{NULL, NULL, NULL},
};

static int wm8753_add_widgets(struct snd_soc_codec *codec)
{
	int i;

	for(i = 0; i < ARRAY_SIZE(wm8753_dapm_widgets); i++) {
		snd_soc_dapm_new_control(codec, &wm8753_dapm_widgets[i]);
	}

	/* set up the WM8753 audio map */
	for(i = 0; audio_map[i][0] != NULL; i++) {
		snd_soc_dapm_connect_input(codec, audio_map[i][0],
			audio_map[i][1], audio_map[i][2]);
	}

	snd_soc_dapm_new_widgets(codec);
	return 0;
}

/* PLL divisors */
struct _pll_div {
	u32 pll_in;		/* ext clock input */
	u32 pll_out;	/* pll out freq */
	u32 div2:1;
	u32 n:4;
	u32 k:24;
};

/*
 * PLL divisors -
 */
static const struct _pll_div pll_div[] = {
   {13000000,	12288000,	0,	0x7,    0x23F54A},
   {13000000,	11289600,	0,	0x6,    0x3CA2F5},
   {12000000,	12288000,	0,	0x8,    0x0C49BA},
   {12000000,	11289600,	0,	0x7,    0x21B08A},
   {24000000,	12288000,	1,	0x8,    0x0C49BA},
   {24000000,	11289600,	1,	0x7,    0x21B08A},
   {12288000,	11289600,	0,	0x7,	0x166667},
   {26000000,	11289600,	1,	0x6,	0x3CA2F5},
   {26000000,	12288000,	1,	0x7,	0x23F54A},
};

static u32 wm8753_config_pll(struct snd_soc_codec *codec,
	struct snd_soc_codec_dai *dai, int pll)
{
	u16 reg;
	int found = 0;

    if (pll == 1) {
		reg = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xffef;
        if (!dai->pll_in || !dai->mclk) {
            /* disable PLL1  */
            wm8753_write(codec, WM8753_PLL1CTL1, 0x0026);
			wm8753_write(codec, WM8753_CLOCK, reg);
			return 0;
        } else {
            u16 value = 0;
            int i = 0;

            /* if we cant match, then use good values for N and K */
            for (;i < ARRAY_SIZE(pll_div); i++) {
                if (pll_div[i].pll_out == dai->pll_out &&
                	pll_div[i].pll_in == dai->pll_in) {
                	found = 1;
                    break;
                }
            }

			if (!found)
				goto err;

            /* set up N and K PLL divisor ratios */
            /* bits 8:5 = PLL_N, bits 3:0 = PLL_K[21:18] */
            value = (pll_div[i].n << 5) + ((pll_div[i].k & 0x3c0000) >> 18);
            wm8753_write(codec, WM8753_PLL1CTL2, value);

            /* bits 8:0 = PLL_K[17:9] */
            value = (pll_div[i].k & 0x03fe00) >> 9;
            wm8753_write(codec, WM8753_PLL1CTL3, value);

            /* bits 8:0 = PLL_K[8:0] */
            value = pll_div[i].k & 0x0001ff;
            wm8753_write(codec, WM8753_PLL1CTL4, value);

            /* set PLL1 as input and enable */
            wm8753_write(codec, WM8753_PLL1CTL1, 0x0027 |
            	(pll_div[i].div2 << 3));
			wm8753_write(codec, WM8753_CLOCK, reg | 0x0010);
        }
    } else {
		reg = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xfff7;
        if (!dai->pll_in || !dai->mclk) {
            /* disable PLL2  */
            wm8753_write(codec, WM8753_PLL2CTL1, 0x0026);
			wm8753_write(codec, WM8753_CLOCK, reg);
			return 0;
        } else {
            u16 value = 0;
            int i = 0;

            /* if we cant match, then use good values for N and K */
            for (;i < ARRAY_SIZE(pll_div); i++) {
                if (pll_div[i].pll_out == dai->pll_out &&
                	pll_div[i].pll_in == dai->pll_in) {
                		found = 1;
                		break;
                }
            }

			if (!found)
				goto err;

            /* set up N and K PLL divisor ratios */
            /* bits 8:5 = PLL_N, bits 3:0 = PLL_K[21:18] */
            value = (pll_div[i].n << 5) + ((pll_div[i].k & 0x3c0000) >> 18);
            wm8753_write(codec, WM8753_PLL2CTL2, value);

            /* bits 8:0 = PLL_K[17:9] */
            value = (pll_div[i].k & 0x03fe00) >> 9;
            wm8753_write(codec, WM8753_PLL2CTL3, value);

            /* bits 8:0 = PLL_K[8:0] */
            value = pll_div[i].k & 0x0001ff;
            wm8753_write(codec, WM8753_PLL2CTL4, value);

            /* set PLL1 as input and enable */
            wm8753_write(codec, WM8753_PLL2CTL1, 0x0027 |
            	(pll_div[i].div2 << 3));
			wm8753_write(codec, WM8753_CLOCK, reg | 0x0008);
        }
    }

    return dai->pll_in;
err:
	return 0;
}

struct _coeff_div {
	u32 mclk;
	u32 rate;
	u16 fs;
	u8 sr:5;
	u8 usb:1;
};

/* codec hifi mclk (after PLL) clock divider coefficients */
static const struct _coeff_div coeff_div[] = {
	/* 8k */
	{12288000, 8000, 1536, 0x6, 0x0},
	{11289600, 8000, 1408, 0x16, 0x0},
	{18432000, 8000, 2304, 0x7, 0x0},
	{16934400, 8000, 2112, 0x17, 0x0},
	{12000000, 8000, 1500, 0x6, 0x1},

	/* 11.025k */
	{11289600, 11025, 1024, 0x18, 0x0},
	{16934400, 11025, 1536, 0x19, 0x0},
	{12000000, 11025, 1088, 0x19, 0x1},

	/* 16k */
	{12288000, 16000, 768, 0xa, 0x0},
	{18432000, 16000, 1152, 0xb, 0x0},
	{12000000, 16000, 750, 0xa, 0x1},

	/* 22.05k */
	{11289600, 22050, 512, 0x1a, 0x0},
	{16934400, 22050, 768, 0x1b, 0x0},
	{12000000, 22050, 544, 0x1b, 0x1},

	/* 32k */
	{12288000, 32000, 384, 0xc, 0x0},
	{18432000, 32000, 576, 0xd, 0x0},
	{12000000, 32000, 375, 0xa, 0x1},

	/* 44.1k */
	{11289600, 44100, 256, 0x10, 0x0},
	{16934400, 44100, 384, 0x11, 0x0},
	{12000000, 44100, 272, 0x11, 0x1},

	/* 48k */
	{12288000, 48000, 256, 0x0, 0x0},
	{18432000, 48000, 384, 0x1, 0x0},
	{12000000, 48000, 250, 0x0, 0x1},

	/* 88.2k */
	{11289600, 88200, 128, 0x1e, 0x0},
	{16934400, 88200, 192, 0x1f, 0x0},
	{12000000, 88200, 136, 0x1f, 0x1},

	/* 96k */
	{12288000, 96000, 128, 0xe, 0x0},
	{18432000, 96000, 192, 0xf, 0x0},
	{12000000, 96000, 125, 0xe, 0x1},
};

static int get_coeff(int mclk, int rate)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(coeff_div); i++) {
		if (coeff_div[i].rate == rate && coeff_div[i].mclk == mclk)
			return i;
	}
	return -EINVAL;
}

/* supported HiFi input clocks (that don't use PLL) */
const static int hifi_clks[] = {11289600, 12000000, 12288000,
	16934400, 18432000};

/* The HiFi interface can be clocked in one of two ways:-
 *  o No PLL - MCLK is used directly.
 *  o PLL    - PLL is used to generate audio MCLK from input clock.
 *
 * We use the direct method if we can as it saves power.
 */
static unsigned int wm8753_config_i2s_sysclk(struct snd_soc_codec_dai *dai,
	struct snd_soc_clock_info *info, unsigned int clk)
{
	int i, pll_out;

	/* is clk supported without the PLL */
	for(i = 0; i < ARRAY_SIZE(hifi_clks); i++) {
		if (clk == hifi_clks[i]) {
			dai->mclk = clk;
			dai->pll_in = dai->pll_out = 0;
			dai->clk_div = 1;
			return clk;
		}
	}

	/* determine best PLL output speed */
	if (info->bclk_master &
		(SND_SOC_DAIFMT_CBM_CFM | SND_SOC_DAIFMT_CBM_CFS)) {
		pll_out = info->fs * info->rate;
	} else {
		/* calc slave clock */
		switch (info->rate){
			case 11025:
			case 22050:
			case 44100:
			case 88200:
				pll_out = 11289600;
				break;
			default:
				pll_out = 12288000;
				break;
		}
	}

	/* are input & output clocks supported by PLL */
	for (i = 0;i < ARRAY_SIZE(pll_div); i++) {
		if (pll_div[i].pll_in == clk && pll_div[i].pll_out == pll_out) {
			dai->pll_in = clk;
			dai->pll_out = dai->mclk = pll_out;
			return pll_out;
		}
	}

	/* this clk is not supported */
	return 0;
}

/* valid PCM clock dividers * 2 */
static int pcm_divs[] = {2, 6, 11, 4, 8, 12, 16};

/* The Voice interface can be clocked in one of four ways:-
 *  o No PLL - MCLK is used directly.
 *  o Div    - MCLK is directly divided.
 *  o PLL    - PLL is used to generate audio MCLK from input clock.
 *  o PLL & Div - PLL and post divider are used.
 *
 * We use the non PLL methods if we can, as it saves power.
 */

static unsigned int wm8753_config_pcm_sysclk(struct snd_soc_codec_dai *dai,
	struct snd_soc_clock_info *info, unsigned int clk)
{
	int i, j, best_clk = info->fs * info->rate;

	/* can we run at this clk without the PLL ? */
	for (i = 0; i < ARRAY_SIZE(pcm_divs); i++) {
		if ((best_clk >> 1) * pcm_divs[i] == clk) {
			dai->pll_in = 0;
			dai->clk_div = pcm_divs[i];
			dai->mclk = best_clk;
printk("out %d div %d\n", dai->pll_out, dai->clk_div);
			return dai->mclk;
		}
	}

	/* now check for PLL support */
	for (i = 0; i < ARRAY_SIZE(pll_div); i++) {
		if (pll_div[i].pll_in == clk) {
			for (j = 0; j < ARRAY_SIZE(pcm_divs); j++) {
				if (pll_div[i].pll_out == pcm_divs[j] * (best_clk >> 1)) {
					dai->pll_in = clk;
					dai->pll_out = pll_div[i].pll_out;
					dai->clk_div = pcm_divs[j];
					dai->mclk = best_clk;
printk("pll out %d div %d\n", dai->pll_out, dai->clk_div);
					return dai->mclk;
				}
			}
		}
	}
printk("out %d div %d\n", dai->pll_out, dai->clk_div);
	/* this clk is not supported */
	return 0;
}

/* set the format and bit size for ADC and Voice DAC */
static void wm8753_adc_vdac_prepare(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 voice = wm8753_read_reg_cache(codec, WM8753_PCM) & 0x01e0;

	/* interface format */
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
		voice |= 0x0002;
		break;
	case SND_SOC_DAIFMT_RIGHT_J:
		break;
	case SND_SOC_DAIFMT_LEFT_J:
		voice |= 0x0001;
		break;
	case SND_SOC_DAIFMT_DSP_A:
		voice |= 0x0003;
		break;
	case SND_SOC_DAIFMT_DSP_B:
		voice |= 0x0013;
		break;
	}

	/* bit size */
	switch (rtd->codec_dai->dai_runtime.pcmfmt) {
	case SNDRV_PCM_FMTBIT_S16_LE:
		break;
	case SNDRV_PCM_FMTBIT_S20_3LE:
		voice |= 0x0004;
		break;
	case SNDRV_PCM_FMTBIT_S24_LE:
		voice |= 0x0008;
		break;
	case SNDRV_PCM_FMTBIT_S32_LE:
		voice |= 0x000c;
		break;
	}

	wm8753_write(codec, WM8753_PCM, voice);
}

/* configure PCM DAI */
static int wm8753_pcm_dai_prepare(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 voice, ioctl, srate, srate2, fs, bfs, clock;
	unsigned int rate;

	bfs = SND_SOC_FSBD_REAL(rtd->codec_dai->dai_runtime.bfs);
	fs = rtd->codec_dai->dai_runtime.fs;
	rate = snd_soc_get_rate(rtd->codec_dai->dai_runtime.pcmrate);
	voice = wm8753_read_reg_cache(codec, WM8753_PCM) & 0x001f;

	/* set master/slave audio interface */
	ioctl = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x01f1;
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_CLOCK_MASK) {
	case SND_SOC_DAIFMT_CBM_CFM:
		ioctl |= 0x0002;
	case SND_SOC_DAIFMT_CBM_CFS:
		voice |= 0x0040;
		break;
	}

	/* do we need to enable the PLL */
	if (rtd->codec_dai->pll_in) {
		if (wm8753_config_pll(codec, rtd->codec_dai, 2) !=
			rtd->codec_dai->pll_in) {
			err("could not set pll to %d --> %d",
				rtd->codec_dai->pll_in, rtd->codec_dai->pll_out);
			return -ENODEV;
		}
	}

	/* set up PCM divider */
	clock = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0x003f;
	switch (rtd->codec_dai->clk_div) {
	case 2: /* 1 */
		break;
	case 6: /* 3 */
		clock |= (0x2 << 6);
		break;
	case 11: /* 5.5 */
		clock |= (0x3 << 6);
		break;
	case 4: /* 2 */
		clock |= (0x4 << 6);
		break;
	case 8: /* 4 */
		clock |= (0x5 << 6);
		break;
	case 12: /* 6 */
		clock |= (0x6 << 6);
		break;
	case 16: /* 8 */
		clock |= (0x7 << 6);
		break;
	default:
		printk(KERN_ERR "wm8753: invalid PCM clk divider %d\n",
			rtd->codec_dai->clk_div);
		break;
	}
	wm8753_write(codec, WM8753_CLOCK, clock);
printk("prep div %d\n", rtd->codec_dai->clk_div);
printk("clk %x\n", wm8753_read_reg_cache(codec, WM8753_CLOCK));
	/* set bclk divisor rate */
	srate2 = wm8753_read_reg_cache(codec, WM8753_SRATE2) & 0x003f;
	switch (bfs) {
	case 1:
		break;
	case 2:
		srate2 |= (0x1 << 6);
		break;
	case 4:
		srate2 |= (0x2 << 6);
		break;
	case 8:
		srate2 |= (0x3 << 6);
		break;
	case 16:
		srate2 |= (0x4 << 6);
		break;
	}
	wm8753_write(codec, WM8753_SRATE2, srate2);

	srate = wm8753_read_reg_cache(codec, WM8753_SRATE1) & 0x017f;
	if (rtd->codec_dai->dai_runtime.fs == 384)
		srate |= 0x80;
	wm8753_write(codec, WM8753_SRATE1, srate);

	/* clock inversion */
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_IB_IF:
		voice |= 0x0090;
		break;
	case SND_SOC_DAIFMT_IB_NF:
		voice |= 0x0080;
		break;
	case SND_SOC_DAIFMT_NB_IF:
		voice |= 0x0010;
		break;
	}
printk("voice %x %x ioctl %x %x srate2 %x %x srate1 %x %x\n",
	WM8753_PCM, voice, WM8753_IOCTL, ioctl, WM8753_SRATE2,
	srate2, WM8753_SRATE1, srate);

	wm8753_write(codec, WM8753_IOCTL, ioctl);
	wm8753_write(codec, WM8753_PCM, voice);
	return 0;
}

/* configure hifi DAC wordlength and format */
static void wm8753_hdac_prepare(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 hifi = wm8753_read_reg_cache(codec, WM8753_HIFI) & 0x01e0;

	/* interface format */
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
		hifi |= 0x0002;
		break;
	case SND_SOC_DAIFMT_RIGHT_J:
		break;
	case SND_SOC_DAIFMT_LEFT_J:
		hifi |= 0x0001;
		break;
	case SND_SOC_DAIFMT_DSP_A:
		hifi |= 0x0003;
		break;
	case SND_SOC_DAIFMT_DSP_B:
		hifi |= 0x0013;
		break;
	}

	/* bit size */
	switch (rtd->codec_dai->dai_runtime.pcmfmt) {
	case SNDRV_PCM_FMTBIT_S16_LE:
		break;
	case SNDRV_PCM_FMTBIT_S20_3LE:
		hifi |= 0x0004;
		break;
	case SNDRV_PCM_FMTBIT_S24_LE:
		hifi |= 0x0008;
		break;
	case SNDRV_PCM_FMTBIT_S32_LE:
		hifi |= 0x000c;
		break;
	}

	wm8753_write(codec, WM8753_HIFI, hifi);
}

/* configure i2s (hifi) DAI clocking */
static int wm8753_i2s_dai_prepare(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 srate, bfs, hifi, ioctl;
	unsigned int rate;
	int i = 0;

	bfs = SND_SOC_FSBD_REAL(rtd->codec_dai->dai_runtime.bfs);
	rate = snd_soc_get_rate(rtd->codec_dai->dai_runtime.pcmrate);
	hifi = wm8753_read_reg_cache(codec, WM8753_HIFI) & 0x001f;

	/* is coefficient valid ? */
	if ((i = get_coeff(rtd->codec_dai->mclk, rate)) < 0)
		return i;

	srate = wm8753_read_reg_cache(codec, WM8753_SRATE1) & 0x01c0;
	wm8753_write(codec, WM8753_SRATE1, srate | (coeff_div[i].sr << 1) |
		coeff_div[i].usb);

	/* do we need to enable the PLL */
	if (rtd->codec_dai->pll_in) {
		if (wm8753_config_pll(codec, rtd->codec_dai, 1) !=
			rtd->codec_dai->pll_in) {
			err("could not set pll to %d --> %d",
				rtd->codec_dai->pll_in, rtd->codec_dai->pll_out);
			return -ENODEV;
		}
	}

	/* set bclk divisor rate */
	srate = wm8753_read_reg_cache(codec, WM8753_SRATE2) & 0x01c7;
	switch (bfs) {
	case 1:
		break;
	case 2:
		srate |= (0x1 << 3);
		break;
	case 4:
		srate |= (0x2 << 3);
		break;
	case 8:
		srate |= (0x3 << 3);
		break;
	case 16:
		srate |= (0x4 << 3);
		break;
	}
	wm8753_write(codec, WM8753_SRATE2, srate);

	/* set master/slave audio interface */
	ioctl = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f2;
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_CLOCK_MASK) {
	case SND_SOC_DAIFMT_CBM_CFM:
		ioctl |= 0x0001;
	case SND_SOC_DAIFMT_CBM_CFS:
		hifi |= 0x0040;
		break;
	}

	/* clock inversion */
	switch (rtd->codec_dai->dai_runtime.fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_IB_IF:
		hifi |= 0x0090;
		break;
	case SND_SOC_DAIFMT_IB_NF:
		hifi |= 0x0080;
		break;
	case SND_SOC_DAIFMT_NB_IF:
		hifi |= 0x0010;
		break;
	}
	wm8753_write(codec, WM8753_IOCTL, ioctl | 0x0008);
	wm8753_write(codec, WM8753_HIFI, hifi);
	return 0;
}

static int wm8753_mode1v_prepare (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 mode, clock;

	/* set clk source as pcmclk */
	clock = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xfffb;
	wm8753_write(codec, WM8753_CLOCK, clock);
	mode = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f3;
	wm8753_write(codec, WM8753_IOCTL, mode);
	wm8753_adc_vdac_prepare(substream);
	return wm8753_pcm_dai_prepare(substream);
}

static int wm8753_mode1h_prepare (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 mode;

	mode = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f3;
	wm8753_write(codec, WM8753_IOCTL, mode);
	wm8753_hdac_prepare(substream);
	return wm8753_i2s_dai_prepare(substream);
}

static int wm8753_mode2_prepare (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 mode, clock;

	/* set clk source as pcmclk */
	clock = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xfffb;
	wm8753_write(codec, WM8753_CLOCK, clock);

	mode = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f3;
	wm8753_write(codec, WM8753_IOCTL, mode | (0x1 << 2));

	wm8753_adc_vdac_prepare(substream);
	return wm8753_i2s_dai_prepare(substream);
}

static int wm8753_mode3_prepare (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 mode, clock;

	/* set clk source as mclk */
	clock = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xfffb;
	wm8753_write(codec, WM8753_CLOCK, clock | 0x4);

	mode = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f3;
	wm8753_write(codec, WM8753_IOCTL, mode | (0x2 << 2));

	wm8753_hdac_prepare(substream);
	wm8753_adc_vdac_prepare(substream);
	return wm8753_i2s_dai_prepare(substream);
}

static int wm8753_mode4_prepare (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	u16 mode, clock;

	/* set clk source as mclk */
	clock = wm8753_read_reg_cache(codec, WM8753_CLOCK) & 0xfffb;
	wm8753_write(codec, WM8753_CLOCK, clock | 0x4);

	mode = wm8753_read_reg_cache(codec, WM8753_IOCTL) & 0x00f3;
	wm8753_write(codec, WM8753_IOCTL, mode | (0x3 << 2));

	wm8753_hdac_prepare(substream);
	wm8753_adc_vdac_prepare(substream);
	return wm8753_i2s_dai_prepare(substream);
}

static int wm8753_mode_startup (struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_device *socdev = rtd->socdev;
	struct snd_soc_codec *codec = socdev->codec;
	int i;

	/* most of the DAI modes are mutually exclusive */
	if (codec->active) {

		/* find active dai and check whether it matches current id */
		for(i = 0; i < ARRAY_SIZE(wm8753_dai); i++) {
			if (wm8753_dai[i].active &&
				rtd->codec_dai->id != wm8753_dai[i].id) {
				return -EBUSY;
			}
		}
	}

	return 0;
}

static int wm8753_mute(struct snd_soc_codec *codec,
	struct snd_soc_codec_dai *dai, int mute)
{
	u16 mute_reg = wm8753_read_reg_cache(codec, WM8753_DAC) & 0xfff7;

	/* the digital mute covers the HiFi and Voice DAC's on the WM8753.
	 * make sure we check if they are not both active when we mute */
	if (mute && dai->id == 1) {
		if (!wm8753_dai[WM8753_DAI_MODE1_VOICE].playback.active ||
			!wm8753_dai[WM8753_DAI_MODE1_HIFI].playback.active)
			wm8753_write(codec, WM8753_DAC, mute_reg | 0x8);
	} else {
		if (mute)
			wm8753_write(codec, WM8753_DAC, mute_reg | 0x8);
		else
			wm8753_write(codec, WM8753_DAC, mute_reg);
	}

	return 0;
}

static int wm8753_dapm_event(struct snd_soc_codec *codec, int event)
{
	u16 pwr_reg = wm8753_read_reg_cache(codec, WM8753_PWR1) & 0xfe3e;

	switch (event) {
	case SNDRV_CTL_POWER_D0: /* full On */
		/* set vmid to 50k and unmute dac */
		wm8753_write(codec, WM8753_PWR1, pwr_reg | 0x00c0);
		break;
	case SNDRV_CTL_POWER_D1: /* partial On */
	case SNDRV_CTL_POWER_D2: /* partial On */
		/* set vmid to 5k for quick power up */
		wm8753_write(codec, WM8753_PWR1, pwr_reg | 0x01c1);
		break;
	case SNDRV_CTL_POWER_D3hot: /* Off, with power */
		/* mute dac and set vmid to 500k, enable VREF */
		wm8753_write(codec, WM8753_PWR1, pwr_reg | 0x0141);
		break;
	case SNDRV_CTL_POWER_D3cold: /* Off, without power */
		wm8753_write(codec, WM8753_PWR1, 0x0001);
		break;
	}
	codec->dapm_state = event;
	return 0;
}

/*
 * The WM8753 supports upto 4 different and mutually exclusive DAI
 * configurations. This gives 5 PCM's available for use.
 *
 * 1. Voice over PCM DAI - HIFI DAC over HIFI DAI
 * 2. Voice over HIFI DAI - HIFI disabled
 * 3. Voice disabled - HIFI over HIFI
 * 4. Voice disabled - HIFI over HIFI, uses voice DAI LRC for capture
 */
struct snd_soc_codec_dai wm8753_dai[] = {
/* DAI mode 1 */
{	.name = "WM8753 Voice (1)",
	.id = 1,
	.playback = {
		.stream_name = "Voice Playback",
		.channels_min = 1,
		.channels_max = 1,},
	.capture = {
		.stream_name = "Voice Capture",
		.channels_min = 1,
		.channels_max = 2,},
	.config_sysclk = wm8753_config_pcm_sysclk,
	.digital_mute = wm8753_mute,
	.ops = {
		.prepare = wm8753_mode1v_prepare,
		.startup = wm8753_mode_startup,},
	.caps = {
		.num_modes = ARRAY_SIZE(wm8753_voice_modes),
		.mode = wm8753_voice_modes,},
},
{	.name = "WM8753 HiFi (1)",
	.id = 1,
	.playback = {
		.stream_name = "HiFi Playback",
		.channels_min = 1,
		.channels_max = 2,},
	.config_sysclk = wm8753_config_i2s_sysclk,
	.digital_mute = wm8753_mute,
	.ops = {
		.prepare = wm8753_mode1h_prepare,
		.startup = wm8753_mode_startup,},
	.caps = {
		.num_modes = ARRAY_SIZE(wm8753_hifi_modes),
		.mode = wm8753_hifi_modes,},
},
/* DAI mode 2 */
{	.name = "WM8753 Voice (2)",
	.id = 2,
	.playback = {
		.stream_name = "Voice Playback",
		.channels_min = 1,
		.channels_max = 1,},
	.capture = {
		.stream_name = "Voice Capture",
		.channels_min = 1,
		.channels_max = 2,},
	.config_sysclk = wm8753_config_i2s_sysclk,
	.digital_mute = wm8753_mute,
	.ops = {
		.prepare = wm8753_mode2_prepare,
		.startup = wm8753_mode_startup,},
	.caps = {
		.num_modes = ARRAY_SIZE(wm8753_voice_modes),
		.mode = wm8753_voice_modes,},
},
/* DAI mode 3 */
{	.name = "WM8753 HiFi (3)",
	.id = 3,
	.playback = {
		.stream_name = "HiFi Playback",
		.channels_min = 1,
		.channels_max = 2,},
	.capture = {
		.stream_name = "HiFi Capture",
		.channels_min = 1,
		.channels_max = 2,},
	.config_sysclk = wm8753_config_i2s_sysclk,
	.digital_mute = wm8753_mute,
	.ops = {
		.prepare = wm8753_mode3_prepare,
		.startup = wm8753_mode_startup,},
	.caps = {
		.num_modes = ARRAY_SIZE(wm8753_hifi_modes),
		.mode = wm8753_hifi_modes,},
},
/* DAI mode 4 */
{	.name = "WM8753 HiFi (4)",
	.id = 4,
	.playback = {
		.stream_name = "HiFi Playback",
		.channels_min = 1,
		.channels_max = 2,},
	.capture = {
		.stream_name = "HiFi Capture",
		.channels_min = 1,
		.channels_max = 2,},
	.config_sysclk = wm8753_config_i2s_sysclk,
	.digital_mute = wm8753_mute,
	.ops = {
		.prepare = wm8753_mode4_prepare,
		.startup = wm8753_mode_startup,},
	.caps = {
		.num_modes = ARRAY_SIZE(wm8753_mixed_modes),
		.mode = wm8753_mixed_modes,},
},
};
EXPORT_SYMBOL_GPL(wm8753_dai);

static void wm8753_work(void *data)
{
	struct snd_soc_codec *codec = (struct snd_soc_codec *)data;
	wm8753_dapm_event(codec, codec->dapm_state);
}

static int wm8753_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct snd_soc_device *socdev = platform_get_drvdata(pdev);
	struct snd_soc_codec *codec = socdev->codec;

	wm8753_dapm_event(codec, SNDRV_CTL_POWER_D3cold);
	return 0;
}

static int wm8753_resume(struct platform_device *pdev)
{
	struct snd_soc_device *socdev = platform_get_drvdata(pdev);
	struct snd_soc_codec *codec = socdev->codec;
	int i;
	u8 data[2];
	u16 *cache = codec->reg_cache;

	/* Sync reg_cache with the hardware */
	for (i = 0; i < ARRAY_SIZE(wm8753_reg); i++) {
		if (i + 1 == WM8753_RESET)
			continue;
		data[0] = ((i + 1) << 1) | ((cache[i] >> 8) & 0x0001);
		data[1] = cache[i] & 0x00ff;
		codec->hw_write(codec->control_data, data, 2);
	}

	wm8753_dapm_event(codec, SNDRV_CTL_POWER_D3hot);

	/* charge wm8753 caps */
	if (codec->suspend_dapm_state == SNDRV_CTL_POWER_D0) {
		wm8753_dapm_event(codec, SNDRV_CTL_POWER_D2);
		codec->dapm_state = SNDRV_CTL_POWER_D0;
		queue_delayed_work(wm8753_workq, &wm8753_dapm_work,
			msecs_to_jiffies(caps_charge));
	}

	return 0;
}

/*
 * initialise the WM8753 driver
 * register the mixer and dsp interfaces with the kernel
 */
static int wm8753_init(struct snd_soc_device *socdev)
{
	struct snd_soc_codec *codec = socdev->codec;
	int reg, ret = 0;

	codec->name = "WM8753";
	codec->owner = THIS_MODULE;
	codec->read = wm8753_read_reg_cache;
	codec->write = wm8753_write;
	codec->dapm_event = wm8753_dapm_event;
	codec->dai = wm8753_dai;
	codec->num_dai = ARRAY_SIZE(wm8753_dai);
	codec->reg_cache_size = ARRAY_SIZE(wm8753_reg);

	codec->reg_cache =
			kzalloc(sizeof(u16) * ARRAY_SIZE(wm8753_reg), GFP_KERNEL);
	if (codec->reg_cache == NULL)
		return -ENOMEM;
	memcpy(codec->reg_cache, wm8753_reg,
		sizeof(u16) * ARRAY_SIZE(wm8753_reg));
	codec->reg_cache_size = sizeof(u16) * ARRAY_SIZE(wm8753_reg);

	wm8753_reset(codec);

	/* register pcms */
	ret = snd_soc_new_pcms(socdev, SNDRV_DEFAULT_IDX1, SNDRV_DEFAULT_STR1);
	if (ret < 0) {
		kfree(codec->reg_cache);
		return ret;
	}

	/* charge output caps */
	wm8753_dapm_event(codec, SNDRV_CTL_POWER_D2);
	codec->dapm_state = SNDRV_CTL_POWER_D3hot;
	queue_delayed_work(wm8753_workq,
		&wm8753_dapm_work, msecs_to_jiffies(caps_charge));

	/* set the update bits */
	reg = wm8753_read_reg_cache(codec, WM8753_LDAC);
	wm8753_write(codec, WM8753_LDAC, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_RDAC);
	wm8753_write(codec, WM8753_RDAC, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_LOUT1V);
	wm8753_write(codec, WM8753_LOUT1V, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_ROUT1V);
	wm8753_write(codec, WM8753_ROUT1V, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_LOUT2V);
	wm8753_write(codec, WM8753_LOUT2V, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_ROUT2V);
	wm8753_write(codec, WM8753_ROUT2V, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_LINVOL);
	wm8753_write(codec, WM8753_LINVOL, reg | 0x0100);
	reg = wm8753_read_reg_cache(codec, WM8753_RINVOL);
	wm8753_write(codec, WM8753_RINVOL, reg | 0x0100);

	wm8753_add_controls(codec);
	wm8753_add_widgets(codec);
	ret = snd_soc_register_card(socdev);
	if (ret < 0) {
		snd_soc_free_pcms(socdev);
		snd_soc_dapm_free(socdev);
	}

	return ret;
}

/* If the i2c layer weren't so broken, we could pass this kind of data
   around */
static struct snd_soc_device *wm8753_socdev;

#if defined (CONFIG_I2C) || defined (CONFIG_I2C_MODULE)

/*
 * WM8753 2 wire address is determined by GPIO5
 * state during powerup.
 *    low  = 0x1a
 *    high = 0x1b
 */
#define I2C_DRIVERID_WM8753 0xfefe /* liam -  need a proper id */

static unsigned short normal_i2c[] = { 0, I2C_CLIENT_END };

/* Magic definition of all other variables and things */
I2C_CLIENT_INSMOD;

static struct i2c_driver wm8753_i2c_driver;
static struct i2c_client client_template;

static int wm8753_codec_probe(struct i2c_adapter *adap, int addr, int kind)
{
	struct snd_soc_device *socdev = wm8753_socdev;
	struct wm8753_setup_data *setup = socdev->codec_data;
	struct snd_soc_codec *codec = socdev->codec;
	struct i2c_client *i2c;
	int ret;

	if (addr != setup->i2c_address)
		return -ENODEV;

	client_template.adapter = adap;
	client_template.addr = addr;

	i2c = kzalloc(sizeof(struct i2c_client), GFP_KERNEL);
	if (i2c == NULL){
		kfree(codec);
		return -ENOMEM;
	}
	memcpy(i2c, &client_template, sizeof(struct i2c_client));
	i2c_set_clientdata(i2c, codec);
	codec->control_data = i2c;

	ret = i2c_attach_client(i2c);
	if (ret < 0) {
		err("failed to attach codec at addr %x\n", addr);
		goto err;
	}

	ret = wm8753_init(socdev);
	if (ret < 0) {
		err("failed to initialise WM8753\n");
		goto err;
	}

	return ret;

err:
	kfree(codec);
	kfree(i2c);
	return ret;
}

static int wm8753_i2c_detach(struct i2c_client *client)
{
	struct snd_soc_codec *codec = i2c_get_clientdata(client);
	i2c_detach_client(client);
	kfree(codec->reg_cache);
	kfree(client);
	return 0;
}

static int wm8753_i2c_attach(struct i2c_adapter *adap)
{
	return i2c_probe(adap, &addr_data, wm8753_codec_probe);
}

/* corgi i2c codec control layer */
static struct i2c_driver wm8753_i2c_driver = {
	.driver = {
		.name = "WM8753 I2C Codec",
		.owner = THIS_MODULE,
	},
	.id =             I2C_DRIVERID_WM8753,
	.attach_adapter = wm8753_i2c_attach,
	.detach_client =  wm8753_i2c_detach,
	.command =        NULL,
};

static struct i2c_client client_template = {
	.name =   "WM8753",
	.driver = &wm8753_i2c_driver,
};
#endif

static int wm8753_probe(struct platform_device *pdev)
{
	struct snd_soc_device *socdev = platform_get_drvdata(pdev);
	struct wm8753_setup_data *setup;
	struct snd_soc_codec *codec;
	int ret = 0;

	info("WM8753 Audio Codec %s", WM8753_VERSION);

	setup = socdev->codec_data;
	codec = kzalloc(sizeof(struct snd_soc_codec), GFP_KERNEL);
	if (codec == NULL)
		return -ENOMEM;

	socdev->codec = codec;
	mutex_init(&codec->mutex);
	INIT_LIST_HEAD(&codec->dapm_widgets);
	INIT_LIST_HEAD(&codec->dapm_paths);
	wm8753_socdev = socdev;
	INIT_WORK(&wm8753_dapm_work, wm8753_work, codec);
	wm8753_workq = create_workqueue("wm8753");
	if (wm8753_workq == NULL) {
		kfree(codec);
		return -ENOMEM;
	}
#if defined (CONFIG_I2C) || defined (CONFIG_I2C_MODULE)
	if (setup->i2c_address) {
		normal_i2c[0] = setup->i2c_address;
		codec->hw_write = (hw_write_t)i2c_master_send;
		ret = i2c_add_driver(&wm8753_i2c_driver);
		if (ret != 0)
			printk(KERN_ERR "can't add i2c driver");
	}
#else
		/* Add other interfaces here */
#endif
	return ret;
}

/* power down chip */
static int wm8753_remove(struct platform_device *pdev)
{
	struct snd_soc_device *socdev = platform_get_drvdata(pdev);
	struct snd_soc_codec *codec = socdev->codec;

	if (codec->control_data)
		wm8753_dapm_event(codec, SNDRV_CTL_POWER_D3cold);
	if (wm8753_workq)
		destroy_workqueue(wm8753_workq);
	snd_soc_free_pcms(socdev);
	snd_soc_dapm_free(socdev);
#if defined (CONFIG_I2C) || defined (CONFIG_I2C_MODULE)
	i2c_del_driver(&wm8753_i2c_driver);
#endif
	kfree(codec);

	return 0;
}

struct snd_soc_codec_device soc_codec_dev_wm8753 = {
	.probe = 	wm8753_probe,
	.remove = 	wm8753_remove,
	.suspend = 	wm8753_suspend,
	.resume =	wm8753_resume,
};

EXPORT_SYMBOL_GPL(soc_codec_dev_wm8753);

MODULE_DESCRIPTION("ASoC WM8753 driver");
MODULE_AUTHOR("Liam Girdwood");
MODULE_LICENSE("GPL");
