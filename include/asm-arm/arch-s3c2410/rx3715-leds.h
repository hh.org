/*
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 */

#ifndef __ASM_ARCH_RX3715_LEDS_H
#define __ASM_ARCH_RX3715_LEDS_H "rx3715-leds.h"

EXTERN_LED_TRIGGER_SHARED(rx3715_radio_trig);

#endif  // __ASM_ARCH_RX3715_LEDS_H
