#include <linux/input.h>

struct gpio_keys_button {
	/* Configuration parameters */
	int keycode;
	int gpio;
	int active_low;
	char *desc;
	/* Internal state vars */
	u8  down;
};

struct gpio_keys_platform_data {
	struct gpio_keys_button *buttons;
	int nbuttons;
	struct input_dev *input;
};
