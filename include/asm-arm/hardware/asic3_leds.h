/*
 * LEDs support for HTC ASIC3 devices.
 *
 * Copyright (c) 2006  Anton Vorontsov <cbou@mail.ru>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/leds.h>

struct asic3_leds_machinfo;

struct asic3_led {
	struct led_classdev led_cdev;
	int hw_num;	/* Number of "hardware-accelerated" led */
	int gpio_num;	/* Number of GPIO if hw_num == -1 */
	struct asic3_leds_machinfo *machinfo;
};

struct asic3_leds_machinfo {
	int num_leds;
	struct asic3_led *leds;
	struct platform_device *asic3_pdev;
};

extern int asic3_leds_register(void);
extern void asic3_leds_unregister(void);

#ifdef CONFIG_LEDS_TRIGGER_TIMER_ASIC3

struct asic3_timer_data {
	unsigned long delay_on;  /* milliseconds on */
	unsigned long delay_off; /* milliseconds off */
};

extern void led_trigger_register_asic3_timer(const char *name,
                                             struct led_trigger **trig);
extern void led_trigger_unregister_asic3_timer(struct led_trigger *trig);

#else

#define led_trigger_register_asic3_timer(x, y)  do {} while(0)
#define led_trigger_unregister_asic3_timer(x)  do {} while(0)

#endif // CONFIG_LEDS_TRIGGERS
