/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 * History:
 *
 * 2002-08-23   Jamey Hicks        GPIO and IRQ support for iPAQ H5400
 *
 */

#ifndef SAMCOP_BASE_H
#define SAMCOP_BASE_H

extern void samcop_set_gpio_a (struct device *dev, u32 mask, u32 bits);
extern void samcop_set_gpio_b (struct device *dev, u32 mask, u32 bits);
extern void samcop_set_gpio_c (struct device *dev, u32 mask, u32 bits);
extern void samcop_set_gpio_d (struct device *dev, u32 mask, u32 bits);
extern void samcop_set_gpio_e (struct device *dev, u32 mask, u32 bits);

extern u32 samcop_get_gpio_a (struct device *dev);
extern u32 samcop_get_gpio_b (struct device *dev);
extern u32 samcop_get_gpio_c (struct device *dev);
extern u32 samcop_get_gpio_d (struct device *dev);
extern u32 samcop_get_gpio_e (struct device *dev);

extern void samcop_reset_fcd (struct device *dev);

extern int samcop_irq_base (struct device *dev);

extern void samcop_set_spcr (struct device *dev, u32 mask, u32 bits);
#endif
