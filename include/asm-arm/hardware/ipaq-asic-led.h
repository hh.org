#ifndef IPAQ_ASIC_LED_H
#define IPAQ_ASIC_LED_H

#define LED_TBS		0x0f		/* Low 4 bits sets time base, max = 13		*/
					/* Note: max = 5 on hx4700			*/
					/* 0: maximum time base				*/
					/* 1: maximum time base / 2			*/
					/* n: maximum time base / 2^n			*/

#define LED_EN		(1 << 4)	/* LED ON/OFF 0:off, 1:on			*/
#define LED_AUTOSTOP	(1 << 5)	/* LED ON/OFF auto stop set 0:disable, 1:enable */ 
#define LED_ALWAYS	(1 << 6)	/* LED Interrupt Mask 0:No mask, 1:mask		*/

#endif
