/*
 * include/asm/arm/arch-pxa/htcuniversal-asic.h
 *
 * Authors: Giuseppe Zompatori <giuseppe_zompatori@yahoo.it>
 *
 * based on previews work, see below:
 * 
 * include/asm/arm/arch-pxa/hx4700-asic.h
 *      Copyright (c) 2004 SDG Systems, LLC
 *
 */

#ifndef _HTCUNIVERSAL_ASIC_H_
#define _HTCUNIVERSAL_ASIC_H_

#include <asm/hardware/ipaq-asic3.h>

/* ASIC3 */

#define HTCUNIVERSAL_ASIC3_GPIO_PHYS	PXA_CS4_PHYS
#define HTCUNIVERSAL_ASIC3_MMC_PHYS	PXA_CS3_PHYS

/* TODO: some information is missing here */

/* ASIC3 GPIO A bank */

#define GPIOA_I2C_EN	 	 	 0	/* Output */
#define GPIOA_SPK_PWR1_ON	 	 1	/* Output */
#define GPIOA_AUDIO_PWR_ON	 	 2	/* Output */
#define GPIOA_EARPHONE_PWR_ON	 	 3	/* Output */

#define GPIOA_UNKNOWN4			 4	/* Output */
#define GPIOA_BUTTON_BACKLIGHT_N	 5	/* Input  */
#define GPIOA_SPK_PWR2_ON	 	 6	/* Output */
#define GPIOA_BUTTON_RECORD_N		 7	/* Input  */

#define GPIOA_BUTTON_CAMERA_N		 8	/* Input  */
#define GPIOA_UNKNOWN9			 9	/* Output */
#define GPIOA_FLASHLIGHT 		10	/* Output */
#define GPIOA_COVER_ROTATE_N 		11	/* Input  */

#define GPIOA_TOUCHSCREEN_N		12	/* Input  */
#define GPIOA_VOL_UP_N  		13	/* Input  */
#define GPIOA_VOL_DOWN_N 		14	/* Input  */
#define GPIOA_LCD_PWR5_ON 		15	/* Output */

/* ASIC3 GPIO B bank */

#define GPIOB_BB_READY			 0	/* Input */
#define GPIOB_CODEC_PDN			 1	/* Output */
#define GPIOB_UNKNOWN2			 2	/* Input  */
#define GPIOB_BB_UNKNOWN3		 3	/* Input  */

#define GPIOB_BT_IRQ			 4	/* Input  */
#define GPIOB_CLAMSHELL_N		 5	/* Input  */
#define GPIOB_LCD_PWR3_ON		 6	/* Output */
#define GPIOB_BB_ALERT			 7	/* Input  */

#define GPIOB_BB_RESET2			 8	/* Output */
#define GPIOB_EARPHONE_N		 9	/* Input  */
#define GPIOB_MICRECORD_N		 9	/* Input  */
#define GPIOB_NIGHT_SENSOR		11	/* Input  */

#define GPIOB_UMTS_DCD			12	/* Input  */
#define GPIOB_UNKNOWN13			13	/* Input  */
#define GPIOB_CHARGE_EN 		14	/* Output */
#define GPIOB_USB_PUEN 			15	/* Output */

/* ASIC3 GPIO C bank */

#define GPIOC_LED_RED  		   	 0	/* Output */
#define GPIOC_LED_GREEN		   	 1	/* Output */
#define GPIOC_LED_BLUE			 2	/* Output */
#define GPIOC_BOARDID3  		 3	/* Input  */

#define GPIOC_WIFI_IRQ_N  		 4	/* Input  */
#define GPIOC_WIFI_RESET  		 5	/* Output */
#define GPIOC_WIFI_PWR1_ON  		 6	/* Output */
#define GPIOC_BT_RESET  		 7	/* Output */

#define GPIOC_UNKNOWN8  		 8	/* Output */
#define GPIOC_LCD_PWR1_ON		 9	/* Output */
#define GPIOC_LCD_PWR2_ON 		10	/* Output */
#define GPIOC_BOARDID2  		11	/* Input  */

#define GPIOC_BOARDID1			12	/* Input  */
#define GPIOC_BOARDID0  		13	/* Input  */
#define GPIOC_BT_PWR_ON			14	/* Output */
#define GPIOC_CHARGE_ON			15	/* Output */

/* ASIC3 GPIO D bank */

#define GPIOD_KEY_OK_N			 0	/* Input  */
#define GPIOD_KEY_RIGHT_N		 1	/* Input  */
#define GPIOD_KEY_LEFT_N		 2	/* Input  */
#define GPIOD_KEY_DOWN_N		 3	/* Input  */

#define GPIOD_KEY_UP_N  		 4	/* Input  */
#define GPIOD_SDIO_DET			 5	/* Input  */
#define GPIOD_WIFI_PWR2_ON		 6	/* Output */
#define GPIOD_HW_REBOOT			 7	/* Output */

#define GPIOD_BB_RESET1			 8	/* Output */
#define GPIOD_UNKNOWN9			 9	/* Output */
#define GPIOD_VIBRA_PWR_ON		10	/* Output */
#define GPIOD_WIFI_PWR3_ON		11	/* Output */

#define GPIOD_FL_PWR_ON  		12	/* Output */
#define GPIOD_LCD_PWR4_ON		13	/* Output */
#define GPIOD_BL_KEYP_PWR_ON		14	/* Output */
#define GPIOD_BL_KEYB_PWR_ON		15	/* Output */

#endif /* _HTCUNIVERSAL_ASIC_H_ */

extern struct platform_device htcuniversal_asic3;
