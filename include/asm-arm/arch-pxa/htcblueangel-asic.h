/*
 *
 * Definitions for HTC Blueangel
 *
 * Copyright 2004 Xanadux.org
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Author: w4xy@xanadux.org
 *
 * Heavily based on h3900_asic.h
 *
 */

#ifndef _INCLUDE_BLUEANGEL_ASIC_H_ 
#define _INCLUDE_BLUEANGEL_ASIC_H_

#include <asm/hardware/ipaq-asic3.h>

/****************************************************/
/*
 * This ASIC is at CS3# + 0x01800000
 */

#define GPIOA_BT_PWR1_ON	(1 << 0)
#define GPIOA_MIC_PWR_ON	(1 << 7)
#define GPIOA_WLAN_PWR1         (1 << 3)
#define GPIOA_WLAN_RESET        (1 << 4)
#define GPIOA_WLAN_PWR2         (1 << 5)

#define GPIOB_LCD_PWR1_ON	(1 << 3)
#define GPIOB_LCD_PWR2_ON	(1 << 4)
#define GPIOB_LCD_PWR3_ON	(1 << 5)
#define GPIOB_FL_PWR_ON        	(1 << 6)  /* Frontlight power on */
#define GPIOB_PHONEL_PWR_ON	(1 << 7)
#define GPIOB_VIBRA_PWR_ON	(1 << 9)
#define GPIOB_OWM_EN 		(1 << 10)
#define GPIOB_BT_PWR2_ON	(1 << 12)
#define GPIOB_WLAN_PWR3         (1 << 13)
#define GPIOB_SPK_PWR_ON	(1 << 15)

#define GPIOC_USB_PULLUP_N 	(1 << 10)
#define GPIOC_HEADPHONE_IN 	(1 << 12)
#define GPIOC_KEYBL_PWR_ON	(1 << 13)
#define GPIOC_LCD_PWR4_ON	(1 << 14)
#define GPIOC_LCD_PWR5_ON	(1 << 15)

#define GPIOD_QKBD_IRQ		4
#define GPIOD_AC_CHARGER_N      3
#define GPIOD_OWM_EN 		(1 << 10)

#define BLUEANGEL_WWW_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+5)
#define BLUEANGEL_OK_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+7)
#define BLUEANGEL_WINDOWS_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+8)
#define BLUEANGEL_RECORD_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+9)
#define BLUEANGEL_CAMERA_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+10)
#define BLUEANGEL_VOL_UP_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+11)
#define BLUEANGEL_VOL_DOWN_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+12)
#define BLUEANGEL_MAIL_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+15)

#define BLUEANGEL_WWW_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+5)
#define BLUEANGEL_OK_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+7)
#define BLUEANGEL_WINDOWS_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+8)
#define BLUEANGEL_RECORD_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+9)
#define BLUEANGEL_CAMERA_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+10)
#define BLUEANGEL_VOL_UP_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+11)
#define BLUEANGEL_VOL_DOWN_BTN_IRQ	(ASIC3_GPIOD_IRQ_BASE+12)
#define BLUEANGEL_MAIL_BTN_IRQ		(ASIC3_GPIOD_IRQ_BASE+15)

#endif  // _INCLUDE_BLUEANGEL_ASIC_H_ 
