/*
 * Asus Mypal 600/620 GPIO definitions.
 *
 * Author: Nicolas Pouillon <nipo@ssji.net>
 *
 */

#ifndef _ASUS_620_GPIO_
#define _ASUS_620_GPIO_

#define GET_A620_GPIO(gpio) \
	(GPLR(GPIO_NR_A620_ ## gpio) & GPIO_bit(GPIO_NR_A620_ ## gpio))

#define SET_A620_GPIO(gpio, setp) \
do { \
if (setp) \
	GPSR(GPIO_NR_A620_ ## gpio) = GPIO_bit(GPIO_NR_A620_ ## gpio); \
else \
	GPCR(GPIO_NR_A620_ ## gpio) = GPIO_bit(GPIO_NR_A620_ ## gpio); \
} while (0)

#define SET_A620_GPIO_N(gpio, setp) \
do { \
if (setp) \
	GPCR(GPIO_NR_A620_ ## gpio ## _N) = GPIO_bit(GPIO_NR_A620_ ## gpio ## _N); \
else \
	GPSR(GPIO_NR_A620_ ## gpio ## _N) = GPIO_bit(GPIO_NR_A620_ ## gpio ## _N); \
} while (0)

#define A620_IRQ(gpio) \
	IRQ_GPIO(GPIO_NR_A620_ ## gpio)

#define GPIO_NR_A620_POWER_BUTTON_N		(0)
/* 1 must be reboot*/
#define GPIO_NR_A620_HOME_BUTTON_N		(2)
#define GPIO_NR_A620_CALENDAR_BUTTON_N	(3)
#define GPIO_NR_A620_CONTACTS_BUTTON_N	(4)
#define GPIO_NR_A620_TASKS_BUTTON_N		(5)
/* 6 missing */
/*
 * 7 has some strange reactions to me (Nipo). It makes some ramdom
 * clicks on normal usage, lots more under load.
 */
/* 8 missing */
#define GPIO_NR_A620_CFCARD_RDY_N		(9)

#define GPIO_NR_A620_USB_DETECT			(10)
#define GPIO_NR_A620_RECORD_BUTTON_N	(11)
/* 12 missing */
/* 13 missing */
/* 14 missing */
#define GPIO_NR_A620_COM_DETECT_N		(15)
#define GPIO_NR_A620_BACKLIGHT			(16)
/* 17 missing (PWM1?) */

/* 18 Power detect, see below */
/* 19 missing */
/*
 * 18-20
 * going 0/1 with Power
 * input
 * 18: Interrupt on none
 * 20: Interrupt on RE/FE
 *
 * Putting GPIO_NR_A620_AC_IN_N on 20 is arbitrary
 */
#define GPIO_NR_A620_AC_IN				(20)
/* 21 missing */
/* 22 missing */
#define GPIO_NR_A620_AD7873_SCLK		(23)	/* SSP clock */
#define GPIO_NR_A620_AD7873_SFRM		(24)	/* SSP Frame */
#define GPIO_NR_A620_AD7873_STXD		(25)	/* SSP transmit */
#define GPIO_NR_A620_AD7873_SRXD		(26)	/* SSP receive */
#define GPIO_NR_A620_AD7873_SEXTCLK		(27)	/* SSP ext_clk */

/*
 * IIS to UDA1380 ?
 *
 * Most probable client is
 *
#define GPIO_NR_A620_AC97_BITCLK		(28)
#define GPIO_NR_A620_AC97_SDATA_IN		(29)
#define GPIO_NR_A620_AC97_SDATA_OUT		(30)
#define GPIO_NR_A620_AC97_SYNC			(31)
 *
 * 32 seems to be sound related as well
 */

/*
 * USB pull-up
 * Must be input disconnected (floating)
 * Must be output set to 1 connected
 */
#define GPIO_NR_A620_USBP_PULLUP		(33)

/*
 * Serial connector has no handshaking
 */
#define GPIO_NR_A620_COM_RXD			(34)
#define GPIO_NR_A620_COM_TXD			(39)

/*
 * Joystick directions and button
 * Center is 35
 * Directions triggers one or two of 36,67,40,41
 *
 *   36   36,37     37
 *
 *  36,41   35    37,40
 *
 *   41   40,41     40
 */

#define GPIO_NR_A620_JOY_PUSH_N			(35)
#define GPIO_NR_A620_JOY_NW_N			(36)
#define GPIO_NR_A620_JOY_NE_N			(37)
/* 38 missing */
/* 39 is serial TxD, see above (34) */
#define GPIO_NR_A620_JOY_SE_N			(40)
#define GPIO_NR_A620_JOY_SW_N			(41)

#define GPIO_A620_JOY_DIR	(((GPLR1&0x300)>>6)|((GPLR1&0x30)>>4))

/*
 * 42-45 Bluetooth ?
 */

#define GPIO_NR_A620_IR_RXD				(46)
#define GPIO_NR_A620_IR_TXD				(47)

/*
 * 48-57 Card ?
 * Maybe not for 53
 */
/*
 * 58 through 77 is LCD signals
 */

/*
 * Power management (scaling to 200, 300, 400MHz)
 * Chart is:
 *     78 79
 * 200  1  0
 * 300  0  1
 * 400  1  1
 */
#define GPIO_NR_A620_PWRMGMT0			(78)
#define GPIO_NR_A620_PWRMGMT1			(79)

/*
 *
 */



/*
 * Other infos
 */

#define ASUS_IDE_BASE 		0x00000000
#define ASUS_IDE_SIZE  		0x04000000
#define ASUS_IDE_VIRTUAL		0xF8000000


#endif /* _ASUS_620_GPIO_ */
