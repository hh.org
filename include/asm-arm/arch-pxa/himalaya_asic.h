/*
 *
 * Definitions for HTC Himalaya
 *
 * Copyright 2004 Xanadux.org
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Author: w4xy@xanadux.org
 *
 * Heavily based on h3900_asic.h
 *
 */

#ifndef _INCLUDE_HIMALAYA_ASIC_H_ 
#define _INCLUDE_HIMALAYA_ASIC_H_

#include <asm/arch-sa1100/h3600.h>

/* The H3900 shares a lot of code in common with the H3800 */
#include <asm/hardware/ipaq-asic2.h>
#include <asm/hardware/ipaq-asic3.h>

/****************************************************/
/*
 * This ASIC is at CS3# + 0x01800000
 */
#if defined(CONFIG_MACH_HIMALAYA)

/* these gpio's are on GPIO_A */
#define GPIOA_RS232_ON		(1 << 1)   /* Turn on power to the RS232 chip ? */
 #define GPIOA_USB_ON_N		(1 << 3)   /* Turn on power to USB? */

/* these gpio's are on GPIO_B */

#define GPIOB_GSM_POWER        (1 << 0)   /* Uncertain. Maybe bug in bootloader? */
#define GPIOB_GSM_RESET        (1 << 1)   /* Uncertain. Maybe bug in bootloader? */
//#define GPIOB_LCD_5V_ON        (1 << 3)   /* Enables LCD_5V, inferred from H3900 */
//#define GPIOB_CH_TIMER         (1 << 4)   /* Charger */
#define GPIOB_LCD_5V_ON        (1 << 5)   /* Enables LCD_5V */
#define GPIOB_LCD_ON           (1 << 6)   /* Enables LCD_3V */
//#define GPIOB_LCD_PCI          (1 << 7)   /* Connects to PDWN on LCD controller */
//#define GPIOB_LCD_ON           (1 << 7)   /* Enables LCD_3V inferred from H3900*/
#define GPIOB_LCD_NV_ON        (1 << 8)   /* Inferred from H3900 naming */
//#define GPIOB_TEST_POINT_170   (1 << 8)
#define GPIOB_CIR_CTL_PWR_ON   (1 << 9)
#define GPIOB_AUD_RESET        (1 << 10)
#define GPIOB_LCD_9V_ON        (1 << 11)  /* Inferred from H3900 naming */
//#define GPIOB_BT_PWR_ON        (1 << 11)  /* Bluetooth power on */
#define GPIOB_SPK_ON           (1 << 12)  /* Built-in speaker on */
#define GPIOB_FL_PWR_ON        (1 << 13)  /* Frontlight power on */
#define GPIOB_VIBRATE_ON       (1 << 14)  /* Vibrator */
#define GPIOB_TEST_POINT_123   (1 << 15)

#define ASIC3GPIO_INIT_DIR		0xFFFF			// initial status, sleep direction
#define ASIC3GPIO_INIT_OUT		0x8200			// Strain 2001.12.15
#define ASIC3GPIO_BATFALT_OUT		0x8001
#define ASIC3GPIO_SLEEP_OUT		0x8001
#define ASIC3CLOCK_INIT			0x0

#define GPIOC_BOARDID0 (1 << 3)
#define GPIOC_BOARDID1 (1 << 4)
#define GPIOC_BOARDID2 (1 << 5)

/* this little GPIO went to market^H^H^H^H^H^H^H^H^H^H is on GPIO_D on the h1910... */
#define GPIOB_H1900_IR_ON_N    (1 << 8)

#endif

#endif  // _INCLUDE_HIMALAYA_ASIC_H_
