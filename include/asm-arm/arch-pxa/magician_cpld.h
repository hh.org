#ifndef _MAGICIAN_CPLD_H_
#define _MAGICIAN_CPLD_H_

extern void magician_egpio_enable (struct device *dev, int bit);
extern void magician_egpio_disable (struct device *dev, int bit);

#endif /* _MAGICIAN_CPLD_H_ */
