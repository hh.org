/*
 * linux/battery.h
 *
 * battery class system definitions
 *
 * (c) 2003 Ian Molton <spyro@f2s.com>
 *
 *  You may use this under the GPL2 license.
 *
 */
#ifndef _LINUX_BATTERY_H
#define _LINUX_BATTERY_H

#include <linux/device.h>

/* For systems where the charger determines the maximum battery capacity
 * the min and max fields should be used to present these values to user
 * space. unused fields should be NULL and will not appear in sysfs.
 *
 * For systems where these values are constant for the given battery type
 * these fields should not be used, and user space should look up the values
 * based on the battery id or other information such as the device model.
 *
 * Units are mV, mA, and mAh, where appropriate.
 * temperature is in 10ths of a degree CENTIGRADE.
 *
 */
                                                                         
struct battery {
	struct class_device class_dev;
	const char *name;
	char *id;
	int type;
	int (*get_min_voltage)(struct battery *);
	int (*get_min_current)(struct battery *);
	int (*get_min_charge)(struct battery *);
	int (*get_max_voltage)(struct battery *);
	int (*get_max_current)(struct battery *);
	int (*get_max_charge)(struct battery *);
	int (*get_temp)(struct battery *);
	int (*get_voltage)(struct battery *);
	int (*get_current)(struct battery *);
	int (*get_charge)(struct battery *);
	int (*get_status)(struct battery *);
};

#define BATTERY_STATUS_UNKNOWN      0
#define BATTERY_STATUS_CHARGING     1
#define BATTERY_STATUS_DISCHARGING  2
#define BATTERY_STATUS_NOT_CHARGING 3

extern int battery_class_register(struct battery *);
extern void battery_class_unregister(struct battery *);

#endif
