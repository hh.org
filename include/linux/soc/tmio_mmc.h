#define MMC_CLOCK_DISABLED 0
#define MMC_CLOCK_ENABLED  1
#include <linux/platform_device.h>


struct tmio_mmc_hwconfig {
	void (*hwinit)(struct platform_device *sdev);
	void (*set_mmc_clock)(struct platform_device *sdev, int state);
	short address_shift;
};

