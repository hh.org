/*
 * Copyright 2006 Roman Moravcik <roman.moravcik@gmail.com>
 *
 * LED driver for HP iPAQ rx3715
 *
 * Based on hx4700_leds.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/leds.h>
#include <linux/soc/asic3_base.h>

#include <asm/mach-types.h>

#include <asm/arch/hardware.h>
#include <asm/arch/rx3715-asic3.h>
#include <asm/arch/rx3715-leds.h>

#include <asm/hardware/asic3_leds.h>

DEFINE_LED_TRIGGER_SHARED_GLOBAL(rx3715_radio_trig);
EXPORT_LED_TRIGGER_SHARED(rx3715_radio_trig);

extern struct platform_device s3c_device_asic3;

struct asic3_led rx3715_leds[] = {
	{
		.led_cdev  = {
			.name	        = "rx3715:green",
			.default_trigger = "rx3715-charging",
		},
		.hw_num = 0,

	},
	{
		.led_cdev  = {
			.name	         = "rx3715:red",
			.default_trigger = "rx3715-chargefull",
		},
		.hw_num = 1,
	},
	{
		.led_cdev  = {
			.name	         = "rx3715:blue",
			.default_trigger = "rx3715-radio",
		},
		.hw_num = 2,
	},
};

void rx3715_leds_release(struct device *dev)
{
	return;
}
  
static struct asic3_leds_machinfo rx3715_leds_machinfo = {
	.num_leds = ARRAY_SIZE(rx3715_leds),
	.leds = rx3715_leds,
	.asic3_pdev = &s3c_device_asic3,
};

static struct platform_device rx3715_leds_pdev = {
	.name = "asic3-leds",
	.dev = {
		.platform_data = &rx3715_leds_machinfo,
		.release = rx3715_leds_release,
	},
};

static int __init rx3715_led_init(void)
{
	int ret;

        printk(KERN_INFO "iPAQ rx3715 Leds Driver\n");
	led_trigger_register_shared("rx3715-radio", &rx3715_radio_trig);
	
	ret = asic3_leds_register();
	if (ret)
	    goto asic3_leds_failed;

	ret = platform_device_register(&rx3715_leds_pdev);
	if (ret)
	    goto platform_device_failed;
	
	goto success;

platform_device_failed:
	asic3_leds_unregister();
asic3_leds_failed:
	led_trigger_unregister_shared(rx3715_radio_trig);
success:
	return ret;
}

static void __exit rx3715_led_exit(void)
{
	led_trigger_unregister_shared(rx3715_radio_trig);
	platform_device_unregister(&rx3715_leds_pdev);
	asic3_leds_unregister();
}

module_init(rx3715_led_init);
module_exit(rx3715_led_exit);

MODULE_AUTHOR("Roman Moravcik <roman.moravcik@gmail.com>");
MODULE_DESCRIPTION("HP iPAQ rx3715 LED driver");
MODULE_LICENSE("GPL");
