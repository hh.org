/*
 * Hardware definitions for SA1100-based HP iPAQ Handheld Computers
 *
 * Copyright 2000-2002 Compaq Computer Corporation.
 * Copyright 2002-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 */

unsigned long common_read_egpio( enum ipaq_egpio_type x);
int h3600_common_irq_number(int egpio_nr);
void __init h3xxx_map_io(void);

