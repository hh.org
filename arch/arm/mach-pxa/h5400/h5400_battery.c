/*
 * Battery driver for iPAQ h5400
 *
 * Copyright 2005 Phil Blundell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/apm.h>

#include <asm/mach/arch.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/h5400-asic.h>
#include <asm/arch/h5400-gpio.h>

#include <asm/hardware/samcop_base.h>

#define POWER_NONE	0
#define POWER_AC	1
#define POWER_USB	2

static int power_status;

static inline int
h5400_power_status (void)
{
	int val = samcop_get_gpio_a (&h5400_samcop.dev);

	if (val & SAMCOP_GPIO_GPA_ADP_IN_STATUS)
		return POWER_AC;

	if (val & SAMCOP_GPIO_GPA_USB_DETECT)
		return POWER_USB;

	return POWER_NONE;
}

static void 
h5400_apm_get_power_status (struct apm_power_info *info)
{
	info->ac_line_status = (power_status != POWER_NONE) ? APM_AC_ONLINE : APM_AC_OFFLINE;
}

static irqreturn_t
h5400_ac_in_isr (int irq, void *dev_id, struct pt_regs *regs)
{
	SET_H5400_GPIO (CHG_EN, 0);
	SET_H5400_GPIO (USB_CHG_RATE, 0);
	SET_H5400_GPIO (EXT_CHG_RATE, 0);

	power_status = h5400_power_status ();

	switch (power_status) {
	case POWER_AC:
		printk (KERN_DEBUG "h5400_battery: charger on (AC)\n");
		SET_H5400_GPIO (EXT_CHG_RATE, 1);
		SET_H5400_GPIO (CHG_EN, 1);
		break;
	case POWER_USB:
		printk (KERN_DEBUG "h5400_battery: charger on (USB)\n");
		SET_H5400_GPIO (USB_CHG_RATE, 1);
		SET_H5400_GPIO (CHG_EN, 1);
		break;
	case POWER_NONE:
		printk (KERN_DEBUG "h5400_battery: charger off\n");
		break;
	}

	return IRQ_HANDLED;
}

static int
h5400_battery_init (void)
{
	int result, irq_base;

	if (! machine_is_h5400 ())
		return -ENODEV;

	irq_base = samcop_irq_base (&h5400_samcop.dev);

	result = request_irq (irq_base + _IRQ_SAMCOP_WAKEUP2, h5400_ac_in_isr, 
			      SA_INTERRUPT | SA_SAMPLE_RANDOM, "AC presence", NULL);
	if (result) {
		printk (KERN_CRIT "%s: unable to grab AC in IRQ\n", __FUNCTION__);
		return result;
	}

	result = request_irq (irq_base + _IRQ_SAMCOP_WAKEUP3, h5400_ac_in_isr, 
			      SA_INTERRUPT | SA_SAMPLE_RANDOM, "USB presence", NULL);
	if (result) {
		printk (KERN_CRIT "%s: unable to grab AC in IRQ\n", __FUNCTION__);
		return result;
	}

	h5400_ac_in_isr (0, NULL, NULL);

	apm_get_power_status = h5400_apm_get_power_status;

	return 0;
}

static void
h5400_battery_exit (void)
{
	int irq_base;

	irq_base = samcop_irq_base (&h5400_samcop.dev);

	free_irq (irq_base + _IRQ_SAMCOP_WAKEUP2, NULL);
	free_irq (irq_base + _IRQ_SAMCOP_WAKEUP3, NULL);
	apm_get_power_status = NULL;
}

module_init (h5400_battery_init)
module_exit (h5400_battery_exit)
