/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 * Copyright 2004, 2005 Phil Blundell
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 * History:
 *
 * 2002-08-23   Jamey Hicks        GPIO and IRQ support for iPAQ H5400
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/pm.h>
#include <linux/bootmem.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/fb.h>
#include <../drivers/video/pxafb.h>
#include <linux/input.h>
#include <linux/input_pda.h>

#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/arch/h5400-gpio.h>
#include <asm/arch/h5400-asic.h>
#include <asm/arch/h5400-init.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/udc.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/serial.h>
#include <asm/hardware/gpio_keys.h>
#include <asm/arch/pxa-dmabounce.h>
#include <asm/arch/irq.h>
#include <asm/types.h>
#include <asm/hardware/samcop_base.h>

#include "../generic.h"

/***********************************************************************************/
/*      EGPIO, etc                                                                 */
/***********************************************************************************/

#ifdef DeadCode

static void h5400_control_egpio( enum ipaq_egpio_type x, int setp )
{
	switch (x) {

	case IPAQ_EGPIO_RS232_ON:
		SET_H5400_GPIO (POWER_RS232_N, !setp);
		break;

	default:
		printk("%s: unhandled ipaq gpio=%d\n", __FUNCTION__, x);
	}
}

static void h5400_set_led (enum led_color color, int duty_time, int cycle_time)
{
	static const u8 h5400_lednum[5] = { 4, 0, 1, 2, 3 };
	static unsigned long led_state;
	int lednum, ledvalue;
	unsigned long flags;

	lednum = h5400_lednum[color];
	ledvalue = duty_time ? 0x3 | (duty_time << 4) | (cycle_time << 12) : 0;

	local_irq_save (flags);

	/* Clock needs to be left enabled if any LEDs are flashing */
	if (duty_time && (duty_time < cycle_time))
		led_state |= 1 << lednum;
	else
		led_state &= ~1 << lednum;

	H5400_ASIC_CPM_ClockControl |= H5400_ASIC_CPM_CLKCON_LED_CLKEN;

	H5400_ASIC_LED_CONTROL(lednum) = ledvalue;

	if (led_state == 0)
		H5400_ASIC_CPM_ClockControl &= ~H5400_ASIC_CPM_CLKCON_LED_CLKEN;

	local_irq_restore (flags);
}

#endif

/****************************************************************************
 * Bluetooth functions and data structures
 ****************************************************************************/

/* We need a little magic function switching here to keep samcop_base as
 * a kernel module
 */

void (*h5400_set_samcop_gpio_b) (struct device *dev, u32 mask, u32 bits);
EXPORT_SYMBOL(h5400_set_samcop_gpio_b);

void
h5400_set_gpio_b (struct device *dev, u32 mask, u32 bits)
{
	if (h5400_set_samcop_gpio_b)
		h5400_set_samcop_gpio_b(dev, mask, bits);
}

static void h5400_bluetooth_power(int on)
{
	if (on) {
		/* apply reset. The chip requires that the reset pins go
		 * high 2ms after voltage is applied to VCC and IOVCC. So it
		 * is driven low now before we set the power pins. It
		 * is assumed that the reset pins are tied together
		 * on the h5[1,4,5]xx handhelds. */
		SET_H5400_GPIO(BT_M_RESET, !on);
		
		/* Select the 'operating environment' pins to run/normal mode.
		 * Setting these pins to ENV_0 = 0 and ENV_1 = 1 would put the
		 * In-System-Programming (ISP) mode. In theory that would
		 * allow the host computer to rewrite the firmware.
		 */
		SET_H5400_GPIO(BT_ENV_0, on);
		SET_H5400_GPIO(BT_ENV_1, on);
		/* configure power pins */
		h5400_set_gpio_b(&h5400_samcop.dev,
				 SAMCOP_GPIO_GPB_BLUETOOTH_3V0_ON,
				 SAMCOP_GPIO_GPB_BLUETOOTH_3V0_ON);
		SET_H5400_GPIO(BT_2V8_N, !on);
                /* A 2ms delay between voltage application and reset driven
                 * high is a requirement of the power-on cycle of the device.
                 */
		mdelay(2);
		SET_H5400_GPIO(BT_M_RESET, on);
	} else {
		h5400_set_gpio_b(&h5400_samcop.dev,
				 SAMCOP_GPIO_GPB_BLUETOOTH_3V0_ON,
				 ~SAMCOP_GPIO_GPB_BLUETOOTH_3V0_ON);
		SET_H5400_GPIO(BT_2V8_N, !on);
	}
}

static void h5400_btuart_configure(int state)
{
	switch (state) {
	case PXA_UART_CFG_PRE_STARTUP:
		pxa_gpio_mode(GPIO42_BTRXD_MD);
		pxa_gpio_mode(GPIO43_BTTXD_MD);
		pxa_gpio_mode(GPIO44_BTCTS_MD);
		pxa_gpio_mode(GPIO45_BTRTS_MD);
		h5400_bluetooth_power(1);
		break;

	case PXA_UART_CFG_PRE_SHUTDOWN:
		h5400_bluetooth_power(0);
		break;

	default:
		break;
	}
}

static void h5400_hwuart_configure(int state)
{
	switch (state) {
	case PXA_UART_CFG_PRE_STARTUP:
		pxa_gpio_mode(GPIO42_HWRXD_MD);
		pxa_gpio_mode(GPIO43_HWTXD_MD);
		pxa_gpio_mode(GPIO44_HWCTS_MD);
		pxa_gpio_mode(GPIO45_HWRTS_MD);
		h5400_bluetooth_power(1);
		break;

	case PXA_UART_CFG_PRE_SHUTDOWN:
		h5400_bluetooth_power(0);
		break;

	default:
		break;
	}
}

static struct platform_pxa_serial_funcs h5400_btuart_funcs = {
	.configure = h5400_btuart_configure,
};

static struct platform_pxa_serial_funcs h5400_hwuart_funcs = {
	.configure = h5400_hwuart_configure,
};

static __inline__ void 
fix_msc (void)
{
	/* fix CS0 for h5400 flash. */
	/* fix CS1 for MediaQ chip.  select 16-bit bus and vlio.  */
	/* fix CS5 for SAMCOP.  */
	MSC0 = 0x129c24f2;
	(void)MSC0;
	MSC1 = 0x7ff424fa;
	(void)MSC1;
	MSC2 = 0x7ff47ff4;
	(void)MSC2;

	MDREFR |= 0x02080000;
}

#if 0
static int h5400_pm_callback( int req )
{
	int result = 0;
	static int gpio_0, gpio_1, gpio_2;	/* PXA GPIOs */
	
	printk("%s: %d\n", __FUNCTION__, req);

	switch (req) {
	case PM_RESUME:
		/* bootldr will have screwed up MSCs, so set them right again */
		fix_msc ();

#if 0
		H5400_ASIC_GPIO_GPA_DAT = gpio_a;
		H5400_ASIC_GPIO_GPB_DAT = gpio_b;
#endif

		GPSR0 = gpio_0;
		GPCR0 = ~gpio_0;
		GPSR1 = gpio_1;
		GPCR1 = ~gpio_1;
		GPSR2 = gpio_2;
		GPCR2 = ~gpio_2;

#ifdef DeadCode
		if ( ipaq_model_ops.pm_callback_aux )
			result = ipaq_model_ops.pm_callback_aux(req);
#endif
		break;

	case PM_SUSPEND:
#ifdef DeadCode
		if ( ipaq_model_ops.pm_callback_aux &&
		     ((result = ipaq_model_ops.pm_callback_aux(req)) != 0))
			return result;
#endif

#if 0		
		gpio_a = H5400_ASIC_GPIO_GPA_DAT;
		gpio_b = H5400_ASIC_GPIO_GPB_DAT;
#endif

		gpio_0 = GPLR0;
		gpio_1 = GPLR1;
		gpio_2 = GPLR2;
		break;

	default:
		printk("%s: unrecognized PM callback\n", __FUNCTION__);
		break;
	}
	return result;
}
#endif

/***********************************************************************************/
/*      Miscellaneous                                                              */
/***********************************************************************************/



/***********************************************************************************/
/*      Initialisation                                                             */
/***********************************************************************************/

static short h5400_gpio_modes[] __initdata = {
	GPIO_NR_H5400_POWER_BUTTON | GPIO_IN,			/* GPIO0 */
	GPIO_NR_H5400_RESET_BUTTON_N | GPIO_IN,			/* GPIO1 */
	GPIO_NR_H5400_OPT_INT | GPIO_IN,			/* GPIO2 */
	GPIO_NR_H5400_BACKUP_POWER | GPIO_IN,			/* GPIO3 */	/* XXX should be an output? */
	GPIO_NR_H5400_ACTION_BUTTON | GPIO_IN,			/* GPIO4 */
	GPIO_NR_H5400_COM_DCD_SOMETHING | GPIO_IN,		/* GPIO5 */
	6 | GPIO_IN,						/* GPIO6 NC */
	GPIO_NR_H5400_RESET_BUTTON_AGAIN_N | GPIO_IN,		/* GPIO7 */
	8 | GPIO_OUT,						/* GPIO8 NC */
	GPIO_NR_H5400_RSO_N | GPIO_IN,				/* GPIO9 BATT_FAULT */
	GPIO_NR_H5400_ASIC_INT_N | GPIO_IN,			/* GPIO10 */
	GPIO_NR_H5400_BT_ENV_0 | GPIO_OUT,			/* GPIO11 */
	GPIO12_32KHz_MD | GPIO_OUT,				/* GPIO12 NC */
	GPIO_NR_H5400_BT_ENV_1 | GPIO_OUT,			/* GPIO13 */
	GPIO_NR_H5400_BT_WU | GPIO_IN,				/* GPIO14 */
	GPIO15_nCS_1_MD,					/* GPIO15 */
	16 | GPIO_OUT,						/* GPIO16 NC */
	17 | GPIO_OUT,						/* GPIO17 NC */

	22 | GPIO_OUT,						/* GPIO22 NC */
	GPIO23_SCLK_MD,
	GPIO_NR_H5400_OPT_SPI_CS_N | GPIO_OUT,
	GPIO25_STXD_MD,
	GPIO26_SRXD_MD,
	27 | GPIO_OUT,						/* GPIO27 NC */

	GPIO34_FFRXD_MD,
	GPIO35_FFCTS_MD,
	GPIO36_FFDCD_MD,
	GPIO37_FFDSR_MD,
	GPIO38_FFRI_MD,
	GPIO39_FFTXD_MD,
	GPIO40_FFDTR_MD,
	GPIO41_FFRTS_MD,
	GPIO42_BTRXD_MD,
	GPIO43_BTTXD_MD,
	GPIO44_BTCTS_MD,
	GPIO45_BTRTS_MD,

	GPIO_NR_H5400_IRDA_SD | GPIO_OUT,			/* GPIO58 */
	59 | GPIO_OUT,						/* GPIO59 XXX docs say "usb charge on" input */
	GPIO_NR_H5400_POWER_SD_N | GPIO_OUT,			/* GPIO60 XXX not really active low? */
	GPIO_NR_H5400_POWER_RS232_N | GPIO_OUT | GPIO_DFLT_HIGH,
	GPIO_NR_H5400_POWER_ACCEL_N | GPIO_OUT | GPIO_DFLT_HIGH,
	63 | GPIO_OUT,						/* GPIO63 NC */
	GPIO_NR_H5400_OPT_NVRAM | GPIO_OUT ,
	GPIO_NR_H5400_CHG_EN | GPIO_OUT ,
	GPIO_NR_H5400_USB_PULLUP | GPIO_OUT ,
	GPIO_NR_H5400_BT_2V8_N | GPIO_OUT | GPIO_DFLT_HIGH,
	GPIO_NR_H5400_EXT_CHG_RATE | GPIO_OUT ,
	69 | GPIO_OUT,						/* GPIO69 NC */
	GPIO_NR_H5400_CIR_RESET | GPIO_OUT ,
	GPIO_NR_H5400_POWER_LIGHT_SENSOR_N | GPIO_OUT | GPIO_DFLT_HIGH,
	GPIO_NR_H5400_BT_M_RESET | GPIO_OUT ,
	GPIO_NR_H5400_STD_CHG_RATE | GPIO_OUT ,
	GPIO_NR_H5400_SD_WP_N | GPIO_IN,			/* GPIO74 XXX docs say output */
	GPIO_NR_H5400_MOTOR_ON_N | GPIO_OUT | GPIO_DFLT_HIGH,
	GPIO_NR_H5400_HEADPHONE_DETECT | GPIO_IN ,
	GPIO_NR_H5400_USB_CHG_RATE | GPIO_OUT ,
	GPIO78_nCS_2_MD,
	GPIO79_nCS_3_MD,
	GPIO80_nCS_4_MD
};

static void __init h5400_map_io(void)
{
	int i;
	int cpuid;

	pxa_map_io ();

	/* Configure power management stuff. */
	PWER = PWER_GPIO0 | PWER_RTC;
	PFER = PWER_GPIO0 | PWER_RTC;
	PRER = 0;
	PCFR = PCFR_OPDE;
	CKEN = CKEN6_FFUART;

#if 0
	h5400_asic_write_register (H5400_ASIC_CPM_ClockControl, H5400_ASIC_CPM_CLKCON_LED_CLKEN);
	h5400_asic_write_register (H5400_ASIC_LED_LEDPS, 0xf42400);		/* 4Hz */

	H5400_ASIC_SET_BIT (H5400_ASIC_CPM_ClockControl, H5400_ASIC_CPM_CLKCON_GPIO_CLKEN);
	H5400_ASIC_SET_BIT (H5400_ASIC_GPIO_GPA_CON2, 0x16);
#endif

	for (i = 0; i < ARRAY_SIZE(h5400_gpio_modes); i++) {
		int mode = h5400_gpio_modes[i];
		pxa_gpio_mode(mode);
	}

#if 0
	/* Does not currently work */
	/* Add wakeup on AC plug/unplug */
	PWER  |= PWER_GPIO12;
#endif

	fix_msc ();

	/* Set sleep states for PXA GPIOs */
	PGSR0 = GPSRx_SleepValue;
	PGSR1 = GPSRy_SleepValue;
	PGSR2 = GPSRz_SleepValue;

	/* hook up btuart & hwuart and power bluetooth */
	/* The h54xx has pxa-250, so it gets btuart */
	/* and h51xx and h55xx have pxa-255, so they get hwuart */
	cpuid = read_cpuid(CPUID_ID);
	if (((cpuid >> 4) & 0xfff) == 0x2d0) {
		hwuart_device.dev.platform_data = &h5400_hwuart_funcs;
	} else {
		btuart_device.dev.platform_data = &h5400_btuart_funcs;
	}
}

/* ------------------- */

static int 
h5400_udc_is_connected (void)
{
	return 1;
}

static void 
h5400_udc_command (int cmd) 
{
	switch (cmd)
	{
	case PXA2XX_UDC_CMD_DISCONNECT:
		GPCR(GPIO_NR_H5400_USB_PULLUP) |= GPIO_bit(GPIO_NR_H5400_USB_PULLUP);
		GPDR(GPIO_NR_H5400_USB_PULLUP) &= ~GPIO_bit(GPIO_NR_H5400_USB_PULLUP);
		break;
	case PXA2XX_UDC_CMD_CONNECT:
		GPDR(GPIO_NR_H5400_USB_PULLUP) |= GPIO_bit(GPIO_NR_H5400_USB_PULLUP);
		GPSR(GPIO_NR_H5400_USB_PULLUP) |= GPIO_bit(GPIO_NR_H5400_USB_PULLUP);
		break;
	default:
		printk("_udc_control: unknown command!\n");
		break;
	}
}

static struct pxa2xx_udc_mach_info h5400_udc_mach_info = {
	.udc_is_connected = h5400_udc_is_connected,
	.udc_command      = h5400_udc_command,
};

/* ------------------- */

static struct resource samcop_resources[] = {
	[0] = {
		.start	= H5400_SAMCOP_BASE,
		.end	= H5400_SAMCOP_BASE + 0x00ffffff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_GPIO_H5400_ASIC_INT,
		.flags  = IORESOURCE_IRQ,
	},
};

static struct samcop_platform_data samcop_platform_data = {
	.clocksleep = SAMCOP_CPM_CLKSLEEP_UCLK_ON,
	.pllcontrol = 0x60002,		/* value from wince via bootblaster */
};

struct platform_device h5400_samcop = {
	.name		= "samcop",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(samcop_resources),
	.resource	= samcop_resources,
	.dev		= {
		.platform_data = &samcop_platform_data,
	},
};
EXPORT_SYMBOL(h5400_samcop);

static struct gpio_keys_button h5400_button_table[] = {
	{ _KEY_POWER, GPIO_NR_H5400_POWER_BUTTON, 1 },
};

static struct gpio_keys_platform_data h5400_pxa_keys_data = {
	.buttons = h5400_button_table,
	.nbuttons = ARRAY_SIZE(h5400_button_table),
};

static struct platform_device h5400_pxa_keys = {
	.name = "gpio-keys",
	.dev = {
		.platform_data = &h5400_pxa_keys_data,
	},
};

static void __init
h5400_init (void)
{
	platform_device_register (&h5400_samcop);
	platform_device_register (&h5400_pxa_keys);
	pxa_set_udc_info (&h5400_udc_mach_info);
}

MACHINE_START(H5400, "HP iPAQ H5400")
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params	= 0xa0000100,
        .map_io		= h5400_map_io,
        .init_irq	= pxa_init_irq,
        .timer = &pxa_timer,
        .init_machine = h5400_init,
MACHINE_END
