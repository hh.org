/*
 * linux/arch/arm/mach-pxa/htcalpine/htcalpine.c
 *
 *  Support for the Intel XScale based Palm PDAs. Only the LifeDrive is
 *  supported at the moment.
 *
 *  Author: Alex Osborne <bobofdoom@gmail.com>
 *
 *  USB stubs based on aximx30.c (Michael Opdenacker)
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/fb.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/udc.h>

#include <asm/arch/htcalpine-gpio.h>

#include "tsc2046_ts.h"

#include "../generic.h"

#define GPSR_BIT(n) (GPSR((n)) = GPIO_bit((n)))
#define GPCR_BIT(n) (GPCR((n)) = GPIO_bit((n)))

static struct pxafb_mach_info htcalpine_lcd __initdata = {
	.pixclock		= 115384,
	.xres			= 240,
	.yres			= 320,
	.bpp			= 16,
	.hsync_len		= 34,
	.vsync_len		= 8,
	.left_margin		= 34,
	.right_margin		= 34,
	.upper_margin		= 7,
	.lower_margin		= 7,

//	.sync			= FB_SYNC_HOR_LOW_ACT|FB_SYNC_VERT_LOW_ACT,

	/* fixme: this is a hack, use constants instead. */
	.lccr0			= 0x04000081,
	.lccr3			= 0x04400000,

};

static struct tsc2046_mach_info htcalpine_ts_platform_data = {
       .port     = 2,
       .clock    = CKEN3_SSP2,
       .pwrbit_X = 1,
       .pwrbit_Y = 1,
       .irq	 = HTCALPINE_IRQ(TOUCHPANEL_IRQ_N)
};

static struct platform_device htcalpine_ts        = {
       .name = "htcalpine_ts",
       .dev  = {
              .platform_data = &htcalpine_ts_platform_data,
       },
};

static struct platform_device *devices[] __initdata = {
	&htcalpine_ts,
//	&htcalpine_flash,
//	&htcalpine_pxa_keys,
};

/****************************************************************
 * USB client controller
 ****************************************************************/

static void udc_command(int cmd)
{
	switch (cmd)
	{
		case PXA2XX_UDC_CMD_DISCONNECT:
			printk(KERN_NOTICE "USB cmd disconnect\n");
                        GPCR_BIT(GPIO_NR_HTCALPINE_USB_PUEN);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			printk(KERN_NOTICE "USB cmd connect\n");
                        GPSR_BIT(GPIO_NR_HTCALPINE_USB_PUEN);
			break;
	}
}

static struct pxa2xx_udc_mach_info htcalpine_udc_mach_info = {
	.udc_command      = udc_command,
};

static void __init htcalpine_init(void)
{
	set_pxa_fb_info( &htcalpine_lcd );
	platform_add_devices( devices, ARRAY_SIZE(devices) );
	pxa_set_udc_info( &htcalpine_udc_mach_info );
}

MACHINE_START(HTCALPINE, "HTC Alpine")
	.phys_io	= 0x40000000,
	.io_pg_offst	= io_p2v(0x40000000),
	.boot_params	= 0xa0000100,
	.map_io 	= pxa_map_io,
	.init_irq	= pxa_init_irq,
	.timer  	= &pxa_timer,
	.init_machine	= htcalpine_init,
MACHINE_END

