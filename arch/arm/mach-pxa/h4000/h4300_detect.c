/*
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Copyright (C) 2006 Paul Sokolosvky
 * Based on code from h4300_kbd.c
 *
 */

#include <linux/module.h>

int h4000_machine_is_h4300(void);

int h4000_machine_is_h4300()
{
	static int flag = -1;

	if (flag != -1)  return flag;
	    
	flag = 1;

	return flag;
}

EXPORT_SYMBOL(h4000_machine_is_h4300);

