/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * History:
 * 2004-??-??  Shawn Anderson    Derived the from aximx3.c aximx5.c e7xx.c 
 *                               h1900.c h2200.c h3900.c h5400.c and friends.
 * 2004-04-01  Eddi De Pieri     Move lcd stuff so it can be used as a module.
 * 2004-04-19  Eddi De Pieri     Moving to new 2.6 standard 
 *                               (platform_device / device_driver structure)
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>

#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>

#include <asm/arch/bitfield.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/pxafb.h>
#include <linux/soc/asic3_base.h>
#include <asm/arch/h4000-gpio.h>
#include <asm/arch/h4000-init.h>
#include <asm/arch/h4000-asic.h>

#include <asm/hardware/ipaq-asic3.h>

#include "../generic.h"
#define asic3 &h4000_asic3.dev

void h4000_ll_pm_init(void);

extern struct platform_device h4000_bl;
static struct platform_device h4000_lcd       = { .name = "h4000_lcd", };
static struct platform_device h4300_kbd       = { .name = "h4300_kbd", };
static struct platform_device h4000_buttons   = { .name = "h4000_buttons", };
static struct platform_device h4000_ts        = { .name = "h4000_ts", };
static struct platform_device h4000_udc       = { .name = "h4000_udc", };
static struct platform_device h4000_pcmcia    = { .name = "h4000_pcmcia", };
static struct platform_device h4000_batt      = { .name = "h4000_batt", };

static struct platform_device *h4000_asic3_devices[] __initdata = {
	&h4000_lcd,
#ifdef CONFIG_IPAQ_H4000_BACKLIGHT
	&h4000_bl,
#endif
	&h4300_kbd,
	&h4000_buttons,
	&h4000_ts,
	&h4000_udc,
	&h4000_pcmcia,
	&h4000_batt,
};

static struct asic3_platform_data h4000_asic3_platform_data = {
	.gpio_a = {
		.dir            = 0xfc7f,
		.init           = 0x0000 | GPIOA_RS232_ON,
		.sleep_out      = 0x0000,
		.batt_fault_out = 0x0000,
		.alt_function   = 0x0000,
		.sleep_conf     = 0x000c,
	},
	.gpio_b = {
		.dir            = 0xddbf, /* 0xdfbd for h4300 kbd irq */
		.init           = 0x1c00, /* 0x1c05 for h4300 kbd wakeup/power*/
		.sleep_out      = 0x0000,
		.batt_fault_out = 0x0000,
		.alt_function   = 0x0000,
		.sleep_conf     = 0x000c,
	},
	.gpio_c = {
		.dir            = 0xffff, /* 0xfff7 for h4300 key rxd spi */
		.init           = 0x4700,
		.sleep_out      = 0x4000,
		.batt_fault_out = 0x4000,
		.alt_function   = 0x0003, /* 0x003b for h4300 kbd spi */ 
		.sleep_conf     = 0x000c,
	},
	.gpio_d = {
		.dir            = 0xef03, /* 0xef7b for h4300, no buttons here*/
		.init           = 0x0f02 | GPIOD_USB_ON | GPIOD_USB_PULLUP,
		.sleep_out      = 0x0100,
		.batt_fault_out = 0x0100,
		.alt_function   = 0x0000,
		.sleep_conf     = 0x000c,
	},
	.child_platform_devs     = h4000_asic3_devices,
	.num_child_platform_devs = ARRAY_SIZE(h4000_asic3_devices),
};

static struct resource h4000_asic3_resources[] = {
        /* GPIO part */
	[0] = {
		.start  = H4000_ASIC3_PHYS,
		.end    = H4000_ASIC3_PHYS + IPAQ_ASIC3_MAP_SIZE,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_GPIO(GPIO_NR_H4000_ASIC3_IRQ),
		.flags  = IORESOURCE_IRQ,
	},
        /* SD part */
	[2] = {
		.start  = H4000_ASIC3_SD_PHYS,
		.end    = H4000_ASIC3_SD_PHYS + IPAQ_ASIC3_MAP_SIZE,
		.flags  = IORESOURCE_MEM,
	},
	[3] = {
		.start  = IRQ_GPIO(GPIO_NR_H4000_SD_IRQ_N),
		.flags  = IORESOURCE_IRQ,
	},
};

struct platform_device h4000_asic3 = {
	.name           = "asic3",
	.id             = 0,
	.num_resources  = ARRAY_SIZE(h4000_asic3_resources),
	.resource       = h4000_asic3_resources,
	.dev = { .platform_data = &h4000_asic3_platform_data, },
};
EXPORT_SYMBOL(h4000_asic3);


void h4000_set_led(int color, int duty_time, int cycle_time)
{
        if (color == H4000_RED_LED) {
                asic3_set_led(asic3, 0, duty_time, cycle_time);
                asic3_set_led(asic3, 1, 0, cycle_time);
        }

        if (color == H4000_GREEN_LED) {
                asic3_set_led(asic3, 1, duty_time, cycle_time);
                asic3_set_led(asic3, 0, 0, cycle_time);
        }

        if (color == H4000_YELLOW_LED) {
                asic3_set_led(asic3, 1, duty_time, cycle_time);
                asic3_set_led(asic3, 0, duty_time, cycle_time);
        }
}
EXPORT_SYMBOL(h4000_set_led);


/*
 * LCCR0: 0x003008f9 -- ENB=0x1,  CMS=0x0, SDS=0x0, LDM=0x1,
 *                      SFM=0x1,  IUM=0x1, EFM=0x1, PAS=0x1,
 *                      res=0x0,  DPD=0x0, DIS=0x0, QDM=0x1,
 *                      PDD=0x0,  BM=0x1,  OUM=0x1, res=0x0
 * LCCR1: 0x13070cef -- BLW=0x13, ELW=0x7, HSW=0x3, PPL=0xef
 * LCCR2: 0x0708013f -- BFW=0x7,  EFW=0x8, VSW=0x0, LPP=0x13f
 * LCCR3: 0x04700008 -- res=0x0,  DPC=0x0, BPP=0x4, OEP=0x0, PCP=0x1
 *                      HSP=0x1,  VSP=0x1, API=0x0, ACD=0x0, PCD=0x8
 */

static struct pxafb_mode_info h4000_lcd_modes[] __initdata = {
{
        .pixclock     = 171521,     // (160756 > 180849)
        .xres         = 240,        // PPL + 1
        .yres         = 320,        // LPP + 1
        .bpp          = 16,         // BPP (0x4 == 16bits)
        .hsync_len    = 4,          // HSW + 1
        .vsync_len    = 1,          // VSW + 1
        .left_margin  = 20,         // BLW + 1
        .upper_margin = 8,          // BFW + 1
        .right_margin = 8,          // ELW + 1
        .lower_margin = 8,          // EFW + 1
        .sync         = 0,
},
};

static struct pxafb_mach_info sony_acx502bmu __initdata = {
        .modes		= h4000_lcd_modes,
        .num_modes	= ARRAY_SIZE(h4000_lcd_modes), 
        
        .lccr0        = LCCR0_Act | LCCR0_Sngl | LCCR0_Color,
        .lccr3        = LCCR3_OutEnH | LCCR3_PixFlEdg | LCCR3_Acb(0),

        //.pxafb_backlight_power = ,
        //.pxafb_lcd_power =       ,
};

/*
CS3#    0x0C000000, 1 ;HTC Asic3 chip select
CS4#    0x10000000, 1 ;SD I/O controller
CS5#    0x14000000, 1 ;DBG LED REGISTER
        0x15000000, 1 ;DBG LAN REGISTER
        0x16000000, 1 ;DBG PPSH REGISTER */

static void __init h4000_init_irq(void)
{
	pxa_init_irq();
}

static void __init h4000_map_io(void)
{
	pxa_map_io();

	/* Wake up enable. */
	PWER = PWER_GPIO0 | PWER_RTC;
	/* Wake up on falling edge. */
	PFER = PWER_GPIO0 | PWER_RTC;
	/* Wake up on rising edge. */
	PRER = 0;
	/* 3.6864 MHz oscillator power-down enable */
	PCFR = PCFR_OPDE;
}

static void __init h4000_init(void)
{
	set_pxa_fb_info(&sony_acx502bmu);
#ifdef CONFIG_PM
	h4000_ll_pm_init();
#endif
	platform_device_register(&h4000_asic3);
}

MACHINE_START(H4000, "HP iPAQ H4000")
	/* Maintainer h4000 port team h4100-port@handhelds.org */
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params	= 0xa0000100,
	.map_io		= h4000_map_io,
	.init_irq	= h4000_init_irq,
	.init_machine	= h4000_init,
	.timer		= &pxa_timer,
MACHINE_END

/* vim: set ts=8 tw=80 shiftwidth=8 noet: */

