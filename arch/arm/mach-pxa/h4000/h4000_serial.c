/* 
 * h4000_serial.c:
 * Turns on serial support when a cord is attached.
 * 
 * */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/platform_device.h>
#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <linux/soc/asic3_base.h>
#include "../generic.h"
#include <asm/arch/h4000-gpio.h>
#include <asm/arch/h4000-asic.h>

extern struct platform_device h4000_asic3;

static irqreturn_t h4000_rs232(int irq, void *dev_id, struct pt_regs *regs)
{
	if (GET_H4000_GPIO(SERIAL_DETECT))
		asic3_set_gpio_out_a(&h4000_asic3.dev,
					  GPIOA_RS232_ON, GPIOA_RS232_ON);
	else
		asic3_set_gpio_out_a(&h4000_asic3.dev, GPIOA_RS232_ON, 0);
	return IRQ_HANDLED;
}

static int __init h4000_serial_init(void)
{
	if (request_irq(IRQ_GPIO(GPIO_NR_H4000_SERIAL_DETECT), &h4000_rs232,
			SA_INTERRUPT, "h4000_rs232", NULL))
		printk("request_irq failed for h4000 serial detection\n");
	else {
		set_irq_type(IRQ_GPIO(GPIO_NR_H4000_SERIAL_DETECT),
			     IRQT_BOTHEDGE);
		pxa_gpio_mode(GPIO34_FFRXD_MD);
		pxa_gpio_mode(GPIO35_FFCTS_MD);
		pxa_gpio_mode(GPIO36_FFDCD_MD);
		pxa_gpio_mode(GPIO37_FFDSR_MD);
		pxa_gpio_mode(GPIO38_FFRI_MD);
		pxa_gpio_mode(GPIO39_FFTXD_MD);
		pxa_gpio_mode(GPIO40_FFDTR_MD);
		pxa_gpio_mode(GPIO41_FFRTS_MD);
	}
	return 0;
}

module_init(h4000_serial_init);
MODULE_LICENSE("GPL");

