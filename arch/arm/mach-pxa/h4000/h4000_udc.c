/*
 * h4000_udc.c:
 * h4000 specific code for the pxa255 usb device controller.
 * 
 * */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/udc.h>
#include <linux/soc/asic3_base.h>
#include <asm/arch/h4000-gpio.h>
#include <asm/arch/h4000-asic.h>

extern struct platform_device h4000_asic3;

static void h4000_udc_command(int cmd)
{
	switch (cmd) {
	case PXA2XX_UDC_CMD_DISCONNECT:
		asic3_set_gpio_out_d(&h4000_asic3.dev,
					  GPIOD_USB_PULLUP, 0);
		break;
	case PXA2XX_UDC_CMD_CONNECT:
		asic3_set_gpio_out_d(&h4000_asic3.dev,
					  GPIOD_USB_PULLUP, GPIOD_USB_PULLUP);
		break;
	default:
		printk("_udc_control: unknown command!\n");
		break;
	}
}

static int h4000_udc_is_connected(void)
{
	return (!(GET_H4000_GPIO(USB_DETECT_N)));
}

static struct pxa2xx_udc_mach_info h4000_udc_info __initdata = {
	.udc_is_connected = h4000_udc_is_connected,
	.udc_command      = h4000_udc_command,
};

static int h4000_udc_probe(struct platform_device * dev)
{
	pxa_set_udc_info(&h4000_udc_info);
	return 0;
}

static struct platform_driver h4000_udc_driver = {
	.driver	  = {
		.name     = "h4000_udc",
	},
	.probe    = h4000_udc_probe,
};

static int __init h4000_udc_init(void)
{
	return platform_driver_register(&h4000_udc_driver);
}

module_init(h4000_udc_init);
MODULE_LICENSE("GPL");

