/*
 * Copyright (C) 2003 Joshua Wise
 * Copyright (c) 2002,2003 SHARP Corporation
 * Copyright (C) 2005 Pawel Kolodziejski
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * HAL code based on h5400_asic_io.c, which is
 *  Copyright (C) 2003 Compaq Computer Corporation.
 *
 * Author:  Joshua Wise <joshua at joshuawise.com>
 *          June 2003
 *
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/device.h>
#include <linux/sysctl.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/soc/asic3_base.h>
#include <linux/platform_device.h>

#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/arch/hardware.h>
#include <asm/irq.h>
#include <asm/mach/irq.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch-pxa/h4000-gpio.h>
#include <asm/arch-pxa/h4000-asic.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/apm.h>

/* Actually had battery fault at 3358 -pfalcon
   Also, wince appears to shutdown at much higer
   value, and using this one gives battery percentages
   completely not matching wince. Anyway, way to solve 
   this is to figure out how to get battery *capacity*,
   not voltage.
*/
#define BATTERY_MIN 3400
#define BATTERY_MAX 3950

/* from ads7846.c */
/* The ADS7846 has touchscreen and other sensors.
 * Earlier ads784x chips are somewhat compatible.
 */
#define ADS_START               (1 << 7)
#define ADS_A2A1A0_d_y          (1 << 4)        /* differential */
#define ADS_A2A1A0_d_z1         (3 << 4)        /* differential */
#define ADS_A2A1A0_d_z2         (4 << 4)        /* differential */
#define ADS_A2A1A0_d_x          (5 << 4)        /* differential */
#define ADS_A2A1A0_temp0        (0 << 4)        /* non-differential */
#define ADS_A2A1A0_vbatt        (2 << 4)        /* non-differential */
#define ADS_A2A1A0_vaux         (6 << 4)        /* non-differential */
#define ADS_A2A1A0_temp1        (7 << 4)        /* non-differential */
#define ADS_8_BIT               (1 << 3)
#define ADS_12_BIT              (0 << 3)
#define ADS_SER                 (1 << 2)        /* non-differential */
#define ADS_DFR                 (0 << 2)        /* differential */
#define ADS_PD10_PDOWN          (0 << 0)        /* lowpower mode + penirq */
#define ADS_PD10_ADC_ON         (1 << 0)        /* ADC on */
#define ADS_PD10_REF_ON         (2 << 0)        /* vREF on + penirq */
#define ADS_PD10_ALL_ON         (3 << 0)        /* ADC + vREF on */


extern struct platform_device h4000_asic3;
#define asic3 &h4000_asic3.dev
extern void h4000_set_led(int color, int duty_time, int cycle_time);
extern unsigned long h4000_spi_putget(ulong data);

static struct timer_list timer_bat;

int battery_power;

static void h4000_battery(unsigned long nr)
{
	int sample;

        /* Select main battery voltage source VS_MBAT */
	asic3_set_gpio_out_d(asic3, GPIOD_MUX_SEL1|GPIOD_MUX_SEL0, 0);
	/* Likely not yet fully settled, by seems stable */
	udelay(50);
        sample = h4000_spi_putget(ADS_START | ADS_A2A1A0_vaux | ADS_12_BIT | ADS_SER | ADS_PD10_ADC_ON);
        //printk("sample vaux= %d\n", sample);

	/* Throw away invalid samples, this does happen soon after resume for example. */
	if (sample > 0) {
		battery_power = ((sample - BATTERY_MIN) * 100) / (BATTERY_MAX - BATTERY_MIN);
		if (battery_power < 0)
			battery_power = 0;
		else if (battery_power > 100)
			battery_power = 100;
	}


	if (!!(GPLR(GPIO_NR_H4000_AC_IN_N) & GPIO_bit(GPIO_NR_H4000_AC_IN_N))) {
		if (battery_power > 50) {
			asic3_set_led(asic3, 1, 0, 0x100);
			asic3_set_led(asic3, 0, 0, 0x100);
		} else if ((battery_power < 50) && (battery_power > 10))
			h4000_set_led(H4000_GREEN_LED, 0x101, 0x100);
		else if (battery_power < 10)
			h4000_set_led(H4000_RED_LED, 0x101, 0x100);
	}

	//printk("bat: %d\n", battery_power);

	mod_timer(&timer_bat, jiffies + (1000 * HZ) / 1000);
}

typedef void (*apm_get_power_status_t)(struct apm_power_info*);

static void h4000_apm_get_power_status(struct apm_power_info *info)
{
	info->battery_life = battery_power;

	if (!(GPLR(GPIO_NR_H4000_AC_IN_N) & GPIO_bit(GPIO_NR_H4000_AC_IN_N)))
		info->ac_line_status = APM_AC_ONLINE;
	else
		info->ac_line_status = APM_AC_OFFLINE;

	if (GPLR(GPIO_NR_H4000_CHARGING) & GPIO_bit(GPIO_NR_H4000_CHARGING))
		info->battery_status = APM_BATTERY_STATUS_CHARGING;
	/*else if (!(GPLR(GPIO_NR_H4000_MBAT_IN) & GPIO_bit(GPIO_NR_H4000_MBAT_IN)))
		info->battery_status = APM_BATTERY_STATUS_NOT_PRESENT;*/
	else {
		if (battery_power > 50)
			info->battery_status = APM_BATTERY_STATUS_HIGH;
		else if (battery_power < 5)
			info->battery_status = APM_BATTERY_STATUS_CRITICAL;
		else
			info->battery_status = APM_BATTERY_STATUS_LOW;
	}

	/* Consider one "percent" per minute, which is shot in the sky. */
	info->time = battery_power;
	info->units = APM_UNITS_MINS;
}

int set_apm_get_power_status(apm_get_power_status_t t)
{
	apm_get_power_status = t;

	return 0;
}



static int h4000_batt_probe(struct platform_device *dev)
{
	init_timer(&timer_bat);
	timer_bat.function = h4000_battery;
	timer_bat.data = (unsigned long)NULL;

	mod_timer(&timer_bat, jiffies + (1000 * HZ) / 1000);

#ifdef CONFIG_PM
	set_apm_get_power_status(h4000_apm_get_power_status);
#endif

	return 0;
}

#ifdef CONFIG_PM
static int h4000_batt_resume(struct platform_device *pdev)
{
	return 0;
}
#else
#define h4000_batt_resume NULL
#endif

static struct platform_driver h4000_batt_driver = {
	.driver		= {
		.name	= "h4000_batt",
	},
	.probe		= h4000_batt_probe,
#ifdef CONFIG_PM
	.suspend	= NULL,
	.resume		= h4000_batt_resume,
#endif
};

static int __init h4000_batt_init(void)
{
	if (!machine_is_h4000())
		return -ENODEV;

	return platform_driver_register(&h4000_batt_driver);
}

static void __exit h4000_batt_exit(void)
{
	del_timer_sync(&timer_bat);
	platform_driver_unregister(&h4000_batt_driver);
}

module_init(h4000_batt_init)
module_exit(h4000_batt_exit)

MODULE_AUTHOR("Paul Sokolovsky");
MODULE_DESCRIPTION("Battery driver for the iPAQ h4xxx");
MODULE_LICENSE("GPL");
