/*
 * Copyright (C) 2003 Joshua Wise
 * Copyright (c) 2002,2003 SHARP Corporation
 * Copyright (C) 2005 Pawel Kolodziejski
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * HAL code based on h5400_asic_io.c, which is
 *  Copyright (C) 2003 Compaq Computer Corporation.
 *
 * Author:  Joshua Wise <joshua at joshuawise.com>
 *          June 2003
 *
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/device.h>
#include <linux/sysctl.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/soc/asic3_base.h>
#include <linux/platform_device.h>

#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/arch/hardware.h>
#include <asm/irq.h>
#include <asm/mach/irq.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch-pxa/h4000-gpio.h>
#include <asm/arch-pxa/h4000-asic.h>
#include <asm/hardware/ipaq-asic3.h>

#define SAMPLE_TIMEOUT 20	/* sample every 20ms */

extern struct platform_device h4000_asic3;
#define asic3 &h4000_asic3.dev

static struct timer_list timer_pen;
static struct input_dev *idev;

static spinlock_t ts_lock;
static int irq_disable;
static int touch_pressed;

static void report_touchpanel(int x, int y, int pressure)
{
	input_report_key(idev, BTN_TOUCH, pressure != 0);
	input_report_abs(idev, ABS_PRESSURE, pressure);
	input_report_abs(idev, ABS_X, x);
	input_report_abs(idev, ABS_Y, y);
	input_sync(idev);
}

#define CTRL_START  0x80
#define CTRL_YPOS   0x10
#define CTRL_Z1POS  0x30
#define CTRL_Z2POS  0x40
#define CTRL_XPOS   0x50
#define CTRL_TEMP0  0x04
#define CTRL_TEMP1  0x74
#define CTRL_VBAT   0x24
#define CTRL_AUX_N  0x64
#define CTRL_PD0    0x01
#define CTRL_PD1    0x02

#define SSSR_TNF_MSK    (1u << 2)
#define SSSR_RNE_MSK    (1u << 3)

unsigned long h4000_spi_putget(ulong data)
{
	unsigned long ret, flags;
	int timeout;

	spin_lock_irqsave(&ts_lock, flags);

	SSDR = data;

	timeout = 100000;
	while ((SSSR & SSSR_TNF_MSK) != SSSR_TNF_MSK && --timeout);
	if (timeout == 0) {
	    printk("%s: warning: timeout while waiting for SSSR_TNF_MSK\n", __FUNCTION__);
	};

	udelay(1);

	timeout = 100000;
	while ((SSSR & SSSR_RNE_MSK) != SSSR_RNE_MSK && --timeout);
	if (timeout == 0) {
	    printk("%s: warning: timeout while waiting for SSSR_RNE_MSK\n", __FUNCTION__);
	};

	ret = (SSDR);
	spin_unlock_irqrestore(&ts_lock, flags);
	return ret;
}

#define ADSCTRL_ADR_SH          4       // Address setting

typedef struct ts_pos_s {
	unsigned long xd;
	unsigned long yd;
} ts_pos_t;

void read_xydata(ts_pos_t *tp)
{
#define	abscmpmin(x,y,d) ( ((int)((x) - (y)) < (int)(d)) && ((int)((y) - (x)) < (int)(d)) )
	unsigned long cmd;
	unsigned int t, x, y, z[2];
	unsigned long pressure;
	int i,j,k;
	int d = 8, c = 10;
	int err = 0;

	for(i = j = k = 0, x = y = 0;; i = 1) {
		/* Pressure */
		cmd = CTRL_PD0 | CTRL_PD1 | CTRL_START | CTRL_Z1POS;
		t = h4000_spi_putget(cmd);
		z[i] = h4000_spi_putget(cmd);
 
		if (i)
		    break;

		/* X-axis */
		cmd = CTRL_PD0 | CTRL_PD1 | CTRL_START | CTRL_XPOS;
		x = h4000_spi_putget(cmd);
		for(j = 0; !err; j++) {
			t = x;
			x = h4000_spi_putget(cmd);
			if (abscmpmin(t, x, d))
				break;
			if (j > c) {
				err = 1;
				//printk("ts: x(%d,%d,%d)\n", t, x, t - x);
			}
		}

		/* Y-axis */
		cmd = CTRL_PD0 | CTRL_PD1 | CTRL_START | CTRL_YPOS;
		y = h4000_spi_putget(cmd);
		for (k = 0; !err; k++) {
			t = y;
			y = h4000_spi_putget(cmd);
			if (abscmpmin(t ,y , d))
				break;
			if (k > c) {
				err = 1;
				//printk("ts: y(%d,%d,%d)\n", t, y, t - y);
			}
		}
	}
	pressure = 1;
	for (i = 0; i < 2; i++) {
		if (!z[i])
			pressure = 0;
	}
	if (pressure) {
		for (i = 0; i < 2; i++){
			if (z[i] < 10)
				err = 1;
		}
		if (x >= 4095)
			err = 1;
	}

	cmd &= ~(CTRL_PD0 | CTRL_PD1);
	t = h4000_spi_putget(cmd);

	if (err == 0 && pressure != 0) {
		//printk("ts: pxyp=%d(%d/%d,%d/%d)%d\n", z[0], x, j, y, k, z[1]);
	} else {
		//printk("pxype=%d,%d,%d,%d\n", z[0], x, y, z[1]);
		x = 0; y = 0;
	}
	tp->xd = x;
	tp->yd = y;
}

static irqreturn_t h4000_stylus(int irq, void* data)
{
	ts_pos_t ts_pos;

	if (irq == IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N) && irq_disable == 0) {
		//printk("IRQ\n");
		irq_disable = 1;
		disable_irq(IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N));
	}

	read_xydata(&ts_pos);

	if (ts_pos.xd == 0 || ts_pos.yd == 0) {
		report_touchpanel(0, 0, 0);
		//printk("touch released\n");
		if (irq_disable == 1) {
			enable_irq(IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N));
			irq_disable = 0;
		}
		return IRQ_HANDLED;
	}

	//printk("%04d %04d\n", (int)ts_pos.xd, (int)ts_pos.yd);
	//printk("touch pressed\n");
	report_touchpanel(ts_pos.xd, ts_pos.yd, 1);

	mod_timer(&timer_pen, jiffies + (SAMPLE_TIMEOUT * HZ) / 1000);

	//printk("callback\n");
	return IRQ_HANDLED;
};

static void h4000_ts_timer(unsigned long nr)
{
	h4000_stylus(0, NULL);
}

void h4000_ts_init_chip(void)
{
	/* now to set up GPIOs... */
	GPDR(GPIO23_SCLK) |=  GPIO_bit(GPIO23_SCLK);
	GPDR(GPIO24_SFRM) |=  GPIO_bit(GPIO24_SFRM);
	GPDR(GPIO25_STXD) |=  GPIO_bit(GPIO25_STXD);
	GPDR(GPIO26_SRXD) &= ~GPIO_bit(GPIO26_SRXD);
	pxa_gpio_mode(GPIO23_SCLK_MD);
	pxa_gpio_mode(GPIO24_SFRM_MD);
	pxa_gpio_mode(GPIO25_STXD_MD);
	pxa_gpio_mode(GPIO26_SRXD_MD);

	SSCR0 = 0;
	SSCR0 |= 0xB; /* 12 bits */
	SSCR0 |= SSCR0_National;
	SSCR0 |= 0x1100; /* 100 mhz */

	SSCR1 = 0;

	SSCR0 |= SSCR0_SSE;

	h4000_spi_putget(CTRL_YPOS | CTRL_START);
	mdelay(5);
	h4000_spi_putget(CTRL_Z1POS | CTRL_START);
	mdelay(5);
	h4000_spi_putget(CTRL_Z2POS | CTRL_START);
	mdelay(5);
	h4000_spi_putget(CTRL_XPOS | CTRL_START);
	mdelay(5);
}

int h4000_spi_init(void)
{
	if (!machine_is_h4000()) {
		return -ENODEV;
	}

	h4000_ts_init_chip();

	init_timer(&timer_pen);
	timer_pen.function = h4000_ts_timer;
	timer_pen.data = (unsigned long)NULL;

        idev = input_allocate_device();
        if (!idev)
                return -ENOMEM;

	idev->name = "ads7876";
	idev->phys = "touchscreen/ads7876";

	set_bit(EV_ABS, idev->evbit);
	set_bit(EV_KEY, idev->evbit);
	set_bit(ABS_X, idev->absbit);
	set_bit(ABS_Y, idev->absbit);
	set_bit(ABS_PRESSURE, idev->absbit);
	set_bit(BTN_TOUCH, idev->keybit);
	idev->absmin[ABS_PRESSURE] = 0;
	idev->absmax[ABS_PRESSURE] = 1;
	idev->absmin[ABS_X] = 190;
	idev->absmax[ABS_X] = 1860;
	idev->absmin[ABS_Y] = 150;
	idev->absmax[ABS_Y] = 1880;

	input_register_device(idev);

	spin_lock_init(&ts_lock);

	touch_pressed = 0;
	irq_disable = 0;
	set_irq_type(IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N), IRQT_FALLING);
	request_irq(IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N), h4000_stylus, SA_SAMPLE_RANDOM, "stylus", NULL);

	return 0;
}

static int h4000_ts_probe(struct platform_device *dev)
{
	h4000_spi_init();

	return 0;
}

#ifdef CONFIG_PM
static int h4000_ts_resume(struct platform_device *dev)
{
	h4000_ts_init_chip();

	return 0;
}
#else
#define h4000_ts_resume NULL
#endif

static struct platform_driver h4000_ts_driver = {
	.driver		= {
		.name	= "h4000_ts",
	},
	.probe		= h4000_ts_probe,
#ifdef CONFIG_PM
	.suspend	= NULL,
	.resume		= h4000_ts_resume,
#endif
};

static int __init h4000_ts_init(void)
{
	if (!machine_is_h4000())
		return -ENODEV;

	return platform_driver_register(&h4000_ts_driver);
}

static void __exit h4000_ts_exit(void)
{
	del_timer_sync(&timer_pen);

	free_irq(IRQ_GPIO(GPIO_NR_H4000_PEN_IRQ_N), NULL);

	SSCR0 &= ~SSCR0_SSE;

	input_unregister_device(idev);

	platform_driver_unregister(&h4000_ts_driver);
}

module_init(h4000_ts_init)
module_exit(h4000_ts_exit)

EXPORT_SYMBOL(h4000_spi_putget);

MODULE_AUTHOR("Joshua Wise, Pawel Kolodziejski");
MODULE_DESCRIPTION("Touchscreen support for the iPAQ h4xxx");
MODULE_LICENSE("GPL");
