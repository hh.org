/*
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * History:
 *
 * 2004-03-01   Eddi De Pieri      Adapted for h4000 using h3900_lcd.c 
 * 2004         Shawn Anderson     Lcd hacking on h4000
 * see h3900_lcd.c for more history.
 *
 */

#include <linux/types.h>
#include <asm/arch/hardware.h>  /* for pxa-regs.h (__REG) */
#include <linux/platform_device.h>
#include <asm/arch/pxa-regs.h>  /* LCCR[0,1,2,3]* */
#include <asm/arch/bitfield.h>  /* for pxa-regs.h (Fld, etc) */
#include <asm/arch/pxafb.h>     /* pxafb_mach_info, set_pxa_fb_info */
#include <asm/mach-types.h>     /* machine_is_h4000 */
#include <linux/lcd.h>          /* lcd_device */
#include <linux/backlight.h>    /* backlight_device */
#include <linux/fb.h>
#include <linux/err.h>
#include <linux/delay.h>

#include <asm/arch/h4000-gpio.h>
#include <asm/arch/h4000-asic.h>
#include <asm/hardware/ipaq-asic3.h>
#include <linux/soc/asic3_base.h>

extern struct platform_device h4000_asic3;

static int h4000_lcd_get_power(struct lcd_device *lm)
{
	/* Get the LCD panel power status (0: full on, 1..3: controller
	 * power on, flat panel power off, 4: full off) */

	if (asic3_get_gpio_status_b(&h4000_asic3.dev) & GPIOB_LCD_PCI) {
		if (asic3_get_gpio_out_b(&h4000_asic3.dev) & GPIOB_LCD_ON)
			return 0;
		else
			return 2;
	} else
		return 4;
}

static int h4000_lcd_set_power(struct lcd_device *lm, int power)
{
	 /* Enable or disable power to the LCD (0: on; 4: off) */

	if ( power < 1 ) {
		asic3_set_gpio_out_c(&h4000_asic3.dev,
				GPIOC_LCD_3V3_ON, GPIOC_LCD_3V3_ON);
		mdelay(30);
		asic3_set_gpio_out_c(&h4000_asic3.dev, 
				GPIOC_LCD_N3V_EN, GPIOC_LCD_N3V_EN);
		mdelay(5);
		asic3_set_gpio_out_b(&h4000_asic3.dev,
				GPIOB_LCD_ON, GPIOB_LCD_ON);
		mdelay(50);
		asic3_set_gpio_out_c(&h4000_asic3.dev,
				GPIOC_LCD_5V_EN, GPIOC_LCD_5V_EN);
		mdelay(5);
	} else {
		mdelay(5);
		asic3_set_gpio_out_c(&h4000_asic3.dev, GPIOC_LCD_5V_EN, 0);
		mdelay(50);
		asic3_set_gpio_out_b(&h4000_asic3.dev, GPIOB_LCD_ON, 0);
		mdelay(5);
		asic3_set_gpio_out_c(&h4000_asic3.dev, GPIOC_LCD_N3V_EN,0);
		mdelay(100);
		asic3_set_gpio_out_c(&h4000_asic3.dev, GPIOC_LCD_3V3_ON,0);
	}

	if ( power < 4 ) {
		mdelay(17);     // Wait one from before turning on
		asic3_set_gpio_out_b(&h4000_asic3.dev,
				GPIOB_LCD_PCI, GPIOB_LCD_PCI);
	} else {
		asic3_set_gpio_out_b(&h4000_asic3.dev, GPIOB_LCD_PCI, 0);
		mdelay(30);     // Wait before turning off
	}
	
	return 0;
}

static int h4000_lcd_get_contrast(struct lcd_device *ld)
{
	/* Get the current contrast setting (0-max_contrast) */
//DBG*/	printk("%s: not implemented yet\n", __FUNCTION__);
	return 0;
}

static int h4000_lcd_set_contrast(struct lcd_device *ld, int contrast)
{
	/* Set LCD panel contrast */
//DBG*/	printk("%s: not implemented yet\n", __FUNCTION__);
	return 0;
}

static struct lcd_properties h4000_lcd_properties =
{
	.owner          = THIS_MODULE,
	.get_power      = h4000_lcd_get_power,
	.set_power      = h4000_lcd_set_power,
	.max_contrast   = 7,
	.get_contrast   = h4000_lcd_get_contrast,
	.set_contrast   = h4000_lcd_set_contrast,
};

static struct lcd_device *h4000_lcd_dev;

static int h4000_lcd_probe(struct platform_device *pdev)
{
	h4000_lcd_dev = lcd_device_register("pxa2xx-fb", NULL,
			&h4000_lcd_properties);
	if (IS_ERR(h4000_lcd_dev)) {
		printk("h4000-lcd: Error registering device\n");
		return -1;
	}

	return 0;
}

static int h4000_lcd_remove(struct platform_device *pdev)
{
	h4000_lcd_set_power(h4000_lcd_dev, 4);
	lcd_device_unregister(h4000_lcd_dev);

	return 0;
}

static int h4000_lcd_suspend(struct platform_device *pdev, pm_message_t state)
{
	h4000_lcd_set_power(h4000_lcd_dev, 4);
	return 0;
}

static int h4000_lcd_resume(struct platform_device *pdev)
{
	h4000_lcd_set_power(h4000_lcd_dev, 0);
	return 0;
}

static struct platform_driver h4000_lcd_driver = {
	.driver	  = {
	    .name = "h4000_lcd",
	},
	.probe    = h4000_lcd_probe,
	.remove   = h4000_lcd_remove,
	.suspend  = h4000_lcd_suspend,
	.resume   = h4000_lcd_resume,
};

static int h4000_lcd_init(void)
{
	if (!machine_is_h4000())
		return -ENODEV;

	return platform_driver_register(&h4000_lcd_driver);
}

static void h4000_lcd_exit(void)
{
	lcd_device_unregister(h4000_lcd_dev);
	platform_driver_unregister(&h4000_lcd_driver);
}

module_init(h4000_lcd_init);
module_exit(h4000_lcd_exit);

MODULE_AUTHOR("h4000 port team h4100-port@handhelds.org");
MODULE_DESCRIPTION("Framebuffer driver for iPAQ H4000");
MODULE_LICENSE("GPL");

/* vim: set ts=8 tw=80 shiftwidth=8 noet: */

