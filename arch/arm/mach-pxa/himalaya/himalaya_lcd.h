#define HIMALAYA_ATI_CHIP_BASE 0xF5010000
#define HIMALAYA_ATI_VIDMEM_BASE 0xF4000000

#define HIMALAYA_ATI_REG_CRTC_FRAME_VPOS     	0x4B4
#define HIMALAYA_ATI_REG_GRAPHIC_H_DISP      	0x42C
#define HIMALAYA_ATI_REG_CRTC_FRAME          	0x4B0
#define HIMALAYA_ATI_REG_GENLCD_CNTL3        	0x524
#define HIMALAYA_ATI_REG_CRTC_SS             	0x48C
#define HIMALAYA_ATI_REG_CRTC_PS1_ACTIVE     	0x4F0
#define HIMALAYA_ATI_REG_GENLCD_CNTL2        	0x4D0
#define HIMALAYA_ATI_REG_PLL_REF_FB_DIV      	0x84
#define HIMALAYA_ATI_REG_CRTC_LS             	0x490
#define HIMALAYA_ATI_REG_CRTC_REV            	0x494
#define HIMALAYA_ATI_REG_CRTC_GOE            	0x4AC
#define HIMALAYA_ATI_REG_GRAPHIC_V_DISP      	0x430
#define HIMALAYA_ATI_REG_GENLCD_CNTL1        	0x4CC
#define HIMALAYA_ATI_REG_ACTIVE_H_DISP       	0x424
#define HIMALAYA_ATI_REG_CLK_PIN_CNTL        	0x80
#define HIMALAYA_ATI_REG_GRAPHIC_PITCH       	0x41C
#define HIMALAYA_ATI_REG_PLL_CNTL            	0x88
#define HIMALAYA_ATI_REG_GRAPHIC_OFFSET      	0x418
#define HIMALAYA_ATI_REG_LCDD_CNTL1          	0x4C4
#define HIMALAYA_ATI_REG_SCLK_CNTL           	0x8C
#define HIMALAYA_ATI_REG_LCDD_CNTL2          	0x4C8
#define HIMALAYA_ATI_REG_LCD_FORMAT          	0x410
#define HIMALAYA_ATI_REG_PCLK_CNTL           	0x90
#define HIMALAYA_ATI_REG_GRAPHIC_CTRL        	0x414
#define HIMALAYA_ATI_REG_CRTC_DCLK           	0x49C
#define HIMALAYA_ATI_REG_CRTC_GCLK           	0x4A8
#define HIMALAYA_ATI_REG_MC_FB_LOCATION      	0x188
#define HIMALAYA_ATI_REG_CRTC_DEFAULT_COUNT  	0x4E0
#define HIMALAYA_ATI_REG_CRTC_GS             	0x4A0
#define HIMALAYA_ATI_REG_CRTC_VPOS_GS        	0x4A4
#define HIMALAYA_ATI_REG_LCD_BACKGROUND_COLOR	0x4E4
#define HIMALAYA_ATI_REG_CLK_TEST_CNTL       	0x94
#define HIMALAYA_ATI_REG_CRTC_TOTAL          	0x420
#define HIMALAYA_ATI_REG_PWRMGT_CNTL         	0x98
#define HIMALAYA_ATI_REG_ACTIVE_V_DISP       	0x428
#define HIMALAYA_ATI_REG_DISP_DEBUG          	0x4D4
#define HIMALAYA_ATI_REG_DISP_DEBUG2         	0x538

