/*
* Driver interface to the touchscreen on the HTC Himalaya
*
* Copyright (C) 2004 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
* Copyright (C) 2004 w4xy@xanadux.org
*
* Use consistent with the GNU GPL is permitted,
* provided that this copyright notice is
* preserved in its entirety in all copies and derived works.
*
* HAL code based on h5400_asic_io.c, which is
*  Copyright (C) 2003 Compaq Computer Corporation.
*
* Driver based on the h1900_ts.c, which is
* Copyright (C) 2003, Joshua Wise <joshua at joshuawise.com>
*
*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/config.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/input.h>

#if 0
#include <asm/arch/hardware.h>
//#include <asm/arch-sa1100/h3600.h>
//#include <asm/arch-sa1100/SA-1100.h>

#include <asm/mach/irq.h>
/*#include <asm/arch-pxa/himalaya-gpio.h>*/
#include <asm/hardware/ipaq-asic3.h>
#endif

#include <asm/irq.h>
#include <asm/arch/hardware.h>

#include <asm/mach-types.h>
#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/arch/udc.h>

#include <asm/arch/himalaya-gpio.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/arch-pxa/h3900-asic.h>
#include <linux/soc/asic3_base.h>
#include "tsc2200.h"
#include "pxa_nssp.h"
#include <asm/arch/pxa-regs.h>
#include <asm/apm.h>

static int gp = 74;
MODULE_PARM(gp, "i");
MODULE_PARM_DESC(gp,"GPIO # (while testing for ASIC3 GPIO)");

static char kp_name[] = "tsc2200_kp";
static char kp_phys[] = "touchscreen/tsc2200";

extern struct platform_device himalaya_asic3;
struct input_dev kp_input_dev;
static struct work_struct kp_workqueue;
static struct work_struct kp_readworkqueue;
static int protected;

#define SAMPLE_TIMEOUT 8	/* sample every 10ms */
#define CONVERSION_TIMEOUT 2	/* wait 1ms for a conversion */
#undef UBERDEBUGGY

#define GPIO_NR_HIMALAYA_KP_IRQ_N 8
#define HIMALAYA_KP_IRQ	IRQ_GPIO(GPIO_NR_HIMALAYA_KP_IRQ_N)

//static struct timer_list kp_timer;

typedef enum
{
	TSC_DO_NOTHING       = 0,
	TSC_PEN_DOWN_EXPECT_CONVERT,
	TSC_PEN_DOWN_CONVERT_DONE,
	TSC_PEN_UP,

} TSC_PEN_STATE;

static int tsc_state = TSC_DO_NOTHING;

static void kp_tsc2200_start_conv(void)
{
	tsc2200_write(TSC2200_CTRLREG_ADC,
//		TSC2200_CTRLREG_ADC_PSM_TSC2200 |
		TSC2200_CTRLREG_ADC_AD0 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
	printk("%s: %X.\n", __FUNCTION__, tsc2200_read(TSC2200_CTRLREG_ADC));
}

static void kp_read(struct input_dev *dev)
{
	unsigned int bytes[4];

	/* read positions. */
	bytes[0] = tsc2200_read(TSC2200_DATAREG_X);
	bytes[1] = tsc2200_read(TSC2200_DATAREG_Y);
	bytes[2] = tsc2200_read(TSC2200_DATAREG_Z1);
	bytes[3] = tsc2200_read(TSC2200_DATAREG_Z2);

	input_report_abs(dev, ABS_X, bytes[0] & 0xfff);
	input_report_abs(dev, ABS_Y, bytes[1] & 0xfff);
	input_report_abs(dev, ABS_PRESSURE, 0xfff);
//	input_report_abs(dev, ABS_PRESSURE, bytes[2] & 0xfff);
	input_sync(dev);

#if 0
	printk("%s: %03X-%03x-%03x-%03x / %i\n", __FUNCTION__, bytes[0], bytes[1], bytes[2], bytes[3], tsc2200_dav());
	printk("%s: BAT1: %03X\nBAT2: %03x\nAUX1: %03x\nAUX2: %03x\nTEMP1: %03x\nTEMP2: %03x\n", __FUNCTION__, tsc2200_read(TSC2200_DATAREG_BAT1), tsc2200_read(TSC2200_DATAREG_BAT2), tsc2200_read(TSC2200_DATAREG_AUX1), tsc2200_read(TSC2200_DATAREG_AUX2), tsc2200_read(TSC2200_DATAREG_TEMP1), tsc2200_read(TSC2200_DATAREG_TEMP2));
#endif
}

static void kp_up(struct input_dev *dev)
{
	input_report_abs(dev, ABS_PRESSURE, 0x0);
	input_sync(dev);
}

static int kp_pressed(void)
{
	return ( GPLR(GPIO_NR_HIMALAYA_KP_IRQ_N) & GPIO_bit(GPIO_NR_HIMALAYA_KP_IRQ_N) ) ? 0 : 1;
}

static irqreturn_t kp_irq(int irq, void* data, struct pt_regs *regs)
{
	printk("%s: got interrupt...\n", __FUNCTION__);
	printk("ASIC3 int status:\n  %X\n  %X\n  %X\n  %X\n",
		asic3_get_gpio_intstatus_a(&himalaya_asic3.dev),
		asic3_get_gpio_intstatus_b(&himalaya_asic3.dev),
		asic3_get_gpio_intstatus_c(&himalaya_asic3.dev),
		asic3_get_gpio_intstatus_d(&himalaya_asic3.dev));

	asic3_set_gpio_intstatus_c(&himalaya_asic3.dev, 0xfe00, 0x0);

	asic3_set_gpio_mask_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_dir_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_trigtype_c(&himalaya_asic3.dev, 0xfe00, 0xfe00);
	asic3_set_gpio_rising_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_triglevel_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_sleepmask_c(&himalaya_asic3.dev, 0xfe00, 0x0);
//	asic3_set_gpio_intstatus_c(&himalaya_asic3.dev, 0xfe00, 0x0800);
	asic3_set_gpio_alt_fn_c(&himalaya_asic3.dev, 0xfe00, 0x0);

//	disable_irq(HIMALAYA_KP_IRQ);
//	schedule_work(&kp_workqueue);

	return IRQ_HANDLED;
}

static void kp_work(void* data)
{
	struct input_dev *dev = (struct input_dev *)data;

//	printk("%i: workqueue fired...\n", jiffies);
	if ( !protected ) {
		down_interruptible(&tsc2200_sem);
		printk("protected.\n");
		protected++;
//		kp_tsc2200_initialise();
//		printk("initialised.\n");
//		tsc2200_write(TSC2200_CTRLREG_ADC, 0x8506);
		kp_tsc2200_start_conv();
	}
//	printk("0: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
	printk("%s: DAV=%i, pressed=%i.\n", __FUNCTION__, tsc2200_dav(), kp_pressed());
//	msleep(3);
	while ( !(tsc2200_read(TSC2200_CTRLREG_ADC) & 0x4000) &&  !tsc2200_dav()) {
//		printk("1: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
		msleep(1);
	}
#if 0
	while ( !tsc2200_dav() ) {
//		printk("2: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
	}
#endif
	if ( kp_pressed() ) {
//		printk("3: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
		schedule_work(&kp_workqueue);
		kp_read(dev);
	} else {
//		printk("4: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
		protected--;
		kp_up(dev);
		printk("unprotecting.\n");
		up(&tsc2200_sem);
		enable_irq(HIMALAYA_KP_IRQ);
	}
};

int kp_tsc2200_open( struct input_dev *dev )
{
	if ( !machine_is_himalaya() ) {
		printk("%s: unknown iPAQ model %s\n", __FUNCTION__, h3600_generic_name() );
		return -ENODEV;
	}
	
	printk("%s: init work queue...\n", __FUNCTION__);
//	INIT_WORK(&kp_workqueue, kp_work, dev);
//	INIT_WORK(&kp_readworkqueue, kp_readwork, dev);
	
	printk("%s: setup ASIC3...\n", __FUNCTION__);
	asic3_set_gpio_mask_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_dir_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_trigtype_c(&himalaya_asic3.dev, 0xfe00, 0xfe00);
	asic3_set_gpio_rising_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_triglevel_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	asic3_set_gpio_sleepmask_c(&himalaya_asic3.dev, 0xfe00, 0x0);
//	asic3_set_gpio_intstatus_c(&himalaya_asic3.dev, 0xfe00, 0x0800);
	asic3_set_gpio_alt_fn_c(&himalaya_asic3.dev, 0xfe00, 0x0);
	
	printk("%s: set irq...\n", __FUNCTION__);
	/* now set up the pen action GPIO */

	pxa_gpio_mode(gp | GPIO_IN);
	set_irq_type(IRQ_GPIO(gp), IRQT_BOTHEDGE);
	request_irq (IRQ_GPIO(gp), kp_irq, SA_SAMPLE_RANDOM, "himalaya_kp", (void*)&kp_input_dev);

	udelay(300);
	printk("%s: %X\n", __FUNCTION__, tsc2200_read(TSC2200_CTRLREG_ADC) );
	udelay(300);
//	enable_irq(IRQ_GPIO(gp));

	return 0;
}

void kp_tsc2200_close( struct input_dev *dev )
{
	printk("%s: closing tsc2200.\n", __FUNCTION__);
	
	disable_irq(IRQ_GPIO(gp));

	/* make sure to free the IRQ... */
	free_irq(IRQ_GPIO(gp), dev);
	
	/* now free the timer... */
//	del_timer_sync (&kp_timer);
}

static void kp_release(struct device *dev)
{
	
}

struct device kp_tsc2200_dev = {
	.parent		= &nssp_bus,
	.bus		= &nssp_bus_type,
	.bus_id		= "kp_tsc2200",
	.release	= kp_release,
};

static int kp_setup(struct input_dev *dev)
{
	memset(dev, 0x0, sizeof(*dev));

	init_input_dev(dev);

	dev->private = dev;

	dev->name = kp_name;
	dev->phys = kp_phys;
	dev->open = kp_tsc2200_open;
	dev->close = kp_tsc2200_close;

	set_bit(EV_ABS, dev->evbit);
	
	set_bit(ABS_X, dev->absbit);
	set_bit(ABS_Y, dev->absbit);
	set_bit(ABS_PRESSURE, dev->absbit);
	dev->absmin[ABS_PRESSURE] = 0;
	dev->absmax[ABS_PRESSURE] = 0xfff;
	dev->absmax[ABS_X] = 0xfff;
	dev->absmax[ABS_Y] = 0xfff;
	dev->absfuzz[ABS_X] = 2;
	dev->absfuzz[ABS_Y] = 2;
	dev->absflat[ABS_X] = 4;
	dev->absflat[ABS_Y] = 4;

	dev->id.bustype = BUS_PXA_NSSP;
	dev->id.vendor = 0xffff;
	dev->id.product = 0xffff;
	dev->id.version = 0xffff;
	
	dev->dev = &kp_tsc2200_dev;

	device_register(dev->dev);
	input_register_device(dev);

	return 0;
}

static int __init kp_init(void)
{
	kp_setup (&kp_input_dev);

	return 0;
}

static void __exit kp_exit(void)
{
//	cancel_delayed_work(&kp_workqueue);
	flush_scheduled_work();
	device_unregister(&kp_tsc2200_dev);
	input_unregister_device(&kp_input_dev);
}

module_init(kp_init)
module_exit(kp_exit)

MODULE_AUTHOR("Matthias Burghardt");
MODULE_DESCRIPTION("Touchscreen (TI TSC2200) support for the HTC Himalaya");

