/*
 * Hardware definitions for HTC Himalaya
 *
 * Copyright 2004 Xanadux.org
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Authors: w4xy@xanadux.org
 *
 * History:
 *
 * 2004-02-07	W4XY		   Initial port heavily based on h1900.c
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/bootmem.h>

#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>
#include <asm/types.h>
#include <asm/delay.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irq.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/udc.h>

#include <asm/hardware/ipaq-ops.h>
#include <asm/arch/himalaya-gpio.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/arch-pxa/himalaya_asic.h>

#include <linux/serial_core.h>

#include "../generic.h"

#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/soc/asic3_base.h>
#include <asm-arm/arch-pxa/serial.h>

struct ipaq_model_ops ipaq_model_ops;
EXPORT_SYMBOL(ipaq_model_ops);

extern struct platform_device himalaya_asic3;
struct platform_pxa_serial_funcs pxa_serial_funcs [] = {
	{}, /* No special FFUART options */
	{}, /* No special BTUART options */
	{}, /* No special STUART options */
};
/* Machine-specific code may want to replace the functions */
EXPORT_SYMBOL (pxa_serial_funcs);

static void msleep(unsigned int msec)
{
	        current->state = TASK_INTERRUPTIBLE;
		        schedule_timeout( (msec * HZ + 999) / 1000);
}

static struct ipaq_model_ops himalaya_model_ops __initdata = {
	.generic_name = "himalaya",
};

#if 1
static irqreturn_t himalaya_rs232_irq(int irq, void *dev_id)
{
	int connected = 0;

	if ((GPLR(GPIO_NR_HIMALAYA_RS232_IN) & GPIO_bit(GPIO_NR_HIMALAYA_RS232_IN)))
		connected = 1;

	if (connected) {
		printk("Himalaya: RS232 connected\n");
		GPDR(GPIO_NR_HIMALAYA_UART_POWER) |= GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
		GPSR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
		asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);
		asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);
	} else {
		printk("Himalaya: RS232 disconnected\n");
		GPDR(GPIO_NR_HIMALAYA_UART_POWER) |= GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
		GPCR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
		asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);
		asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_RS232_ON, 0);
	}
	return IRQ_HANDLED;
}

static struct irqaction himalaya_rs232_irqaction = {
        name:		"himalaya_rs232",
	handler:	himalaya_rs232_irq,
	flags:		SA_INTERRUPT
};
#endif

static void __init himalaya_init_irq( void )
{
	/* Initialize standard IRQs */
	pxa_init_irq();

#if 1
	setup_irq	(IRQ_NR_HIMALAYA_RS232_IN,	&himalaya_rs232_irqaction);
	set_irq_type	(IRQ_NR_HIMALAYA_RS232_IN,	IRQT_BOTHEDGE);
#endif
}

static void ser_ffuart_gpio_config(int mode)
{
	/* 
	 * full-function uart initialisation: set this up too.
	 *
	 * even though the bootloader (xscale.S) loads them up,
	 * we blatted them all, above!
	 */

	GPDR(GPIO_NR_HIMALAYA_UART_POWER) |= GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
	GPSR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);

	/* for good measure, power on the rs232 uart */
	asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);
	asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);

	/* okay.  from radio_extbootldr, we have to clear the
	 * gpio36 alt-gpio, set the direction to out,
	 * and set the gpio36 line high.
	 *
	 * lkcl01jan2006: the pxa_gpio_op() function is what i used
	 * to get this sequence right.  what was replaced here - 
	 * a call to pxa_gpio_mode() and a manual GADR(...) call - 
	 * was complete rubbish.
	 *
	 * using pxa_gpio_op() is _so_ much more obvious and simple.
	 * and correct.
	 */
	printk("gsm reset\n");
	pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_NR | 
			                  GPIO_MD_MASK_DIR | GPIO_MD_MASK_FN,
			                  GPIO_OUT);

	// set GPIO 36 to high.
	pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_SET, GPIO_MD_HIGH);

	/* then power on the gsm (and out direction for good measure) */
	printk("gsm reset gpio off\n");
	asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, 0);

	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, 0);
	msleep(1);

	/* then switch it off... */
	printk("gsm reset gpio on\n");
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);
	msleep(1);

	/* then switch it on again... */
	printk("gsm reset gpio off\n");
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, 0);
	msleep(1);

	/* and finally, enable the gpio thingy.  set all of them just for fun */
	//pxa_gpio_mode(GPIO34_FFRXD_MD);
	//pxa_gpio_mode(GPIO35_FFCTS_MD);
	pxa_gpio_mode(GPIO36_FFDCD_MD);
	//pxa_gpio_mode(GPIO37_FFDSR_MD);
	//pxa_gpio_mode(GPIO38_FFRI_MD);
	//pxa_gpio_mode(GPIO39_FFTXD_MD);
	//pxa_gpio_mode(GPIO40_FFDTR_MD);
	//pxa_gpio_mode(GPIO41_FFRTS_MD);
	//pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_SET, 0);
	//
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);
	asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
	asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
	msleep(1);
}

static void ser_stuart_gpio_config(int mode)
{
	/* standard uart initialisation: might as well get it over with, here... */
	/* the standard uart apparently deals with GSM commands */
	//pxa_gpio_mode(GPIO46_STRXD_MD); // STD_UART receive data
	//pxa_gpio_mode(GPIO47_STTXD_MD); // STD_UART transmit data

	GPDR(GPIO_NR_HIMALAYA_UART_POWER) |= GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
	GPSR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);

	asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);
	asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);

	asic3_set_gpio_dir_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);

	pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_NR | 
			                  GPIO_MD_MASK_DIR | GPIO_MD_MASK_FN,
			                  GPIO_OUT);

	// set GPIO36 to high
	pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_SET, GPIO_MD_HIGH);

	/* okay.  from radio_extbootldr, we have to clear the
	 * gpio36 alt-gpio, set the direction to out,
	 * and set the gpio36 line high.
	 *
	 */
	printk("gsm reset\n");
	//asic3_set_gpio_dir_a(&himalaya_asic3.dev, 1<<2, 1<<2);
	//asic3_set_gpio_out_a(&himalaya_asic3.dev, 1<<2, 1<<2);

	asic3_set_gpio_dir_a(&himalaya_asic3.dev, 1<<2, 1<<2);
	asic3_set_gpio_out_a(&himalaya_asic3.dev, 1<<2, 0);

	/* then power on the gsm */
	printk("gsm reset gpio off\n");
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, 0);
	msleep(1);

	/* then switch it off... */
	printk("gsm reset gpio on\n");
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);
	msleep(1);

	/* then switch it on again... */
	printk("gsm reset gpio off\n");
	asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, 0);
	//msleep(1);

	//asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);
	//asic3_set_gpio_dir_a(&himalaya_asic3.dev, 1<<2, 1<<2);
	//asic3_set_gpio_out_a(&himalaya_asic3.dev, 1<<2, 1<<2);

	pxa_gpio_op(GPIO36_FFDCD, GPIO_MD_MASK_NR | 
			                  GPIO_MD_MASK_DIR,
			                  GPIO_ALT_FN_1_IN);

	//asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
	//asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_RS232_ON, GPIOA_RS232_ON);

	//asic3_set_gpio_out_b(&himalaya_asic3.dev, GPIOB_GSM_RESET, GPIOB_GSM_RESET);
}

#if 0
#define H3900_ASIC3_VIRT 0xf3800000
static struct map_desc himalaya_io_desc[] __initdata = {
 /* virtual            physical           length      domain                                    */
  { 0xf5000000, 0x08000000, 0x00020000, MT_DEVICE}, /* ATI chip */
  { 0xfd000000, 0x04000000, 0x00010000, MT_DEVICE}, /* ATI chip */
  { H3900_ASIC3_VIRT,  HIMALAYA_ASIC3_PHYS,  0x02000000, MT_DEVICE}, /* static memory bank 3  CS#3 */
#if 0
  { 0xfb000000, 0x41000000, 0x01000000, MT_DEVICE}, /* SPI interface? */
#endif
}; 
#endif

#if 0
int himalaya_boardid;
EXPORT_SYMBOL(himalaya_boardid);

static void	himalaya_get_boardid(void)
{
	unsigned int *ptr = (unsigned int *) (H3900_ASIC3_VIRT + _IPAQ_ASIC3_GPIO_C_Base + _IPAQ_ASIC3_GPIO_Status );
	unsigned int gpioc = *ptr;

	himalaya_boardid=0;
	if( gpioc & GPIOC_BOARDID0 ) {
		himalaya_boardid |= 1;
	}
	if( gpioc & GPIOC_BOARDID1 ) {
		himalaya_boardid |= 2;
	}
	if( gpioc & GPIOC_BOARDID2 ) {
		himalaya_boardid |= 4;
	}
	printk("Himalaya Board ID 0x%x\n", himalaya_boardid);
}
#endif

/*
 * Common map_io initialization
 */
static void __init himalaya_map_io(void)
{
	int i;

	pxa_map_io();
#if 0
	iotable_init(himalaya_io_desc, ARRAY_SIZE(himalaya_io_desc));
	himalaya_get_boardid();	
#endif
#if 0
	PGSR0 = GPSRx_SleepValue;
	PGSR1 = GPSRy_SleepValue;
	PGSR2 = GPSRz_SleepValue;
#endif

#if 1
	GAFR0_L = 0x98000000;
	GAFR0_U = 0x494A8110;
	GAFR1_L = 0x699A8159;
	GAFR1_U = 0x0005AAAA;
	GAFR2_L = 0xA0000000;
	GAFR2_U = 0x00000002;

	/* don't do these for now because one of them turns the screen to mush */
	/* reason: the ATI chip gets reset / LCD gets disconnected:
	 * a fade-to-white means that the ati 3200 registers are set incorrectly */
	GPCR0   = 0xFF00FFFF;
	GPCR1   = 0xFFFFFFFF;
	GPCR2   = 0xFFFFFFFF;

	GPSR0   = 0x444F88EF;
	GPSR1   = 0x57BF7306;
	GPSR2   = 0x03FFE008;
	PGSR0   = 0x40DF88EF;
	PGSR1   = 0x53BF7206;
	PGSR2   = 0x03FFE000;
	GPDR0   = 0xD7E9A042;
	GPDR1   = 0xFCFFABA3;
	GPDR2   = 0x000FEFFE;
	GPSR0   = 0x444F88EF;
	GPSR1   = 0xD7BF7306;
	GPSR2   = 0x03FFE008;
	GRER0   = 0x00000000;
	GRER1   = 0x00000000;
	GRER2   = 0x00000000;
	GFER0   = 0x00000000;
	GFER1   = 0x00000000;
	GFER2   = 0x00000000;
#endif

#if 1
#if 1
	/* power up the UARTs which likely got switched off, above */
	GPDR(GPIO_NR_HIMALAYA_UART_POWER) |= GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
	GPSR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
#endif

	pxa_serial_funcs[2].configure = (void *) ser_stuart_gpio_config;
	pxa_serial_funcs[0].configure = (void *) ser_ffuart_gpio_config;
	stuart_device.dev.platform_data = &pxa_serial_funcs[2];		
	ffuart_device.dev.platform_data = &pxa_serial_funcs[0];
        
	/* guess about the STUART and FFUART being 22 power!
	GPDR(22) |= GPIO_bit(22);
	GPSR(22) = GPIO_bit(22);
	 */

	/* disable all ongoing DMA */
	for(i = 0; i < 16; i++) DCSR(i) = 7;
#endif

#if 0
	/* Add wakeup on AC plug/unplug (and resume button) */
	PWER = PWER_RTC | PWER_GPIO4 | PWER_GPIO0;
	PFER = PWER_RTC | PWER_GPIO4 | PWER_GPIO0;
	PRER =            PWER_GPIO4 | PWER_GPIO0;
	PCFR = PCFR_OPDE;
#endif

	ipaq_model_ops = himalaya_model_ops;
}

/* LCD */

static struct platform_device himalaya_lcd = {
  .name = "himalaya-lcd",
  .id = -1,
  .dev = {
    .platform_data = NULL,
  },
};


static struct platform_device *himalaya_asic3_devices[] __initdata = {
  &himalaya_lcd,
};

/*
 * the ASIC3 should really only be referenced via the asic3_base
 * module.  it contains functions something like asic3_gpio_b_out()
 * which should really be used rather than macros.
 *
 * TODO: divorce dependence on h3900 naming.
 */
static struct asic3_platform_data asic3_platform_data = {
	.gpio_a = {
		.dir		= 0xbfff,
		.init		= 0x406b, /* ou 4063 */
		.sleep_out	= 0x4001,
		.batt_fault_out	= 0x4001,
		.sleep_conf	= 0x000c, /* or 0x0008 */
		.alt_function	= 0x9800, /* Caution: need to be set to a correct value */ /* Old : 0000 */		
	},
	.gpio_b = {
#if 1
		.dir		= 0xffff,            /* All outputs */
		.init		= 0x0fb8,
		.sleep_out	= 0x8220,
		.batt_fault_out	= 0x0220,
		.sleep_conf	= 0x000c, /* or 0x000c */
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
		/*.init = GPIO3_IR_ON_N | GPIO3_RS232_ON | GPIO3_TEST_POINT_123,
		.sleep_out = 0x8220,
		.batt_fault_out = 0x0220,
		*/
#endif          
	},
	.gpio_c = {
#if 1
		.dir		= 0x0187,
		.init		= 0xfe04,            
		.sleep_out	= 0xfe00,            
		.batt_fault_out	= 0xfe00,            
		.sleep_conf	= 0x0008, 
		.alt_function	= 0x0000, /* or 0x0003 */ /* Caution: need to be set to a correct value */
#endif          
	},
	.gpio_d = {
#if 1
		.dir		= 0x10e0,
		.init		= 0x6907,            
		.sleep_mask	= 0x0000,
		.sleep_out	= 0x6927,            
		.batt_fault_out = 0x6927,  
		.sleep_conf	= 0x0008, 
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
#endif          
	},
  .child_platform_devs     = himalaya_asic3_devices,
  .num_child_platform_devs = ARRAY_SIZE(himalaya_asic3_devices),

};

static struct resource asic3_resources[] = {
	[0] = {
		.start  = HIMALAYA_ASIC3_PHYS,
		.end    = HIMALAYA_ASIC3_PHYS + 0xfffff,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_NR_HIMALAYA_ASIC3,
		.end    = IRQ_NR_HIMALAYA_ASIC3,
		.flags  = IORESOURCE_IRQ,
	},
};


/* FIXME: rename h3900_asic to something more generic, in all modules,
 * or use the same technique like ipaq_model_ops to have a specific
 * and also a generic name for the same thing
 */
struct platform_device himalaya_asic3 = {
	.name       = "asic3",
	.id     = 0,
	.num_resources  = ARRAY_SIZE(asic3_resources),
	.resource   = asic3_resources,
	.dev = {
		.platform_data = &asic3_platform_data
	},
};
EXPORT_SYMBOL(himalaya_asic3);

#if 0
static struct resource himalayafb_resources[] = {
	[0] = {
		.start	= 0x08100000,
		.end	= 0x0812ffff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IRQ_LCD,
		.end	= IRQ_LCD,
		.flags	= IORESOURCE_IRQ,
	},
};

static u64 fb_dma_mask = ~(u64)0;

static struct platform_device himalayafb_device = {
	.name		= "himalaya-fb",
	.id		= 0,
	.dev		= {
		// Previously, this was &pxa_fb_info.
		// Since we give priority to platform_data in the driver, this does not make sense.
		// So, we'll set this to NULL here, and then assign it down in set_pxa_fb_info.
 		.platform_data	= 0,
		.dma_mask	= &fb_dma_mask,
		.coherent_dma_mask = 0xffffffff,
	},
	.num_resources	= ARRAY_SIZE(himalayafb_resources),
	.resource	= himalayafb_resources,
};
#endif

static struct platform_device *devices[] __initdata = {
	&himalaya_asic3,
#if 0
	&himalayafb_device,
#endif
};

void himalaya_hereiam(void) {
//	asic3_set_gpio_out_b(&himalaya_asic3.dev, 0x8, 0xfb0);
}

static int himalaya_udc_is_connected(void) {
	printk("udc_is_connected: request returns %x\n", !(GPLR(GPIO_NR_HIMALAYA_USB_DETECT_N) & GPIO_bit(GPIO_NR_HIMALAYA_USB_DETECT_N)));
	if ((GPLR(GPIO_NR_HIMALAYA_USB_DETECT_N) & GPIO_bit(GPIO_NR_HIMALAYA_USB_DETECT_N))) {
		asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
		asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
	} else {
		asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
		asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, 0);
	}
	return (!(GPLR(GPIO_NR_HIMALAYA_USB_DETECT_N) & GPIO_bit(GPIO_NR_HIMALAYA_USB_DETECT_N)));
}

static void himalaya_udc_command(int cmd) {
	switch(cmd){
		case PXA2XX_UDC_CMD_DISCONNECT:
			printk("_udc_control: disconnect\n");
			GPDR(GPIO_NR_HIMALAYA_USB_PULLUP_N) |= GPIO_bit(GPIO_NR_HIMALAYA_USB_PULLUP_N);
			GPDR(GPIO_NR_HIMALAYA_CHARGER_EN) |= GPIO_bit(GPIO_NR_HIMALAYA_CHARGER_EN);
			GPSR(GPIO_NR_HIMALAYA_USB_PULLUP_N) = GPIO_bit(GPIO_NR_HIMALAYA_USB_PULLUP_N);
			GPCR(GPIO_NR_HIMALAYA_CHARGER_EN) = GPIO_bit(GPIO_NR_HIMALAYA_CHARGER_EN);
			asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
			asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
		break;
		case PXA2XX_UDC_CMD_CONNECT:
			printk("_udc_control: connect\n");
			GPDR(GPIO_NR_HIMALAYA_USB_PULLUP_N) |= GPIO_bit(GPIO_NR_HIMALAYA_USB_PULLUP_N);
			GPDR(GPIO_NR_HIMALAYA_CHARGER_EN) |= GPIO_bit(GPIO_NR_HIMALAYA_CHARGER_EN);
			GPSR(GPIO_NR_HIMALAYA_USB_PULLUP_N) = GPIO_bit(GPIO_NR_HIMALAYA_USB_PULLUP_N);
			msleep(100);
			GPCR(GPIO_NR_HIMALAYA_USB_PULLUP_N) = GPIO_bit(GPIO_NR_HIMALAYA_USB_PULLUP_N);
			GPSR(GPIO_NR_HIMALAYA_CHARGER_EN) = GPIO_bit(GPIO_NR_HIMALAYA_CHARGER_EN);
			asic3_set_gpio_dir_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, GPIOA_USB_ON_N);
			asic3_set_gpio_out_a(&himalaya_asic3.dev, GPIOA_USB_ON_N, 0);
		break;
		default:
			printk("_udc_control: unknown command!\n");
		break;
	}
}

static struct pxa2xx_udc_mach_info himalaya_udc_mach_info = {
	.udc_is_connected = himalaya_udc_is_connected,
	.udc_command      = himalaya_udc_command,
};

static void __init himalaya_init(void)
{
	platform_add_devices (devices, ARRAY_SIZE (devices));
	
#if 0
	himalaya_dump_asic_gpio(0x000);
	himalaya_dump_asic_gpio(0x100);
	himalaya_dump_asic_gpio(0x200);
	himalaya_dump_asic_gpio(0x300);
#endif
	
	pxa_set_udc_info(&himalaya_udc_mach_info);

}

MACHINE_START(HIMALAYA, "HTC Himalaya")
	/* MAINTAINER("Xanadux.org")*/
	.phys_io = 0x40000000, 
  .io_pg_offst = (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params = 0xa0000100,
	.map_io = himalaya_map_io,
	.init_irq = himalaya_init_irq,
  .timer = &pxa_timer,
  .init_machine = himalaya_init,
MACHINE_END

