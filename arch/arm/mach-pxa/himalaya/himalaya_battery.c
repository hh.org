/*
* Driver interface to the battery on the HTC Himalaya
*
* Use consistent with the GNU GPL is permitted,
* provided that this copyright notice is
* preserved in its entirety in all copies and derived works.
*
*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/config.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <linux/battery.h>

#include "tsc2200.h"
#include "pxa_nssp.h"

static void bat_tsc2200_start_conv_ps(void)
{
	tsc2200_write(TSC2200_CTRLREG_REF, 31);
	tsc2200_write(TSC2200_CTRLREG_ADC,
		TSC2200_CTRLREG_ADC_AD3 |
		TSC2200_CTRLREG_ADC_AD1 |
		TSC2200_CTRLREG_ADC_AD0 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
}

static void bat_tsc2200_start_conv_temp(void)
{
	tsc2200_write(TSC2200_CTRLREG_ADC,
		TSC2200_CTRLREG_ADC_AD3 |
		TSC2200_CTRLREG_ADC_AD1 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
}

static int himalaya_battery_get_status(struct battery *bat)
{
	printk("%s: in.\n", __FUNCTION__);
	
	if ( GPLR(63) & GPIO_bit(63) ) {
		return BATTERY_STATUS_CHARGING;
	} else {
		return BATTERY_STATUS_NOT_CHARGING;
	}
}

static int himalaya_battery_get_voltage(struct battery *bat)
{
	int retval;
	
	printk("%s: in.\n", __FUNCTION__);

	down_interruptible(&tsc2200_sem);
	bat_tsc2200_start_conv_ps();

	while ( !(tsc2200_read(TSC2200_CTRLREG_ADC) & 0x4000) &&
			 !tsc2200_dav()) {
		printk("1: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
		msleep(1);
	}
	
	retval = tsc2200_read(TSC2200_DATAREG_BAT1);
	printk("%s: BAT1: %03X\nBAT2: %03x\nAUX1: %03x\nAUX2: %03x\nTEMP1: %03x\nTEMP2: %03x\n", __FUNCTION__, tsc2200_read(TSC2200_DATAREG_BAT1), tsc2200_read(TSC2200_DATAREG_BAT2), tsc2200_read(TSC2200_DATAREG_AUX1), tsc2200_read(TSC2200_DATAREG_AUX2), tsc2200_read(TSC2200_DATAREG_TEMP1), tsc2200_read(TSC2200_DATAREG_TEMP2));
	
	up(&tsc2200_sem);
	return retval;
}

static int himalaya_battery_get_temp(struct battery *bat)
{
	int retval;
	
	printk("%s: in.\n", __FUNCTION__);

	down_interruptible(&tsc2200_sem);
	bat_tsc2200_start_conv_temp();

	while ( !(tsc2200_read(TSC2200_CTRLREG_ADC) & 0x4000) &&
			 !tsc2200_dav()) {
		printk("1: %X\n", tsc2200_read(TSC2200_CTRLREG_ADC));
		msleep(1);
	}
	
	retval = tsc2200_read(TSC2200_DATAREG_TEMP1);
	
	up(&tsc2200_sem);
	return retval;
}

struct battery battery_dev = {
	.name           = "main battery",
	.id		= "battery0",
	.get_voltage	= himalaya_battery_get_voltage,
	.get_status	= himalaya_battery_get_status,
	.get_temp	= himalaya_battery_get_temp,
};

static struct device bat_tsc2200_dev = {
	.parent		= &nssp_bus,
	.bus		= &nssp_bus_type,
	.bus_id		= "bat_tsc2200",
};

static int bat_setup(void)
{
	device_register(&bat_tsc2200_dev);
	battery_class_register(&battery_dev);
	
	return 0;
}

static int __init bat_init(void)
{
	bat_setup();

	return 0;
}

static void __exit bat_exit(void)
{
	battery_class_unregister(&battery_dev);
	device_unregister(&bat_tsc2200_dev);
}

module_init(bat_init)
module_exit(bat_exit)

MODULE_AUTHOR("Matthias Burghardt");
MODULE_DESCRIPTION("Battery (TI TSC2200) support for the HTC Himalaya");

