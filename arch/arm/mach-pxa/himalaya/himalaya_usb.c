/*
 * USB initialisation for Himalaya, the HTC PocketPC.
 *
 * License: GPL
 *
 * Author: Luke Kenneth Casson Leighton, Copyright(C) 2004
 *
 * Copyright(C) 2004, Luke Kenneth Casson Leighton.
 *
 * History:
 *
 * 2004-02-19	Luke Kenneth Casson Leighton	created.
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/pm.h>

#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach/arch.h>
#include <asm/mach-types.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/himalaya-gpio.h>

#include <linux/soc/asic3_base.h>

extern struct platform_device himalaya_asic3;

static void himalaya_init_usb (void)
{
	/*
	 * thought to be a power-up sequence for the output.
   	GPSR(GPIO_NR_HIMALAYA_UART_POWER) = GPIO_bit(GPIO_NR_HIMALAYA_UART_POWER);
	asic3_set_gpio_dir_b(&himalaya_asic3.dev, 2, 2);
	asic3_set_gpio_out_b(&himalaya_asic3.dev, 2, 2);
	 */
}


int __init
himalaya_usb_init (void)
{
	int rc = 0;

	if (! machine_is_himalaya() )
		return -ENODEV;

	himalaya_init_usb();

	return rc;
}

void __exit
himalaya_usb_exit (void)
{
	/* power down usb? */
}

module_init (himalaya_usb_init);
module_exit (himalaya_usb_exit);
