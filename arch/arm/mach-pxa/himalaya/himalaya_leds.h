enum led_color {
	BLUE_LED,
	GREEN_LED,
	YELLOW_LED,
	RED_LED
};

void himalaya_set_led(enum led_color color, int duty_time, int cycle_time);

