/*
 * LED interface for Himalaya, the HTC PocketPC.
 *
 * License: GPL
 *
 * Author: Luke Kenneth Casson Leighton, Copyright(C) 2004
 *
 * Copyright(C) 2004, Luke Kenneth Casson Leighton.
 *
 * History:
 *
 * 2004-02-19	Luke Kenneth Casson Leighton	created.
 *
 */
 
#include <linux/module.h>
#include <linux/delay.h>

#include <asm/hardware/ipaq-asic3.h>
#include <asm/mach-types.h>
#include <asm/arch/ipaq.h>

//#include "himalaya_leds.h"

extern struct platform_device himalaya_asic3;
extern struct ipaq_model_ops ipaq_model_ops;

#define CLOCK_CDEX_LED0      (1 << 6)  /* clock for led 0? */
#define CLOCK_CDEX_LED1      (1 << 7)  /* clock for led 1? */

void himalaya_set_led (enum led_color color, int duty_time, int cycle_time)
{
	if (color == RED_LED)
	{		
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 0) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 0) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 0) = 0;
		udelay(1);
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 0) = duty_time;
		
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 1) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 1) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 1) = 0;
	};
	
	if (color == GREEN_LED)
	{
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 1) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 1) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 1) = 0;
		udelay(1);
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 1) = duty_time;
		
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 0) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 0) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 0) = 0;	
	};
	
	if (color == YELLOW_LED)
	{
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 1) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 1) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 1) = 0;
				
		IPAQ_ASIC3_LED_TimeBase(H3900_ASIC3_VIRT, 0) = 0x6 | LEDTBS_BLINK;
		IPAQ_ASIC3_LED_PeriodTime(H3900_ASIC3_VIRT, 0) = cycle_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 0) = 0;
		
		udelay(1);
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 1) = duty_time;
		IPAQ_ASIC3_LED_DutyTime(H3900_ASIC3_VIRT, 0) = duty_time;
	};
	
	if (color == BLUE_LED)
		printk("%s: Blue LED not yet enabled.\n", __FUNCTION__);
}
EXPORT_SYMBOL(himalaya_set_led);

static int himalaya_led_init (void)
{
	int retval = 0;

	if (! machine_is_himalaya() )
		return -ENODEV;

	ipaq_model_ops.set_led = himalaya_set_led;
	return retval;
}

static void himalaya_led_exit (void)
{
	ipaq_model_ops.set_led = NULL;
}

module_init (himalaya_led_init);
module_exit (himalaya_led_exit);

