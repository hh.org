/*
 *  linux/arch/arm/mach-pxa/asus620_lcd.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  Copyright (c) 2003 Adam Turowski
 *  Copyright(C) 2004 Vitaliy Sardyko
 *
 *  2003-12-03: Adam Turowski
 *		initial code.
 *  2004-11-07: Vitaliy Sardyko
 *		updated to 2.6.7
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/notifier.h>
#include <linux/lcd.h>
#include <linux/backlight.h>

#include <asm/mach-types.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/asus620-init.h>
#include <asm/arch/asus620-gpio.h>
#include <linux/fb.h>
#include <asm/arch/pxafb.h>

#define DEBUG 1



#if DEBUG
#  define DPRINTK(fmt, args...)	printk("%s: " fmt, __FUNCTION__ , ## args)
#else
#  define DPRINTK(fmt, args...)
#endif


static int asus620_lcd_set_power (struct lcd_device *lm, int setp)
{
	return 0;
}

static int asus620_lcd_get_power (struct lcd_device *lm)
{
	return 0;
} 
struct lcd_properties asus620_lcd_properties =
{
	.owner = THIS_MODULE,
	.set_power = asus620_lcd_set_power,
	.get_power = asus620_lcd_get_power,
}; 
/* asus620: LCCR0: 0x003000f9	-- ENB | LDM | SFM | IUM | EFM | PAS | QDM | BM | OUM
	 LCCR1: 0x15150cef	-- PPL=0xef, HSW=0x4, ELW=0xA, BLW=0x1A
         LCCR2: 0x06060d3f	-- BFW=0x0, EFW=0x30, VSW=0xC, LPP=0x13F
         LCCR3: 0x04700007      -- PCD=0x8, ACB=0x0, API=0x0, VSP, HSP, PCP, BPP=0x4 */

static struct pxafb_mach_info asus620_fb_info  = {
	.pixclock =	171521,
	.bpp =		16,
	.xres =		240,
	.yres =		320,
	.hsync_len =	4,
	.vsync_len =	4,
	.left_margin =	22,
	.upper_margin =	6,
	.right_margin =	22,
	.lower_margin =	6,
	.sync =		0,
	.lccr0 =	(LCCR0_LDM | LCCR0_SFM | LCCR0_IUM | LCCR0_EFM | LCCR0_PAS | LCCR0_QDM | LCCR0_BM | LCCR0_OUM),
	.lccr3 =	(LCCR3_HorSnchL | LCCR3_VrtSnchL | LCCR3_16BPP | LCCR3_PCP)
};

static int asus620_backlight_set_power (struct backlight_device *bm, int on)
{
	return 0;
}

static int asus620_backlight_get_power (struct backlight_device *bm)
{
	return 0;
}

static struct backlight_properties asus620_backlight_properties =
{
	.owner         = THIS_MODULE,
	.set_power     = asus620_backlight_set_power,
	.get_power     = asus620_backlight_get_power,
};

static struct lcd_device *pxafb_lcd_device;
static struct backlight_device *pxafb_backlight_device;

int asus620_lcd_init (void)
{
	int ret = 0;

	if (!machine_is_a620 ())
		return -ENODEV;

	set_pxa_fb_info (&asus620_fb_info);

	pxafb_lcd_device = lcd_device_register("pxafb", NULL, &asus620_lcd_properties);
	if (IS_ERR (pxafb_lcd_device)) {
		DPRINTK(" LCD device doesn't registered, booting failed !\n");
		return ret;
	}

	pxafb_backlight_device = backlight_device_register("pxafb", NULL, 
						           &asus620_backlight_properties);

	return ret;
}


static void asus620_lcd_exit (void)
{
	lcd_device_unregister (pxafb_lcd_device);
	backlight_device_unregister (pxafb_backlight_device);
}

module_init(asus620_lcd_init);
module_exit(asus620_lcd_exit);

MODULE_AUTHOR("Adam Turowski");
MODULE_DESCRIPTION("LCD driver for Asus 620");
MODULE_LICENSE("GPL");
