/*
 *  linux/arch/arm/mach-pxa/asus620.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  Copyright (c) 2004 Vitaliy Sardyko
 *  Copyright (c) 2003,2004 Adam Turowski
 *  Copyright (c) 2001 Cliff Brake, Accelent Systems Inc.
 *
 *
 *  2001-09-13: Cliff Brake <cbrake@accelent.com>
 *              Initial code for IDP
 *  2003-12-03: Adam Turowski
 *		code adaptation for Asus 620
 *  2004-07-23: Vitaliy Sardyko
 *		updated to 2.6.7 format,
 *		split functions between modules.
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/major.h>
#include <linux/fs.h>
#include <linux/interrupt.h>

#include <asm/setup.h>
#include <asm/memory.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/irq.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <asm/arch/udc.h>
#include <asm/arch/pxa-regs.h>
#include <../drivers/pcmcia/soc_common.h>

#include <asm/arch/asus620-init.h>
#include <asm/arch/asus620-gpio.h>

#include "../generic.h"

#define DEBUG 1

#if DEBUG
#  define DPRINTK(fmt, args...)	printk("%s: " fmt, __FUNCTION__ , ## args)
#else
#  define DPRINTK(fmt, args...)
#endif


static u64	fb_dma_mask = ~(u64)0;

static void __init asus620_init_irq(void)
{
	pxa_init_irq();
}

static int asus620_udc_is_connected(void) {
	DPRINTK("asus620_udc_is_connected\n");
	return GET_A620_GPIO(USB_DETECT);
}

static void asus620_udc_command(int cmd) {

	DPRINTK("asus620_udc_command: %d\n", cmd);

	switch(cmd){
		case PXA2XX_UDC_CMD_DISCONNECT:
			pxa_gpio_mode (GPIO_NR_A620_USBP_PULLUP | GPIO_IN);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			pxa_gpio_mode (GPIO_NR_A620_USBP_PULLUP | GPIO_OUT);
			SET_A620_GPIO(USBP_PULLUP, 1);
			break;
		default:
			DPRINTK("asus620_udc_control: unknown command!\n");
			break;
	}
}


static struct map_desc asus620_io_desc[] __initdata = {
 // virtual     physical    length      type
   {ASUS_IDE_VIRTUAL, ASUS_IDE_BASE, ASUS_IDE_SIZE, MT_DEVICE}
};

static struct resource pxafb_resources[]=
{
	[0] = {
		.start = 0x44000000,
		.end = 0x44000010,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_LCD,
		.end = IRQ_LCD,
		.flags = IORESOURCE_IRQ,
		},
};

static struct resource ad7873_resources[]=
{
	[0] = {
		.start = 0x41000000,
		.end = 0x41000100,
		.flags = IORESOURCE_MEM,
	},
};


static struct platform_device pxafb_device = {
	.name = "pxafb",
	.id = 0,
	.dev = {
		.dma_mask = &fb_dma_mask,
		.coherent_dma_mask = 0xFFFFFFFF,
		},
	.num_resources = ARRAY_SIZE(pxafb_resources),
	.resource = pxafb_resources,
};

static struct platform_device ad7873_device =
{
	.name = "pxa-ad7873",
	.id = 0,
	.num_resources = ARRAY_SIZE(ad7873_resources),
	.resource = ad7873_resources,
};



static void __init asus620_map_io(void)
{
	pxa_map_io();
	iotable_init( asus620_io_desc, ARRAY_SIZE(asus620_io_desc) );

	/* Wake up on CF/SD card insertion, Power and Record buttons,
	   USB plug/unplug */
	PWER =
		PWER_GPIO0 | PWER_GPIO2 | PWER_GPIO3 | PWER_GPIO4 |
		PWER_GPIO5 | PWER_GPIO11 | /* Buttons */
		PWER_GPIO9 | /* CF */
		PWER_GPIO10 | /* USB */
		PWER_RTC;
	PFER =
		PWER_GPIO0 | PWER_GPIO2 | PWER_GPIO3 | PWER_GPIO4 |
		PWER_GPIO5 | PWER_GPIO11 | /* Buttons */
		PWER_GPIO9 | /* CF */
		PWER_RTC;
	PRER = PWER_GPIO10; /* USB */
	PCFR = PCFR_OPDE;

	DPRINTK("asus620_map_io\n");

}

static struct pxa2xx_udc_mach_info asus620_udc_mach_info = {
	.udc_is_connected = asus620_udc_is_connected,
	.udc_command      = asus620_udc_command,
};


static struct platform_device pxa2xx_pcmcia_device = {
        .name           = "pxa2xx-pcmcia",
        .id             = 0,
};

static struct platform_device pxa2xx_udc_device =
{
	.name	=	"pxa2xx_udc",
	.id	=	0,
};

static struct platform_device *devices[] __initdata = {
	&pxa2xx_pcmcia_device,
//	&pxa2xx_udc_device,
	&pxafb_device,
	&ad7873_device
};

static void __init asus620_init(void)
{
	DPRINTK("asus620_init()\n");

	pxa_set_udc_info(&asus620_udc_mach_info);
	platform_add_devices(devices, ARRAY_SIZE(devices));
}

MACHINE_START(A620, "Asus 620")
	/* Maintainer: Adam Turowski,Vitaliy Sardyko */
	.phys_ram	= 0xa0000000,
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
	.map_io		= asus620_map_io,
	.boot_params	= 0xa0000100,
	.init_irq	= asus620_init_irq,
	.init_machine = asus620_init,
MACHINE_END

