/*
 * linux/arch/arm/mach-pxa/htcbeetles/htcbeetles.c
 *
 *  Support for the Intel XScale based Palm PDAs. Only the LifeDrive is
 *  supported at the moment.
 *
 *  Author: Alex Osborne <bobofdoom@gmail.com>
 *
 *  USB stubs based on aximx30.c (Michael Opdenacker)
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fb.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/udc.h>

#include <asm/arch/htcbeetles-gpio.h>
#include <asm/arch/htcbeetles-asic.h>

#include <asm/hardware/ipaq-asic3.h>
#include <linux/soc/asic3_base.h>

#include "../generic.h"

static struct pxafb_mach_info htcbeetles_lcd __initdata = {
	.pixclock		= 480769, // LCCR4 bit is set!
	.xres			= 240,
	.yres			= 240,
	.bpp			= 16,
	.hsync_len		= 4,
	.vsync_len		= 2,
	.left_margin		= 12,
	.right_margin		= 8,
	.upper_margin		= 3,
	.lower_margin		= 3,

//	.sync			= FB_SYNC_HOR_LOW_ACT|FB_SYNC_VERT_LOW_ACT,

	/* fixme: this is a hack, use constants instead. */
	.lccr0			= 0x042000b1,
	.lccr3			= 0x04700019,

};

static struct platform_device htcbeetles_udc       = { .name = "htcbeetles_udc", };

static struct platform_device *htcbeetles_asic3_devices[] __initdata = {
//	&htcbeetles_lcd,
	&htcbeetles_udc,
};

static struct asic3_platform_data htcbeetles_asic3_platform_data = {

   /*
    * These registers are configured as they are on Wince.
    */
        .gpio_a = {
		.dir            = 0xbfff, 
		.init           = 0xc0a5,
		.sleep_out      = 0x0000,
		.batt_fault_out = 0x0000,
		.alt_function   = 0x6000, //
		.sleep_conf     = 0x000c,
        },
        .gpio_b = {
		.dir            = 0xe008,
		.init           = 0xd347,
		.sleep_out      = 0x0000,
		.batt_fault_out = 0x0000,
		.alt_function   = 0x0000, //
                .sleep_conf     = 0x000c,
        },
        .gpio_c = {
                .dir            = 0xfff7,
                .init           = 0xb640,
                .sleep_out      = 0x0000,
                .batt_fault_out = 0x0000,
		.alt_function   = 0x003b, // GPIOC_LED_RED | GPIOC_LED_GREEN | GPIOC_LED_BLUE
                .sleep_conf     = 0x000c,
        },
        .gpio_d = {
		.dir            = 0xffff,
		.init           = 0x2330,
		.sleep_out      = 0x0000,
		.batt_fault_out = 0x0000,
		.alt_function   = 0x0000, //
		.sleep_conf     = 0x0008,
        },
	.bus_shift = 1,

	.child_platform_devs     = htcbeetles_asic3_devices,
	.num_child_platform_devs = ARRAY_SIZE(htcbeetles_asic3_devices),
};

static struct resource htcbeetles_asic3_resources[] = {
	[0] = {
		.start	= HTCBEETLES_ASIC3_GPIO_PHYS,
		.end	= HTCBEETLES_ASIC3_GPIO_PHYS + IPAQ_ASIC3_MAP_SIZE,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= HTCBEETLES_IRQ(ASIC3_EXT_INT),
		.end	= HTCBEETLES_IRQ(ASIC3_EXT_INT),
		.flags	= IORESOURCE_IRQ,
	},
	[2] = {
		.start  = HTCBEETLES_ASIC3_MMC_PHYS,
		.end    = HTCBEETLES_ASIC3_MMC_PHYS + IPAQ_ASIC3_MAP_SIZE,
		.flags  = IORESOURCE_MEM,
	},
	[3] = {
		.start  = HTCBEETLES_IRQ(ASIC3_SDIO_INT_N),
		.flags  = IORESOURCE_IRQ,
	},
};

struct platform_device htcbeetles_asic3 = {
	.name           = "asic3",
	.id             = 0,
	.num_resources  = ARRAY_SIZE(htcbeetles_asic3_resources),
	.resource       = htcbeetles_asic3_resources,
	.dev = { .platform_data = &htcbeetles_asic3_platform_data, },
};
EXPORT_SYMBOL(htcbeetles_asic3);

static struct platform_device *devices[] __initdata = {
	&htcbeetles_asic3,
};

#if 0
/****************************************************************
 * USB client controller
 ****************************************************************/

static void udc_command(int cmd)
{
	switch (cmd)
	{
		case PXA2XX_UDC_CMD_DISCONNECT:
			printk(KERN_NOTICE "USB cmd disconnect\n");
//                        GPSR_BIT(GPIO_NR_HTCAPACHE_USB);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			printk(KERN_NOTICE "USB cmd connect\n");
//                        GPCR_BIT(GPIO_NR_HTCAPACHE_USB);
			break;
	}
}

static struct pxa2xx_udc_mach_info htcbeetles_udc_mach_info = {
	.udc_command      = udc_command,
};
#endif

static void __init htcbeetles_init(void)
{
	set_pxa_fb_info( &htcbeetles_lcd );

	platform_device_register(&htcbeetles_asic3);
//	platform_add_devices( devices, ARRAY_SIZE(devices) );
//	pxa_set_udc_info( &htcbeetles_udc_mach_info );
}

MACHINE_START(HTCBEETLES, "HTC Beetles")
	.phys_io	= 0x40000000,
	.io_pg_offst    = (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params	= 0xa0000100,
	.map_io 	= pxa_map_io,
	.init_irq	= pxa_init_irq,
	.timer  	= &pxa_timer,
	.init_machine	= htcbeetles_init,
MACHINE_END

