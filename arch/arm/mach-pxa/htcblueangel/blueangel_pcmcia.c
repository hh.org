/*
 * HTC Blueangel PCMCIA support
 *
 * Copyright (c) 2005 SDG Systems, LLC
 *
 * Based on code from iPAQ h2200
 *   Copyright � 2004 Koen Kooi <koen@vestingbar.nl>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/soc-old.h>

#include <asm/hardware.h>
#include <asm/irq.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/htcblueangel-gpio.h>
#include <asm/arch/htcblueangel-asic.h>
#include <asm/hardware/ipaq-asic3.h>
#include <linux/soc/asic3_base.h>

#include "../../../../drivers/pcmcia/soc_common.h"

extern struct platform_device blueangel_asic3;

/*
 * CF resources used on blueangel :
 *
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_POE_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PWE_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PIOR_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PIOW_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PCE1_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PCE2_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PREG_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PWAIT_N_MD);
 *     pxa_gpio_mode(GPIO_NR_BLUEANGEL_PIOIS16_N_MD);
 */

#if 0
#define _debug(s, args...) printk (KERN_INFO s, ##args)
#define _debug_func(s, args...) _debug ("%s: " s, __FUNCTION__, ##args)
#else
#define _debug(s, args...)
#define _debug_func(s, args...) _debug ("%s: " s, __FUNCTION__, ##args)
#endif

#if 0
static struct pcmcia_irqs blueangel_cd_irq[] = {
	{ 0, 0,  "PCMCIA CD" }	/* fill in IRQ below */
};
#endif

static int power_en=0;

static void
blueangel_cf_reset( int state )
{
	printk("reset\n");
#if 0
	_debug_func ("%d\n", state);
	SET_HX4700_GPIO( CF_RESET, state?1:0 );
#endif
}

static int
blueangel_pcmcia_hw_init( struct soc_pcmcia_socket *skt )
{
	int statusd;
	int irq;

	_debug_func ("\n");

	printk("hw_init\n");
#if 0
	/* Turn on sleep mode, I think */
	asic3_set_extcf_select(&blueangel_asic3.dev,
		ASIC3_EXTCF_CF_SLEEP | (ASIC3_EXTCF_CF0_SLEEP_MODE * 3) | ASIC3_EXTCF_CF0_BUF_EN | ASIC3_EXTCF_CF0_PWAIT_EN,
		ASIC3_EXTCF_CF_SLEEP | 0                                | 0                      | 0
	);
	mdelay(50);

	statusd = asic3_get_gpio_status_d( &blueangel_asic3.dev );
	power_en = (statusd & (1<<GPIOD_CF_CD_N)) ? 0 : 1;

	irq = hx4700_cd_irq[0].irq = asic3_irq_base( &hx4700_asic3.dev )
			+ ASIC3_GPIOD_IRQ_BASE + GPIOD_CF_CD_N;
	skt->irq = HX4700_IRQ( CF_RNB );

	if (power_en) {
		hx4700_egpio_enable( EGPIO4_CF_3V3_ON );
		set_irq_type( irq, IRQT_RISING );
	}
	else {
		hx4700_egpio_disable( EGPIO4_CF_3V3_ON );
		set_irq_type( irq, IRQT_FALLING );
	}

	pxa_gpio_mode(GPIO_NR_HX4700_POE_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PWE_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PIOR_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PIOW_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PCE1_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PCE2_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PREG_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PWAIT_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PIOIS16_N_MD | GPIO_DFLT_HIGH);
	pxa_gpio_mode(GPIO_NR_HX4700_PSKTSEL_MD);

	return soc_pcmcia_request_irqs(skt, blueangel_cd_irq, ARRAY_SIZE(blueangel_cd_irq));
#endif
	return 0;
}

/*
 * Release all resources.
 */
static void
blueangel_pcmcia_hw_shutdown( struct soc_pcmcia_socket *skt )
{
	printk("shutdown\n");
#if 0
	_debug_func ("\n");
	soc_pcmcia_free_irqs( skt, blueangel_cd_irq, ARRAY_SIZE(blueangel_cd_irq) );
#endif
}

static void
blueangel_pcmcia_socket_state( struct soc_pcmcia_socket *skt,
    struct pcmcia_state *state )
{
	int statusd;

	printk("socket_state\n");
	state->detect = 1;
	state->ready  = 1;
	state->bvd1   = 1;
	state->bvd2   = 1;
	state->wrprot = 0;
	state->vs_3v  = 1;
	state->vs_Xv  = 0;

#if 0
	if (state->detect)
	    asic3_set_gpio_status_c( &hx4700_asic3.dev, 1<<GPIOC_CIOR_N,
		1<<GPIOC_CIOR_N );
	else
	    asic3_set_gpio_status_c( &hx4700_asic3.dev, 1<<GPIOC_CIOR_N, 0 );
#endif

	_debug( "detect:%d ready:%d vcc:%d\n",
	       state->detect, state->ready, state->vs_3v );
}

static int
blueangel_pcmcia_config_socket( struct soc_pcmcia_socket *skt,
    const socket_state_t *state )
{
	int statusc;

#if 0
	statusc = asic3_get_gpio_status_c( &hx4700_asic3.dev );
#endif
	printk("config_socket\n");
	/* Silently ignore Vpp, output enable, speaker enable. */
	_debug_func( "Reset:%d Vcc:%d Astatusc=0x%x\n",
	    (state->flags & SS_RESET) ? 1 : 0, state->Vcc,
	    statusc );

#if 0
	blueangel_cf_reset( state->flags & SS_RESET );
	if (state->Vcc == 0) {
		/* Turn on sleep mode, I think.  No docs for these bits. */
		asic3_set_extcf_select(&blueangel_asic3.dev,
			ASIC3_EXTCF_CF_SLEEP | (ASIC3_EXTCF_CF0_SLEEP_MODE * 3) | ASIC3_EXTCF_CF0_BUF_EN | ASIC3_EXTCF_CF0_PWAIT_EN,
			ASIC3_EXTCF_CF_SLEEP | 0                                | 0                      | 0
		);
		blueangel_egpio_disable( EGPIO4_CF_3V3_ON );
		power_en = 0;
	}
	else if (state->Vcc == 33 || state->Vcc == 50) {
		blueangel_egpio_enable( EGPIO4_CF_3V3_ON );
		mdelay(1);
		/* Turn on CF stuff without messing with SD bit 14.  */
		asic3_set_extcf_select(&blueangel_asic3.dev,
			ASIC3_EXTCF_CF_SLEEP | (ASIC3_EXTCF_CF0_SLEEP_MODE * 3) | ASIC3_EXTCF_CF0_BUF_EN | ASIC3_EXTCF_CF0_PWAIT_EN,
			0                    | (ASIC3_EXTCF_CF0_SLEEP_MODE * 2) | ASIC3_EXTCF_CF0_BUF_EN | ASIC3_EXTCF_CF0_PWAIT_EN
		);
		power_en = 1;
	}
	else {
		printk (KERN_ERR "%s: Unsupported Vcc:%d\n",
			__FUNCTION__, state->Vcc);
	}
#endif
	return 0;
}

/*
 * Enable card status IRQs on (re-)initialisation.  This can
 * be called at initialisation, power management event, or
 * pcmcia event.
 */
static void
blueangel_pcmcia_socket_init(struct soc_pcmcia_socket *skt)
{
	printk("socket_init\n");
#if 0
	_debug_func ("\n");
	blueangel_cf_reset(0);
#endif
}

/*
 * Disable card status IRQs on suspend.
 */
static void
blueangel_pcmcia_socket_suspend( struct soc_pcmcia_socket *skt )
{
	printk("socket_suspend\n");
#if 0
	_debug_func ("\n");
	blueangel_cf_reset(1);
#endif
}

static struct pcmcia_low_level blueangel_pcmcia_ops = {
	.owner          	= THIS_MODULE,
	.nr         		= 1,
	.hw_init        	= blueangel_pcmcia_hw_init,
	.hw_shutdown		= blueangel_pcmcia_hw_shutdown,
	.socket_state		= blueangel_pcmcia_socket_state,
	.configure_socket	= blueangel_pcmcia_config_socket,
	.socket_init		= blueangel_pcmcia_socket_init,
	.socket_suspend		= blueangel_pcmcia_socket_suspend,
};

static struct platform_device blueangel_pcmcia_device = {
	.name			= "pxa2xx-pcmcia",
	.dev			= {
		.platform_data	= &blueangel_pcmcia_ops,
	}
};

static int __init
blueangel_pcmcia_init(void)
{
	_debug_func ("\n");

	return platform_device_register( &blueangel_pcmcia_device );
}

static void __exit
blueangel_pcmcia_exit(void)
{
	platform_device_unregister( &blueangel_pcmcia_device );
}

module_init(blueangel_pcmcia_init);
module_exit(blueangel_pcmcia_exit);

MODULE_AUTHOR("Todd Blumer, SDG Systems, LLC");
MODULE_DESCRIPTION("HTC Blueangel PCMCIA/CF platform-specific driver");
MODULE_LICENSE("GPL");

/* vim600: set noexpandtab sw=8 ts=8 :*/

