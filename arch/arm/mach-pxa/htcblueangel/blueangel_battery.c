/*
* Driver interface to the battery on the HTC Blueangel
*
* Use consistent with the GNU GPL is permitted,
* provided that this copyright notice is
* preserved in its entirety in all copies and derived works.
*
*/

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/version.h>
//#include <linux/config.h>
#include <linux/battery.h>
#include <linux/leds.h>
#include <linux/soc/asic3_base.h>
#include <linux/platform_device.h>

#include <asm/apm.h>
#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/htcblueangel-gpio.h>
#include <asm/arch/htcblueangel-asic.h>

#include <linux/soc/tsc2200.h>

#ifdef DEBUG
#define dprintk(x...) printk(x)
#else
#define dprintk(x...)
#endif

extern struct platform_device blueangel_tsc2200;
DEFINE_LED_TRIGGER(ledtrig_cradle);



static void bat_tsc2200_start_conv_scan(void)
{
	tsc2200_write(&blueangel_tsc2200.dev, TSC2200_CTRLREG_REF, 31);
	tsc2200_write(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC,
		TSC2200_CTRLREG_ADC_AD3 |
		TSC2200_CTRLREG_ADC_AD1 |
		TSC2200_CTRLREG_ADC_AD0 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
}

static void bat_tsc2200_start_conv_temp(void)
{
	tsc2200_write(&blueangel_tsc2200.dev, TSC2200_CTRLREG_REF, 31);
	tsc2200_write(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC,
		TSC2200_CTRLREG_ADC_AD3 |
		TSC2200_CTRLREG_ADC_AD1 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
}

static void bat_tsc2200_stop_conv(void)
{
	tsc2200_write(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC,
		TSC2200_CTRLREG_ADC_STS |
		TSC2200_CTRLREG_ADC_AD0 |
		TSC2200_CTRLREG_ADC_RES (TSC2200_CTRLREG_ADC_RES_12BITP) |
		TSC2200_CTRLREG_ADC_AVG (TSC2200_CTRLREG_ADC_8AVG) |
		TSC2200_CTRLREG_ADC_CL  (TSC2200_CTRLREG_ADC_CL_2MHZ_12BIT) |
		TSC2200_CTRLREG_ADC_PV  (TSC2200_CTRLREG_ADC_PV_500uS) );
}

static int blueangel_battery_get_status(struct battery *bat)
{
	dprintk("%s: in.\n", __FUNCTION__);
	
	if ( GPLR(63) & GPIO_bit(63) ) {
		return BATTERY_STATUS_CHARGING;
	} else {
		return BATTERY_STATUS_NOT_CHARGING;
	}
}


static int blueangel_battery_get_voltage1(struct battery *bat)
{
	int retval;
	
	dprintk("%s: in.\n", __FUNCTION__);

	//down_interruptible(&tsc2200_sem);
	//tsc2200_lock(&blueangel_tsc2200.dev);
	bat_tsc2200_start_conv_scan();

	while ( !(tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC) & 0x4000) &&
			 !tsc2200_dav(&blueangel_tsc2200.dev )) {
		dprintk("B2: %X\n", tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC));
		mdelay(1);
	}
	
	retval = tsc2200_read(&blueangel_tsc2200.dev, TSC2200_DATAREG_AUX1);
	
	bat_tsc2200_stop_conv();
	//tsc2200_unlock(&blueangel_tsc2200.dev);
	//up(&tsc2200_sem);
	return retval;
}
static int blueangel_battery_get_voltage(struct battery *bat)
{
	int retval;
	
	dprintk("%s: in.\n", __FUNCTION__);

	//down_interruptible(&tsc2200_sem);
	//tsc2200_lock(&blueangel_tsc2200.dev);
	bat_tsc2200_start_conv_scan();

	while ( !(tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC) & 0x4000) &&
			 !tsc2200_dav(&blueangel_tsc2200.dev )) {
		dprintk("B2: %X\n", tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC));
		mdelay(1);
	}
	
	retval = tsc2200_read(&blueangel_tsc2200.dev, TSC2200_DATAREG_AUX2);
	
	bat_tsc2200_stop_conv();
	//tsc2200_unlock(&blueangel_tsc2200.dev);
	//up(&tsc2200_sem);
	return retval;
}

static int blueangel_battery_get_temp(struct battery *bat)
{
	int retval;
	
	dprintk("%s: in.\n", __FUNCTION__);

	//down_interruptible(&tsc2200_sem);
	//tsc2200_lock(&blueangel_tsc2200.dev);
	bat_tsc2200_start_conv_temp();

	while ( !(tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC) & 0x4000) &&
			 !tsc2200_dav(&blueangel_tsc2200.dev)) {
		dprintk("1: %X\n", tsc2200_read(&blueangel_tsc2200.dev, TSC2200_CTRLREG_ADC)); 
		mdelay(1);
	}
	
	retval = tsc2200_read(&blueangel_tsc2200.dev, TSC2200_DATAREG_TEMP1);
	dprintk("retval=0x%x\n", retval);
	
	bat_tsc2200_stop_conv();
	//tsc2200_unlock(&blueangel_tsc2200.dev);
	//up(&tsc2200_sem);
	dprintk("%s: out\n", __FUNCTION__);
	return retval;
}

struct battery battery_dev = {
	.name           = "Main battery",
	.id		= "battery0",
	.get_voltage	= blueangel_battery_get_voltage,
	.get_status	= blueangel_battery_get_status,
	//.get_temp	= blueangel_battery_get_temp,
};

struct battery battery_dev1 = {
	.name           = "Backup battery",
	.id		= "battery1",
	.get_voltage	= blueangel_battery_get_voltage1,
	.get_status	= blueangel_battery_get_status,
	//.get_temp	= blueangel_battery_get_tempi1,
};

static void
blueangel_battery_get_power_status (struct apm_power_info *info)
{
	int val;
	int val1;
	int full=0x8ea;
	int empty=0x8a3;

	int full1=0xb0f;
	int empty1=0x8a3;
	val=blueangel_battery_get_voltage(NULL);
	val1=blueangel_battery_get_voltage1(NULL);

	info->battery_life = (val1-empty)*100/(full-empty);
	if (info->battery_life < 0 || info->battery_life > 100) 
		printk("battery voltage (0x%x) not within [0x%x,0x%x]. Please report.\n", val, full, empty);
	if (info->battery_life < 0) 
		info->battery_life = 0;
	if (info->battery_life > 100)
		info->battery_life = 100;
        info->ac_line_status = APM_AC_OFFLINE;
	if (!(asic3_get_gpio_status_d(&blueangel_tsc2200.dev) & GPIOD_AC_CHARGER_N)) {
        	info->ac_line_status = APM_AC_ONLINE;
	}
	if (!(GPLR(GPIO_NR_BLUEANGEL_USB_DETECT_N) & GPIO_bit(GPIO_NR_BLUEANGEL_USB_DETECT_N))) {
        	info->ac_line_status = APM_AC_ONLINE;
	}
	info->units=APM_UNITS_MINS;
	info->time = 360 * info->battery_life/100;	/* time remaining */
	info->battery_flag = 0;
	if (info->ac_line_status == APM_AC_OFFLINE) {
		#ifdef CONFIG_LEDS_TRIGGERS
			led_trigger_event(ledtrig_cradle, LED_OFF);
		#endif
		info->battery_status = APM_BATTERY_STATUS_CRITICAL;
		if(info->battery_life > 5) {
			info->battery_status = APM_BATTERY_STATUS_LOW;
		}
		if(info->battery_life > 20) {
			info->battery_status = APM_BATTERY_STATUS_HIGH;
		}
	} else {
		info->battery_status = APM_BATTERY_STATUS_CHARGING;
		#ifdef CONFIG_LEDS_TRIGGERS
			led_trigger_event(ledtrig_cradle, LED_FULL);
		#endif
	}
}

static int bat_setup(void)
{
	battery_class_register(&battery_dev);
	battery_class_register(&battery_dev1);
	return 0;
}

static int __init bat_init(void)
{
	bat_setup();
	blueangel_battery_get_voltage(NULL);
	blueangel_battery_get_voltage1(NULL);
	blueangel_battery_get_temp(NULL);
	apm_get_power_status = blueangel_battery_get_power_status;
		#ifdef CONFIG_LEDS_TRIGGERS
	led_trigger_register_simple("charge", &ledtrig_cradle);
		#endif

	return 0;
}

static void __exit bat_exit(void)
{
	apm_get_power_status = NULL;
	battery_class_unregister(&battery_dev);
	battery_class_unregister(&battery_dev1);
		#ifdef CONFIG_LEDS_TRIGGERS
        led_trigger_unregister_simple(ledtrig_cradle);
		#endif
}

module_init(bat_init)
module_exit(bat_exit)

MODULE_AUTHOR("Matthias Burghardt");
MODULE_DESCRIPTION("Battery (TI TSC2200) support for the HTC Blueangel");
MODULE_LICENSE("GPL");
