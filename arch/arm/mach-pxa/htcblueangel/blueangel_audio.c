/*
 * LED interface for Himalaya, the HTC PocketPC.
 *
 * License: GPL
 *
 * Author: Luke Kenneth Casson Leighton, Copyright(C) 2004
 *
 * Copyright(C) 2004, Luke Kenneth Casson Leighton.
 *
 * History:
 *
 * 2004-02-19	Luke Kenneth Casson Leighton	created.
 *
 */
 
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/dma-mapping.h>
#include <linux/soc/asic3_base.h>

#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/ssp.h>
#include <asm/arch/dma.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/arch/htcblueangel-asic.h>

#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/initval.h>
#include <sound/uda1380.h>

static char *id="blueangel_audio";
static snd_card_t *card;
struct ssp_dev audio_ssp_dev;

struct i2c_client *uda1380;

extern struct platform_device blueangel_asic3;

#define MAX_BUFFER_SIZE	65536
#define MAX_DMA_SIZE	4096

#define chip_t blueangel_audio_t

#ifdef DEBUG
#define dprintk(x...) printk(x)
#else
#define dprintk(x...)
#endif



typedef enum stream_id_t{
	PLAYBACK=0,
	CAPTURE,
	MAX_STREAMS,
}stream_id_t;

typedef struct blueangel_audio_stream {
	char *id;			/* identification string */
	int  stream_id;			/* numeric identification */	
	
	int dma_ch;			/* DMA channel used */
	volatile u32 *drcmr;		/* reference the DMA DRCMRC register
					   for i2s tx or rx */
	
	dma_addr_t buffer;		/* buffer (physical address) */
	unsigned int buffer_size;	/* buffer size */
	
	pxa_dma_desc *dma_dring;	/* DMA descriptors ring */
	char *dma_dring_store;		/* storage space for DMA descriptors
					   ring (alignment issue) */
	dma_addr_t dma_dring_p;		/* (physical address) */
	dma_addr_t dma_dring_store_p;	
		
	unsigned int periods;		/* periods */
	unsigned int dma_descriptors;	/* DMA descriptors in ring */ 
	unsigned int dma_size;		/* DMA transfer size */
	unsigned int dma_dpp;		/* DMA descriptors per period */
	
	volatile int dma_running;	/* DMA running? */
	
  	snd_pcm_substream_t *stream;
}blueangel_audio_stream_t;

typedef struct snd_card_blueangel_audio {
	snd_card_t *card;
	snd_pcm_t *pcm;
	struct i2c_client *uda1380;
	long samplerate;
	blueangel_audio_stream_t *s[MAX_STREAMS];
	snd_info_entry_t *proc_entry;
	int power_on[4];
	int playback_active;
	int record_active;
	int mic_power;
	int spk_power;
	struct pm_dev *pm_dev;
} blueangel_audio_t;

static struct snd_card_blueangel_audio *blueangel_audio = NULL;

static void blueangel_audio_set_power(blueangel_audio_t *card)
{
	int mic_power;
	int spk_power;
	mic_power=(card->power_on[2] && card->record_active) || card->power_on[3];
	spk_power=(card->power_on[0] && card->playback_active) || card->power_on[1];
	dprintk("blueangel_audio_set_power %d %d\n", mic_power, spk_power);
	if (mic_power != card->mic_power)
		asic3_set_gpio_out_a(&blueangel_asic3.dev, GPIOA_MIC_PWR_ON, mic_power ? GPIOA_MIC_PWR_ON : 0);
	if (spk_power != card->spk_power)
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_SPK_PWR_ON, spk_power ? GPIOB_SPK_PWR_ON : 0);
	card->mic_power=mic_power;
	card->spk_power=spk_power;
}

#define AUDIO_RATE_DEFAULT	44100

static void blueangel_audio_set_samplerate(blueangel_audio_t *blueangel_audio, long val, int force)
{
	struct i2c_client *uda1380 = blueangel_audio->uda1380;
	struct uda1380_cfg cfg;
	int mute,div;

	if (val == blueangel_audio->samplerate && !force)
		return;

	mute = snd_uda1380_mute(uda1380, 1);

	pxa_i2s_set_samplerate(val);
	div=705600/val;
	div-=1;
	dprintk("val=%ld div=%d\n", val, div);
        ssp_config(&audio_ssp_dev,
                        SSCR0_TI | SSCR0_ECS | SSCR0_DataSize(0x10),
                        SSCR1_TxTresh(8) | SSCR1_RxTresh(8),
                        0,
                        div << 8
        );
        ssp_enable(&audio_ssp_dev);
	if (val >= 48000)
		cfg.pll = REG0_PLL_25TO50;
	else if (val >= 44100)
		cfg.pll = REG0_PLL_25TO50;
	else if (val >= 22050)
		cfg.pll = REG0_PLL_12TO25;
	else if (val >= 16000)
		cfg.pll = REG0_PLL_12TO25;
	else
		cfg.pll = REG0_PLL_6TO12;

        cfg.format = FMT_MSB;
	cfg.fs=256;
	cfg.use_pll=1;
	
	i2c_clients_command(uda1380->adapter, I2C_UDA1380_CONFIGURE, &cfg);
	blueangel_audio->samplerate = val;

	snd_uda1380_mute(uda1380, mute);
}

static void blueangel_audio_audio_init(blueangel_audio_t *blueangel_audio)
{
	unsigned long flags;
	
	/* Setup the uarts */
	pxa_i2s_init();

	/* Blip the UDA1380 reset line */
	local_irq_save(flags);
	/* uda1380_device_controls.set_codec_reset(1); */
	local_irq_restore(flags);
	
	/* Enable the audio power */
	/* uda1380_device_controls.set_codec_power(1); */
	
	mdelay(1);

	local_irq_save(flags);
	/* uda1380_device_controls.set_codec_reset(0); */
	local_irq_restore(flags);

	/* Wait for the UDA1380 to wake up */
	mdelay(1);

	dprintk("blueangel_audio_audio_init: uda1380=%p\n", blueangel_audio->uda1380);
	if (blueangel_audio->uda1380)
		dprintk("blueangel_audio_audio_init: uda1380->adapter=%p\n", blueangel_audio->uda1380->adapter);
	if (blueangel_audio->uda1380 && blueangel_audio->uda1380->adapter) {
	/* Initialize the UDA1380 internal state */
	i2c_clients_command(blueangel_audio->uda1380->adapter,
			    I2C_UDA1380_OPEN, 0);

	blueangel_audio_set_samplerate(blueangel_audio, blueangel_audio->samplerate, 1);
	
	snd_uda1380_mute(blueangel_audio->uda1380, 0);
	} else {
		dprintk("*** UDA1380 ERROR***\n");
	}
}



static void audio_dma_setup_descriptors_ring(blueangel_audio_stream_t *s)
{
	int i, j;
	unsigned int p_size, d_size, di;
	dma_addr_t p, d_buf;
	d_size = sizeof(pxa_dma_desc);

	/* how many dma descriptors? */
	s->dma_size = MAX_DMA_SIZE;
	p_size = s->buffer_size / s->periods; /* period size */
	s->dma_dpp = (p_size - 1) / s->dma_size + 1; /* dma descriptors
						        per period */ 
	
	/* allocate storage space for descriptors ring,
	   included 15 bytes extra for the alignment issue */
	s->dma_dring_store = dma_alloc_coherent(NULL, d_size * s->periods * s->dma_dpp + 15,
		 				&s->dma_dring_store_p, GFP_KERNEL);
	  		
	/* setup the descriptors ring pointer, aligned on a 16 bytes boundary */
	s->dma_dring = ((dma_addr_t) s->dma_dring_store & 0xf) ?
			 (pxa_dma_desc *) (((dma_addr_t) s->dma_dring_store & ~0xf) + 16) :
			 (pxa_dma_desc *) s->dma_dring_store;
	s->dma_dring_p = s->dma_dring_store_p +
			  (dma_addr_t) s->dma_dring - (dma_addr_t) s->dma_dring_store;
	
	/* fill the descriptors ring */
	di = 0; /* current descriptor index */
	p = s->buffer; /* p: current period (address in buffer) */
	d_buf = s->buffer; /* d_buf: address in buffer
			             for current dma descriptor */
	 /* iterate over periods */
	for (i = 0; i < s->periods; i++, p += p_size) { 
		/* iterate over dma descriptors in a period */
		for (j = 0; j < s->dma_dpp; j++, di++, d_buf += s->dma_size) {
			/* link to next descriptor */
			s->dma_dring[di].ddadr = s->dma_dring_p + (di + 1) * d_size;
			if (s->stream_id == PLAYBACK) {
				s->dma_dring[di].dsadr = d_buf;
				s->dma_dring[di].dtadr = __PREG(SSDR_P1);
				s->dma_dring[di].dcmd = DCMD_TXPCDR;
			}
			else {
				s->dma_dring[di].dsadr = __PREG(SADR);
				s->dma_dring[di].dtadr = d_buf;
				s->dma_dring[di].dcmd = DCMD_RXPCDR;
			}
			s->dma_dring[di].dcmd |=
			  ((p + p_size - d_buf) >= s->dma_size) ?
			  s->dma_size : p + p_size - d_buf; /* transfer length */
		}
		s->dma_dring[di - 1].dcmd |= DCMD_ENDIRQEN; /* period irq */
	}
	s->dma_dring[di - 1].ddadr = s->dma_dring_p; /* close the ring */
}

static void audio_dma_free_descriptors_ring(blueangel_audio_stream_t *s)
{
	unsigned int d_size = sizeof(pxa_dma_desc);
	if (s->dma_dring_store) {
		dma_free_coherent(NULL, d_size * s->periods * s->dma_dpp + 15,
				  s->dma_dring_store, s->dma_dring_store_p);
		s->dma_dring_store = NULL;
		s->dma_dring = NULL;
		s->dma_dring_store_p = 0;
		s->dma_dring_p = 0;
	}	
}

static void audio_dma_request(blueangel_audio_stream_t *s,
			      void (*audio_dma_irq)(int , void *))
{
	int err;
	dprintk("audio_dma_request\n");
	err = pxa_request_dma(s->id, DMA_PRIO_LOW, audio_dma_irq, s);
	if (err < 0) {
		dprintk("panic: cannot allocate DMA for %s\n", s->id);
		/* CHRI-TODO: handle this error condition gracefully */
	}
	s->dma_ch = err;
	
	if (s->stream_id == CAPTURE) {
		s->drcmr = &DRCMRRXSADR;
		*(s->drcmr) = s->dma_ch | DRCMR_MAPVLD;
	}
	else {
		s->drcmr = &DRCMRTXSSDR;
		*(s->drcmr) = s->dma_ch | DRCMR_MAPVLD;
	}
}

static void audio_dma_free(blueangel_audio_stream_t *s)
{
	pxa_free_dma(s->dma_ch);
}


static u_int audio_get_dma_pos(blueangel_audio_stream_t *s)
{
	snd_pcm_substream_t * substream = s->stream;
	snd_pcm_runtime_t *runtime = substream->runtime;
	unsigned int offset_bytes;
	unsigned int offset;
	int ch = s->dma_ch;
	u32 pos;

	if (s->stream_id == CAPTURE)
		pos = DTADR(ch);
	else
		pos = DSADR(ch);
	offset_bytes =  pos - s->buffer; 
	offset = bytes_to_frames(runtime,offset_bytes);

	return offset;
}

static void audio_stop_dma(blueangel_audio_stream_t *s) 
{
	DCSR(s->dma_ch) = DCSR_STOPIRQEN;
	
	while (! (DCSR(s->dma_ch) & DCSR_STOPSTATE)) {
		if (!in_interrupt())
			schedule();
	}
	
	s->dma_running = 0;
}

static void audio_start_dma(blueangel_audio_stream_t *s, int restart)
{
	dprintk("audio_start_dma\n");
	/* kick the DMA */
	DDADR(s->dma_ch) = s->dma_dring_p;
	DCSR(s->dma_ch) = DCSR_RUN;	
	s->dma_running = 1;
}

static void audio_dma_irq(int chan, void *dev_id)
{
	blueangel_audio_stream_t *s = (blueangel_audio_stream_t *) dev_id;
	int ch = s->dma_ch;
	u32 dcsr;
	dprintk("audio_dma_irq\n");
	
	dcsr = DCSR(ch);
	DCSR(ch) = dcsr & ~DCSR_STOPIRQEN;

	if (dcsr & DCSR_BUSERR) {
		dprintk("bus error\n");
		return ;
	}

	if (dcsr & DCSR_ENDINTR) {
		snd_pcm_period_elapsed(s->stream);
		return;
	}
}

static int snd_card_blueangel_audio_pcm_trigger(stream_id_t stream_id,
					    snd_pcm_substream_t * substream, int cmd)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	dprintk("snd_card_blueangel_audio_pcm_trigger\n");
	
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
		if (chip->s[PLAYBACK]->dma_running) {
			audio_stop_dma(chip->s[PLAYBACK]);
		}
		if (chip->s[CAPTURE]->dma_running) {
			audio_stop_dma(chip->s[CAPTURE]);
		}
		audio_start_dma(chip->s[stream_id],0);
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		audio_stop_dma(chip->s[stream_id]);
		break;
	default:
		return -EINVAL;
		break;
	}
	return 0;

}

static snd_pcm_hardware_t snd_blueangel_audio_capture =
{
	.info			= (SNDRV_PCM_INFO_INTERLEAVED |
				   SNDRV_PCM_INFO_BLOCK_TRANSFER |
				   SNDRV_PCM_INFO_MMAP | SNDRV_PCM_INFO_MMAP_VALID),
	.formats		= SNDRV_PCM_FMTBIT_S16_LE,
	.rates			= (SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_16000 |\
				   SNDRV_PCM_RATE_22050 | \
				   SNDRV_PCM_RATE_44100 | SNDRV_PCM_RATE_48000 |\
				   SNDRV_PCM_RATE_KNOT),
	.rate_min		= 8000,
	.rate_max		= 48000,
	.channels_min		= 2,
	.channels_max		= 2,
	.buffer_bytes_max	= MAX_BUFFER_SIZE, 
	.period_bytes_min	= 4096,
	.period_bytes_max	= MAX_BUFFER_SIZE/2,
	.periods_min		= 2,
	.periods_max		= MAX_BUFFER_SIZE/4096,
	.fifo_size		= 16,
};

static snd_pcm_hardware_t snd_blueangel_audio_playback =
{
	.info			= (SNDRV_PCM_INFO_INTERLEAVED |
				   SNDRV_PCM_INFO_BLOCK_TRANSFER |
				   SNDRV_PCM_INFO_MMAP | SNDRV_PCM_INFO_MMAP_VALID),
	.formats		= SNDRV_PCM_FMTBIT_S16_LE,
	.rates			= (SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_16000 |\
                                   SNDRV_PCM_RATE_22050 | \
				   SNDRV_PCM_RATE_44100 | SNDRV_PCM_RATE_48000 |\
				   SNDRV_PCM_RATE_KNOT),
	.rate_min		= 8000,
	.rate_max		= 48000,
	.channels_min		= 2,
	.channels_max		= 2,
	.buffer_bytes_max	= MAX_BUFFER_SIZE, 
	.period_bytes_min	= 4096,
	.period_bytes_max	= MAX_BUFFER_SIZE/2,
	.periods_min		= 2,
	.periods_max		= MAX_BUFFER_SIZE/4096,
	.fifo_size		= 16,
};


static unsigned int rates[] = { 8000, 16000, 22050, 44100, 48000 };

#define RATES sizeof(rates) / sizeof(rates[0])

static snd_pcm_hw_constraint_list_t hw_constraints_rates = {
	.count	= RATES,
	.list	= rates,
	.mask	= 0,
};

/* Constraint: buffer_size = period_size * periods
   A better way to assure it specifying directly? */
static unsigned int period_sizes[] = { 1024, 2048, 4096, 8192, 16384, 32768 };
static unsigned int buffer_sizes[] = { 2048, 4096, 8192, 16384, 32768, 65536 };
#define PERIOD_SIZES sizeof(period_sizes) / sizeof(period_sizes[0])
#define BUFFER_SIZES sizeof(period_sizes) / sizeof(period_sizes[0])

static snd_pcm_hw_constraint_list_t hw_constraints_period_sizes = {
        .count = PERIOD_SIZES,
        .list = period_sizes,
        .mask = 0
};

static snd_pcm_hw_constraint_list_t hw_constraints_buffer_sizes = {
        .count = BUFFER_SIZES,
        .list = buffer_sizes,
        .mask = 0
};
	
static int snd_card_blueangel_audio_playback_open(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_runtime_t *runtime = substream->runtime;
        int err;

	dprintk("snd_card_blueangel_audio_playback_open\n");

	chip->s[PLAYBACK]->stream = substream;

	runtime->hw = snd_blueangel_audio_playback;

	snd_pcm_hw_constraint_list(runtime, 0,
                                   SNDRV_PCM_HW_PARAM_PERIOD_BYTES,
                                   &hw_constraints_period_sizes);
	snd_pcm_hw_constraint_list(runtime, 0,
                                   SNDRV_PCM_HW_PARAM_BUFFER_BYTES,
                                   &hw_constraints_buffer_sizes);

	if ((err = snd_pcm_hw_constraint_list(runtime, 0, SNDRV_PCM_HW_PARAM_RATE,
					      &hw_constraints_rates)) < 0)
		return err;

	chip->playback_active=1;
	blueangel_audio_set_power(chip);

	return 0;
}


static int snd_card_blueangel_audio_playback_close(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);

	dprintk("snd_card_blueangel_audio_playback_close\n");

	chip->s[PLAYBACK]->dma_running = 0;
	chip->s[PLAYBACK]->stream = NULL;

	chip->playback_active=0;
	blueangel_audio_set_power(chip);

	return 0;
}

static int snd_card_blueangel_audio_playback_ioctl(snd_pcm_substream_t * substream,
					       unsigned int cmd, void *arg)
{
	dprintk("snd_card_blueangel_audio_playback_ioctl\n");

	return snd_pcm_lib_ioctl(substream, cmd, arg);
}

static int snd_card_blueangel_audio_playback_prepare(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_runtime_t *runtime = substream->runtime;
        
	dprintk("snd_card_blueangel_audio_playback_prepare\n");
	/* set requested samplerate */
	blueangel_audio_set_samplerate(chip, runtime->rate, 0);
	chip->samplerate = runtime->rate;
        
	return 0;
}

static int snd_card_blueangel_audio_playback_trigger(snd_pcm_substream_t * substream, int cmd)
{
	dprintk("snd_card_blueangel_audio_playback_trigger\n");
	return snd_card_blueangel_audio_pcm_trigger(PLAYBACK, substream, cmd);
}

static snd_pcm_uframes_t snd_card_blueangel_audio_playback_pointer(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_uframes_t pos;

	dprintk("snd_card_blueangel_audio_playback_pointer\n");	
	pos = audio_get_dma_pos(chip->s[PLAYBACK]);
	return pos;
}

static int snd_card_blueangel_audio_capture_open(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_runtime_t *runtime = substream->runtime;
	int err;
	
	chip->s[CAPTURE]->stream = substream;
	
	runtime->hw = snd_blueangel_audio_capture;

	snd_pcm_hw_constraint_list(runtime, 0,
                                   SNDRV_PCM_HW_PARAM_PERIOD_BYTES,
                                   &hw_constraints_period_sizes);
	snd_pcm_hw_constraint_list(runtime, 0,
                                   SNDRV_PCM_HW_PARAM_BUFFER_BYTES,
                                   &hw_constraints_buffer_sizes);

	if ((err = snd_pcm_hw_constraint_list(runtime, 0, SNDRV_PCM_HW_PARAM_RATE,
					      &hw_constraints_rates)) < 0)
		return err;        

	/* uda1380_device_controls.set_mic_power(1); */
	return 0;
}


static int snd_card_blueangel_audio_capture_close(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);

	chip->s[CAPTURE]->dma_running = 0;
	chip->s[CAPTURE]->stream = NULL;
      
	/* uda1380_device_controls.set_mic_power(0); */

	dprintk("%s: capture closed\n", __FUNCTION__);
	
	return 0;
}

static int snd_card_blueangel_audio_capture_ioctl(snd_pcm_substream_t * substream,
					unsigned int cmd, void *arg)
{
	return snd_pcm_lib_ioctl(substream, cmd, arg);
}

static int snd_card_blueangel_audio_capture_prepare(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_runtime_t *runtime = substream->runtime;

	/* set requested samplerate */
	blueangel_audio_set_samplerate(chip, runtime->rate, 0);
        chip->samplerate = runtime->rate;

	return 0;
}

static int snd_card_blueangel_audio_capture_trigger(snd_pcm_substream_t * substream, int cmd)
{
	return snd_card_blueangel_audio_pcm_trigger(CAPTURE, substream, cmd);
}

static snd_pcm_uframes_t snd_card_blueangel_audio_capture_pointer(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	snd_pcm_uframes_t pos;

	pos = audio_get_dma_pos(chip->s[CAPTURE]);
	return pos;
}

static int snd_blueangel_audio_hw_params(snd_pcm_substream_t * substream,
				     snd_pcm_hw_params_t * hw_params)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);       

	int err;

	dprintk("snd_blueangel_audio_hw_params\n");
	/* allocate the buffer */
	err = snd_pcm_lib_malloc_pages(substream, params_buffer_bytes(hw_params));
	if (err<0)
		dprintk("snd_pcm_lib_malloc_pages failed!\n");
	
	/* setup the dma descriptors ring, that refers the buffer;
	   release the old one before in case, since hw_params
	   may be called more than one time*/
	if (chip->s[PLAYBACK]->stream == substream) {
		if (chip->s[PLAYBACK]->dma_dring)
			audio_dma_free_descriptors_ring(chip->s[PLAYBACK]);
		chip->s[PLAYBACK]->buffer = substream->runtime->dma_addr;
		chip->s[PLAYBACK]->buffer_size = params_buffer_bytes(hw_params);
		chip->s[PLAYBACK]->periods = params_periods(hw_params);
		audio_dma_setup_descriptors_ring(chip->s[PLAYBACK]);
	}
	else {
		if (chip->s[CAPTURE]->dma_dring)
			audio_dma_free_descriptors_ring(chip->s[CAPTURE]);
		chip->s[CAPTURE]->buffer = substream->runtime->dma_addr;
		chip->s[CAPTURE]->buffer_size = params_buffer_bytes(hw_params);
		chip->s[CAPTURE]->periods = params_periods(hw_params);
		audio_dma_setup_descriptors_ring(chip->s[CAPTURE]);
	}
	
	return err;
}

static int snd_blueangel_audio_hw_free(snd_pcm_substream_t * substream)
{
	blueangel_audio_t *chip = snd_pcm_substream_chip(substream);
	dprintk("snd_blueangel_audio_hw_free\n");
	
	/* free the dma descriptors ring */
	if (chip->s[PLAYBACK]->stream == substream) {
		audio_dma_free_descriptors_ring(chip->s[PLAYBACK]);
	}
	else {
		audio_dma_free_descriptors_ring(chip->s[CAPTURE]);
	}
	
	return snd_pcm_lib_free_pages(substream);
}

static snd_pcm_ops_t snd_card_blueangel_audio_playback_ops = {
	.open			= snd_card_blueangel_audio_playback_open,
	.close			= snd_card_blueangel_audio_playback_close,
	.ioctl			= snd_card_blueangel_audio_playback_ioctl,
	.hw_params	        = snd_blueangel_audio_hw_params,
	.hw_free	        = snd_blueangel_audio_hw_free,
	.prepare		= snd_card_blueangel_audio_playback_prepare,
	.trigger		= snd_card_blueangel_audio_playback_trigger,
	.pointer		= snd_card_blueangel_audio_playback_pointer,
};

static snd_pcm_ops_t snd_card_blueangel_audio_capture_ops = {
	.open			= snd_card_blueangel_audio_capture_open,
	.close			= snd_card_blueangel_audio_capture_close,
	.ioctl			= snd_card_blueangel_audio_capture_ioctl,
	.hw_params	        = snd_blueangel_audio_hw_params,
	.hw_free	        = snd_blueangel_audio_hw_free,
	.prepare		= snd_card_blueangel_audio_capture_prepare,
	.trigger		= snd_card_blueangel_audio_capture_trigger,
	.pointer		= snd_card_blueangel_audio_capture_pointer,
};


static int __init snd_card_blueangel_audio_pcm(blueangel_audio_t *blueangel_audio, int device, int substreams)
{

	int err;
		
	blueangel_audio->samplerate = AUDIO_RATE_DEFAULT;
	
	if ((err = snd_pcm_new(blueangel_audio->card, "UDA1380 PCM", device,
			       substreams, substreams, &(blueangel_audio->pcm))) < 0)
		return err;

	err = snd_pcm_lib_preallocate_pages_for_all(blueangel_audio->pcm ,SNDRV_DMA_TYPE_DEV , NULL, MAX_BUFFER_SIZE, MAX_BUFFER_SIZE);
	if (err < 0)
	  dprintk("buffer preallocation failed with code %d\n", err);
	
	snd_pcm_set_ops(blueangel_audio->pcm, SNDRV_PCM_STREAM_PLAYBACK, &snd_card_blueangel_audio_playback_ops);
	snd_pcm_set_ops(blueangel_audio->pcm, SNDRV_PCM_STREAM_CAPTURE, &snd_card_blueangel_audio_capture_ops);
	blueangel_audio->pcm->private_data = blueangel_audio;
	blueangel_audio->pcm->info_flags = 0;
	strcpy(blueangel_audio->pcm->name, "UDA1380 PCM");

	blueangel_audio->s[PLAYBACK] = kcalloc(1,sizeof(blueangel_audio_stream_t), GFP_KERNEL);
	blueangel_audio->s[CAPTURE] = kcalloc(1,sizeof(blueangel_audio_stream_t), GFP_KERNEL);

	blueangel_audio_audio_init(blueangel_audio);

	/* setup nameing */
	blueangel_audio->s[PLAYBACK]->id = "UDA1380 PLAYBACK";
	blueangel_audio->s[CAPTURE]->id = "UDA1380 CAPTURE";

	blueangel_audio->s[PLAYBACK]->stream_id = PLAYBACK;
	blueangel_audio->s[CAPTURE]->stream_id = CAPTURE;

	/* setup DMA controller */
	audio_dma_request(blueangel_audio->s[PLAYBACK], audio_dma_irq);
	audio_dma_request(blueangel_audio->s[CAPTURE], audio_dma_irq);

	blueangel_audio->s[PLAYBACK]->dma_running = 0;
	blueangel_audio->s[CAPTURE]->dma_running = 0;
	
	/* descriptors ring */
	blueangel_audio->s[PLAYBACK]->dma_dring = NULL;
	blueangel_audio->s[PLAYBACK]->dma_dring_p = 0;
	blueangel_audio->s[PLAYBACK]->dma_dring_store = NULL;
	blueangel_audio->s[CAPTURE]->dma_dring = NULL;
	blueangel_audio->s[CAPTURE]->dma_dring_p = 0;
	blueangel_audio->s[CAPTURE]->dma_dring_store = NULL;
	
	return 0;
}

void snd_blueangel_audio_free(snd_card_t *card)
{
	blueangel_audio_t *chip = card->private_data;

	if (! chip)
		return;

	if (chip->s[PLAYBACK] && chip->s[CAPTURE]) {
		chip->s[PLAYBACK]->drcmr = 0;
		chip->s[CAPTURE]->drcmr = 0;

		audio_dma_free(chip->s[PLAYBACK]);
		audio_dma_free(chip->s[CAPTURE]);

		kfree(chip->s[PLAYBACK]);
		kfree(chip->s[CAPTURE]);

		chip->s[PLAYBACK] = NULL;
		chip->s[CAPTURE] = NULL;
	}

	kfree(chip);
	card->private_data = NULL;
}

static void
blueangel_audio_power_callback(int item, int on, void *data)
{
	blueangel_audio_t *chip = data;

	chip->power_on[item]=on;
	blueangel_audio_set_power(chip);
}

int
blueangel_audio_pm(struct pm_dev *dev, pm_request_t rqst, void *data)
{
	printk("blueangel_audio_pm\n");
	return 0;
}

static int blueangel_audio_init (void)
{
	int err;

	card = snd_card_new(-1, id, THIS_MODULE, 0);
	if (card == NULL)
		return -ENOMEM;

	blueangel_audio = kcalloc(1,sizeof(*blueangel_audio), GFP_KERNEL);
	if (blueangel_audio == NULL) {
		err = -ENOMEM;
		goto nodev;
	}
         
	card->private_data = (void *)blueangel_audio;
	card->private_free = snd_blueangel_audio_free;

	blueangel_audio->card = card;
	
	request_module("i2c-pxa");
	request_module("snd-uda1380");

	asic3_set_clock_cdex (&blueangel_asic3.dev, CLOCK_CDEX_CONTROL_CX, CLOCK_CDEX_CONTROL_CX);
	GPDR(GPIO23_SCLK) |= GPIO_bit(GPIO23_SCLK);
	GPDR(GPIO24_SFRM) |= GPIO_bit(GPIO24_SFRM);
	GPDR(GPIO25_STXD) |= GPIO_bit(GPIO25_STXD);
	pxa_gpio_mode(GPIO23_SCLK_MD);
	pxa_gpio_mode(GPIO24_SFRM_MD);
	pxa_gpio_mode(GPIO25_STXD_MD);
	ssp_init(&audio_ssp_dev, 1, NULL );



	snd_uda1380_register_power_callback(blueangel_audio_power_callback, blueangel_audio);
	dprintk("snd_uda1380_activate\n");
	snd_uda1380_activate();
	uda1380=uda1380_get_i2c_client();
	dprintk("uda1380=%p\n", uda1380);

        snd_uda1380_mute(uda1380, 0);
	asic3_set_clock_cdex (&blueangel_asic3.dev, CLOCK_CDEX_SOURCE<<1, CLOCK_CDEX_SOURCE<<1);
	blueangel_audio->uda1380 = uda1380_get_i2c_client();
	dprintk("snd_uda1380_mixer_create\n");
	snd_uda1380_mixer_create(card);
	if (! blueangel_audio->uda1380 || ! blueangel_audio->uda1380->adapter) {
		dprintk("uda1380_get_i2c_client failed %p\n", blueangel_audio->uda1380);
		if (blueangel_audio->uda1380)
			dprintk("adapter=%p\n", blueangel_audio->uda1380->adapter);
	} else {
		if ((err = snd_card_blueangel_audio_pcm(blueangel_audio, 0, 2)) < 0) {
			dprintk("snd_chip_uda1380_pcm failed\n");
			goto nodev;
		}
	}
	blueangel_audio->pm_dev=pm_register(PM_SYS_DEV, PM_SYS_UNKNOWN, blueangel_audio_pm);
	
	dprintk("client=%p\n",uda1380_get_i2c_client());
        strcpy(card->driver, "UDA1380");
        strcpy(card->shortname, "UDA1380");
        sprintf(card->longname, "PXA + Philips UDA1380 driver");
        if ((err = snd_card_register(card)) == 0) {
                dprintk("Audio support initialized\n");
		dprintk("client2=%p\n",uda1380_get_i2c_client());
		return 0;
	}
nodev:
	snd_card_free(card);
	return err;
}

static void blueangel_audio_exit (void)
{
	pm_unregister(blueangel_audio->pm_dev);
	ssp_disable(&audio_ssp_dev);
	ssp_exit(&audio_ssp_dev);

	snd_uda1380_mixer_free();
	snd_uda1380_deactivate();
	snd_card_free(card);
}

module_init (blueangel_audio_init);
module_exit (blueangel_audio_exit);

