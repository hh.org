/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 * History:
 *
 * 2003-05-14	Joshua Wise        Adapted for the HP iPAQ H1900
 * 2002-08-23   Jamey Hicks        Adapted for use with PXA250-based iPAQs
 * 2001-10-??   Andrew Christian   Added support for iPAQ H3800
 *                                 and abstracted EGPIO interface.
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <video/w100fb.h>
#include <linux/platform_device.h>

#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/arch/htcblueangel-asic.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxafb.h>
#include "blueangel_lcd.h"

#include <linux/soc/asic3_base.h>

extern struct platform_device blueangel_asic3;


#define BLUEANGEL_ATI_W3200_PHYS      PXA_CS2_PHYS

extern int blueangel_boardid;

static struct lcd_device *blueangel_lcd_device;
static int blueangel_lcd_power;

static void
blueangel_lcd_hw_init_pre(void)
{
	printk("blueangel_lcd_hw_init_pre");
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOB_FL_PWR_ON, GPIOB_FL_PWR_ON);

	switch (blueangel_boardid) 
	{
	 case 4:
	 case 5:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR1_ON, 0);
		mdelay(10);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR2_ON, GPIOB_LCD_PWR2_ON);
		mdelay(1);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR3_ON, GPIOB_LCD_PWR3_ON);
		mdelay(10);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR1_ON, GPIOB_LCD_PWR1_ON);
	 break;
	 case 6:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR2_ON, GPIOB_LCD_PWR2_ON);
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_LCD_PWR5_ON, GPIOC_LCD_PWR5_ON);
		udelay(600);
	 break;
	}
}

static void
blueangel_lcd_hw_init_post(void)
{
	printk("blueangel_lcd_hw_init_post");

	switch (blueangel_boardid) 
	{
	 case 6:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR1_ON, GPIOB_LCD_PWR1_ON);
		mdelay(10);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR3_ON, GPIOB_LCD_PWR3_ON);
		mdelay(10);
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_LCD_PWR4_ON, GPIOC_LCD_PWR4_ON);
		mdelay(20);
	}
}

static void
blueangel_lcd_hw_off(void)
{
	printk("blueangel_lcd_hw_off\n");
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOB_FL_PWR_ON, 0);
	switch (blueangel_boardid) 
	{
	 case 6:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR1_ON, 0);
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_LCD_PWR4_ON, 0);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR3_ON, 0);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR2_ON, 0);
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_LCD_PWR5_ON, 0);
	 break;
	 case 4:
	 case 5:
		mdelay(5);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR1_ON, 0);
		mdelay(2);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR3_ON, 0);
		mdelay(2);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_LCD_PWR2_ON, 0);
	 break;
	}
}

static int blueangel_lcd_set_power( struct lcd_device *ld, int level)
{
//	printk("FB_BLANK_UNBLANK %d FB_BLANK_NORMAL %d\n", FB_BLANK_UNBLANK, FB_BLANK_NORMAL);
//	printk("FB_BLANK_VSYNC_SUSPEND %d FB_BLANK_HSYNC_SUSPEND %d\n", FB_BLANK_VSYNC_SUSPEND, FB_BLANK_HSYNC_SUSPEND);
//	printk("FB_BLANK_POWERDOWN %d\n", FB_BLANK_POWERDOWN);
//	printk("blueangel LCD power %d\n",level);
	switch (level) {
	case FB_BLANK_UNBLANK:
	case FB_BLANK_NORMAL:
		/* no - don't do it! you need to re-initialise the w100
		 * chip. as well.  sorry.*/
#if 1
		blueangel_lcd_hw_init_pre();
		blueangel_lcd_hw_init_post();
#endif
		break;
	case FB_BLANK_VSYNC_SUSPEND:
	case FB_BLANK_HSYNC_SUSPEND:
		break;
	case FB_BLANK_POWERDOWN:
		blueangel_lcd_hw_off();
		break;
	}
	blueangel_lcd_power=level;
	return 0;
}

static int blueangel_lcd_get_power(struct lcd_device *ld)
{
	printk("blueangel_lcd_get_power\n");
	return blueangel_lcd_power;
}

static struct lcd_properties blueangel_lcd_props = {
	.owner	       = THIS_MODULE,
	.set_power     = blueangel_lcd_set_power,
	.get_power     = blueangel_lcd_get_power,
};

static void
blueangel_lcd_suspend(struct w100fb_par *wfb)
{
#if 0
	ati_gpios[0] = w100fb_gpio_read(W100_GPIO_PORT_A);
	ati_gpios[1] = w100fb_gpcntl_read(W100_GPIO_PORT_A);
	ati_gpios[2] = w100fb_gpio_read(W100_GPIO_PORT_B);
	ati_gpios[3] = w100fb_gpcntl_read(W100_GPIO_PORT_B);
	w100fb_gpio_write(W100_GPIO_PORT_A, 0xDFE00000 );
	w100fb_gpcntl_write(W100_GPIO_PORT_A, 0xFFFF0000 );
	w100fb_gpio_write(W100_GPIO_PORT_B, 0x00000000 );
	w100fb_gpcntl_write(W100_GPIO_PORT_B, 0xFFFFFFFF );
	save_bright = PWM_PWDUTY1;
#endif

	blueangel_lcd_hw_off();
}

static void
blueangel_lcd_resume_pre(struct w100fb_par *wfb)
{
#if 0
	w100fb_gpio_write(W100_GPIO_PORT_A, ati_gpios[0] );
	w100fb_gpcntl_write(W100_GPIO_PORT_A, ati_gpios[1] );
	w100fb_gpio_write(W100_GPIO_PORT_B, ati_gpios[2] );
	w100fb_gpcntl_write(W100_GPIO_PORT_B, ati_gpios[3] );
	lcd_hw_init();
	LCD_SET_BRIGHT( save_bright );
#endif
	blueangel_lcd_hw_init_pre();
}

static void
blueangel_lcd_resume_post(struct w100fb_par *wfb)
{
	blueangel_lcd_hw_init_post();
}

static void
blueangel_lcd_w100_resume(struct w100fb_par *wfb) {
	blueangel_lcd_resume_pre(wfb);
	msleep(30);
	blueangel_lcd_resume_post(wfb);
}


struct w100_tg_info blueangel_tg_info = {
	.suspend	= blueangel_lcd_suspend,
	.resume		= blueangel_lcd_w100_resume,
//	.resume_pre		= blueangel_lcd_resume_pre,
//	.resume_post		= blueangel_lcd_resume_post,
};

static struct w100_gen_regs blueangel_w100_regs = {
	.lcd_format =        0x00000003,
	.lcdd_cntl1 =        0x00000000,
	.lcdd_cntl2 =        0x0003ffff,
	.genlcd_cntl1 =      0x00fff003,	//  0x00fff003
	.genlcd_cntl2 =      0x00000003,	
	.genlcd_cntl3 =      0x000102aa,
};

static struct w100_mode blueangel_w100_modes_4[] = {
{
	.xres 		= 240,
	.yres 		= 320,
	.left_margin	= 0,
	.right_margin	= 31,
	.upper_margin	= 15,
	.lower_margin	= 0,
	.crtc_ss	= 0x80150014,
	.crtc_ls        = 0xa0fb00f7,
	.crtc_gs	= 0xc0080007,
	.crtc_vpos_gs	= 0x80007,
	.crtc_rev	= 0x0000000a,
	.crtc_dclk	= 0x81700030,
	.crtc_gclk	= 0x8015010f,
	.crtc_goe	= 0x00000000,
	.pll_freq 	= 80,
	.pixclk_divider = 15,
	.pixclk_divider_rotated = 15,
	.pixclk_src     = CLK_SRC_PLL,
	.sysclk_divider = 0,
	.sysclk_src     = CLK_SRC_PLL,
},
};

static struct w100_mode blueangel_w100_modes_5[] = {
{
	.xres 		= 240,
	.yres 		= 320,
	.left_margin	= 0,
	.right_margin	= 31,
	.upper_margin	= 15,
	.lower_margin	= 0,
	.crtc_ss	= 0x80150014,
	.crtc_ls        = 0xa0020110,
	.crtc_gs	= 0xc0890088,
	.crtc_vpos_gs	= 0x01450144,
	.crtc_rev	= 0x0000000a,
	.crtc_dclk	= 0xa1700030,
	.crtc_gclk	= 0x8015010f,
	.crtc_goe	= 0x00000000,
	.pll_freq 	= 57,
	.pixclk_divider = 4,
	.pixclk_divider_rotated = 4,
	.pixclk_src     = CLK_SRC_PLL,
	.sysclk_divider = 0,
	.sysclk_src     = CLK_SRC_PLL,
},
};

static struct w100_mode blueangel_w100_modes_6[] = {
{
	.xres 		= 240,
	.yres 		= 320,
	.left_margin 	= 20,
	.right_margin 	= 19,
	.upper_margin 	= 3,
	.lower_margin 	= 2,
	.crtc_ss	= 0x80150014,
	.crtc_ls	= 0xa0020110,
	.crtc_gs	= 0xc0890088,
	.crtc_vpos_gs	= 0x01450144,
	.crtc_rev	= 0x0000000a,
	.crtc_dclk	= 0xa1700030,
	.crtc_gclk	= 0x8015010f,
	.crtc_goe	= 0x00000000,
	.pll_freq 	= 57,
	.pixclk_divider = 4,
	.pixclk_divider_rotated = 4,
	.pixclk_src     = CLK_SRC_PLL,
	.sysclk_divider = 0,
	.sysclk_src     = CLK_SRC_PLL,
},
};

struct w100_mem_info blueangel_mem_info = {
	.ext_cntl = 0x01040010,
	.sdram_mode_reg = 0x00250000,
	.ext_timing_cntl = 0x00001545,
	.io_cntl = 0x7ddd7333,
	.size = 0x3fffff,
};

struct w100_bm_mem_info blueangel_bm_mem_info = {
	.ext_mem_bw = 0xfbfd2d07,
	.offset = 0x000c0000,
	.ext_timing_ctl = 0x00043f7f,
	.ext_cntl = 0x00000010,
	.mode_reg = 0x006c0000,
	.io_cntl = 0x000e0fff,
	.config = 0x08300562,
};

static struct w100_gpio_regs blueangel_w100_gpio_info = {
	.init_data1 = 0x00000000,	// GPIO_DATA
	.gpio_dir1  = 0xe0000000,	// GPIO_CNTL1
	.gpio_oe1   = 0x003c2000,	// GPIO_CNTL2
	.init_data2 = 0x00000000,	// GPIO_DATA2
	.gpio_dir2  = 0x00000000,	// GPIO_CNTL3
	.gpio_oe2   = 0x00000000,	// GPIO_CNTL4
};

static struct w100fb_mach_info blueangel_fb_info = {
	.tg = &blueangel_tg_info,
	.mem = &blueangel_mem_info, 
	.bm_mem = &blueangel_bm_mem_info, 
	.gpio = &blueangel_w100_gpio_info,
	.regs = &blueangel_w100_regs,
	.modelist = blueangel_w100_modes_6,
	.num_modes = 1,
	.xtal_freq = 16000000,
};


static struct resource blueangel_fb_resources[] = {
	[0] = {
		.start	= BLUEANGEL_ATI_W3200_PHYS,
		.end	= BLUEANGEL_ATI_W3200_PHYS + 0x00ffffff,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device blueangel_fb_device = {
	.name	= "w100fb",
	.id	= 0,
	.dev	= {
		.platform_data = &blueangel_fb_info,
	},
	.num_resources	= ARRAY_SIZE( blueangel_fb_resources ),
	.resource	= blueangel_fb_resources,
};

static int
blueangel_lcd_probe( struct device *dev )
{
	int ret;
	
	printk("in blueangel_lcd_probe\n");

	blueangel_lcd_device = lcd_device_register("w100fb", (void *)&blueangel_fb_info, &blueangel_lcd_props);
	if (IS_ERR(blueangel_lcd_device)) {
		return PTR_ERR(blueangel_lcd_device);
	}

	ret = platform_device_register( &blueangel_fb_device );
	// TODO:
	
	return ret;
}

static int
blueangel_lcd_remove( struct device *dev )
{
	lcd_device_unregister (blueangel_lcd_device);
	platform_device_unregister(&blueangel_fb_device);
	return 0;
}

static int
blueangel_lcd_resume(struct device *dev) {
	blueangel_lcd_hw_init_pre();
	msleep(30);
	blueangel_lcd_hw_init_post();
	return 0;
}


struct platform_driver blueangel_lcd_driver = {
	.driver = {
		.name     = "blueangel-lcd",
		.bus	  = &platform_bus_type,
		.probe    = blueangel_lcd_probe,
		.remove   = blueangel_lcd_remove,
		.resume   = blueangel_lcd_resume,
	}
};


int __init
blueangel_lcd_init (void)
{
	printk("blueangel_lcd_init\n");
	if (! machine_is_blueangel())
		return -ENODEV;

	switch (blueangel_boardid)
	{
	 case 0x4:
		blueangel_fb_info.modelist=blueangel_w100_modes_4;
	 break;
	 case 0x5:
		blueangel_fb_info.modelist=blueangel_w100_modes_5;
	 break;
	 case 0x6:
		blueangel_fb_info.modelist=blueangel_w100_modes_6;
	 break;
	 default:
	 	printk("blueangel lcd_init: unknown boardid=%d. Using 0x6\n",blueangel_boardid);
		blueangel_fb_info.modelist=blueangel_w100_modes_6;
	}

	return platform_driver_register( &blueangel_lcd_driver );
	
	
}

void __exit
blueangel_lcd_exit (void)
{
	platform_driver_unregister( &blueangel_lcd_driver );
}

module_init (blueangel_lcd_init);
module_exit (blueangel_lcd_exit);
MODULE_LICENSE("GPL");
