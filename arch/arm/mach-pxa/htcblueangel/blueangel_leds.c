/*
 * LED interface for Himalaya, the HTC PocketPC.
 *
 * License: GPL
 *
 * Author: Luke Kenneth Casson Leighton, Copyright(C) 2004
 *
 * Copyright(C) 2004, Luke Kenneth Casson Leighton.
 *
 * History:
 *
 * 2004-02-19	Luke Kenneth Casson Leighton	created.
 *
 */
 
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/leds.h>
#include <linux/soc/asic3_base.h>

#include <asm/io.h>
#include <asm/arch/htcblueangel-asic.h>
#include <asm/mach-types.h>


#ifdef DEBUG
#define dprintk(x...) printk(x)
#else
#define dprintk(x...)
#endif

extern struct platform_device blueangel_asic3;
static volatile void *virt;

#define RED_LED 0
#define GREEN_LED 1
#define YELLOW_LED 2
#define BLUE_LED 6
#define WIFI_LED 7
#define PHONE_LED 8
#define KEYB_LED 9
#define VIBRA 10

#define CLOCK_CDEX_LED0      (1 << 6)  /* clock for led 0? */
#define CLOCK_CDEX_LED1      (1 << 7)  /* clock for led 1? */

struct blueangel_led_data {
        int                     hw_num;                 // 0 (amber), 1 (green), 2 (blue)
        int                     duty_time;
        int                     cycle_time;
        int                     registered;
        int                     brightness;
        int                     color;
};

#define to_blueangel_led_data(d) container_of(d, struct blueangel_led_data, props)

static int
led_read(int reg)
{
	volatile unsigned short *addr=((volatile unsigned short *)(virt+0xa));	
	volatile unsigned char *data=((volatile unsigned char *)(virt+0xc));	

	*addr |= 0x80;
	*addr = (*addr & 0xff80) | (reg & 0x7f);

	dprintk("led_read[0x%x]=0x%x\n", reg, *data);	
	return *data;
}

static void
led_write(int reg, int val)
{
	volatile unsigned short *addr=((volatile unsigned short *)(virt+0xa));	
	volatile unsigned short *data=((volatile unsigned short *)(virt+0xc));	

	dprintk("led_write[0x%x]=0x%x\n", reg, val);	
	*addr = (*addr & 0xff80) | (reg & 0x7f);
	*addr &= 0xff7f;

	*data = (*data & 0xff00) | (val & 0xff);

}

void blueangel_set_led (int led, int brightness, int duty_time, int cycle_time)
{
	dprintk("blueangel_set_led\n");
	if (led <= 2) {
		if (brightness) {
			duty_time=(duty_time*128+500)/1000;
			cycle_time=(cycle_time*128+500)/1000;
		} else {
			duty_time=0;
			cycle_time=1;
		}
	}
	if (led == 6 || led == 7 || led==8) {
		if (brightness) {
			duty_time=(duty_time*16+500)/1000;
			cycle_time=(cycle_time*16+500)/1000;
		} else {
			duty_time=0;
			cycle_time=1;
		}
	}
	switch(led) {
	case 0:
		asic3_set_led(&blueangel_asic3.dev, 0, duty_time, cycle_time);
		break;
	case 1:
		asic3_set_led(&blueangel_asic3.dev, 1, duty_time, cycle_time);
		break;
	case 2:
		asic3_set_led(&blueangel_asic3.dev, 0, duty_time, cycle_time);
		asic3_set_led(&blueangel_asic3.dev, 1, duty_time, cycle_time);
		break;
	case 6:
		led_write(0, duty_time);
		led_write(1, cycle_time-duty_time);
		if (brightness) {
			led_write(4, led_read(4) | 0x1);
			led_write(0x20, led_read(0x20) & ~0x4);
		} else {
			led_write(4, led_read(4) &  ~0x1);
			led_write(0x20, led_read(0x20) | 0x4);
		}
		break;
	case 7:
		led_write(2, duty_time);
		led_write(3, cycle_time-duty_time);
		if (brightness) {
			led_write(4, led_read(4) | 0x2);
			led_write(0x20, led_read(0x20) & ~0x8);
		} else {
			led_write(4, led_read(4) &  ~0x2);
			led_write(0x20, led_read(0x20) | 0x8);
		}
		break;
	case 8:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_PHONEL_PWR_ON, (brightness > 0) ? GPIOB_PHONEL_PWR_ON : 0);
		break;
	case 9:
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_KEYBL_PWR_ON, (brightness > 0) ? GPIOC_KEYBL_PWR_ON : 0);
		break;
	case 10:
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_VIBRA_PWR_ON, (brightness > 0) ? GPIOB_VIBRA_PWR_ON : 0);
		break;
	}
}

EXPORT_SYMBOL(blueangel_set_led);

static void htcblueangel_led_red_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(RED_LED,100,1000,1000);
	else
		blueangel_set_led(RED_LED,0,1000,1000);
}

static void htcblueangel_led_green_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(GREEN_LED,100,1000,1000);
	else
		blueangel_set_led(GREEN_LED,0,1000,1000);
}

static void htcblueangel_led_yellow_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(YELLOW_LED,100,1000,1000);
	else
		blueangel_set_led(YELLOW_LED,0,1000,1000);
}

static void htcblueangel_led_blue_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(BLUE_LED,100,1000,1000);
	else
		blueangel_set_led(BLUE_LED,0,1000,1000);
}
static void htcblueangel_led_wifi_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(WIFI_LED,100,1000,1000);
	else
		blueangel_set_led(WIFI_LED,0,1000,1000);
}

static void htcblueangel_led_phone_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(PHONE_LED,100,1000,1000);
	else
		blueangel_set_led(PHONE_LED,0,1000,1000);
}

static void htcblueangel_led_keyb_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(KEYB_LED,100,1000,1000);
	else
		blueangel_set_led(KEYB_LED,0,1000,1000);
}

static void htcblueangel_led_vibra_set(struct led_classdev *led_cdev,
                                        enum led_brightness value)
{
	if (value)
		blueangel_set_led(VIBRA,100,1000,1000);
	else
		blueangel_set_led(VIBRA,0,1000,1000);
}

static struct led_classdev htcblueangel_yellow_led = {
        .name                   = "yellow",
        .default_trigger        = "charge",
        .brightness_set         =  htcblueangel_led_yellow_set,
};

static struct led_classdev htcblueangel_red_led = {
        .name                   = "red",
        .default_trigger        = "message",
        .brightness_set         =  htcblueangel_led_red_set,
};
static struct led_classdev htcblueangel_green_led = {
        .name                   = "green",
        .default_trigger        = "phone",
        .brightness_set         =  htcblueangel_led_green_set,
};
static struct led_classdev htcblueangel_blue_led = {
        .name                   = "blue",
        .default_trigger        = "bluetooth",
        .brightness_set         =  htcblueangel_led_blue_set,
};
static struct led_classdev htcblueangel_wifi_led = {
        .name                   = "wifi",
        .default_trigger        = "wifi",
        .brightness_set         =  htcblueangel_led_wifi_set,
};
static struct led_classdev htcblueangel_phone_led = {
        .name                   = "phone",
        .default_trigger        = "call",
        .brightness_set         =  htcblueangel_led_phone_set,
};
static struct led_classdev htcblueangel_keyb_led = {
        .name                   = "keyboard",
        .default_trigger        = "keyboard",
        .brightness_set         =  htcblueangel_led_keyb_set,
};
static struct led_classdev htcblueangel_vibra_led = {
        .name                   = "vibra",
//        .default_trigger        = "keyboard",
        .brightness_set         =  htcblueangel_led_vibra_set,
};

static int blueangel_led_probe(struct platform_device *pdev)
{
	int ret=0;

	printk("blueangel_led_probe\n");
        /* Turn on the LED controllers in CDEX */
        asic3_set_clock_cdex(&blueangel_asic3.dev,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2
        );
	blueangel_set_led(RED_LED, 0, 0, 0);
	blueangel_set_led(GREEN_LED, 0, 0, 0);
	blueangel_set_led(YELLOW_LED, 0, 0, 0);
	blueangel_set_led(BLUE_LED, 0, 0, 0);
	blueangel_set_led(WIFI_LED, 0, 0, 0);
	blueangel_set_led(PHONE_LED, 0, 0, 0);
	blueangel_set_led(KEYB_LED, 0, 0, 0);
	blueangel_set_led(VIBRA, 0, 0, 0);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_red_led);
        if (ret < 0)
	        led_classdev_unregister(&htcblueangel_red_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_green_led);
        if (ret < 0)
	        led_classdev_unregister(&htcblueangel_green_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_yellow_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_yellow_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_blue_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_blue_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_wifi_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_wifi_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_phone_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_phone_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_keyb_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_keyb_led);

        ret = led_classdev_register(&pdev->dev, &htcblueangel_vibra_led);
        if (ret < 0)
        	led_classdev_unregister(&htcblueangel_vibra_led);

	return ret;
}

static int blueangel_led_remove(struct platform_device *pdev)
{

	dprintk("blueangel_led_remove\n");
        led_classdev_unregister(&htcblueangel_yellow_led);
        led_classdev_unregister(&htcblueangel_green_led);
        led_classdev_unregister(&htcblueangel_red_led);
       	led_classdev_unregister(&htcblueangel_blue_led);
       	led_classdev_unregister(&htcblueangel_wifi_led);
       	led_classdev_unregister(&htcblueangel_phone_led);
       	led_classdev_unregister(&htcblueangel_keyb_led);
       	led_classdev_unregister(&htcblueangel_vibra_led);
	return 0;
}

static int 
blueangel_led_suspend(struct platform_device *dev, pm_message_t state)
{
//	if (level == SUSPEND_POWER_DOWN) {
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_PHONEL_PWR_ON, 0);
		asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_KEYBL_PWR_ON, 0);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_VIBRA_PWR_ON, 0);
       	led_classdev_suspend(&htcblueangel_red_led);
       	led_classdev_suspend(&htcblueangel_green_led);
       	led_classdev_suspend(&htcblueangel_yellow_led);
       	led_classdev_suspend(&htcblueangel_blue_led);
       	led_classdev_suspend(&htcblueangel_wifi_led);
       	led_classdev_suspend(&htcblueangel_phone_led);
       	led_classdev_suspend(&htcblueangel_keyb_led);
       	led_classdev_suspend(&htcblueangel_vibra_led);

//	}
	return 0;
}

static int
blueangel_led_resume(struct platform_device *pdev)
{
//	if (level == RESUME_POWER_ON) {
		//asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_PHONEL_PWR_ON, leds[3].brightness ? GPIOB_PHONEL_PWR_ON : 0);
		//asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_KEYBL_PWR_ON, leds[4].brightness ? GPIOC_KEYBL_PWR_ON : 0);
		//asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_VIBRA_PWR_ON, leds[5].brightness ? GPIOB_VIBRA_PWR_ON : 0);
       	led_classdev_resume(&htcblueangel_red_led);
       	led_classdev_resume(&htcblueangel_green_led);
       	led_classdev_resume(&htcblueangel_yellow_led);
       	led_classdev_resume(&htcblueangel_blue_led);
       	led_classdev_resume(&htcblueangel_wifi_led);
       	led_classdev_resume(&htcblueangel_phone_led);
       	led_classdev_resume(&htcblueangel_keyb_led);
       	led_classdev_resume(&htcblueangel_vibra_led);
//	}
	
	return 0;
}


static struct platform_driver blueangel_led_driver = {
	.probe   = blueangel_led_probe,
	.remove  = blueangel_led_remove,
	.suspend = blueangel_led_suspend,
	.resume  = blueangel_led_resume,
	.driver  = {
		.name = "htcblueangel-led",
	}, 

};

static int blueangel_led_init (void)
{
	int ret = 0;

	if (! machine_is_blueangel() )
		return -ENODEV;

	printk("Register HTC Blueangel LED class module\r\n");
	virt=ioremap(0x11000000, 4096);
	//printk("0x%x 0x%x 0x%x 0x%x 0x%x\n", led_read(0), led_read(1), led_read(2), led_read(3), led_read(4));
	//printk("0x%x 0x%x 0x%x\n", led_read(0x20), led_read(0x21), led_read(0x22));
	return platform_driver_register(&blueangel_led_driver);
}

static void blueangel_led_exit (void)
{
	printk("Unregister HTC Blueangel LED class module\r\n");
	iounmap(virt);
 	platform_driver_unregister(&blueangel_led_driver);
}

module_init (blueangel_led_init);
module_exit (blueangel_led_exit);

MODULE_LICENSE("GPL");
