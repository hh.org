/*
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * 2006-10-30: Moved udc code from blueangel.c
 *             (Michael Horne <asylumed@gmail.com>)
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/udc.h>

#include <asm/hardware/ipaq-asic3.h>
#include <linux/soc/asic3_base.h>

#include <asm/arch/htcblueangel-gpio.h>
#include <asm/arch/htcblueangel-asic.h>

extern struct platform_device blueangel_asic3;

static int blueangel_udc_is_connected(void) {
	int ret = !(GPLR(GPIO_NR_BLUEANGEL_USB_DETECT_N) & GPIO_bit(GPIO_NR_BLUEANGEL_USB_DETECT_N));
	printk("udc_is_connected returns %d\n",ret);

	return ret;
}

static void blueangel_udc_command(int cmd) {
	switch(cmd){
		case PXA2XX_UDC_CMD_DISCONNECT:
			printk("_udc_control: disconnect\n");
			asic3_set_gpio_dir_c(&blueangel_asic3.dev, GPIOC_USB_PULLUP_N, GPIOC_USB_PULLUP_N);
			asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_USB_PULLUP_N, GPIOC_USB_PULLUP_N);
                break;
                case PXA2XX_UDC_CMD_CONNECT:
			printk("_udc_control: connect\n");
			asic3_set_gpio_dir_c(&blueangel_asic3.dev, GPIOC_USB_PULLUP_N, GPIOC_USB_PULLUP_N);
			asic3_set_gpio_out_c(&blueangel_asic3.dev, GPIOC_USB_PULLUP_N, 0);
		break;
		default:
			printk("_udc_control: unknown command!\n");
		break;
	}
}

static struct pxa2xx_udc_mach_info blueangel_udc_mach_info = {
	.udc_is_connected = blueangel_udc_is_connected,
	.udc_command      = blueangel_udc_command,
};

static int blueangel_udc_probe(struct device *dev)
{
	printk("blueangel udc register");

    	pxa_set_udc_info(&blueangel_udc_mach_info);
	return 0;
}

static struct platform_driver blueangel_udc_driver = {
	.driver   = {
	        .probe   = blueangel_udc_probe,
		.name    = "blueangel-udc",
	}
};

static int blueangel_udc_init(void) {
	return platform_driver_register(&blueangel_udc_driver);
}

module_init(blueangel_udc_init);

MODULE_AUTHOR("Michael Horne <asylumed@gmail.com>");
MODULE_DESCRIPTION("UDC init for the htc blueangel");
MODULE_LICENSE("GPL");
