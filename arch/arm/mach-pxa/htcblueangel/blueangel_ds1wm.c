/*
 * blueangel_ds1wm.c - HTC Blueangel DS1WM-in-AICr2 driver
 *
 * Copyright (C) 2006 Philipp Zabel <philipp.zabel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>

#include <asm/arch/pxa-regs.h>
#include <linux/io.h>
#include <asm/arch/htcblueangel-asic.h>
#include <linux/soc/asic3_base.h>

#include <linux/irq.h>
#include <linux/interrupt.h>

#include <linux/clk.h>
#include <asm/arch/clock.h>
#include "../../../../drivers/w1/masters/ds1wm.h"

extern struct platform_device blueangel_asic3;
unsigned int __iomem *iobase;


static int read_register(int reg)
{
        volatile unsigned short *addr=((volatile unsigned short *)(iobase+0xa));
        volatile unsigned char *data=((volatile unsigned char *)(iobase+0xc));

        *addr |= 0x80;
        *addr = (*addr & 0xff80) | (reg & 0x7f);

        printk("read_register[0x%x]=0x%x\n", reg, *data);
        return *data;
}

static void write_register(int reg, int val)
{
        volatile unsigned short *addr=((volatile unsigned short *)(iobase+0xa));
        volatile unsigned short *data=((volatile unsigned short *)(iobase+0xc));

        printk("write_register[0x%x]=0x%x\n", reg, val);
        *addr = (*addr & 0xff80) | (reg & 0x7f);
        *addr &= 0xff7f;

        *data = (*data & 0xff00) | (val & 0xff);

}

static struct resource blueangel_ds1wm_resources[] = {
	[0] = {
		.start  = 0x11000000,
		.end	= 0x11000000 + 0x08,
		.flags  = IORESOURCE_MEM,
	},
/*
	[1] = {
		.start  = IRQ_GPIO(113),
		.end    = IRQ_GPIO(113),
		.flags  = IORESOURCE_IRQ,

	}
*/
};

static struct ds1wm_platform_data blueangel_ds1wm_platform_data = {
	.bus_shift = 1,
};

static struct platform_device *blueangel_ds1wm;
#if 0
 = {
	.name		= "ds1wm",
	.id		= -1,
	.num_resources	= ARRAY_SIZE(blueangel_ds1wm_resources),
	.resource	= blueangel_ds1wm_resources,
	.dev =  {
		.platform_data = blueangel_ds1wm_platform_data,
		},
};
#endif

static void blueangel_ds1wm_enable(struct clk *clock)
{
	/* I don't know how to enable the 4MHz OWM clock here */
	asic3_set_gpio_out_b(&blueangel_asic3.dev,0x0400, 0);
	//asic3_set_gpio_out_c(&blueangel_asic3.dev,11, 1);
	//asic3_set_gpio_out_a(&blueangel_asic3.dev,13, 1);

	printk ("blueangel_ds1wm: OWM_EN Low (active)\n");
}

static void blueangel_ds1wm_disable(struct clk *clock)
{
	asic3_set_gpio_out_b(&blueangel_asic3.dev,0x0400,1);
	//asic3_set_gpio_out_c(&blueangel_asic3.dev,11,0);
	//asic3_set_gpio_out_a(&blueangel_asic3.dev,13, 0);

	printk ("blueangel_ds1wm: GPIOB10 High (inactive)\n");

	/* I don't know how to disable the 4MHz OWM clock here */
}

static struct clk ds1wm_clk = {
        .name    = "ds1wm",
        .rate    = 4000000,
        .parent  = NULL,
	.enable  = blueangel_ds1wm_enable,
	.disable = blueangel_ds1wm_disable,
};

/*
 * This handles the interrupt from one port.
 */
static irqreturn_t
ds1wm_irq(int irq, void *dev_id, struct pt_regs *regs)
{
        printk("DS1WM IRQ %d\n",irq);
        //receive_chars(dev_id);
        return IRQ_HANDLED;
}

static int __devinit blueangel_ds1wm_init(void)
{
        int ret;
	int base_irq;
	printk("HTC Blueangel DS1WM driver\n");
	iobase = ioremap_nocache( 0x11000000, 0x0c); // map PASIC3 for EGPIO registers
	base_irq = asic3_irq_base(&blueangel_asic3.dev);
        set_irq_type(base_irq + _IPAQ_ASIC3_GPIO_BANK_D + 2, IRQ_TYPE_EDGE_FALLING);
        ret = request_irq(base_irq + _IPAQ_ASIC3_GPIO_BANK_D + 2, NULL, 0, "htcblueangel_ds1wm", &blueangel_ds1wm);
        if (ret) {
		printk("blueangel_ds1wm: failed to register irq to ASIC3 device\n");
	}


	if (clk_register(&ds1wm_clk) < 0)
		printk(KERN_ERR "failed to register DS1WM clock\n");

        blueangel_ds1wm = platform_device_alloc("ds1wm", -1);
        if (!blueangel_ds1wm) {
		printk("blueangel_ds1wm: failed to allocate platform device\n");
		ret = -ENOMEM;
		goto exit_unmap;
	}

	blueangel_ds1wm->num_resources = ARRAY_SIZE(blueangel_ds1wm_resources);
	blueangel_ds1wm->resource = blueangel_ds1wm_resources;
        blueangel_ds1wm->dev.platform_data = &blueangel_ds1wm_platform_data;
        ret = platform_device_add(blueangel_ds1wm);

        if (ret) {
                platform_device_put(blueangel_ds1wm);
		printk("blueangel_ds1wm: failed to add platform device\n");
	}

        return ret;

exit_unmap:
	iounmap((void __iomem *)iobase);
	return ret;
}

static void __devexit blueangel_ds1wm_exit(void)
{
	platform_device_unregister(blueangel_ds1wm);
	iounmap((void __iomem *)iobase);
}

MODULE_AUTHOR("Philipp Zabel <philipp.zabel@gmail.com>");
MODULE_DESCRIPTION("DS1WM driver");
MODULE_LICENSE("GPL");

module_init(blueangel_ds1wm_init);
module_exit(blueangel_ds1wm_exit);
