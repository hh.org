/*
 * Hardware definitions for HTC Blueangel
 *
 * Copyright 2004 Xanadux.org
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Authors: w4xy@xanadux.org
 *
 * History:
 *
 * 2004-02-07	W4XY		   Initial port heavily based on h1900.c
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/input.h>                                                                                                                         
#include <linux/input_pda.h>   
#include <linux/pm.h>
#include <linux/bootmem.h>
#include <linux/platform_device.h>

#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>
#include <asm/types.h>
#include <asm/delay.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irq.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/udc.h>

#include <asm/hardware/ipaq-ops.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/hardware/gpio_keys.h> 
#include <asm/hardware/asic3_keys.h>
#include <asm/arch/htcblueangel-gpio.h>
#include <asm/arch/htcblueangel-asic.h>
#include <linux/serial_core.h>
#include <asm-arm/arch-pxa/serial.h>

#include "../generic.h"

#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/soc/asic3_base.h>


struct ipaq_model_ops ipaq_model_ops;
EXPORT_SYMBOL(ipaq_model_ops);

extern struct platform_device blueangel_asic3;
extern struct platform_device blueangel_tsc2200;
struct platform_pxa_serial_funcs pxa_serial_funcs [] = {
	{}, /* No special FFUART options */
	{}, /* No special BTUART options */
	{}, /* No special STUART options */
	{}, /* No special HWUART options */
};

static struct ipaq_model_ops blueangel_model_ops __initdata = {
      .generic_name = "blueangel",
};


static void __init blueangel_init_irq( void )
{
	/* Initialize standard IRQs */
	pxa_init_irq();
}

static void ser_stuart_gpio_config(int enable)
{
	printk("ser_stuart_gpio_config %d\n", enable);
	if (enable == PXA_UART_CFG_PRE_STARTUP) {
    STISR=0;
  }
}

static void ser_hwuart_gpio_config(int enable)
{
	printk("ser_hwuart_gpio_config %d\n", enable);
	if (enable == PXA_UART_CFG_PRE_STARTUP) {
		GPDR(GPIO42_HWRXD) &= ~(GPIO_bit(GPIO42_HWRXD));
		GPDR(GPIO43_HWTXD) |= GPIO_bit(GPIO43_HWTXD);
		GPDR(GPIO44_HWCTS) &= ~(GPIO_bit(GPIO44_HWCTS));
		GPDR(GPIO45_HWRTS) |= GPIO_bit(GPIO45_HWRTS);
		pxa_gpio_mode(GPIO42_HWRXD_MD);
		pxa_gpio_mode(GPIO43_HWTXD_MD);
		pxa_gpio_mode(GPIO44_HWCTS_MD);
		pxa_gpio_mode(GPIO45_HWRTS_MD);
		asic3_set_gpio_dir_a(&blueangel_asic3.dev, GPIOA_BT_PWR1_ON, GPIOA_BT_PWR1_ON);
		asic3_set_gpio_out_a(&blueangel_asic3.dev, GPIOA_BT_PWR1_ON, GPIOA_BT_PWR1_ON);
		asic3_set_gpio_dir_b(&blueangel_asic3.dev, GPIOB_BT_PWR2_ON, GPIOB_BT_PWR2_ON);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_BT_PWR2_ON, GPIOB_BT_PWR2_ON);
  } else if (enable == PXA_UART_CFG_POST_SHUTDOWN) {
		asic3_set_gpio_out_a(&blueangel_asic3.dev, GPIOA_BT_PWR1_ON, 0);
		asic3_set_gpio_out_b(&blueangel_asic3.dev, GPIOB_BT_PWR2_ON, 0);
	}
}

int blueangel_boardid;
EXPORT_SYMBOL(blueangel_boardid);

static void
blueangel_get_boardid(void)
{
	int i;

	int save_gpdr=GPDR(GPIO_NR_BLUEANGEL_BOARDID3);
	GPDR(GPIO_NR_BLUEANGEL_BOARDID3) &= ~(GPIO_bit(GPIO_NR_BLUEANGEL_BOARDID3));
	for (i = 0 ; i < 1000; i++);
	blueangel_boardid=0;
	if (GPLR(GPIO_NR_BLUEANGEL_BOARDID0) & GPIO_bit(GPIO_NR_BLUEANGEL_BOARDID0))
		blueangel_boardid |= 1;
	if (GPLR(GPIO_NR_BLUEANGEL_BOARDID1) & GPIO_bit(GPIO_NR_BLUEANGEL_BOARDID1))
		blueangel_boardid |= 2;
	if (GPLR(GPIO_NR_BLUEANGEL_BOARDID2) & GPIO_bit(GPIO_NR_BLUEANGEL_BOARDID2))
		blueangel_boardid |= 4;
	if (GPLR(GPIO_NR_BLUEANGEL_BOARDID3) & GPIO_bit(GPIO_NR_BLUEANGEL_BOARDID3))
		blueangel_boardid |= 8;
	GPDR(GPIO_NR_BLUEANGEL_BOARDID3)=save_gpdr;
	printk("Blue Angel Board ID 0x%x\n", blueangel_boardid);
	system_rev=blueangel_boardid;
}

/*
 * Common map_io initialization
 */
static void __init blueangel_map_io(void)
{
	int i;

	pxa_map_io();
	blueangel_get_boardid();

#if 0
	PGSR0 = GPSRx_SleepValue;
	PGSR1 = GPSRy_SleepValue;
	PGSR2 = GPSRz_SleepValue;
#endif

	STISR=0;	/* Disable UART mode of STUART */
	printk("CKEN=0x%x CKEN11_USB=0x%x\n", CKEN, CKEN11_USB);	
	pxa_set_cken(CKEN11_USB, 1);
	printk("CKEN=0x%x\n", CKEN);	
#if 0

	GAFR0_L = 0x98000000;
	GAFR0_U = 0x494A8110;
	GAFR1_L = 0x699A8159;
	GAFR1_U = 0x0005AAAA;
	GAFR2_L = 0xA0000000;
	GAFR2_U = 0x00000002;

	/* don't do these for now because one of them turns the screen to mush */
	/* reason: the ATI chip gets reset / LCD gets disconnected:
	 * a fade-to-white means that the ati 3200 registers are set incorrectly */
	GPCR0   = 0xFF00FFFF;
	GPCR1   = 0xFFFFFFFF;
	GPCR2   = 0xFFFFFFFF;

	GPSR0   = 0x444F88EF;
	GPSR1   = 0x57BF7306;
	GPSR2   = 0x03FFE008;
	PGSR0   = 0x40DF88EF;
	PGSR1   = 0x53BF7206;
	PGSR2   = 0x03FFE000;
	GPDR0   = 0xD7E9A042;
	GPDR1   = 0xFCFFABA3;
	GPDR2   = 0x000FEFFE;
	GPSR0   = 0x444F88EF;
	GPSR1   = 0xD7BF7306;
	GPSR2   = 0x03FFE008;
	GRER0   = 0x00000000;
	GRER1   = 0x00000000;
	GRER2   = 0x00000000;
	GFER0   = 0x00000000;
	GFER1   = 0x00000000;
	GFER2   = 0x00000000;
#endif


	pxa_serial_funcs[2].configure = ser_stuart_gpio_config;
	pxa_serial_funcs[3].configure = ser_hwuart_gpio_config;
	stuart_device.dev.platform_data = &pxa_serial_funcs[2];		
	hwuart_device.dev.platform_data = &pxa_serial_funcs[3];

	/* disable all ongoing DMA */
	for(i = 0; i < 16; i++) DCSR(i) = 7;

#if 0
	/* Add wakeup on AC plug/unplug (and resume button) */
	PWER = PWER_RTC | PWER_GPIO4 | PWER_GPIO0;
	PFER = PWER_RTC | PWER_GPIO4 | PWER_GPIO0;
	PRER =            PWER_GPIO4 | PWER_GPIO0;
	PCFR = PCFR_OPDE;
#endif

	ipaq_model_ops = blueangel_model_ops;
}

/* 
 * All the asic3 dependant devices 
 */

extern struct platform_device blueangel_bl;
static struct platform_device blueangel_lcd = { .name = "blueangel-lcd", };
static struct platform_device blueangel_udc = { .name = "blueangel-udc", };
static struct platform_device blueangel_leds = { .name = "blueangel-leds", };

/*
 *  ASIC3 buttons
 */

static struct asic3_keys_button blueangel_asic3_keys_table[] = {
	{KEY_RECORD,		BLUEANGEL_RECORD_BTN_IRQ,	1,	"Record Button"}, 
	{KEY_VOLUMEUP,		BLUEANGEL_VOL_UP_BTN_IRQ,	1,	"Volume Up Button"}, 
	{KEY_VOLUMEDOWN,	BLUEANGEL_VOL_DOWN_BTN_IRQ,	1,	"Volume Down Button"}, 
	{KEY_CAMERA,		BLUEANGEL_CAMERA_BTN_IRQ,	1,	"Camera Button"}, 
	{KEY_MENU,		BLUEANGEL_WINDOWS_BTN_IRQ,	1,	"Windows Button"}, 
	{KEY_EMAIL,		BLUEANGEL_MAIL_BTN_IRQ,		1,	"Mail Button"}, 
	{KEY_WWW,		BLUEANGEL_WWW_BTN_IRQ,		1,	"Internet Button"}, 
	{KEY_KPENTER,		BLUEANGEL_OK_BTN_IRQ,		1,	"Ok Button"}, 

};

static struct asic3_keys_platform_data blueangel_asic3_keys_data = {
	.buttons	= blueangel_asic3_keys_table,
	.nbuttons	= ARRAY_SIZE(blueangel_asic3_keys_table),
	.asic3_dev	= &blueangel_asic3.dev,
};

static struct platform_device blueangel_asic3_keys = {
	.name		= "asic3-keys",
	.dev = {
	    .platform_data = &blueangel_asic3_keys_data,
	}
};

 
static struct platform_device *blueangel_asic3_devices[] __initdata = {
	&blueangel_lcd,
	&blueangel_udc,
#ifdef CONFIG_MACH_BLUEANGEL_BACKLIGHT
	&blueangel_bl,
#endif
	&blueangel_asic3_keys,
	&blueangel_leds,
};


/*
 * the ASIC3 should really only be referenced via the asic3_base
 * module.  it contains functions something like asic3_gpio_b_out()
 * which should really be used rather than macros.
 */

static struct asic3_platform_data asic3_platform_data_6 = {
	.gpio_a = {
		.dir		= 0xbffd,
		.init		= 0x0110,
		.sleep_out	= 0x0010,
		.batt_fault_out	= 0x0010,
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x0008,
		.alt_function	= 0x9800, /* Caution: need to be set to a correct value */
	},
	.gpio_b = {
		.dir		= 0xfffc,
		.init		= 0x48f8,
		.sleep_out	= 0x0000,
		.batt_fault_out	= 0x0000,
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
	},
	.gpio_c = {
		.dir		= 0xfff7,
		.init		= 0xc344,            
		.sleep_out	= 0x04c4,            
		.batt_fault_out	= 0x0484,            
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x003b, /* Caution: need to be set to a correct value */
	},
	.gpio_d = {
		.dir		= 0x0040,
		.init		= 0x3e1b,            
		.sleep_out	= 0x3e1b,            
		.batt_fault_out = 0x3e1b,  
		.sleep_mask	= 0x0000,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
	},
	.bus_shift=1,
	.child_platform_devs     = blueangel_asic3_devices,                                                                      
	.num_child_platform_devs = ARRAY_SIZE(blueangel_asic3_devices),       
};

static struct asic3_platform_data asic3_platform_data_o = {
	.gpio_a = {
		.dir		= 0xbffd,
		.init		= 0x0110,
		.sleep_out	= 0x0010,
		.batt_fault_out	= 0x0010,
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x0008,
		.alt_function	= 0x9800, /* Caution: need to be set to a correct value */
	},
	.gpio_b = {
		.dir		= 0xfffc,
		.init		= 0x40fc,
		.sleep_out	= 0x0000,
		.batt_fault_out	= 0x0000,
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
	},
	.gpio_c = {
		.dir		= 0xfff7,
		.init		= 0xc344,            
		.sleep_out	= 0x04c4,            
		.batt_fault_out	= 0x0484,            
		.sleep_mask	= 0xffff,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x003b, /* Caution: need to be set to a correct value */
	},
	.gpio_d = {
		.dir		= 0x0040,
		.init		= 0x3e1b,            
		.sleep_out	= 0x3e1b,            
		.batt_fault_out = 0x3e1b,  
		.sleep_mask	= 0x0000,
		.sleep_conf	= 0x000c,
		.alt_function	= 0x0000, /* Caution: need to be set to a correct value */
	},
	.bus_shift=1,
	.child_platform_devs     = blueangel_asic3_devices,                                                                      
	.num_child_platform_devs = ARRAY_SIZE(blueangel_asic3_devices),       
};

static struct resource asic3_resources[] = {
	[0] = {
		.start  = BLUEANGEL_ASIC3_GPIO_PHYS,
		.end    = BLUEANGEL_ASIC3_GPIO_PHYS + 0xfffff,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_NR_BLUEANGEL_ASIC3,
		.end    = IRQ_NR_BLUEANGEL_ASIC3,
		.flags  = IORESOURCE_IRQ,
	},
	[2] = {
		.start  = BLUEANGEL_ASIC3_MMC_PHYS,
		.end    = BLUEANGEL_ASIC3_MMC_PHYS + IPAQ_ASIC3_MAP_SIZE,
		.flags  = IORESOURCE_MEM,
	},
	[3] = {
		.start  = IRQ_GPIO(GPIO_NR_BLUEANGEL_SD_IRQ_N),
		.flags  = IORESOURCE_IRQ,
	},
};

struct platform_device blueangel_asic3 = {
	.name       = "asic3",
	.id     = 0,
	.num_resources  = ARRAY_SIZE(asic3_resources),
	.resource   = asic3_resources,
};
EXPORT_SYMBOL(blueangel_asic3);

/*
 * Magician LEDs
 */
static struct platform_device blueangel_led = {
        .name   = "htcblueangel-led",
        .id     = -1,
};
/*
*
*/


/*
 * GPIO buttons
 */
static struct gpio_keys_button blueangel_button_table[] = {                                                                                          
    { _KEY_POWER, GPIO_NR_BLUEANGEL_POWER_BUTTON_N, 1 },                                                                                           
};                                                                                                                                               

static struct gpio_keys_platform_data blueangel_pxa_keys_data = {                                                                                    
     .buttons = blueangel_button_table,                                                                                                           
     .nbuttons = ARRAY_SIZE(blueangel_button_table),                                                                                              
};                                                                                                                                               
                                                                                                                                                  
static struct platform_device blueangel_pxa_keys = {
    .name = "gpio-keys",
    .dev = {
	.platform_data = &blueangel_pxa_keys_data,
    },
};

/*
 *
 */

static struct platform_device *devices[] __initdata = {
	&blueangel_asic3,
	&blueangel_led,
	&blueangel_tsc2200,
	&blueangel_pxa_keys,
};

void blueangel_hereiam(void) {
//	asic3_set_gpio_out_b(&blueangel_asic3.dev, 0x8, 0xfb0);
}

static void __init blueangel_init(void)
{
	switch (blueangel_boardid)
	{
	 case 6: 
		blueangel_asic3.dev.platform_data=&asic3_platform_data_6;
         break;
         case 4:
         case 5:
		blueangel_asic3.dev.platform_data=&asic3_platform_data_o;
         break;
         default:
		blueangel_asic3.dev.platform_data=&asic3_platform_data_o;
        }

	platform_add_devices (devices, ARRAY_SIZE (devices));
	
}

MACHINE_START(BLUEANGEL, "HTC Blueangel")
        /* Maintainer xanadux.org */
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
        .boot_params	= 0xa0000100,
        .map_io		= blueangel_map_io,
        .init_irq	= blueangel_init_irq,
        .timer 		= &pxa_timer,
        .init_machine 	= blueangel_init,
MACHINE_END

