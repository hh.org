/*
 * Bluetooth support file for calling bluetooth configuration functions
 *
 * Copyright (c) 2005 SDG Systems, LLC
 *
 * 2005-06	Todd Blumer             Initial Revision
 */

#ifndef _HX4700_BT_H
#define _HX4700_BT_H

struct hx4700_bt_funcs {
	void (*configure) ( int state );
};


#endif
