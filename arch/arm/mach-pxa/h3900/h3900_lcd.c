/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 * History:
 *
 * 2006-09-25	Paul Sokolovsky    Split backlight code out to ease maintenance
 * 2003-05-14	Joshua Wise        Adapted for the HP iPAQ H1900
 * 2002-08-23   Jamey Hicks        Adapted for use with PXA250-based iPAQs
 * 2001-10-??   Andrew Christian   Added support for iPAQ H3800
 *                                 and abstracted EGPIO interface.
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach/arch.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/h3900-asic.h>
#include <asm/arch/ipaq.h>

#include <linux/soc/asic2_base.h>
#include <linux/soc/asic3_base.h>

extern struct platform_device h3900_asic2, h3900_asic3;

/*
  On screen enable, we get 
  
     h3800_lcd_power_on(1)
     LCD controller starts
     h3800_lcd_enable(1)

  On screen disable, we get
  
     h3800_lcd_enable(0)
     LCD controller stops
     h3800_lcd_power_on(0)
*/

static int h3900_lcd_set_power( struct lcd_device *lm, int level )
{
	if ( level < 1 ) {
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_ON, GPIO3_LCD_ON);
		mdelay(30);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_NV_ON, GPIO3_LCD_NV_ON);
		mdelay(5);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_9V_ON, GPIO3_LCD_9V_ON);
		mdelay(50);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_5V_ON, GPIO3_LCD_5V_ON);
		mdelay(5);
	} else {
		mdelay(5);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_5V_ON, 0);
		mdelay(50);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_9V_ON, 0);
		mdelay(5);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_NV_ON, 0);
		mdelay(100);
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_ON, 0);
	}

	if ( level < 4 ) {
		mdelay(17);     // Wait one from before turning on
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_PCI, GPIO3_LCD_PCI);
	} else {
		asic3_set_gpio_out_b(&h3900_asic3.dev, GPIO3_LCD_PCI, 0);
		mdelay(30);     // Wait before turning off
	}

	return 0;
}

static int h3900_lcd_get_power( struct lcd_device *lm )
{
	if (asic3_get_gpio_out_b(&h3900_asic3.dev) & GPIO3_LCD_PCI) {
		if (asic3_get_gpio_out_b(&h3900_asic3.dev) & GPIO3_LCD_ON)
			return 0;
		else
			return 2;
	} else
		return 4;
}

static struct lcd_properties h3900_lcd_properties = {
	.owner		= THIS_MODULE,
	.set_power	= h3900_lcd_set_power,
	.get_power	= h3900_lcd_get_power,
};

static struct lcd_device *pxafb_lcd_device;

static int h3900_lcd_probe(struct device * dev)
{
	pxafb_lcd_device = lcd_device_register("pxafb", NULL, &h3900_lcd_properties);
	if (IS_ERR (pxafb_lcd_device))
		return PTR_ERR (pxafb_lcd_device);

	h3900_lcd_set_power (pxafb_lcd_device, 0);

        return 0;
}

static int h3900_lcd_remove(struct device * dev)
{
	h3900_lcd_set_power(pxafb_lcd_device, 4);
        lcd_device_unregister(pxafb_lcd_device);

        return 0;
}

static int h3900_lcd_suspend(struct device * dev, pm_message_t state)
{
        h3900_lcd_set_power(pxafb_lcd_device, 4);
        return 0;
}

static int h3900_lcd_resume(struct device * dev)
{
        h3900_lcd_set_power(pxafb_lcd_device, 0);
        return 0;
}

static struct device_driver h3900_lcd_driver = {
        .name     = "h3900_lcd",
        .bus      = &platform_bus_type,
        .probe    = h3900_lcd_probe,
        .remove   = h3900_lcd_remove,
        .suspend  = h3900_lcd_suspend,
        .resume   = h3900_lcd_resume,
};


static int
h3900_lcd_init (void)
{
	if (!machine_is_h3900())
		return -ENODEV;

        return driver_register(&h3900_lcd_driver);

	return 0;
}

static void
h3900_lcd_exit (void)
{
	lcd_device_unregister(pxafb_lcd_device);
        driver_unregister(&h3900_lcd_driver);
}

module_init (h3900_lcd_init);
module_exit (h3900_lcd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org> and others");
MODULE_DESCRIPTION("iPAQ h3900 LCD driver glue");
