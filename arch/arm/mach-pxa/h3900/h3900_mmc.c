/*
 * MMC glue driver for iPAQ h3900
 *
 * Copyright (c) 2005 Phil Blundell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/err.h>
#include <linux/soc-old.h>
#include <linux/soc/tmio_mmc.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/mach/arch.h>
#include <asm/arch/h3900-asic.h>

#include <linux/soc/asic2_base.h>
#include <linux/soc/asic3_base.h>

extern struct platform_device h3900_asic2, h3900_asic3;

static unsigned long shared;
static int clock_enabled;

#define asic2 &h3900_asic2.dev
#define asic3 &h3900_asic3.dev

static void h3900_set_mmc_clock (struct platform_device *sdev, int status)
{
	switch (status) {
	case MMC_CLOCK_ENABLED:
		if (!clock_enabled) {
			unsigned long val, flags;

			/* Enable clock from asic2 */
			asic2_shared_add (asic2, &shared, ASIC_SHARED_CLOCK_EX1);
			asic2_clock_enable (asic2, ASIC2_CLOCK_SD_2, 1);
			asic3_set_clock_cdex (asic3, CLOCK_CDEX_EX0, CLOCK_CDEX_EX0);
			mdelay (1);

			/* Enable the host clock */
			asic3_set_clock_sel (asic3, CLOCK_SEL_SD_HCLK_SEL | CLOCK_SEL_SD_BCLK_SEL, CLOCK_SEL_SD_HCLK_SEL);
			asic3_set_clock_cdex (asic3, CLOCK_CDEX_SD_HOST | CLOCK_CDEX_SD_BUS, CLOCK_CDEX_SD_HOST);
			mdelay(1);
			asic3_set_clock_cdex (asic3, CLOCK_CDEX_EX1, CLOCK_CDEX_EX1);

			local_irq_save (flags);
			val = asic3_read_register (asic3, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Select);
			val |= ASIC3_EXTCF_SD_MEM_ENABLE;
			asic3_write_register (asic3, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Select, val);

			val = asic3_read_register (asic3, _IPAQ_ASIC3_SDHWCTRL_Base + _IPAQ_ASIC3_SDHWCTRL_SDConf);
			val &= ~(ASIC3_SDHWCTRL_SUSPEND | ASIC3_SDHWCTRL_PCLR); // wake up
			asic3_write_register (asic3, _IPAQ_ASIC3_SDHWCTRL_Base + _IPAQ_ASIC3_SDHWCTRL_SDConf, val);
			local_irq_restore (flags);

			mdelay(10);

			clock_enabled = 1;
			printk("h3900_mmc: clock enabled\n");
		} else {
			printk(KERN_ERR "h3900_mmc: clock was already enabled\n");
		}
		break;

	default:
		if (clock_enabled) {
			asic2_clock_enable (asic2, ASIC2_CLOCK_SD_2, 0);
			asic2_shared_release (asic2, &shared, ASIC_SHARED_CLOCK_EX1);
			clock_enabled = 0;
			printk("h3900_mmc: clock disabled\n");
		} else {
			printk(KERN_ERR "h3900_mmc: clock was already disabled\n");
		}
		break;
	}
}

static struct tmio_mmc_hwconfig h3900_mmc_hwconfig = {
	.set_mmc_clock = h3900_set_mmc_clock,
	.address_shift = 1,
};

static int 
h3900_mmc_probe (struct device *dev)
{
	struct platform_device *pdev = to_platform_device (dev);
	int mmc_irq;

	if (pdev->num_resources == 0)
		return -ENODEV;

	mmc_irq = (int)pdev->resource[0].start;

	return asic3_register_mmc (asic3, mmc_irq, &h3900_mmc_hwconfig);
}

static int
h3900_mmc_remove (struct device *dev)
{
	return asic3_unregister_mmc (asic3);
}

static struct device_driver h3900_mmc_device_driver = {
	.name		= "h3900 mmc",
	.bus		= &platform_bus_type,

	.probe		= h3900_mmc_probe,
	.remove		= h3900_mmc_remove,
};

static int __init 
h3900_mmc_init (void)
{
	int retval = 0;
	retval = driver_register (&h3900_mmc_device_driver);
	return retval;
}

static void __exit 
h3900_mmc_exit (void)
{
	driver_unregister (&h3900_mmc_device_driver);
}

module_init (h3900_mmc_init)
module_exit (h3900_mmc_exit)

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org>");
MODULE_DESCRIPTION("Glue driver for h3900 MMC");
MODULE_SUPPORTED_DEVICE("h3900_mmc");
