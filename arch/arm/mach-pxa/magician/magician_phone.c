/*
 * GSM driver for HTC Magician
 *
 * Copyright (C) 2006, Philipp Zabel
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/module.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/platform_device.h>

#include <asm/arch/magician.h>
#include <asm/mach-types.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/serial.h>
#include <asm/arch/magician_cpld.h>

extern struct platform_device magician_cpld;

static void magician_gsm_reset()
{
	printk ("GSM Reset\n");

	pxa_gpio_mode (10 | GPIO_IN);
	pxa_gpio_mode (108 | GPIO_IN);

	/* FIXME: clear 40, is this OK, or a bug in wince? */
	GPCR(GPIO40_FFDTR) = GPIO_bit(GPIO40_FFDTR);

	GPCR(11) = GPIO_bit(11);
	GPCR(87) = GPIO_bit(87);

	/* stop display to avoid interference? */
	GPCR(GPIO74_LCD_FCLK) = GPIO_bit(GPIO74_LCD_FCLK);

	magician_egpio_enable (&magician_cpld, EGPIO_NR_MAGICIAN_GSM_POWER);

	msleep (150);
	GPSR(GPIO74_LCD_FCLK) = GPIO_bit(GPIO74_LCD_FCLK);
	msleep (150);
	GPCR(GPIO74_LCD_FCLK) = GPIO_bit(GPIO74_LCD_FCLK);

	if (GPLR0 & 0x800) printk ("GSM: GPLR bit 11 already set\n");
	GPSR(11) = GPIO_bit(11);

	/* FIXME: disable+reenable 86, is this OK? */
	GPCR(86) = GPIO_bit(86); // ??
	GPSR(86) = GPIO_bit(86); // GSM_RESET
	GPSR(26) = GPIO_bit(26);
	msleep(150);
	GPCR(86) = GPIO_bit(86);
	msleep(10);

/*
	pxa_gpio_mode (GPIO36_FFDCD_MD); // alt 1 in
	pxa_gpio_mode (GPIO39_FFTXD_MD); // alt 2 out
	pxa_gpio_mode (GPIO43_BTTXD_MD); // alt 2 out
	pxa_gpio_mode (GPIO41_FFRTS_MD); // alt 2 out
	pxa_gpio_mode (GPIO45_BTRTS_MD); // alt 2 out
*/

	if (PGSR0 & 0x800) printk ("GSM: PGSR bit 11 set\n");
	PGSR0 |= 0x800;
}

void magician_gsm_off()
{
	printk ("GSM Off");
	GPCR(11) = GPIO_bit(11);
	GPCR(GPIO74_LCD_FCLK) = GPIO_bit(GPIO74_LCD_FCLK);
	GPCR(87) = GPIO_bit(87);
	GPCR(86) = GPIO_bit(86); // GSM_RESET
	GPCR(26) = GPIO_bit(26); // GSM_POWER
//	pxa_cken_set (CKEN6_FFUART, 0);
//	pxa_cken_set (CKEN7_BTUART, 0);

	pxa_gpio_mode (10 | GPIO_OUT);
	GPCR(10) = GPIO_bit(10);
	pxa_gpio_mode (108 | GPIO_OUT);
	GPCR(108) = GPIO_bit(108);
/*
	pxa_gpio_mode (GPIO34_FFRXD | GPIO_OUT);
	GPCR(34) = GPIO_bit(34);
	pxa_gpio_mode (GPIO35_FFCTS | GPIO_OUT);
	GPCR(35) = GPIO_bit(35);
	pxa_gpio_mode (GPIO36_FFDCD | GPIO_OUT);
	GPCR(36) = GPIO_bit(36);
	pxa_gpio_mode (GPIO39_FFTXD | GPIO_OUT);
	GPCR(39) = GPIO_bit(39);
	pxa_gpio_mode (GPIO41_FFRTS | GPIO_OUT);
	GPCR(41) = GPIO_bit(41);
	pxa_gpio_mode (GPIO42_BTRXD | GPIO_OUT);
	GPCR(42) = GPIO_bit(42);
	pxa_gpio_mode (GPIO43_BTTXD | GPIO_OUT);
	GPCR(43) = GPIO_bit(43);
	pxa_gpio_mode (GPIO44_BTCTS | GPIO_OUT);
	GPCR(44) = GPIO_bit(44);
	pxa_gpio_mode (GPIO45_BTRTS | GPIO_OUT);
	GPCR(45) = GPIO_bit(45);
*/
	PGSR0 &= ~0x800; // 11 off during suspend
	magician_egpio_disable(&magician_cpld, EGPIO_NR_MAGICIAN_GSM_POWER);
}

static void magician_phone_configure (int state)
{
	switch (state) {

	case PXA_UART_CFG_PRE_STARTUP:
		printk( KERN_NOTICE "magician configure phone: PRE_STARTUP\n");
		magician_gsm_reset();
		GPCR(11) = GPIO_bit(11); /* ready to talk */
		break;

	case PXA_UART_CFG_POST_STARTUP:
		printk( KERN_NOTICE "magician configure phone: POST_STARTUP\n");
		break;

	case PXA_UART_CFG_PRE_SHUTDOWN:
		printk( KERN_NOTICE "magician configure phone: PRE_SHUTDOWN\n");
		magician_gsm_off();
		break;

	default:
		break;
	}
}

struct magician_phone_funcs {
        void (*configure) (int state, int suspend);
};

static int magician_phone_probe (struct platform_device *dev)
{
	struct magician_phone_funcs *funcs = (struct magician_phone_funcs *) dev->dev.platform_data;

	/* configure phone UART */
//	pxa_gpio_mode( GPIO_NR_HTCSABLE_PHONE_RXD_MD );
//	pxa_gpio_mode( GPIO_NR_HTCSABLE_PHONE_TXD_MD );
//	pxa_gpio_mode( GPIO_NR_HTCSABLE_PHONE_UART_CTS_MD );
//	pxa_gpio_mode( GPIO_NR_HTCSABLE_PHONE_UART_RTS_MD );

	funcs->configure = magician_phone_configure;

	return 0;
}

static int magician_phone_remove (struct platform_device *dev)
{
	struct magician_phone_funcs *funcs = (struct magician_phone_funcs *) dev->dev.platform_data;

	funcs->configure = NULL;

	return 0;
}

static struct platform_driver phone_driver = {
	.probe   = magician_phone_probe,
	.remove  = magician_phone_remove,
	.driver = {
		.name     = "magician-phone",
	},
};

static __init int magician_phone_init(void)
{
	if (!machine_is_magician())
		return -ENODEV;
	printk(KERN_NOTICE "magician Phone Driver\n");

	return platform_driver_register(&phone_driver);
}

static __exit void magician_phone_exit(void)
{
	platform_driver_unregister(&phone_driver);
}

module_init(magician_phone_init);
module_exit(magician_phone_exit);

MODULE_AUTHOR("Philipp Zabel");
MODULE_DESCRIPTION("HTC Magician GSM driver");
MODULE_LICENSE("GPL");
