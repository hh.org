/*
 * Experimental driver for the CPLD in the HTC Magician
 *
 * Copyright 2006 Philipp Zabel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/version.h>

#include <linux/interrupt.h>
#include <linux/platform_device.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/mach/irq.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>

#include <asm/arch/magician.h>

struct magician_cpld_registers
{
	u32 egpio[3];
	u32 irq_state;
	u32 cable_state;
	u32 cpld14;
	u32 cpld18;
};

struct magician_cpld_data
{
	struct magician_cpld_registers *mapping;
	int irq_nr;
	u32 cached_egpio[3];	// cached egpio registers
};

static inline int u8pos(int bit)
{
	return bit/8;
}

static inline int u8bit(int bit)
{
	return 1<<(bit & (8-1));
}

void magician_egpio_enable (struct platform_device *dev, int bit)
{
	unsigned long flags;
	struct magician_cpld_data *cpld = platform_get_drvdata(dev);
	int pos = u8pos(bit);

	local_irq_save(flags);

	cpld->cached_egpio[pos] |= u8bit(bit);
	printk("egpio set: reg %d = 0x%08x\n", pos, cpld->cached_egpio[pos]);
	cpld->mapping->egpio[pos] = cpld->cached_egpio[pos];

	local_irq_restore(flags);
}

void magician_egpio_disable (struct platform_device *dev, int bit)
{
	unsigned long flags;
	struct magician_cpld_data *cpld = platform_get_drvdata(dev);
	int pos = u8pos(bit);

	local_irq_save(flags);

	cpld->cached_egpio[pos] &= ~u8bit(bit);
	printk("egpio set: reg %d = 0x%08x\n", pos, cpld->cached_egpio[pos]);
	cpld->mapping->egpio[pos] = cpld->cached_egpio[pos];

	local_irq_restore(flags);
}

static void magician_cpld_ack_gpio(unsigned int irq)
{
//	GEDR0 = (1 << (irq - IRQ_GPIO0));
	printk ("magician_cpld_ack_gpio(%d)\n", irq);
}

static void magician_cpld_mask_irq(unsigned int irq)
{
}

static void magician_cpld_unmask_irq(unsigned int irq)
{
}

static struct irqchip magician_cpld_chip = {
	.ack		= magician_cpld_ack_gpio,
	.mask		= magician_cpld_mask_irq,
	.unmask		= magician_cpld_unmask_irq,
};

/*
 * Demux handler for CPLD edge detect interrupts
 */
static void magician_cpld_irq_handler(unsigned int irq, struct irqdesc *desc)
{
	struct magician_cpld_data *cpld = desc->chip_data;
	int result, cable_state;

	/* Acknowledge the parent IRQ */
	desc->chip->ack(irq);

	result = cpld->mapping->irq_state;
	printk ("cpld irq 0x%02x ", result);
	cpld->mapping->irq_state = result;			// ACK (clear)
	printk ("(0x%02x)\n", cpld->mapping->irq_state);

	cable_state = cpld->mapping->cable_state;

	switch (result) {
		case 0x1:
			irq = IRQ_MAGICIAN_SD;
			desc = &irq_desc[irq];
			desc_handle_irq(irq, desc);
			break;
		case 0x2:
			irq = IRQ_MAGICIAN_EP;
			printk ("ep_state = 0x%02x\n", cpld->mapping->cpld18);
			desc = &irq_desc[irq];
			desc_handle_irq(irq, desc);
			break;
		case 0x8:
			irq = IRQ_MAGICIAN_AC;
			printk ("cable_state = 0x%02x\n", cable_state); // pCPLDRegs->cable_state
			desc = &irq_desc[irq];
			desc_handle_irq(irq, desc);
			break;
		case 0xc:
			irq = IRQ_MAGICIAN_USBC;
			desc = &irq_desc[irq];
			desc_handle_irq(irq, desc);
			break;
		default:
			printk ("magician_cpld: unknown irq 0x%x\n", result);
	}
}

static int magician_cpld_probe (struct platform_device *dev)
{
	struct magician_cpld_data *cpld;
	struct resource *res;

	cpld = kmalloc (sizeof (struct magician_cpld_data), GFP_KERNEL);
	if (!cpld)
		return -ENOMEM;
	memset (cpld, 0, sizeof (*cpld));

	platform_set_drvdata(dev, cpld);

	res = platform_get_resource(dev, IORESOURCE_MEM, 0);
	cpld->mapping = (struct magician_cpld_registers *)ioremap (res->start, res->end - res->start);
	if (!cpld->mapping) {
		printk ("magician_cpld: couldn't ioremap\n");
		kfree (cpld);
		return -ENOMEM;
	}

	printk ("MainBoardID = %x\n", cpld->mapping->cpld14 & 0x7);
	printk ("SD write protect bit: %d\n", cpld->mapping->cpld14 & 0x10);
	printk("magician_cpld: lcd_select = %d (%s)\n", cpld->mapping->cpld14 & 0x8,
			(cpld->mapping->cpld14 & 0x8) ? "samsung" : "toppoly");

	/*
	 * Initial values. FIXME: if we're not booting from wince, we should set them ourselves.
	 */
	cpld->cached_egpio[0] = cpld->mapping->egpio[0];
	cpld->cached_egpio[1] = cpld->mapping->egpio[1];
	cpld->cached_egpio[2] = cpld->mapping->egpio[2];
	printk ("initial egpios: 0x%02x 0x%02x 0x%02x\n", cpld->cached_egpio[0], cpld->cached_egpio[1], cpld->cached_egpio[2]);

	cpld->irq_nr = platform_get_irq(dev, 0);
	if (cpld->irq_nr >= 0) {
		unsigned int irq;

		/*
		 * Setup handlers for CPLD irqs, muxed into IRQ_GPIO(13)
		 */
		for (irq = IRQ_MAGICIAN_SD; irq <= IRQ_MAGICIAN_AC; irq++) {
			set_irq_chip(irq, &magician_cpld_chip);
			set_irq_handler(irq, handle_simple_irq);
			set_irq_flags(irq, IRQF_VALID | IRQF_PROBE); // | ACK
		}

		set_irq_chained_handler(cpld->irq_nr, magician_cpld_irq_handler);
		set_irq_type (cpld->irq_nr, IRQT_RISING);
		set_irq_data (cpld->irq_nr, cpld);
		set_irq_chipdata(cpld->irq_nr, cpld);
	}
	return 0;
}

static int magician_cpld_remove (struct platform_device *dev)
{
	struct magician_cpld_data *cpld = platform_get_drvdata(dev);

	if (cpld->irq_nr >= 0) {
		unsigned int irq;

		for (irq = IRQ_MAGICIAN_SD; irq <= IRQ_MAGICIAN_AC; irq++) {
			set_irq_chip(irq, NULL);
			set_irq_handler(irq, NULL);
			set_irq_flags(irq, 0);
		}

		set_irq_chained_handler(cpld->irq_nr, NULL);
	}

	iounmap((void *)cpld->mapping);
	kfree(cpld);
	platform_set_drvdata(dev, NULL);

	return 0;
}


struct platform_driver magician_cpld_driver = {
	.probe    = magician_cpld_probe,
	.remove   = magician_cpld_remove,
	.driver   = {
		.name     = "magician-cpld",
	},
};

static int magician_cpld_init (void)
{
	return platform_driver_register (&magician_cpld_driver);
}

static void magician_cpld_exit (void)
{
	platform_driver_unregister (&magician_cpld_driver);
}

module_init (magician_cpld_init);
module_exit (magician_cpld_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Philipp Zabel <philipp.zabel@gmail.com>");
MODULE_DESCRIPTION("Experimental driver for the CPLD in the HTC Magician");
MODULE_SUPPORTED_DEVICE("magician-cpld");

EXPORT_SYMBOL(magician_egpio_enable);
EXPORT_SYMBOL(magician_egpio_disable);
