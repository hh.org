/*
 * LCD driver for HTC Magician
 *
 * Copyright (C) 2006, Philipp Zabel
 *
 * based on previous work by
 *	Giuseppe Zompatori <giuseppe_zompatori@yahoo.it>,
 *	Andrew Zabolotny <zap@homelink.ru>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/notifier.h>
#include <linux/lcd.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/platform_device.h>

#include <asm/arch/magician.h>
#include <asm/mach-types.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxafb.h>

#include <asm/arch/magician_cpld.h>

static int lcd_select;

/* Samsung LTP280QV */
static struct pxafb_mach_info magician_fb_info_samsung = {
	.pixclock	= 96153,
	.bpp		= 16,
	.xres		= 240,
	.yres		= 320,
	.hsync_len	= 4,
	.vsync_len	= 1,
	.left_margin	= 20,
	.upper_margin	= 7,
	.right_margin	= 8,
	.lower_margin	= 8,
	.sync		= 0,
	.lccr0		= LCCR0_Color | LCCR0_Sngl | LCCR0_Act,
	.lccr3		= LCCR3_PixFlEdg,
	.pxafb_backlight_power = NULL,
	.pxafb_lcd_power = NULL,
};

/* Toppoly TD028STEB1 */
static struct pxafb_mach_info magician_fb_info_toppoly = {
	.pixclock	= 96153,
	.bpp		= 16,
	.xres		= 240,
	.yres		= 320,
	.hsync_len	= 11,
	.vsync_len	= 3,
	.left_margin	= 19,
	.upper_margin	= 2,
	.right_margin	= 10,
	.lower_margin	= 2,
	.sync		= 0,
	.lccr0		= LCCR0_Color | LCCR0_Sngl | LCCR0_Act,
	.lccr3		= LCCR3_PixRsEdg,
	.pxafb_backlight_power = NULL,
	.pxafb_lcd_power = NULL,
};

extern struct platform_device magician_cpld;

static int lcd_power;

static void toppoly_set_power(int power)
{
	if (power) {
		/*
		pxa_gpio_mode(GPIO74_LCD_FCLK_MD);
		pxa_gpio_mode(GPIO75_LCD_LCLK_MD); // done by pxafb
		*/
		magician_egpio_enable(&magician_cpld.dev, 2 /* TOPPOLY_POWER? */);
		pxa_gpio_mode(104 | GPIO_OUT);
		pxa_gpio_mode(106 | GPIO_OUT);
		pxa_gpio_mode(105 | GPIO_OUT);
		GPSR(106) = GPIO_bit(106);
		udelay(2000);
		magician_egpio_enable(&magician_cpld.dev, EGPIO_NR_MAGICIAN_LCD_POWER);
		udelay(2000);
		/* LCCR0 = 0x04000081; // <-- already done by pxafb_enable_controller */
		udelay(2000);
		GPSR(104) = GPIO_bit(104);
		udelay(2000);
		GPSR(105) = GPIO_bit(105);
	} else {
		/* LCCR0 = 0x00000000; // <-- will be done by pxafb_disable_controller afterwards?! */
		udelay(15000);
		GPCR(105) = GPIO_bit(105);
		udelay(500);
		GPCR(104) = GPIO_bit(104);
		udelay(1000);
		GPCR(106) = GPIO_bit(106);
		magician_egpio_disable(&magician_cpld.dev, EGPIO_NR_MAGICIAN_LCD_POWER);
	}
}

#if 0
static int magician_lcd_set_power(int power)
{
	if (lcd_select)
		samsung_set_power(power)
	else
		toppoly_set_power(power);
}
#endif

static int magician_lcd_set_power(struct lcd_device *lm, int level)
{
	printk("magician_lcd_set_power(level=%d) called.\n", level);

	switch (level) {
	case FB_BLANK_UNBLANK:
//		toppoly_set_power(1);
		break;
	case FB_BLANK_VSYNC_SUSPEND:
	case FB_BLANK_HSYNC_SUSPEND:
		break;
	case FB_BLANK_NORMAL:
	case FB_BLANK_POWERDOWN:
//		toppoly_set_power(0);
		break;
	}

	lcd_power = level;

	return 0;
}

static int magician_lcd_get_power(struct lcd_device *lm)
{
	return lcd_power;
}

struct lcd_properties magician_lcd_properties = {
	.owner = THIS_MODULE,
	.set_power = magician_lcd_set_power,
	.get_power = magician_lcd_get_power,
};

static struct lcd_device *pxafb_lcd_device;

static int magician_lcd_probe(struct device *dev)
{
	volatile u8 *p;

	printk(KERN_NOTICE "HTC Magician LCD driver\n");

	/* Check which LCD we have */
	p = (volatile u8 *)ioremap_nocache(PXA_CS3_PHYS, 0x1000);
	if (p) {
		lcd_select = p[14] & 0x8;
		printk("magician_lcd: lcd_select = %d (%s)\n", lcd_select,
		       lcd_select ? "samsung" : "toppoly");
		iounmap((void *)p);
	} else
		printk("magician_lcd: lcd_select ioremap failed\n");

	if (lcd_select)
		set_pxa_fb_info(&magician_fb_info_samsung);
	else
		set_pxa_fb_info(&magician_fb_info_toppoly);

	pxafb_lcd_device =
	    lcd_device_register("pxafb", NULL, &magician_lcd_properties);
	if (IS_ERR(pxafb_lcd_device))
		return PTR_ERR(pxafb_lcd_device);

	return 0;
}

static int magician_lcd_remove(struct device *dev)
{
	lcd_device_unregister(pxafb_lcd_device);

	return 0;
}

#ifdef CONFIG_PM
static int magician_lcd_suspend(struct device *dev, pm_message_t state)
{
	return 0;
}

static int magician_lcd_resume(struct device *dev)
{
	return 0;
}
#endif

static struct device_driver magician_lcd_driver = {
	.name = "magician-lcd",
	.bus = &platform_bus_type,
	.probe = magician_lcd_probe,
	.remove = magician_lcd_remove,
#ifdef CONFIG_PM
	.suspend = magician_lcd_suspend,
	.resume = magician_lcd_resume,
#endif
};

static __init int magician_lcd_init(void)
{
	if (!machine_is_magician())
		return -ENODEV;

	return driver_register(&magician_lcd_driver);
}

static __exit void magician_lcd_exit(void)
{
	driver_unregister(&magician_lcd_driver);
}

module_init(magician_lcd_init);
module_exit(magician_lcd_exit);

MODULE_AUTHOR("Philipp Zabel");
MODULE_DESCRIPTION("HTC Magician LCD driver");
MODULE_LICENSE("GPL");
