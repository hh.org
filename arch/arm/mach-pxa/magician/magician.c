/*
 *
 * Hardware definitions for the HTC Magician
 *
 * Copyright 2006 Philipp Zabel
 *
 * Based on hx4700.c
 * Copyright 2005 SDG Systems, LLC
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/delay.h>

#include <linux/spi/spi.h>
#include <linux/spi/ads7846.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <asm/arch/magician.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/serial.h>
#include <asm/arch/pxa2xx_spi.h>
#include <asm/hardware/gpio_keys.h>
#include <asm/arch/mmc.h>
#include <asm/arch/udc.h>
#include <asm/arch/audio.h>
#include <asm/arch/irda.h>
#include <asm/arch/ohci.h>

#include <linux/corgi_bl.h>

#include "../generic.h"

/*
 * IRDA
 */

static void magician_irda_transceiver_mode(struct device *dev, int mode)
{
	unsigned long flags;

	local_irq_save(flags);
	if (mode & IR_OFF)
		SET_MAGICIAN_GPIO_N(IR_ON, 0);
	else
		SET_MAGICIAN_GPIO_N(IR_ON, 1);
	local_irq_restore(flags);
}

static struct pxaficp_platform_data magician_ficp_platform_data = {
	.transceiver_cap  = IR_SIRMODE | IR_OFF,
	.transceiver_mode = magician_irda_transceiver_mode,
};

/*
 * Phone
 */

struct magician_phone_funcs {
        void (*configure) (int state);
};

static struct magician_phone_funcs phone_funcs;

static void magician_phone_configure (int state, int suspend)
{
	if (phone_funcs.configure)
		phone_funcs.configure (state);
}

static struct platform_pxa_serial_funcs magician_pxa_phone_funcs = {
	.configure = magician_phone_configure,
};

static struct platform_device magician_phone = {
	.name = "magician-phone",
	.dev  = {
		.platform_data = &phone_funcs,
		},
	.id   = -1,
};


/*
 * Initialization code
 */

static void __init magician_map_io(void)
{
	pxa_map_io();
        btuart_device.dev.platform_data = &magician_pxa_phone_funcs;
}

static void __init magician_init_irq(void)
{
	/* int irq; */

	pxa_init_irq();
}

/*
 * Power Management
 */

struct platform_device magician_pm = {
	.name = "magician-pm",
	.id   = -1,
	.dev  = {
		.platform_data = NULL,
	},
};

/*
 * CPLD
 */

static struct resource magician_cpld_resources[] = {
	[0] = {
		.start	= PXA_CS3_PHYS,
		.end	= PXA_CS3_PHYS + 0x1000,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= MAGICIAN_IRQ(CPLD_IRQ),
		.end	= MAGICIAN_IRQ(CPLD_IRQ),
		.flags	= IORESOURCE_IRQ,
	},
};

struct platform_device magician_cpld = {
	.name		= "magician-cpld",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(magician_cpld_resources),
	.resource	= magician_cpld_resources,
};
EXPORT_SYMBOL(magician_cpld);

/*
 * USB Device Controller
 */

static void magician_udc_command(int cmd)
{
	printk ("magician_udc_command(%d)\n", cmd);
	switch(cmd)	{
	case PXA2XX_UDC_CMD_CONNECT:
		UP2OCR = UP2OCR_HXOE | UP2OCR_DMPUE | UP2OCR_DMPUBE;
		SET_MAGICIAN_GPIO(USB_PUEN, 1);
		break;
	case PXA2XX_UDC_CMD_DISCONNECT:
		UP2OCR = UP2OCR_HXOE | UP2OCR_DMPUE | UP2OCR_DMPUBE;
		SET_MAGICIAN_GPIO(USB_PUEN, 0);
		break;
	}
}

static struct pxa2xx_udc_mach_info magician_udc_mach_info __initdata = {
/*	.udc_is_connected = is not used by pxa27x_udc.c at all */
	.udc_command      = magician_udc_command,
};

/*
 * GPIO Keys
 */

static struct gpio_keys_button magician_button_table[] = {
	{KEY_POWER,      GPIO_NR_MAGICIAN_KEY_ON,           0},
	{KEY_F8,         GPIO_NR_MAGICIAN_KEY_PHONE_HANGUP, 0},
	{KEY_F5,         GPIO_NR_MAGICIAN_KEY_CONTACTS,     0},
	{KEY_F9,         GPIO_NR_MAGICIAN_KEY_CALENDAR,     0},
	{KEY_F10,        GPIO_NR_MAGICIAN_KEY_CAMERA,       0},
	{KEY_UP,         GPIO_NR_MAGICIAN_KEY_UP,           0},
	{KEY_DOWN,       GPIO_NR_MAGICIAN_KEY_DOWN,         0},
	{KEY_LEFT,       GPIO_NR_MAGICIAN_KEY_LEFT,         0},
	{KEY_RIGHT,      GPIO_NR_MAGICIAN_KEY_RIGHT,        0},
	{KEY_KPENTER,    GPIO_NR_MAGICIAN_KEY_ENTER,        0},
	{KEY_F9,         GPIO_NR_MAGICIAN_KEY_RECORD,       0},
	{KEY_VOLUMEUP,   GPIO_NR_MAGICIAN_KEY_VOLUMEUP,     0},
	{KEY_VOLUMEDOWN, GPIO_NR_MAGICIAN_KEY_VOLUMEDOWN,   0},
	{KEY_F7,         GPIO_NR_MAGICIAN_KEY_PHONE_LIFT,   0},
};

static struct gpio_keys_platform_data magician_gpio_keys_data = {
	.buttons  = magician_button_table,
	.nbuttons = ARRAY_SIZE(magician_button_table),
};

static struct platform_device magician_gpio_keys = {
	.name = "gpio-keys",
	.dev  = {
		.platform_data = &magician_gpio_keys_data,
		},
	.id   = -1,
};

/*
 * LCD
 */

static struct platform_device magician_lcd = {
	.name = "magician-lcd",
	.dev  = {
		.platform_data = NULL,
	},
	.id   = -1,
};

/*
 * Backlight
 */

static void magician_set_bl_intensity(int intensity)
{
	if (intensity) {
		PWM_CTRL0 = 1;
		PWM_PERVAL0 = 0xc8;
		if (intensity > 0xc7) {
			PWM_PWDUTY0 = intensity - 0x48;
			magician_egpio_enable(&magician_cpld, EGPIO_NR_MAGICIAN_BL_POWER2);
		} else {
			PWM_PWDUTY0 = intensity;
			magician_egpio_disable(&magician_cpld, EGPIO_NR_MAGICIAN_BL_POWER2);
		}
		magician_egpio_enable(&magician_cpld, EGPIO_NR_MAGICIAN_BL_POWER);
		pxa_set_cken(CKEN0_PWM0, 1);
	} else {
		magician_egpio_disable(&magician_cpld, EGPIO_NR_MAGICIAN_BL_POWER);
		pxa_set_cken(CKEN0_PWM0, 0);
	}
}

static struct corgibl_machinfo magician_bl_machinfo = {
	.default_intensity = 0x64,
	.limit_mask        = 0x0b,
	.max_intensity     = 0xc7+0x48,
	.set_bl_intensity  = magician_set_bl_intensity,
};

static struct platform_device magician_bl = {
	.name = "corgi-bl",
	.dev  = {
		.platform_data = &magician_bl_machinfo,
	},
	.id   = -1,
};

/* Touchscreen, the TSC2046 chipset is pin-compatible to ADS7846 */

static struct platform_device magician_ts = {
	.name = "magician-ts",
	.dev = {
		.platform_data = NULL,
		},
};

#if 0
int magician_get_pendown_state()
{
	return GET_MAGICIAN_GPIO(TOUCHPANEL_IRQ_N) ? 0 : 1;
}

EXPORT_SYMBOL(magician_get_pendown_state);

static struct ads7846_platform_data magician_tsc2046_platform_data __initdata = {
	.x_max = 0xffff,
	.y_max = 0xffff,
	.x_plate_ohms = 180,	/* GUESS */
	.y_plate_ohms = 180,	/* GUESS */
	.vref_delay_usecs = 100,
	.pressure_max = 255,
	.debounce_max = 10,
	.debounce_tol = 3,
	.get_pendown_state = &magician_get_pendown_state,
};

struct pxa2xx_spi_chip tsc2046_chip_info = {
	.tx_threshold = 1,
	.rx_threshold = 2,
	.timeout_microsecs = 64,
};

static struct spi_board_info magician_spi_board_info[] __initdata = {
	[0] = {
	       .modalias = "ads7846",
	       .bus_num = 2,
	       .max_speed_hz = 1857143,
	       .irq = MAGICIAN_IRQ(TOUCHPANEL_IRQ_N),
	       .platform_data = &magician_tsc2046_platform_data,
	       .controller_data = &tsc2046_chip_info,
	       },
};

static struct resource pxa_spi_ssp2_resources[] = {
	[0] = {
	       .start = __PREG(SSCR0_P(2)),
	       .end = __PREG(SSCR0_P(2)) + 0x2c,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = IRQ_SSP2,
	       .end = IRQ_SSP2,
	       .flags = IORESOURCE_IRQ,
	       },
};

static struct pxa2xx_spi_master pxa_ssp2_master_info = {
	.ssp_type = PXA27x_SSP,
	.clock_enable = CKEN3_SSP2,
//      .num_chipselect = 1,
};

static struct platform_device pxa_spi_ssp2 = {
	.name = "pxa2xx-spi",
	.id = 2,
	.resource = pxa_spi_ssp2_resources,
	.num_resources = ARRAY_SIZE(pxa_spi_ssp2_resources),
	.dev = {stp
		.platform_data = &pxa_ssp2_master_info,
		},
};
#endif

/*
 * Magician LEDs
 */
static struct platform_device magician_led = {
	.name   = "magician-led",
	.id     = -1,
};

/*
 * MMC/SD
 */

static struct pxamci_platform_data magician_mci_platform_data;

static int magician_mci_init(struct device *dev, irq_handler_t magician_detect_int, void *data)
{
	int err;

	err = request_irq(IRQ_MAGICIAN_SD, magician_detect_int,
			  IRQF_DISABLED | IRQF_SAMPLE_RANDOM,
			  "MMC card detect", data);
	if (err) {
		printk(KERN_ERR "magician_mci_init: MMC/SD: can't request MMC card detect IRQ\n");
		return -1;
	}

	return 0;
}

static void magician_mci_setpower(struct device *dev, unsigned int vdd)
{
	struct pxamci_platform_data* p_d = dev->platform_data;

        if (( 1 << vdd) & p_d->ocr_mask)
		magician_egpio_enable(&magician_cpld, EGPIO_NR_MAGICIAN_SD_POWER);
	else
		magician_egpio_disable(&magician_cpld, EGPIO_NR_MAGICIAN_SD_POWER);
}

static int magician_mci_get_ro(struct device *dev)
{
	/* FIXME return (cpld->mapping->cpld14 & 0x10) ? 1 : 0; */
	return 0;
}

static void magician_mci_exit(struct device *dev, void *data)
{
	free_irq(IRQ_MAGICIAN_SD, data);
}

static struct pxamci_platform_data magician_mci_platform_data = {
	.ocr_mask = MMC_VDD_32_33|MMC_VDD_33_34,
	.init     = magician_mci_init,
	.get_ro   = magician_mci_get_ro,
	.setpower = magician_mci_setpower,
	.exit     = magician_mci_exit,
};


/* USB OHCI */

static int magician_ohci_init(struct device *dev)
{
	/* missing GPIO setup here */

	/* no idea what this does, got the values from haret */
	UHCHR = (UHCHR | UHCHR_SSEP2 | UHCHR_PCPL | UHCHR_CGR) &
	    ~(UHCHR_SSEP1 | UHCHR_SSEP3 | UHCHR_SSE);

	return 0;
}

static struct pxaohci_platform_data magician_ohci_platform_data = {
	.port_mode = PMM_PERPORT_MODE,
	.init = magician_ohci_init,
};

static struct platform_device *devices[] __initdata = {
	&magician_pm,
	&magician_gpio_keys,
	&magician_cpld,
	&magician_lcd,		// needs the cpld to power on/off the lcd
	&magician_bl,
	&magician_ts,
	&magician_led,
	&magician_phone,
#if 0
	&pxa_spi_ssp2,
#endif
};

static void __init magician_init(void)
{

	/* BIG TODO */

#if 0				// GPIO/MSC setup from bootldr
	/* 0x1350       0x41664 (resume) */
	GPSR0 = 0x4034c000;	// 0x0030c800 <-- set 11, clear 18, clear 30
	GPSR1 = 0x0002321a;	// GPSR1 = (PGSR1 & 0x01000100) | 0x0002420a <-- clear 36 44 45, set 46 and get 40 and 66 from PGSR1
	GPSR2 = 0x0009c000;	// 0x0009c000
	GPSR3 = 0x01180000;	// 0x00180000 <-- clear gpio24

	// GPCR only here

	GPDR0 = 0xfdfdc80c;	// 0xfdfdc80c
	GPDR1 = 0xff23990b;	// 0xff23ab83
	GPDR2 = 0x02c9ffff;	// 0x02c9ffff
	GPDR3 = 0x01b60780;	// 0x01b60780
	GAFR0_L = 0xa2000000;	// 0xa2000000
	GAFR0_U = 0x490a845a;	// 0x490a854a <-- 2(1->0) 4(0->1)
	GAFR1_L = 0x6998815a;	// 0x6918005a <-- 20(1->0) 23(2->0) 27(2->0)
	GAFR1_U = 0xaaa07958;	// 0xaaa07958
	GAFR2_L = 0xaa0aaaaa;	// 0xaa0aaaaa
	GAFR2_U = 0x010e0f3a;	// 0x010e0f3a
	GAFR3_L = 0x54000000;	// 0x54000000
	GAFR3_U = 0x00001405;	// 0x00001405
	MSC0 = 0x7ff001a0;	// 0x7ff005a2 <--       ( RBUFF,RRR RDN, RDF, RBW,RT ) ( RBUFF,RRR RDN, RDF, RBW,RT )
	MSC1 = 0x7ff01880;	// 0x18801880 <--
	MSC2 = 0x16607ff0;	// 0x16607ff0
#endif

	/* set USBC_PUEN - in wince it is turned off when the plug is pulled. */
	GPSR(27) = GPIO_bit(27);
		// should this be controlled by the USBC_DET irq from the cpld? (on gpio 13)

	platform_add_devices(devices, ARRAY_SIZE(devices));
#if 0
	spi_register_board_info(magician_spi_board_info,
				ARRAY_SIZE(magician_spi_board_info));
#endif
        pxa_set_mci_info(&magician_mci_platform_data);
	pxa_set_ohci_info(&magician_ohci_platform_data);
//	pxa_set_btuart_info(&magician_pxa_phone_funcs);
	pxa_set_udc_info(&magician_udc_mach_info);
	pxa_set_ficp_info(&magician_ficp_platform_data);
}


MACHINE_START(MAGICIAN, "HTC Magician")
	.phys_io = 0x40000000,.io_pg_offst =
	(io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params = 0xa0000100,
	.map_io = magician_map_io,
	.init_irq = magician_init_irq,
	.timer = &pxa_timer,
	.init_machine = magician_init,
MACHINE_END
