/*
 * Power management driver for HTC Magician (suspend/resume code)
 *
 * Copyright (c) 2006 Philipp Zabel
 *
 * Based on: hx4700_core.c
 * Copyright (c) 2005 SDG Systems, LLC
 *
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/platform_device.h>
#include <linux/pm.h>

#include <asm/io.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxa-pm_ll.h>
#include <asm/arch/magician.h>

static int htc_bootloader = 0;	/* Is the stock HTC bootloader installed? */
static u32 save[4];
static u32 save2[13];

#ifdef CONFIG_PM
static int magician_suspend(struct platform_device *dev, pm_message_t state)
{
//      PWER = PWER_RTC | PWER_GPIO0 | PWER_GPIO1 /* | PWER_WEP1 */;
	PWER = PWER_GPIO0 | PWER_GPIO1;	/* PWER, PFER, PRER = 3 in haret */
	PFER = PWER_GPIO1;
	PRER = PWER_GPIO0;

//      PGSR0 = 0x080DC01C;
//      PGSR1 = 0x34CF0002;
//      PGSR2 = 0x0123C18C;
//      PGSR3 = 0x00100202;	/* PGSR3 = 0x00104202; */

	/*see 3.6.2.3 */
//      PCFR = PCFR_GPROD|PCFR_DC_EN|PCFR_GPR_EN|PCFR_OPDE;
	/* |PCFR_FP|PCFR_PI2CEN; */
	/*haret: 00001071 */
	PCFR = 0x00001071;

//hx4700:
//      PSLR=0xc8000000 /*| (2 << 2)*/ /* SL_PI = 2, PI power domain active, clocks running;*/
	/*     ^---- SYS_DEL =
	   ^--- PWR_DEL */
	/* haret: cc000000 */
	PSLR = 0xcc000000;

	return 0;
}

static int magician_resume(struct platform_device *dev)
{
	u32 tmp;

	printk("resume:\n");
	printk("CCCR = %08x\n", CCCR);
	asm("mrc\tp14, 0, %0, c6, c0, 0":"=r"(tmp));
	printk("CLKCFG = %08x\n", tmp);
	printk("MSC0 = %08x\n", MSC0);
	printk("MSC1 = %08x\n", MSC1);
	printk("MSC2 = %08x\n", MSC2);

	return 0;
}
#else
#define magician_suspend NULL
#define magician_resume NULL
#endif

static void magician_pxa_ll_pm_suspend(unsigned long resume_addr)
{
	int i;
	u32 csum, tmp, *p;

	/* Save the 13 words at 0xa0038000. */
	for (p = phys_to_virt(0xa0038000), i = 0; i < 13; i++)
		save2[i] = p[i];

	/* Save the first four words at 0xa0000000. */
	for (p = phys_to_virt(0xa0000000), i = 0; i < 4; i++)
		save[i] = p[i];

	/* Set the first four words at 0xa0000000 to:
	 * resume address; MMU control; TLB base addr; domain id */
	p[0] = resume_addr;

      asm("mrc\tp15, 0, %0, c1, c0, 0":"=r"(tmp));
	p[1] = tmp & ~(0x3987);	/* mmu off */

      asm("mrc\tp15, 0, %0, c2, c0, 0":"=r"(tmp));
	p[2] = tmp;		/* Shouldn't matter, since MMU will be off. */

      asm("mrc\tp15, 0, %0, c3, c0, 0":"=r"(tmp));
	p[3] = tmp;		/* Shouldn't matter, since MMU will be off. */

	/* Set PSPR to the checksum the HTC bootloader wants to see. */
	for (csum = 0, i = 0; i < 52; i++) {
		tmp = p[i] & 0x1;
		tmp = tmp << 31;
		tmp |= tmp >> 1;
		csum += tmp;
	}

	PSPR = csum;
}

static void magician_pxa_ll_pm_resume(void)
{
	int i;
	u32 *p;

	/* Restore the first four words at 0xa0000000. */
	for (p = phys_to_virt(0xa0000000), i = 0; i < 4; i++)
		p[i] = save[i];

	/* Restore the 13 words at 0xa0038000. */
	for (p = phys_to_virt(0xa0038000), i = 0; i < 13; i++)
		p[i] = save2[i];

	/* XXX Do we need to flush the cache? */
}

struct pxa_ll_pm_ops magician_ll_pm_ops = {
	.suspend = magician_pxa_ll_pm_suspend,
	.resume = magician_pxa_ll_pm_resume,
};

static int magician_pm_probe(struct platform_device *dev)
{
	u32 *bootldr;
	int i;
	u32 tmp;

	printk(KERN_NOTICE "HTC Magician power management driver\n");

	/* Is the stock HTC bootloader installed? */

	bootldr = (u32 *) ioremap(PXA_CS0_PHYS, 1024 * 1024);
	i = 0x0004156c / 4;

	if (bootldr[i] == 0xe59f1534 &&	/* ldr r1, [pc, #1332] ; power base */
	    bootldr[i + 1] == 0xe5914008 &&	/* ldr r4, [r1, #8]    ; PSPR */
	    bootldr[i + 2] == 0xe1320004) {	/* teq r2, r4 */

		printk("Stock HTC bootloader detected\n");
		htc_bootloader = 1;
		pxa_pm_set_ll_ops(&magician_ll_pm_ops);
	}

	iounmap(bootldr);

	printk("CCCR = %08x\n", CCCR);
	asm("mrc\tp14, 0, %0, c6, c0, 0":"=r"(tmp));
	printk("CLKCFG = %08x\n", tmp);
	printk("MSC0 = %08x\n", MSC0);
	printk("MSC1 = %08x\n", MSC1);
	printk("MSC2 = %08x\n", MSC2);

	return 0;
}

struct platform_driver magician_pm_driver = {
	.probe   = magician_pm_probe,
	.suspend = magician_suspend,
	.resume  = magician_resume,
	.driver  = {
		.name = "magician-pm",
	}
};

static int __init magician_pm_init(void)
{
	return platform_driver_register(&magician_pm_driver);
}

static void __exit magician_pm_exit(void)
{
	platform_driver_unregister(&magician_pm_driver);
}

module_init(magician_pm_init);
module_exit(magician_pm_exit);

MODULE_AUTHOR("Philipp Zabel");
MODULE_DESCRIPTION("HTC Magician power management driver");
MODULE_LICENSE("GPL");
