/*
 * Asus MyPal 730 PCMCIA/CF support. Based on a716_pcmcia.c
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * Copyright (C) 2005 Pawel Kolodziejski
 * Copyright (C) 2004 Nicolas Pouillon, Vitaliy Sardyko
 *
 */
/* doesn't work, yet
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/irq.h>

#include <asm/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/asus730-gpio.h>

#include <pcmcia/ss.h>

#include <../drivers/pcmcia/soc_common.h>

#include <linux/platform_device.h>

#define DEBUG 1

static int socket0_ready = 0;
#define GPIO_NR_A730_CF_CARD_DETECT_N	38
#define GPIO_NR_A730_CF_IN_1	11
//#define GPIO_NR_A730_CF_IN_2	14
#define GPIO_NR_A730_CF_OUT_3	22
#define GPIO_NR_A730_CF_OUT_1	80
#define GPIO_NR_A730_CF_OUT_2	91

#define GPIO_CLR(nr) (GPCR(nr) = GPIO_bit(nr))
#define GPIO_SET(nr) (GPSR(nr) = GPIO_bit(nr))

static struct pcmcia_irqs irqs[] = {
	{ 0, A730_IRQ(CF_CARD_DETECT_N), "PCMCIA CD" },
};

void a730_pcmcia_restore(void)
{
	unsigned long flags;

	printk("%s\n",__PRETTY_FUNCTION__);
	local_irq_save(flags);
	local_irq_restore(flags);
}

static int a730_pcmcia_hw_init(struct soc_pcmcia_socket *skt)
{
	int ret;

	if (skt->nr == 0) {
		skt->irq = A730_IRQ(CF_IN_1);//or _2 or something entirelly different
		set_irq_type(irqs->irq, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING);
		ret = soc_pcmcia_request_irqs(skt, irqs, ARRAY_SIZE(irqs));
		//a730_pcmcia_restore();
		socket0_ready = 1;
	} else {
		skt->irq = 0;
		ret = 0;
	}
	return ret;
}

static void a730_pcmcia_hw_shutdown(struct soc_pcmcia_socket *skt)
{
	//DPRINTK("%s\n",__PRETTY_FUNCTION__);
	if (skt->nr == 0)
		soc_pcmcia_free_irqs(skt, irqs, ARRAY_SIZE(irqs));
}

static void a730_pcmcia_socket_state(struct soc_pcmcia_socket *skt, struct pcmcia_state *state)
{
	if (skt->nr == 0) {
		state->detect = GET_A730_GPIO(CF_CARD_DETECT_N) ? 0 : 1;//new
		state->ready  = socket0_ready;
	} else { // disabled fo now
		state->detect = 0;
		state->ready  = 0;
	}
	state->bvd1   = 1;
	state->bvd2   = 1;
	state->wrprot = 0;
	state->vs_3v  = 1;
	state->vs_Xv  = 0;

//	DPRINTK("skt:%d, detect:%d ready:%d 3X:%d%d\n", skt->nr,
//		state->detect, state->ready, state->vs_3v, state->vs_Xv);
}
static int power_en;
/*
    Three set_socket_calls
    1. RESET deasserted
    2. RESET asserted
    3. RESET deasserted
*/
static int a730_pcmcia_configure_socket(struct soc_pcmcia_socket *skt, const socket_state_t *state)
{
	printk("%s: reset = %d, snum = %d\n",__PRETTY_FUNCTION__, (state->flags & SS_RESET ? 1 : 0), skt->nr);
	
	/*if (state->flags & SS_RESET)
	{
	    GPIO_SET(GPIO_NR_A730_CF_OUT_3);
	    mdelay(50);
	    GPIO_CLR(GPIO_NR_A730_CF_OUT_3);
	    GPIO_SET(GPIO_NR_A730_CF_OUT_1);
	    GPIO_CLR(GPIO_NR_A730_CF_OUT_2);
	    power_en = 0;
	    return 0;
	}*/

	if (skt->nr == 0) {
		switch (state->Vcc) {
		case 0:
			printk("%s: vcc=%d\n",__PRETTY_FUNCTION__, state->Vcc);
			GPIO_SET(GPIO_NR_A730_CF_OUT_1);
			GPIO_CLR(GPIO_NR_A730_CF_OUT_2);
			power_en = 0;
			break;
		case 33:
		    printk("%s: vcc=%d\n",__PRETTY_FUNCTION__, state->Vcc);
			GPIO_CLR(GPIO_NR_A730_CF_OUT_1);
			GPIO_SET(GPIO_NR_A730_CF_OUT_2);
			GPIO_SET(GPIO_NR_A730_CF_OUT_3);
			mdelay(50);
			GPIO_CLR(GPIO_NR_A730_CF_OUT_3);
			power_en = 1;
			break;
		case 50:
		    printk("%s: vcc=%d\n",__PRETTY_FUNCTION__, state->Vcc);
			GPIO_CLR(GPIO_NR_A730_CF_OUT_1);
			GPIO_SET(GPIO_NR_A730_CF_OUT_2);
			GPIO_SET(GPIO_NR_A730_CF_OUT_3);
			mdelay(50);
			GPIO_CLR(GPIO_NR_A730_CF_OUT_3);
			power_en = 1;
			break;
		default:
			printk (KERN_ERR "%s: Unsupported Vcc:%d\n", __FUNCTION__, state->Vcc);
		}
		socket0_ready = 1;
		SET_A730_GPIO(CF_POWER, !power_en);
	} else {
	}

	return 0;
}

static void a730_pcmcia_socket_init(struct soc_pcmcia_socket *skt)
{
	printk("%s %d\n",__PRETTY_FUNCTION__,skt->nr);
	//reset
	if (skt->nr == 0) soc_pcmcia_enable_irqs(skt, irqs, ARRAY_SIZE(irqs));
}

static void a730_pcmcia_socket_suspend(struct soc_pcmcia_socket *skt)
{
	printk("%s %d\n",__PRETTY_FUNCTION__,skt->nr);
	//reset
	if (skt->nr == 0) {
		soc_pcmcia_disable_irqs(skt, irqs, ARRAY_SIZE(irqs));
	}
}

static struct pcmcia_low_level a730_pcmcia_ops = { 
	.owner			= THIS_MODULE,
	.first			= 0,
	.nr			= 1/*2*/,
	.hw_init		= a730_pcmcia_hw_init,
	.hw_shutdown		= a730_pcmcia_hw_shutdown,
	.socket_state		= a730_pcmcia_socket_state,
	.configure_socket	= a730_pcmcia_configure_socket,
	.socket_init		= a730_pcmcia_socket_init,
	.socket_suspend		= a730_pcmcia_socket_suspend,
};

static struct platform_device a730_pcmcia_device = {
	.name           = "pxa2xx-pcmcia",
	.id             = 0,
	.dev            = {
		.platform_data = &a730_pcmcia_ops
	}
};

static int __init a730_pcmcia_init(void)
{
	printk("%s\n",__PRETTY_FUNCTION__);
	if (!machine_is_a730())	return -ENODEV;

	return platform_device_register(&a730_pcmcia_device);
}

static void __exit a730_pcmcia_exit(void)
{
	printk("%s\n",__PRETTY_FUNCTION__);
	platform_device_unregister(&a730_pcmcia_device);
}

module_init(a730_pcmcia_init);
module_exit(a730_pcmcia_exit);

MODULE_AUTHOR("Michal Sroczynski");
MODULE_DESCRIPTION("Asus MyPal 730 PCMCIA driver");
MODULE_LICENSE("GPL");
