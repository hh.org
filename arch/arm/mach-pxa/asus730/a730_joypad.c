/*
 * based on arch/arm/mach-pxa/pxa_keys.c
 * and on arch/arm/mach-pxa/asus716/a716_buttons.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * 03 Apr 2006:
 *      Serge Nikolaenko        split a730_keys.c to a730_buttons.c & a730_joupad.c
 */

#include <linux/input.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/arch/asus730-gpio.h>

#include <linux/platform_device.h>

static struct workqueue_struct *a730_pca9535_workqueue;
static struct work_struct a730_pca9535_irq_task;
static DECLARE_MUTEX_LOCKED(pca_mutex);

#define GET_GPIO(gpio) (GPLR(gpio) & GPIO_bit(gpio))
#define DPRINTK(fmt, args...)	printk("%s: " fmt, __FUNCTION__ , ## args)

//static int __init a730_joy_probe(struct device *dev);
static int a730_joy_suspend(struct device *dev, pm_message_t state);
static int a730_joy_resume(struct device *dev);

/*static struct device_driver a730_joystick_driver = {
    .name = "a730_joystick",
    .bus = &platform_bus_type,
    .probe = a730_joy_probe,
#ifdef CONFIG_PM
    .suspend = a730_joy_suspend,
    .resume = a730_joy_resume,
#endif
};*/

static struct input_dev *joy_dev = NULL;

u32 pca9535_read_input(void);//pca9535.ko

static struct
{
    int keycode;
    int mask;
    char *desc;
} joypad[] = 
{
    {KEY_UP, 0x01, "UP"},
    {KEY_RIGHT, 0x02, "RIGHT"},
    {KEY_DOWN, 0x04, "DOWN"},
    {KEY_LEFT, 0x08, "LEFT"},
    {KEY_ENTER, 0x10, "PUSH"},
};

static u32 joypad_prev;

static void a730_pca9535_handler(void *dummy)
{
	u32 dd, data, bits_changed;
	int i;

	dd = data = pca9535_read_input();

	if (data == (u32)-1) return;
	
	data = (~(data >> 8) & 0x1f);
	bits_changed = joypad_prev ^ data;

	if (!bits_changed)
	{
		DPRINTK("!bits_changed E=%d D=%d 4=%d 2=%d 1=%d\n", (dd>>14)&1,(dd>>13)&1,(dd>>4)&1,(dd>>2)&1,(dd>>1)&1);
		return;
	}

	for (i = 0; i < ARRAY_SIZE(joypad); i++)
	{
		if (bits_changed & joypad[i].mask)
		{
			int state = ((bits_changed & data) != 0);
	    		//printk("desc=%s %s\n", joypad[i].desc, (state ? "P" : "R"));
	    		input_report_key(joy_dev, joypad[i].keycode, state);
		}
	}
	input_sync(joy_dev);
	joypad_prev = data;
	up(&pca_mutex);
	return;
}

static irqreturn_t a730_joy_handle(int irq, void *dev_id, struct pt_regs *regs)
{
	if (!down_trylock(&pca_mutex)) queue_work(a730_pca9535_workqueue, &a730_pca9535_irq_task);
	return IRQ_HANDLED;
}

/*static int __init a730_joy_probe(struct device *dev)
{
    return 0;
}*/

static int __init a730_joy_init(void)
{
	int irqflag = IRQF_DISABLED;
#ifdef CONFIG_PREEMPT_RT
	irqflag |= IRQF_NODELAY;
#endif
	if (!machine_is_a730()) return -ENODEV;

	if (!(joy_dev = input_allocate_device())) return -ENOMEM;

	joy_dev->name = "Asus MyPal 730(W) joypad";
	joy_dev->id.bustype = BUS_I2C;
	joy_dev->phys = "a730joy/input0";

	set_bit(EV_KEY, joy_dev->evbit);

	joy_dev->keybit[LONG(KEY_UP)] |= BIT(KEY_UP);
	joy_dev->keybit[LONG(KEY_DOWN)] |= BIT(KEY_DOWN);
	joy_dev->keybit[LONG(KEY_LEFT)] |= BIT(KEY_LEFT);
	joy_dev->keybit[LONG(KEY_RIGHT)] |= BIT(KEY_RIGHT);
	joy_dev->keybit[LONG(KEY_ENTER)] |= BIT(KEY_ENTER);

	request_irq(A730_IRQ(PCA9535_IRQ), a730_joy_handle, irqflag, "Joystick", NULL);
	set_irq_type(A730_IRQ(PCA9535_IRQ), IRQF_TRIGGER_FALLING);

	input_register_device(joy_dev);
	
	a730_pca9535_workqueue = create_singlethread_workqueue("joypad");
	INIT_WORK(&a730_pca9535_irq_task, a730_pca9535_handler, NULL);

	up(&pca_mutex);
	return 0;
}

static void __exit a730_joy_exit(void)
{
	input_unregister_device(joy_dev);
	free_irq(A730_IRQ(PCA9535_IRQ), NULL);
	input_free_device(joy_dev);
}

static int a730_joy_suspend(struct device *dev, pm_message_t state)
{
    //disable_irq(A730_IRQ(PCA9535_IRQ));
    return 0;
}

static int a730_joy_resume(struct device *dev)
{
    //enable_irq(A730_IRQ(PCA9535_IRQ));
    return 0;
}

module_init(a730_joy_init);
module_exit(a730_joy_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Sroczynski <msroczyn@elka.pw.edu.pl>");
MODULE_DESCRIPTION("Keyboard driver for Asus MyPal 730");
