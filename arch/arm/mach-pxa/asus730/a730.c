/*
 * Machine definition for Asus MyPal A730.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * 2005-03-07	Markus Wagner		Started a730.c, based on hx4700.c
 * 2006-01-19	Nickolay Kopitonenko	USB Client proper initialization
 * 2006-03-29   Serge Nikolaenko	USB ohci platform init
 */


#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/device.h>

//#include <linux/mtd/map.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>
#include <asm/apm.h>
#include <asm/arch/asus730-init.h>
#include <asm/arch/asus730-gpio.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/ohci.h>
#include <asm/arch/udc.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/audio.h>
#include <asm/irq.h>
#include <asm/arch/mmc.h>
#include <linux/delay.h>

#include <linux/platform_device.h>

#include "../generic.h"

static struct pxafb_mach_info asus730_fb_info  = {
	.pixclock =     52000,
	.bpp =          16,
	.xres =         480,
	.yres =         640,
	.hsync_len =    4,
	.vsync_len =    4,
	.left_margin =  77,
	.upper_margin = 0,
	.right_margin = 144,
	.lower_margin = 0,
	.sync =         0,
	//.lccr0 =	(LCCR0_PAS),
	.lccr0 =	(LCCR0_Act | LCCR0_Sngl | LCCR0_Color),
	.lccr3 =        0,
	//.pxafb_backlight_power = a730_backlight_power,
	//.pxafb_lcd_power = a730_lcd_power,
};

static struct platform_device a730_device = {
    .name = "a730",
    .id = 0,
};

static struct platform_device a730_power = {
    .name = "a730-power",
};

static struct platform_device a730_keys = {
    .name = "a730-keys",
};

static struct platform_device a730_lcd = {
    .name = "a730-lcd",
};

extern struct platform_device a730_bl;

static struct platform_device a730_bt = {
    .name = "a730-bt",
};

static struct platform_device a730_pcmcia = {
    .name = "a730-pcmcia",
};

static struct platform_device a730_mmc = {
    .name = "a730-mmc",
};

static pxa2xx_audio_ops_t a730_audio_ops = {
    /*
    .startup  = a730_audio_startup,
    .shutdown = mst_audio_shutdown,
    .suspend  = mst_audio_suspend,
    .resume   = mst_audio_resume,
    */
};

static struct platform_device a730_ac97 = {
    .name = "pxa2xx-ac97",
    .id   = -1,
    .dev  = {
        .platform_data = &a730_audio_ops
    },
};

static struct platform_device *a730_devices[] __initdata = {
    &a730_lcd,
    &a730_bl,
    &a730_device,
    &a730_power,
    &a730_keys,
    &a730_bt,
    &a730_pcmcia,
    &a730_mmc,
    &a730_ac97,
};

/* Serial cable */
static void check_serial_cable( void )
{
	/* XXX: Should this be handled better? */
	int connected = GET_A730_GPIO( COM_DCD );
	/* Toggle rs232 transceiver power according to connected status */
	SET_A730_GPIO( RS232_ON, connected);
	SET_A730_GPIO( COM_CTS, connected );
	SET_A730_GPIO( COM_TXD, connected );
	SET_A730_GPIO( COM_DTR, connected );
	SET_A730_GPIO( COM_RTS, connected );
}

static int a730_udc_is_connected (void)
{
	int ret = GET_A730_GPIO(USB_CABLE_DETECT_N);
	printk ("%s: %s %d\n",__FILE__,__PRETTY_FUNCTION__,ret);
	return ret;
}

static void a730_udc_command (int cmd)
{
	switch (cmd) {
		case PXA2XX_UDC_CMD_DISCONNECT:
			SET_A730_GPIO(USB_PULL_UP, 0);
			//SET_A730_GPIO(USB_PUEN, 0);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			SET_A730_GPIO(USB_PULL_UP, 1);
			//SET_A730_GPIO(USB_PUEN, 1);
			break;
	}
}

static int a730_ohci_init(struct device *dev)
{
//	UHCHCON = (UHCHCON_HCFS_USBOPERATIONAL | /*UHCHCON_PLE |*/ UHCHCON_CLE | UHCHCON_CBSR41);//0x97 (USBOPERATIONAL | CLE(may be not set?) | PLE | CBSR=0x3)
//	UHCINTE = (UHCINT_MIE | UHCINT_RHSC | UHCINT_UE | UHCINT_WDH | UHCINT_SO);//0x80000053;// (MIE | RHSC | UE | WDH | SO)
//	UHCINTD = (UHCINT_MIE | UHCINT_RHSC | UHCINT_UE | UHCINT_WDH | UHCINT_SO);//0x80000053;// (MIE | RHSC | UE | WDH | SO)
//	UHCFMI = 0x27782edf;
//	//UHCFMR = 0x2d6b;// (no need to set ?)
//	//UHCFMN = 0xd1bc;// (no need to set ?)
//	//intel says typical val is 0x3e67. wince sets to 0x2a2f
//	UHCPERS = 0x3e67;//0x2a2f
//	UHCLS = 0x628;// (lsthreshold=0x628)
//	UHCRHDA = 0x4001a02;// (numberdownstreamports=1 | psm=1 | overcurrentprotection=1 | noovercurrentprotection=1 | powerontopowergoodtime(bit26)=1)
//	UHCRHDB = 0x0;
//	UHCRHS = 0x0;
//	UHCRHPS1 = 0x100;// (port power on)
//	UHCRHPS2 = 0x100;// (port power on)
//	UHCRHPS3 = 0x100;// (port power on)
//	UHCSTAT = 0x0;
//	UHCHR = (UHCHR_PCPL | UHCHR_CGR);//0x84 (power control polarity low | clock generation reset inactive)
//	UHCHIE = 0x0;
//	UHCHIT = 0x0;

	return 0;
}

static void a730_ohci_exit(struct device *dev)
{
}

static struct pxaohci_platform_data a730_ohci_platform_data = {
	.init		= a730_ohci_init,
	.exit		= a730_ohci_exit,
	.port_mode	= /*PMM_NPS_MODE,//*/PMM_PERPORT_MODE,
};

static struct pxa2xx_udc_mach_info a730_udc_info = {
	.udc_is_connected = a730_udc_is_connected,
	.udc_command      = a730_udc_command,
};

/* Initialization code */
static void __init a730_map_io(void)
{
	pxa_map_io();
	
	PWER = PWER_RTC | PWER_GPIO0 | PWER_GPIO1 | PWER_GPIO10 | PWER_GPIO12 | PWER_GPIO(17) | PWER_GPIO13 | PWER_WEP1 | /*PWER_USBC |*/ (0x1 << 24 /*wakeup enable for standby or sleep*/ | (0x1 << 16 /* CF card */));
	PFER = PWER_GPIO1 | PWER_GPIO12 | PWER_GPIO10;
	PRER = PWER_GPIO0 | PWER_GPIO12;
	
	/*printk("PGSR0 0x%x - 0x%x\n", PGSR0, GPSRx_SleepValue);
	printk("PGSR1 0x%x - 0x%x\n", PGSR1, GPSRy_SleepValue);
	printk("PGSR2 0x%x - 0x%x\n", PGSR2, GPSRz_SleepValue);
	
	printk("GAFR0_L 0x%x - 0x%x\n", GAFR0_L, GAFR0x_InitValue);
        printk("GAFR0_U 0x%x - 0x%x\n", GAFR0_U, GAFR1x_InitValue);
        printk("GAFR1_L 0x%x - 0x%x\n", GAFR1_L, GAFR0y_InitValue);
        printk("GAFR1_U 0x%x - 0x%x\n", GAFR1_U, GAFR1y_InitValue);
        printk("GAFR2_L 0x%x - 0x%x\n", GAFR2_L, GAFR0z_InitValue);
        printk("GAFR2_U 0x%x - 0x%x\n", GAFR2_U, GAFR1z_InitValue);
        
        printk("GPDR0 0x%x - 0x%x\n", GPDR0, GPDRx_InitValue);
        printk("GPDR1 0x%x - 0x%x\n", GPDR1, GPDRy_InitValue);
        printk("GPDR2 0x%x - 0x%x\n", GPDR2, GPDRz_InitValue);
        
        printk("GPSR0 0x%x - 0x%x\n", GPSR0, GPSRx_InitValue);
        printk("GPSR1 0x%x - 0x%x\n", GPSR1, GPSRy_InitValue);
        printk("GPSR2 0x%x - 0x%x\n", GPSR2, GPSRz_InitValue);

        printk("GPCR0 0x%x - 0x%x\n", GPCR0, ~GPSRx_InitValue);
        printk("GPCR1 0x%x - 0x%x\n", GPCR1, ~GPSRy_InitValue);
        printk("GPCR2 0x%x - 0x%x\n", GPCR2, ~GPSRz_InitValue);*/
	
	/* Configure power management stuff. */
        PGSR0 = GPSRx_SleepValue;
        PGSR1 = GPSRy_SleepValue;
	PGSR2 = GPSRz_SleepValue;
        
        /* Set up GPIO direction and alternate function registers */
        GAFR0_L = GAFR0x_InitValue;
        GAFR0_U = GAFR1x_InitValue;
        GAFR1_L = GAFR0y_InitValue;
        GAFR1_U = GAFR1y_InitValue;
        GAFR2_L = GAFR0z_InitValue;
	GAFR2_U = GAFR1z_InitValue;

        /*GPDR0 = GPDRx_InitValue;
        GPDR1 = GPDRy_InitValue;
	GPDR2 = GPDRz_InitValue;*/

/*        GPSR0 = GPSRx_InitValue;
        GPSR1 = GPSRy_InitValue;
        GPSR2 = GPSRz_InitValue;

        GPCR0 = ~GPSRx_InitValue;
        GPCR1 = ~GPSRy_InitValue;
        GPCR2 = ~GPSRz_InitValue;*/
        
        check_serial_cable();
}

static void __init a730_init_irq(void)
{
	pxa_init_irq();
}
#ifdef CONFIG_PM
extern int a730_suspend(struct device *dev, pm_message_t state);
extern int a730_resume(struct device *dev);
#endif

static int a730_probe(struct device *dev)
{
	return !machine_is_a730();
}

extern u32 pca9535_read_input(void);

static void a730_apm_get_power_status(struct apm_power_info *info)
{
    u32 data;
    
    if (!info) return;
    
    data = pca9535_read_input();
    
    if ((data & 0x2) && (data & 0x4000)) info->ac_line_status = APM_AC_OFFLINE;
    else info->ac_line_status = APM_AC_ONLINE;
    
    //TODO:battery
    
    info->time = 0;
    info->units = 0;
}

static struct device_driver a730_driver = {
	.name           = "a730",
	.bus            = &platform_bus_type,
	.probe          = a730_probe,
#ifdef CONFIG_PM
	.suspend        = a730_suspend,
	.resume         = a730_resume,
#endif
};

static int a730_hw_init(void)
{
	GCR &= ~(GCR_PRIRDY_IEN | GCR_SECRDY_IEN);

	//USB host
	//CKEN |= CKEN10_USBHOST;
	//udelay(15);
	//UHCHR &= ~UHCHR_FHR;
}

extern void a730_ll_pm_init (void);

static void __init a730_init (void)
{
	//pxa_gpio_mode(GPIO_NR_A730_AC97_SYSCLK | GPIO_ALT_FN_1_OUT);
	a730_hw_init();
	
	set_pxa_fb_info(&asus730_fb_info);
	pxa_set_udc_info(&a730_udc_info);
//	pxa_set_ohci_info(&a730_ohci_platform_data);
#ifdef CONFIG_PM
	apm_get_power_status = &a730_apm_get_power_status;
	a730_ll_pm_init();
#endif
	driver_register(&a730_driver);
	platform_add_devices(a730_devices, ARRAY_SIZE(a730_devices));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MACHINE_START(A730, "Asus MyPal A730(W)")
	/* "Markus1108wagner@t-online.de" */
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params	= 0xa0000100,
	.map_io		= a730_map_io,
	.init_irq	= a730_init_irq,
	.timer		= &pxa_timer,
	.init_machine	= a730_init,
MACHINE_END
