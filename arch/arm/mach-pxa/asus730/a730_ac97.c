/*
 * based on linux/arch/arm/mach-pxa/palmld/palmld_ac97.c && linux/arch/arm/mach-pxa/palmld/palmtx_ac97.c
 *	Touchscreen/battery driver for ASUS A730(W). WM9712 AC97 codec.
 *	Author: Serge Nikolaenko <mypal_hh@utl.ru>
 */
 
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/device.h>
#include <linux/workqueue.h>
#include <linux/battery.h>
#include <linux/irq.h>

#include <asm/delay.h>
#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/arch/asus730-gpio.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/initval.h>
#include <sound/control.h>
#include <sound/ac97_codec.h>
#include <sound/wm9712.h>

/********************************************************/

//static spinlock_t a730_ac97_lock = SPIN_LOCK_UNLOCKED;

static struct workqueue_struct *wm9712_phonejack_delayqueue;
static struct work_struct wm9712_phonejack_delay_task;
static struct workqueue_struct *wm9712_pendown_workqueue;
static struct work_struct wm9712_pendown_irq_task;
static struct workqueue_struct *wm9712_phonejack_workqueue;
static struct work_struct wm9712_phonejack_irq_task;

static struct device *a730_ac97_dev = NULL;
static struct snd_ac97 *ac97 = NULL;
static struct input_dev *a730_ac97_input = NULL;
//
static u64 last_batt_jiff = 0;
static int battery_voltage = 0;
static int last_voltage = 0;
static int cur_voltage = 0;

#define PJ_IN 1
#define PJ_OUT 0
static int a730_phonejack_state = 1;//jack state (0=plugged out; 1=plugged in)

//
static void a730_wm9712_set_routing(struct snd_ac97 *ac97, int allow_phones, int allow_speaker);
//
static DECLARE_MUTEX(digitiser_mutex);
static DECLARE_MUTEX_LOCKED(pendown_mutex);
static DECLARE_MUTEX_LOCKED(jack_mutex);
static DECLARE_MUTEX(battery_update_mutex);
static DECLARE_MUTEX(jack_state_switch_sem);

/*
 * ac97 codec
 */

void wm97xx_gpio_func(struct snd_ac97 *ac97, int gpio, int func)
{
	int GEn;
	GEn = snd_ac97_read(ac97, 0x56);
	if(func) GEn |= gpio;
	else GEn &= ~gpio;
	snd_ac97_write(ac97, 0x56, GEn);
}
	
void wm97xx_gpio_mode(struct snd_ac97 *ac97, int gpio, int config, int polarity, int sticky, int wakeup)
{
	int GCn, GPn, GSn, GWn;
	GCn = snd_ac97_read(ac97, 0x4c);
	GPn = snd_ac97_read(ac97, 0x4e);
	GSn = snd_ac97_read(ac97, 0x50);
	GWn = snd_ac97_read(ac97, 0x52);
	
	if (config) GCn |= gpio;
	else GCn &= ~gpio;
	
	if (polarity) GPn |= gpio;
	else GPn &= ~gpio;
	
	if (sticky) GSn |= gpio;
	else GSn &= ~gpio;
	
	if (wakeup) GWn |= gpio;
	else GWn &= ~gpio;
	
	snd_ac97_write(ac97, 0x4c, GCn);
	snd_ac97_write(ac97, 0x4e, GPn);
	snd_ac97_write(ac97, 0x50, GSn);
	snd_ac97_write(ac97, 0x52, GWn);
}

static void wm9712_set_digitiser_power(struct device *dev, int value)
{
	u16 d2;
	
	d2 = snd_ac97_read(ac97, AC97_WM97XX_DIGITISER2);
	d2 |= value;
	snd_ac97_write(ac97, AC97_WM97XX_DIGITISER2, d2);
}

#define AC97_LINK_FRAME   21         /* time in uS for AC97 link frame */

static int wm9712_take_reading(int adcsel)
{
	int timeout = 15;
	u16 r76 = 0;
	u16 r7a;

	r76 |= adcsel;		        // set ADCSEL (ADC source)
	r76 |= WM97XX_DELAY(3);		// set settling time delay
	r76 &= ~(1<<11);	        // COO = 0 (single measurement)
	r76 &= ~(1<<10);	        // CTC = 0 (polling mode)
	r76 |=	(1<<15);	        // initiate measurement (POLL)
	
	snd_ac97_write(ac97, 0x76, r76);
	
	// wait settling time
	udelay ((3 * AC97_LINK_FRAME) + 167);
	
	// wait for POLL to go low
	while((snd_ac97_read(ac97, 0x76) & 0x8000) && timeout)
	{
		udelay(AC97_LINK_FRAME);
		timeout--;	
	}

	if (timeout == 0){
		printk("palmld_ac97: discarding reading due to POLL wait timout on 0x76\n");	
		return 0;
	}

	r7a = snd_ac97_read(ac97, 0x7a);
	
	if ((r7a & WM97XX_ADCSEL_MASK) != adcsel)
	{
		printk("palmld_ac97: discarding reading -> wrong ADC source\n");
		return 0;
	}
			
	return (int) r7a;
}

/*
 * battery
 */
 
void update_data(int force)
{
	u16 reading;

	if(!force && ((last_batt_jiff + HZ * 10) > jiffies)) return;
	
	if(down_trylock(&battery_update_mutex) != 0) return;
	
	down(&digitiser_mutex);
	wm9712_set_digitiser_power(a730_ac97_dev, WM97XX_PRP_DET_DIG);
	reading = wm9712_take_reading(WM9712_ADCSEL_BMON);
	wm9712_set_digitiser_power(a730_ac97_dev, WM97XX_PRP_DET);
	up(&digitiser_mutex);
	
	last_voltage = cur_voltage;
	cur_voltage = reading & 0xfff;
	printk("Battery: %d\n", battery_voltage);

	last_batt_jiff = jiffies;
	
	up(&battery_update_mutex);
}
 
int get_min_voltage(struct battery *b)
{
    return 0;
}
int get_min_charge(struct battery *b)
{
    return 0;
}
int get_max_voltage(struct battery *b)
{
    return 4750; /* mV */
}
int get_max_charge(struct battery *b)
{
    return 1;
}
int get_voltage(struct battery *b)
{
	update_data(0);
	return cur_voltage * 3 * 80586 / 100000;
}
int get_charge(struct battery *b)
{
    return 0;
}
int get_status(struct battery *b)
{
    return 0; //power_status == POWER_NONE? 0: 1;
}

static struct battery a730_battery = {
    .name               = "a730-ac97",
    .id                 = "battery0",
    .get_min_voltage    = get_min_voltage,
    .get_min_current    = NULL,
    .get_min_charge     = get_min_charge,
    .get_max_voltage    = get_max_voltage,
    .get_max_current    = NULL,
    .get_max_charge     = get_max_charge,
    .get_temp           = NULL,
    .get_voltage        = get_voltage,
    .get_current        = NULL,
    .get_charge         = get_charge,
    .get_status         = get_status,
};

/*
static int
battery_class_hotplug(struct class_device *dev, char **envp, int num_envp,
    char *buffer, int buffer_size)
{
        return 0;
}
*/

static void battery_class_release(struct class_device *dev)
{
}

static void battery_class_class_release(struct class *class)
{
}

/*
 * touchscreen 
 */

static void wm9712_pendown(void)
{
	int xread, yread, pressure;
	int valid_coords = 0, pendown = 0;
	
	//pen down brought us here
	input_report_key(a730_ac97_input, BTN_TOUCH, 1);
	input_sync(a730_ac97_input);
	
	do {
		xread = wm9712_take_reading(WM97XX_ADCSEL_X);
		yread = wm9712_take_reading(WM97XX_ADCSEL_Y);
		pressure = wm9712_take_reading(WM97XX_ADCSEL_PRES);
		
		//each value need to have bit 15 to be set - pen is down
		pendown = ((xread & 0x8000) && (yread & 0x8000) && (pressure & 0x8000));
		
		xread &= 0xfff;
		yread &= 0xfff;
		pressure &= 0xfff;
		
		//not zero
		valid_coords = xread && yread && pressure;
		
		if (pendown && valid_coords && (pressure < 500))
		{
			input_report_abs(a730_ac97_input, ABS_X, xread);
			input_report_abs(a730_ac97_input, ABS_Y, yread);
			input_report_abs(a730_ac97_input, ABS_PRESSURE, pressure);
			input_sync(a730_ac97_input);
		}
		else break;
		set_current_state(TASK_UNINTERRUPTIBLE);
		schedule_timeout(HZ/100);
		set_current_state(TASK_RUNNING);
	} while(pendown && valid_coords);
	
	input_report_key(a730_ac97_input, BTN_TOUCH, 0);
	input_report_abs(a730_ac97_input, ABS_X, 0);
	input_report_abs(a730_ac97_input, ABS_Y, 0);
	input_report_abs(a730_ac97_input, ABS_PRESSURE, 0);
	input_sync(a730_ac97_input);
}

static void wm9712_pendown_handler(void *data)
{
	u16 levels, polarity;
	
	levels = snd_ac97_read(ac97, AC97_GPIO_STATUS);
	//polarity = snd_ac97_read(ac97, AC97_GPIO_POLARITY);

	if (levels & WM97XX_GPIO_13)
	{
		down(&digitiser_mutex);
		wm9712_pendown();
		up(&digitiser_mutex);
		//polarity &= ~(WM97XX_GPIO_13);
	}
	//else polarity |= WM97XX_GPIO_13;

	levels &= ~WM97XX_GPIO_13;

	//snd_ac97_write(ac97, AC97_GPIO_POLARITY, polarity);
	//drop GPIO13 bit in any case
	snd_ac97_write(ac97, AC97_GPIO_STATUS, levels);
	up(&pendown_mutex);
}

static void wm9712_phonejack_switch_thread(void *data)
{
	msleep(100);
	a730_wm9712_set_routing(ac97, a730_phonejack_state != PJ_OUT, a730_phonejack_state == PJ_OUT);
}

static void wm9712_phonejack_handler(void *data)
{
	u16 levels, polarity;
	
	levels = snd_ac97_read(ac97, AC97_GPIO_STATUS);
	polarity = snd_ac97_read(ac97, AC97_GPIO_POLARITY);
	
	if (levels & WM97XX_GPIO_1)
	{
		if (polarity & WM97XX_GPIO_1)
		{
			a730_phonejack_state = PJ_OUT;//plugged out
			polarity &= ~WM97XX_GPIO_1;
		}
		else
		{
			a730_phonejack_state = PJ_IN;//plugged in
			polarity |= WM97XX_GPIO_1;
		}
		if (down_trylock(&jack_state_switch_sem) == 0)
		{
			queue_work(wm9712_phonejack_delayqueue, &wm9712_phonejack_delay_task);
			up(&jack_state_switch_sem);
		}
	}
	
	levels &= ~WM97XX_GPIO_1;
	
	snd_ac97_write(ac97, AC97_GPIO_POLARITY, polarity);
	snd_ac97_write(ac97, AC97_GPIO_STATUS, levels);
	
	up(&jack_mutex);
}

static irqreturn_t wm9712_pendown_irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{
	if (!down_trylock(&pendown_mutex)) queue_work(wm9712_pendown_workqueue, &wm9712_pendown_irq_task);
	return IRQ_HANDLED;
}

static irqreturn_t wm9712_jack_irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{
	if (!down_trylock(&jack_mutex)) queue_work(wm9712_phonejack_workqueue, &wm9712_phonejack_irq_task);
	return IRQ_HANDLED;
}

static int suspended = 0;

static int a730_wm9712_setup(void)
{
	u16 tmp;
	
	/* reset levels */
	//snd_ac97_write(ac97, AC97_GPIO_STATUS, 0);
	
	/* turn off irq gpio inverting, turn on jack insert detect */
	tmp = snd_ac97_read(ac97, AC97_ADD_FUNC);
        tmp &= ~1;//no invert irq
        tmp |= 2;//wake ena
        tmp &= ~(0x01 << 11);
        tmp |= (0x01 << 12);//tmp |= (0x01 << 12);
        snd_ac97_write(ac97, AC97_ADD_FUNC, tmp);
	
	/* disable digitiser to save power, enable pen-down detect */
	tmp = snd_ac97_read(ac97, AC97_WM97XX_DIGITISER2);
	tmp |= WM97XX_PRP_DET;
	snd_ac97_write(ac97, AC97_WM97XX_DIGITISER2, tmp);

	/* enable interrupts on codec's gpio 2 (connected to cpu gpio 27) */
	wm97xx_gpio_mode(ac97, WM97XX_GPIO_2, WM97XX_GPIO_OUT, WM97XX_GPIO_POL_HIGH, WM97XX_GPIO_NOTSTICKY, WM97XX_GPIO_NOWAKE);
	wm97xx_gpio_func(ac97, WM97XX_GPIO_2, 0);

	/* enable interrupts on codec's gpio 3 (connected to cpu gpio 25) */
	wm97xx_gpio_mode(ac97, WM97XX_GPIO_3, WM97XX_GPIO_OUT, WM97XX_GPIO_POL_HIGH, WM97XX_GPIO_NOTSTICKY, WM97XX_GPIO_NOWAKE);
	wm97xx_gpio_func(ac97, WM97XX_GPIO_3, 0);

	wm97xx_gpio_mode(ac97, WM97XX_GPIO_1, WM97XX_GPIO_IN, WM97XX_GPIO_POL_HIGH, WM97XX_GPIO_STICKY, WM97XX_GPIO_WAKE);
	wm97xx_gpio_func(ac97, WM97XX_GPIO_1, 1);
	wm97xx_gpio_mode(ac97, WM97XX_GPIO_13, WM97XX_GPIO_OUT, WM97XX_GPIO_POL_HIGH, WM97XX_GPIO_NOTSTICKY, WM97XX_GPIO_NOWAKE);
	
	/* turn on the digitiser and pen down detector */
	tmp = snd_ac97_read(ac97, AC97_WM97XX_DIGITISER2);
	tmp |= WM97XX_PRP_DETW;// | (0x1 << 9);//plus, WAIT
	snd_ac97_write(ac97, AC97_WM97XX_DIGITISER2, tmp);
	
	up(&jack_mutex);
	up(&pendown_mutex);
	
	snd_ac97_write(ac97, AC97_GPIO_STATUS, 0);
	snd_ac97_write(ac97, AC97_POWERDOWN, 0);

	return 0;
}

static int a730_wm9712_suspend(struct device *dev, pm_message_t state)
{
	//disable_irq(A730_IRQ(PENDOWN_IRQ));
	//disable_irq(A730_IRQ(JACK_IRQ));
	suspended = 1;
	return 0;
}

static int a730_wm9712_resume(struct device *dev)
{
	suspended = 0;
	a730_wm9712_setup();
	return 0;
}

//set, where pcm will play to
static void a730_wm9712_set_routing(struct snd_ac97 *ac97, int allow_phones, int allow_speaker)
{
	int tmp = 0, reg2 = 0, reg4 = 0, reg18 = 0, reg24 = 0, reg26 = 0;
	
	if (allow_phones) tmp &= ~(1 << 15);
	else tmp |= (1 << 15);
	if (allow_speaker) tmp &= ~(1 << 14);
	else tmp |= (1 << 14);
	
	reg2 = snd_ac97_read(ac97, 0x2);
	reg4 = snd_ac97_read(ac97, 0x4);
	reg18 = snd_ac97_read(ac97, 0x18);
	reg24 = snd_ac97_read(ac97, 0x24);
	reg26 = snd_ac97_read(ac97, 0x26);
	
	if (a730_phonejack_state == PJ_OUT)
        {
        	printk("set_audio_out: p=%d s=%d\n", allow_phones, allow_speaker);
                reg2 |= (1 << 6);
                reg18 |= (1 << 13);
                reg4 |= (1 << 15);
                reg24 = 0x1F77;//speaker
                reg26 |= (1 << 8);
                reg26 &= ~(1 << 9);//DAC on
                reg26 |= (1 << 14);
        }
        else
        {
        	printk("set_audio_out: p=%d s=%d\n", allow_phones, allow_speaker);
                reg2 &= ~(1 << 6);
                reg18 &= ~(1 << 13);
                reg4 &= ~(0x01 << 15);
                reg24 = 0x1CEF;//hphones
                reg26 |= (1 << 8);
                reg26 &= ~(1 << 9);
                reg26 &= ~(1 << 14);
        }
	
	snd_ac97_update(ac97, 0x2, reg2);
        snd_ac97_update(ac97, 0x4, reg4);
        snd_ac97_update(ac97, 0x18, reg18);
        snd_ac97_update(ac97, 0x24, reg24);
        snd_ac97_update(ac97, 0x26, reg26);
	
	//unmute dacs
	//snd_ac97_update_bits(ac97, AC97_PCM, tmp, tmp);
}

static int __init a730_ac97_probe(struct device *dev)
{
	int err;
	int irqflag = IRQF_DISABLED;
#ifdef CONFIG_PREEMPT_RT
	irqflag |= IRQF_NODELAY;
#endif
	
	ac97 = to_ac97_t(dev);
	a730_ac97_dev = dev;

	printk("%s\n", __FUNCTION__);
	
	wm9712_pendown_workqueue = create_singlethread_workqueue("pendownd");
	INIT_WORK(&wm9712_pendown_irq_task, wm9712_pendown_handler, dev);
	wm9712_phonejack_delayqueue = create_singlethread_workqueue("jackdd");
	INIT_WORK(&wm9712_phonejack_delay_task, wm9712_phonejack_switch_thread, dev);
	wm9712_phonejack_workqueue = create_singlethread_workqueue("jackd");
	INIT_WORK(&wm9712_phonejack_irq_task, wm9712_phonejack_handler, dev);

	/* setup irq */
	set_irq_type(A730_IRQ(PENDOWN_IRQ), IRQF_TRIGGER_RISING);
	err = request_irq(A730_IRQ(PENDOWN_IRQ), wm9712_pendown_irq_handler, irqflag, "WM9712 IRQ2", NULL);
	if(err) {
		printk("a730_ac97_probe: cannot request pen down IRQ\n");
		return -1;
	}

	set_irq_type(A730_IRQ(JACK_IRQ), IRQF_TRIGGER_RISING);
	err = request_irq(A730_IRQ(JACK_IRQ), wm9712_jack_irq_handler, irqflag, "WM9712 IRQ3", NULL);
	if(err) {
		printk("a730_ac97_probe: cannot request jack detect IRQ\n");
		return -1;
	}
	
	/* setup the input device */
	a730_ac97_input = input_allocate_device();

	a730_ac97_input->name = "Asus A730(W) touchscreen";
	a730_ac97_input->phys = "touchscreen/A730";
	a730_ac97_input->evbit[0] = BIT(EV_KEY) | BIT(EV_ABS);
	a730_ac97_input->keybit[LONG(BTN_TOUCH)] = BIT(BTN_TOUCH);
	input_set_abs_params(a730_ac97_input, ABS_X, 190, 3900, 5, 0);
	input_set_abs_params(a730_ac97_input, ABS_Y, 190, 3900, 35, 0);
	input_set_abs_params(a730_ac97_input, ABS_PRESSURE, 0, 150, 5, 0);
	a730_ac97_input->dev = dev;
	a730_ac97_input->id.bustype = BUS_HOST;
	input_register_device(a730_ac97_input);
	
	/* register battery */
	if(battery_class_register(&a730_battery)) {
		printk(KERN_ERR "a730_ac97_probe: Could not register battery class\n");
	} else {
		a730_battery.class_dev.class->release       = battery_class_release;
	//	a730_battery.class_dev.class->hotplug       = battery_class_hotplug;
		a730_battery.class_dev.class->class_release = battery_class_class_release;
	}

	a730_wm9712_setup();

	return 0;
}

static int a730_ac97_remove (struct device *dev)
{
	printk("x: %x\n", snd_ac97_read(ac97, AC97_WM97XX_DIGITISER2));
	
	disable_irq(A730_IRQ(PENDOWN_IRQ));
	disable_irq(A730_IRQ(JACK_IRQ));
	
	battery_class_unregister(&a730_battery);
	input_unregister_device(a730_ac97_input);
	
	free_irq(A730_IRQ(PENDOWN_IRQ), NULL);
	free_irq(A730_IRQ(JACK_IRQ), NULL);
	
	enable_irq(A730_IRQ(PENDOWN_IRQ));
	enable_irq(A730_IRQ(JACK_IRQ));
	
	return 0;
}

static struct device_driver a730_ac97_driver = {
	.name           = "WM9712",
	.bus            = &ac97_bus_type,
	.probe          = a730_ac97_probe,
        .remove		= a730_ac97_remove,
#ifdef CONFIG_PM
	.suspend        = a730_wm9712_suspend,
	.resume         = a730_wm9712_resume,
#endif
};

static int __init a730_ac97_init(void)
{
	if(!machine_is_a730()) return -ENODEV;
	printk("%s\n", __FUNCTION__);
	return driver_register(&a730_ac97_driver);
}

static void __exit a730_ac97_exit(void)
{
	driver_unregister(&a730_ac97_driver);
}

module_init(a730_ac97_init);
module_exit(a730_ac97_exit);

MODULE_AUTHOR ("Serge Nikolaenko <mypal_hh@utl.ru>");
MODULE_DESCRIPTION ("WM9712 AC97 codec support for Asus A730(W)");
MODULE_LICENSE ("GPL");
