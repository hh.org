/*
 * based on arch/arm/mach-pxa/pxa_keys.c
 * and on arch/arm/mach-pxa/asus716/a716_buttons.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * 03 Apr 2006:
 *	Serge Nikolaenko	split a730_keys.c to a730_buttons.c & a730_joupad.c
 */

#include <linux/input.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/arch/asus730-gpio.h>

#include <linux/platform_device.h>

#define GET_GPIO(gpio) (GPLR(gpio) & GPIO_bit(gpio))
#define DPRINTK(fmt, args...)	printk("%s: " fmt, __FUNCTION__ , ## args)

static int __init a730_buttons_probe(struct device *);
static int a730_buttons_suspend(struct device *, pm_message_t);
static int a730_buttons_resume(struct device *);

static struct device_driver a730_buttons_driver = {
    .name = "a730_buttons",
    .bus = &platform_bus_type,
    .probe = a730_buttons_probe,
#ifdef CONFIG_PM
    .suspend = a730_buttons_suspend,
    .resume = a730_buttons_resume,
#endif
};

static struct input_dev *buttons_dev;

static struct
{
    u16 keyno;
    u8 irq;
    u8 gpio;
    unsigned char inv;
    char *desc;
} buttontab[] = 
{
    {/*125*/KEY_LEFTMETA, A730_IRQ(BUTTON_LAUNCHER), GPIO_NR_A730_BUTTON_LAUNCHER, 1, "LAUNCHER"},//LWIN keycode
    {KEY_CALENDAR, A730_IRQ(BUTTON_CALENDAR), GPIO_NR_A730_BUTTON_CALENDAR, 1, "CALENDAR"},
    {65, A730_IRQ(BUTTON_CONTACTS), GPIO_NR_A730_BUTTON_CONTACTS, 1, "CONTACTS"},
    {KEY_MEMO, A730_IRQ(BUTTON_TASKS), GPIO_NR_A730_BUTTON_TASKS, 1, "TASKS"},
    {KEY_SUSPEND, A730_IRQ(BUTTON_POWER), GPIO_NR_A730_BUTTON_POWER, 0, "POWER"},
    {KEY_RECORD, A730_IRQ(BUTTON_RECORD), GPIO_NR_A730_BUTTON_RECORD, 1, "RECORD"}
};

static irqreturn_t a730_buttons_handle(int irq, void *dev_id, struct pt_regs *regs)
{
    int button, state;
    
    for (button = 0; button < ARRAY_SIZE(buttontab); button++) if (buttontab[button].irq == irq) break;
    
    if (likely(button < ARRAY_SIZE(buttontab)))
    {
        state = (GET_GPIO(buttontab[button].gpio) ? 0 : 1);
	state = (buttontab[button].inv ? !state : state);
	//printk("desc=%s %s\n", buttontab[button].desc, (state ? "P" : "R"));
	
        input_report_key(buttons_dev, buttontab[button].keyno, state);
	if ((buttontab[button].keyno == KEY_SUSPEND) && state) input_event(buttons_dev, EV_PWR, buttontab[button].keyno, state);
        input_sync(buttons_dev);
    }
    return IRQ_HANDLED;
}

static int __init a730_buttons_probe(struct device *dev)
{
    return 0;
}

static int __init a730_buttons_init(void)
{
	int i, irqflag = IRQF_DISABLED;
#ifdef CONFIG_PREEMPT_RT
	irqflag |= IRQF_NODELAY;
#endif

	if (!machine_is_a730()) return -ENODEV;

	if (!(buttons_dev = input_allocate_device())) return -ENOMEM;

	buttons_dev->name = "Asus MyPal 730(W) buttons";
	buttons_dev->id.bustype = BUS_I2C;
	buttons_dev->phys = "a730btn/input0";

	set_bit(EV_KEY, buttons_dev->evbit);
	set_bit(EV_PWR, buttons_dev->evbit);

	buttons_dev->keybit[LONG(KEY_RECORD)] |= BIT(KEY_RECORD);
	buttons_dev->keybit[LONG(KEY_SUSPEND)] |= BIT(KEY_SUSPEND);
	buttons_dev->keybit[LONG(KEY_CALENDAR)] |= BIT(KEY_CALENDAR);

	for (i = 0; i < ARRAY_SIZE(buttontab); i++)
	{
		//request_irq(buttontab[i].irq, a730_buttons_handle, IRQF_SAMPLE_RANDOM, buttontab[i].desc, NULL);
		request_irq(buttontab[i].irq, a730_buttons_handle, irqflag, buttontab[i].desc, NULL);
		set_irq_type(buttontab[i].irq, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING);
		buttons_dev->keybit[LONG(buttontab[i].keyno)] |= BIT(buttontab[i].keyno);
	}

	input_register_device(buttons_dev);

	return 0;
}

static void __exit a730_buttons_exit(void)
{
    int i;

    input_unregister_device(buttons_dev);
    input_free_device(buttons_dev);
    
    for (i = 0; i < ARRAY_SIZE(buttontab); i++)	free_irq(buttontab[i].irq, NULL);
    free_irq(A730_IRQ(BUTTON_POWER), NULL);
}

static int a730_buttons_suspend(struct device *dev, pm_message_t state)
{
/*    int i;
    
    for (i = 0; i < ARRAY_SIZE(buttontab); i++)
    {
	disable_irq(buttontab[i].irq);
    }*/
    return 0;
}

static int a730_buttons_resume(struct device *dev)
{
/*    int i;
    
    for (i = 0; i < ARRAY_SIZE(buttontab); i++)
    {
	enable_irq(buttontab[i].irq);
    }*/
    return 0;
}

module_init(a730_buttons_init);
module_exit(a730_buttons_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Sroczynski <msroczyn@elka.pw.edu.pl>");
MODULE_DESCRIPTION("Keyboard driver for Asus MyPal 730");
