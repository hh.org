/*
 * LED interface for Himalaya, the HTC PocketPC.
 *
 * License: GPL
 *
 * Author: Luke Kenneth Casson Leighton, Copyright(C) 2004
 *
 * Copyright(C) 2004, Luke Kenneth Casson Leighton.
 *
 * History:
 *
 * 2004-02-19	Luke Kenneth Casson Leighton	created.
 *
 */
 
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/leds.h>
#include <linux/soc/asic3_base.h>

#include <asm/io.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/arch/htcuniversal-gpio.h>
#include <asm/arch/htcuniversal-asic.h>
#include <asm/mach-types.h>

#include "htcuniversal_leds.h"

#ifdef DEBUG
#define dprintk(x...) printk(x)
#else
#define dprintk(x...)
#endif

struct htcuniversal_led_data {
	int                     hw_num;
	int                     duty_time;
	int                     cycle_time;
	int                     registered;
	int                     brightness;
	int                     color;
	struct led_properties   props;
};

#define to_htcuniversal_led_data(d) container_of(d, struct htcuniversal_led_data, props)

void htcuniversal_set_led (int led, int brightness, int duty_time, int cycle_time)
{
	dprintk("htcuniversal_set__asic3_led\n");
	if (led == HTC_RED_LED || led == HTC_GREENR_LED || led == HTC_YELLOW_LED) {
		if (brightness) {
			duty_time=(duty_time*128+500)/1000;
			cycle_time=(cycle_time*128+500)/1000;
		} else {
			duty_time=0;
			cycle_time=1;
		}
	}

	switch(led) {
	case HTC_RED_LED:
		asic3_set_led(&htcuniversal_asic3.dev, 1, duty_time, cycle_time);
		break;
	case HTC_GREENR_LED:
		asic3_set_led(&htcuniversal_asic3.dev, 2, duty_time, cycle_time);
		break;
	case HTC_YELLOW_LED:
		asic3_set_led(&htcuniversal_asic3.dev, 1, duty_time, cycle_time);
		asic3_set_led(&htcuniversal_asic3.dev, 2, duty_time, cycle_time);
		break;
	case HTC_BLUETOOTH_LED:
	case HTC_WIFI_LED:
		asic3_set_led(&htcuniversal_asic3.dev, 0, duty_time, cycle_time);
		break;
	}
}

EXPORT_SYMBOL(htcuniversal_set_led);


int htcuniversal_led_brightness_get(struct device *dev, struct led_properties *props)
{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	return data->brightness;
}

void htcuniversal_led_brightness_set(struct device *dev, struct led_properties *props, int value)

/* Brightness levels supported by the hardware: 0 (off) and 100 (on)*/

{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	data->brightness = value > 0 ? 100 : 0;
	if (! data->brightness && 
	 (data->hw_num == HTC_RED_LED || data->hw_num == HTC_GREENR_LED || data->hw_num == HTC_YELLOW_LED )) {
		data->duty_time=1000;
		data->cycle_time=1000;
	}
	switch (data->hw_num) {
	case HTC_RED_LED:
	case HTC_GREENR_LED:
	case HTC_YELLOW_LED:
		htcuniversal_set_led(data->hw_num, data->brightness, data->duty_time, data->cycle_time);
		break;
	case HTC_PHONE_BL_LED:
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYP_PWR_ON, (value > 0) ? (1<<GPIOD_BL_KEYP_PWR_ON) : 0);
		break;
	case HTC_KEYBD_BL_LED:
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYB_PWR_ON, (value > 0) ? (1<<GPIOD_BL_KEYB_PWR_ON) : 0);
		break;
	case HTC_VIBRA:
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_VIBRA_PWR_ON, (value > 0) ? (1<<GPIOD_VIBRA_PWR_ON) : 0);
		break;
	case HTC_CAM_FLASH_LED:
		asic3_set_gpio_out_a(&htcuniversal_asic3.dev, 1<<GPIOA_FLASHLIGHT, (value > 0) ? (1<<GPIOA_FLASHLIGHT) : 0);
		break;
	case HTC_BLUETOOTH_LED:
		if (value > 0)
		 htcuniversal_egpio_enable(EGPIO5_BT_3V3_ON);
		else
		 htcuniversal_egpio_disable(EGPIO5_BT_3V3_ON);
		break;
	case HTC_WIFI_LED:
		if (value > 0)
		 htcuniversal_egpio_enable(EGPIO6_WIFI_ON);
		else
		 htcuniversal_egpio_disable(EGPIO6_WIFI_ON);
		break;
	}
	printk("brightness=%d for the led=%d\n",value, data->hw_num);
}

int
htcuniversal_led_cycle_get(struct device *dev, struct led_properties *props)
{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	return data->cycle_time;
}

void
htcuniversal_led_cycle_set(struct device *dev, struct led_properties *props, int value)
{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	data->cycle_time=value;
	if (data->brightness)
		htcuniversal_set_led(data->hw_num, data->brightness, data->duty_time, data->cycle_time);
		
}

int
htcuniversal_led_duty_get(struct device *dev, struct led_properties *props)
{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	return data->duty_time;
}

void
htcuniversal_led_duty_set(struct device *dev, struct led_properties *props, int value)
{
	struct htcuniversal_led_data *data = to_htcuniversal_led_data(props);

	data->duty_time=value;
	if (data->brightness)
		htcuniversal_set_led(data->hw_num, data->brightness, data->duty_time, data->cycle_time);
}

static struct htcuniversal_led_data leds[] = {
      	{ 
		.hw_num=HTC_RED_LED,
		.duty_time=1000,
		.cycle_time=1000,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "red",
			.color	       = "red",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_GREENR_LED,
		.duty_time=1000,
		.cycle_time=1000,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "green",
			.color	       = "green",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_YELLOW_LED,
		.duty_time=1000,
		.cycle_time=1000,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "yellow",
			.color	       = "yellow",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_PHONE_BL_LED,
		.brightness=100,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "phone",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_KEYBD_BL_LED,
		.brightness=0,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "keyboard",
			.color	       = "rosa",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_VIBRA,
		.brightness=0,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "vibra",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_BLUETOOTH_LED,
		.duty_time=1000,
		.cycle_time=500,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "bluetooth",
			.color	       = "bt_blue",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_WIFI_LED,
		.duty_time=1000,
		.cycle_time=500,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "wifi",
			.color	       = "wifi_green",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
	{
		.hw_num=HTC_CAM_FLASH_LED,
		.props  = {
			.owner	       = THIS_MODULE,
			.name	       = "flashlight",
			.color	       = "white",
			.brightness_get = htcuniversal_led_brightness_get,
			.brightness_set = htcuniversal_led_brightness_set,
		}
	},
};

static int htcuniversal_led_probe(struct device *dev)
{
	int i,ret=0;

	dprintk("htcuniversal_led_probe\n");
        /* Turn on the LED controllers in CDEX */
        asic3_set_clock_cdex(&htcuniversal_asic3.dev,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2
        );

	htcuniversal_set_led(HTC_RED_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_GREENR_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_YELLOW_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_PHONE_BL_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_KEYBD_BL_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_VIBRA, 0, 0, 0);
	htcuniversal_set_led(HTC_BLUETOOTH_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_WIFI_LED, 0, 0, 0);
	htcuniversal_set_led(HTC_CAM_FLASH_LED, 0, 0, 0);

	for (i = 0; i < ARRAY_SIZE(leds); i++) {
	       ret = leds_device_register(dev, &leds[i].props);
	       leds[i].registered = 1;
	       if (unlikely(ret)) {
		       printk(KERN_WARNING "Unable to register htcuniversal led %s\n", leds[i].props.color);
		       leds[i].registered = 0;
	       }
	}
	return ret;
}

static int htcuniversal_led_remove(struct device *dev)
{
	int i;

	dprintk("htcuniversal_led_remove\n");

	for (i = 0; i < ARRAY_SIZE(leds); i++) {
	       if (leds[i].registered) {
		       leds_device_unregister(&leds[i].props);
	       }
	}
	return 0;
}

static int 
htcuniversal_led_suspend(struct device *dev, u32 state, u32 level)
{
        /* Turn off the LED controllers in CDEX */
        asic3_set_clock_cdex(&htcuniversal_asic3.dev,
                ~CLOCK_CDEX_LED0 & ~CLOCK_CDEX_LED1 & ~CLOCK_CDEX_LED2,
                ~CLOCK_CDEX_LED0 & ~CLOCK_CDEX_LED1 & ~CLOCK_CDEX_LED2
        );
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYP_PWR_ON, 0);
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYB_PWR_ON, 0);
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_VIBRA_PWR_ON, 0);

	return 0;
}

static int
htcuniversal_led_resume(struct device *dev, u32 level)
{
        /* Turn on the LED controllers in CDEX */
        asic3_set_clock_cdex(&htcuniversal_asic3.dev,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2,
                CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2
        );

		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYP_PWR_ON, leds[HTC_PHONE_BL_LED].brightness ? (1<<GPIOD_BL_KEYP_PWR_ON) : 0);
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_BL_KEYB_PWR_ON, leds[HTC_KEYBD_BL_LED].brightness ? (1<<GPIOD_BL_KEYB_PWR_ON) : 0);
		asic3_set_gpio_out_d(&htcuniversal_asic3.dev, 1<<GPIOD_VIBRA_PWR_ON, leds[HTC_VIBRA].brightness ? (1<<GPIOD_VIBRA_PWR_ON) : 0);
	
	return 0;
}


static struct device_driver htcuniversal_led_driver = {
	.name   = "htcuniversal_led",
	.probe  = htcuniversal_led_probe,
	.remove = htcuniversal_led_remove,
	.suspend = htcuniversal_led_suspend,
	.resume = htcuniversal_led_resume,
	.bus    = &platform_bus_type,

};

static struct platform_device htcuniversal_led_dev = {
	.name 	= "htcuniversal_led",
};


static int htcuniversal_led_init (void)
{
	int ret = 0;

	if (! machine_is_htcuniversal() )
		return -ENODEV;

	ret=driver_register(&htcuniversal_led_driver);
	if (! ret) 
		ret=platform_device_register(&htcuniversal_led_dev);
	return ret;
}

static void htcuniversal_led_exit (void)
{
	platform_device_unregister(&htcuniversal_led_dev);
	driver_unregister(&htcuniversal_led_driver);
}

module_init (htcuniversal_led_init);
module_exit (htcuniversal_led_exit);

MODULE_LICENSE("GPL");
