
#define HTC_RED_LED		0
#define HTC_GREENR_LED		1
#define HTC_YELLOW_LED		2
#define HTC_PHONE_BL_LED	3
#define HTC_KEYBD_BL_LED	4
#define	HTC_VIBRA		5
#define HTC_WIFI_LED		6
#define HTC_BLUETOOTH_LED	7
#define HTC_CAM_FLASH_LED	8

extern void htcuniversal_set_led (int led, int brightness, int duty_time, int cycle_time);
