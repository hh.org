/* Support for the ad7877 touchscreen chip attached to the htcapache
 * phone.
 *
 * (c) Copyright 2006 Kevin O'Connor <kevin@koconnor.net>
 *
 * This file may be distributed under the terms of the GNU GPL license.
 */

#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/arch/ssp.h>
#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/htcapache-gpio.h>

/****************************************************************
 * AD7877 specific functions
 ****************************************************************/

enum {
	AD7877_SEQ_YPOS  = 0,
	AD7877_SEQ_XPOS  = 1,
	AD7877_SEQ_Z2    = 2,
	AD7877_SEQ_AUX1  = 3,
	AD7877_SEQ_AUX2  = 4,
	AD7877_SEQ_AUX3  = 5,
	AD7877_SEQ_BAT1  = 6,
	AD7877_SEQ_BAT2  = 7,
	AD7877_SEQ_TEMP1 = 8,
	AD7877_SEQ_TEMP2 = 9,
	AD7877_SEQ_Z1    = 10,

	AD7877_NR_SENSE  = 11,
};

struct ad7877 {
        struct input_dev *input;
	struct ssp_dev ssp;
	int (*ispendown)(void);
	int sent_pen_down;
	int x_plate_ohms;
	int min_pressure;
	u16 read_results_data[AD7877_NR_SENSE];

        spinlock_t              lock;
        struct timer_list       timer;
};

enum {
	REG_CR1     = 0x01,
	REG_CR2     = 0x02,
	REG_SENSORS = 0x10,

	CR1_OFF_READADDR = 2,
	CR1_SLAVEMODE = 0x02,
};

#define	TS_POLL_PERIOD	msecs_to_jiffies(10)

// Build a command that writes to an ad7877 register
static inline int WRITE_REG(int reg, int val)
{
	return (reg << 12) | val;
}

// Send a command and drain the receive buffer.
static u32
ssp_putget(struct ad7877 *ts, u32 data)
{
	u32 ret;
	ssp_write_word(&ts->ssp, data);
	ssp_read_word(&ts->ssp, &ret);
	return ret;
}

// Send an event to the input layer.
static inline void
report_event(struct ad7877 *ts, int pressure, int x, int y)
{
	input_report_abs(ts->input, ABS_PRESSURE, pressure);
	input_report_abs(ts->input, ABS_X, x);
	input_report_abs(ts->input, ABS_Y, y);
	input_sync(ts->input);
}

// Read the most recently reported values from the chip.
static void ad7877_readvals(void *data)
{
	struct ad7877 *ts = data;
	u16 *regs = ts->read_results_data;
	u16 x, y, z1, z2, pendown;
	uint Rt = 0;
	int i;
	unsigned long flag;

	// Read sensed values from ad7877.
	spin_lock_irqsave(&ts->lock, flag);
	ssp_putget(ts, WRITE_REG(REG_CR1, REG_SENSORS << CR1_OFF_READADDR));
	for (i=0; i<AD7877_NR_SENSE; i++)
		ts->read_results_data[i] = ssp_putget(ts, 0);

	// Calculate x, y, and pressure.
	x = regs[AD7877_SEQ_XPOS];
	y = regs[AD7877_SEQ_YPOS];
	z1 = regs[AD7877_SEQ_Z1];
	z2 = regs[AD7877_SEQ_Z2];

	// RTOUCH = (RXPlate) x (XPOSITION /4096) x [(Z2/Z1) - 1]
	if (likely(z1)) {
		Rt = z2;
		Rt -= z1;
		Rt *= x;
		Rt *= ts->x_plate_ohms;
		Rt /= z1;
		Rt /= 4096;
	}

	pendown = ts->ispendown();

//	printk(KERN_NOTICE "TS: pen=%d x=%d y=%d Rt=%d (z1=%d z2=%d)\n"
//	       , pendown, x, y, Rt, z1, z2);

	if (!pendown || Rt < ts->min_pressure) {
		// Pen no longer down.
		if (ts->sent_pen_down) {
			// Send pen up event.
			report_event(ts, 0, 0, 0);
			ts->sent_pen_down = 0;
		}
	} else {
		// Pen down - arrange for events until pen up again.
		ts->sent_pen_down = 1;
		mod_timer(&ts->timer, jiffies + TS_POLL_PERIOD);
		report_event(ts, Rt, x, y);
	}

	spin_unlock_irqrestore(&ts->lock, flag);
}

// Instruct the ad7877 to sense new values.
static void
startSense(struct ad7877 *ts)
{
	ssp_putget(ts, WRITE_REG(REG_CR1, CR1_SLAVEMODE));
}

// Periodic callback while pen is down.
static void ad7877_timer(unsigned long handle)
{
        struct ad7877 *ts = (void *)handle;
        spin_lock(&ts->lock);
	startSense(ts);
        spin_unlock(&ts->lock);
}

// Interrupt handler for when pen is first depressed.
static irqreturn_t ad7877_penirq(int irq, void *handle, struct pt_regs *regs)
{
	struct ad7877 *ts = handle;
        spin_lock(&ts->lock);
	if (! ts->sent_pen_down)
		startSense(ts);
        spin_unlock(&ts->lock);
        return IRQ_HANDLED;
}

// Interrupt handler for when sensing is complete.
static irqreturn_t ad7877_davirq(int irq, void *handle, struct pt_regs *regs)
{
        struct ad7877 *ts = handle;

	ad7877_readvals(ts);

        return IRQ_HANDLED;
}

// XXX
static irqreturn_t ad7877_alertirq(int irq, void *handle, struct pt_regs *regs)
{
        struct ad7877 *ts = handle;

        spin_lock(&ts->lock);
        spin_unlock(&ts->lock);

        return IRQ_HANDLED;
}


/****************************************************************
 * HTCApache specific functions
 ****************************************************************/

#define GPLR_BIT(n) (GPLR((n)) & GPIO_bit((n)))

// Return true if the pen is currently down.
static int
checkPenDown(void)
{
	return !GPLR_BIT(GPIO_NR_HTCAPACHE_TS_PENDOWN);
}

#define MAX_12BIT       ((1<<12)-1)

static int
ts_initirq(void *d)
{
	int err;
        err = request_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_PENDOWN)
			  , ad7877_penirq, SA_TRIGGER_FALLING
			  , "ad7877-pendown", d);
	if (err)
		return err;
        err = request_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_DAV)
			  , ad7877_davirq, SA_TRIGGER_FALLING
			  , "ad7877-dav", d);
	if (err) {
		free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_PENDOWN), d);
		return err;
	}

	return 0; // XXX

        err = request_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_ALERT)
			  , ad7877_alertirq, SA_TRIGGER_FALLING
			  , "ad7877-alert", d);
	if (err) {
		free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_PENDOWN), d);
		free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_DAV), d);
		return err;
	}
	return 0;
}

static void
ts_freeirq(void *d)
{
	free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_PENDOWN), d);
	free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_DAV), d);
	return; // XXX
	free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_TS_ALERT), d);
}

static int __init ts_ssp_probe(struct device *dev)
{
        struct ad7877 *ts;
        struct input_dev *input_dev;
        int ret;

	// Initialize ts data structure.
        ts = kzalloc(sizeof(*ts), GFP_KERNEL);
        if (!ts)
                return -ENOMEM;

	ret = ts_initirq(ts);
	if (ret) {
		kfree(ts);
		return ret;
	}

	ret = ssp_init(&ts->ssp, 1, 0);
	if (ret) {
		printk(KERN_ERR "Unable to register SSP handler!\n");
		ts_freeirq(ts);
		kfree(ts);
		return ret;
	}
	dev->driver_data = ts;

	ts->ispendown = checkPenDown;
	ts->x_plate_ohms = 400; // XXX - don't know real value.
	ts->min_pressure = 1;   // XXX - don't know real value.

        init_timer(&ts->timer);
        ts->timer.data = (unsigned long) ts;
        ts->timer.function = ad7877_timer;

        spin_lock_init(&ts->lock);

	// Initialize input device.
        input_dev = input_allocate_device();
        input_dev->name = "AD7877 Touchscreen";
        set_bit(EV_KEY, input_dev->evbit);
        set_bit(EV_ABS, input_dev->evbit);
	set_bit(ABS_X, input_dev->absbit);
	set_bit(ABS_Y, input_dev->absbit);
	set_bit(ABS_PRESSURE, input_dev->absbit);
        input_set_abs_params(input_dev, ABS_X, 0, MAX_12BIT, 0, 0);
        input_set_abs_params(input_dev, ABS_Y, 0, MAX_12BIT, 0, 0);
        input_set_abs_params(input_dev, ABS_PRESSURE, 0, MAX_12BIT, 0, 0);
        ts->input = input_dev;
	input_register_device(input_dev);

	return 0;
}

static int ts_ssp_remove(struct device *dev)
{
	struct ad7877 *ts = dev->driver_data;

	del_timer_sync(&ts->timer);

	ts_freeirq(ts);
	ssp_exit(&ts->ssp);

	input_unregister_device(ts->input);
	kfree(ts);

	return 0;
}

static struct device_driver ts_ssp_driver = {
	.name           = "htcapache-ad7877",
	.bus            = &platform_bus_type,
	.probe          = ts_ssp_probe,
	.remove         = ts_ssp_remove,
};

static int __init ts_ssp_init(void)
{
	if (!machine_is_htcapache())
		return -ENODEV;

	return driver_register(&ts_ssp_driver);
}

static void __exit ts_ssp_exit(void)
{
	driver_unregister(&ts_ssp_driver);
}

module_init(ts_ssp_init)
module_exit(ts_ssp_exit)

MODULE_LICENSE("GPL");
