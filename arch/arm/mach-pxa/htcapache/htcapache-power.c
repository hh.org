/*
 * Support for power detection code on the HTC Apache phone.
 *
 * (c) Copyright 2006 Kevin O'Connor <kevin@koconnor.net>
 *
 * This file may be distributed under the terms of the GNU GPL license.
 */

#include <linux/kernel.h>
#include <linux/interrupt.h> // request_irq

#include <asm/arch/htcapache-gpio.h>


/****************************************************************
 * Plug in/out events
 ****************************************************************/

// Interrupt handler called when plugging/unplugging mini-usb port
static irqreturn_t
power_isr(int irq, void *dev_id, struct pt_regs *regs)
{
	int haspower = !htcapache_egpio_isset(EGPIO_NR_HTCAPACHE_PWR_IN_PWR);
	int hashigh = !htcapache_egpio_isset(EGPIO_NR_HTCAPACHE_PWR_IN_HIGHPWR);
	if (haspower && !hashigh) {
		// USB plug - activate usb transceiver.
		htcapache_egpio_set(EGPIO_NR_HTCAPACHE_USB_PWR);
	} else {
		htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_USB_PWR);
	}
	printk("USB mini plug event (pwr=%d ac=%d)\n"
	       , haspower, hashigh);
	return IRQ_HANDLED;
}


/****************************************************************
 * Setup
 ****************************************************************/

void
htcapache_power_init(void)
{
	int result;

	// Setup plug interrupt
	result = request_irq(IRQ_EGPIO(EGPIO_NR_HTCAPACHE_PWR_IN_PWR)
			     , power_isr, SA_SAMPLE_RANDOM,
			     "usb_plug", NULL);
	if (result)
		printk("unable to claim irq %d for usb plug event. Got %d\n"
		       , IRQ_EGPIO(EGPIO_NR_HTCAPACHE_PWR_IN_PWR), result);

	// Run the interrupt handler to initialize.
	power_isr(0, NULL, NULL);
}
