/*
 * linux/arch/arm/mach-pxa/htcapache/htcapache.c
 *
 * Support for HTC Apache phones.
 *
 * Based on code from: Alex Osborne <bobofdoom@gmail.com>
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/fb.h>
#include <linux/platform_device.h>
#include <linux/delay.h> // mdelay
#include <linux/input.h>
#include <linux/irq.h> // set_irq_type

#include <asm/mach-types.h>
#include <asm/mach/arch.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/udc.h>
#include <asm/arch/htcapache-gpio.h>
#include <asm/arch/mmc.h>
#include <asm/arch/serial.h>
#include <asm/arch/pxa27x_keyboard.h>
#include <asm/hardware/gpio_keys.h>
#include <asm/arch/irda.h> // struct pxaficp_platform_data

#include "../generic.h"
#include "../../../../drivers/net/wireless/acx/acx_hw.h"

#define GPSR_BIT(n) (GPSR((n)) = GPIO_bit((n)))
#define GPCR_BIT(n) (GPCR((n)) = GPIO_bit((n)))


/****************************************************************
 * Backlight
 ****************************************************************/

static void htcapache_backlight_power(int on)
{
	/**
	  * TODO: check which particular PWM controls the backlight on the LifeDrive
	  * and enable and disable it here. It's PWM1 on the Tungsten T3. Quite
	  * likely to be the same.
	  */
}


/****************************************************************
 * Frame buffer
 ****************************************************************/

static struct pxafb_mach_info htcapache_lcd __initdata = {
	.pixclock		= 192307,
	.xres			= 240,
	.yres			= 320,
	.bpp			= 16,
	.hsync_len		= 11,
	.vsync_len		= 3,

	// These margins are from WinCE
	.left_margin		= 19,
	.right_margin		= 10,
	.upper_margin		= 2,
	.lower_margin		= 2,

	.lccr0			= LCCR0_LDDALT | LCCR0_Act,
	.pxafb_backlight_power	= htcapache_backlight_power,
};


/****************************************************************
 * Touchscreen
 ****************************************************************/

static struct platform_device htcapache_ts = {
	.name = "htcapache-ad7877",
};


/****************************************************************
 * Bluetooth
 ****************************************************************/

static void btuart_configure(int state)
{
	int tries;
	switch (state) {
	case PXA_UART_CFG_POST_STARTUP:
		pxa_gpio_mode(GPIO42_BTRXD_MD);
		pxa_gpio_mode(GPIO43_BTTXD_MD);
		pxa_gpio_mode(GPIO44_BTCTS_MD);
		pxa_gpio_mode(GPIO45_BTRTS_MD);

		htcapache_egpio_set(EGPIO_NR_HTCAPACHE_BT_POWER);
		mdelay(5);
		htcapache_egpio_set(EGPIO_NR_HTCAPACHE_BT_RESET);
		/*
		 * BRF6150's RTS goes low when firmware is ready
		 * so check for CTS=1 (nCTS=0 -> CTS=1). Typical 150ms
		 */
		tries = 0;
		do {
			mdelay(10);
		} while ((BTMSR & MSR_CTS) == 0 && tries++ < 50);
		printk("btuart: post_startup (%d)\n", tries);
		break;
	case PXA_UART_CFG_PRE_SHUTDOWN:
		htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_BT_POWER);
		htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_BT_RESET);
		printk("btuart: pre_shutdown\n");
		break;
	}
}

static struct platform_pxa_serial_funcs btuart_funcs = {
        .configure = btuart_configure,
};


/****************************************************************
 * Irda
 ****************************************************************/

static void irda_transceiver_mode(struct device *dev, int mode)
{
	printk("irda: transceiver_mode=%d\n", mode);
	// XXX - don't know transceiver enable/disable pins.
	// XXX - don't know if FIR supported or how to enable.
}

static struct pxaficp_platform_data apache_ficp_platform_data = {
	.transceiver_cap  = IR_SIRMODE | IR_OFF,
	.transceiver_mode = irda_transceiver_mode,
};


/****************************************************************
 * Wifi
 ****************************************************************/

static int
wlan_start(void)
{
	htcapache_egpio_set(EGPIO_NR_HTCAPACHE_WIFI_POWER1);
	htcapache_egpio_set(EGPIO_NR_HTCAPACHE_WIFI_POWER2);
	htcapache_egpio_set(EGPIO_NR_HTCAPACHE_WIFI_POWER3);
	mdelay(250);
	htcapache_egpio_set(EGPIO_NR_HTCAPACHE_WIFI_RESET);
        mdelay(100);
        return 0;
}

static int
wlan_stop(void)
{
	htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_WIFI_POWER1);
	htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_WIFI_POWER2);
	htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_WIFI_POWER3);
	htcapache_egpio_clear(EGPIO_NR_HTCAPACHE_WIFI_RESET);
        return 0;
}

enum {
	WLAN_BASE = PXA_CS2_PHYS,
};

static struct resource acx_resources[] = {
	[0] = {
		.start  = WLAN_BASE,
		.end    = WLAN_BASE + 0x20,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_EGPIO(EGPIO_NR_HTCAPACHE_WIFI_IN_IRQ),
		.end    = IRQ_EGPIO(EGPIO_NR_HTCAPACHE_WIFI_IN_IRQ),
		.flags  = IORESOURCE_IRQ,
	},
};

static struct acx_hardware_data acx_data = {
	.start_hw       = wlan_start,
	.stop_hw        = wlan_stop,
};

static struct platform_device acx_device = {
	.name   = "acx-mem",
	.dev    = {
		.platform_data = &acx_data,
	},
	.num_resources  = ARRAY_SIZE(acx_resources),
	.resource       = acx_resources,
};


/****************************************************************
 * Pull out keyboard
 ****************************************************************/

static struct pxa27x_keyboard_platform_data htcapache_kbd = {
	.nr_rows = 7,
	.nr_cols = 7,
	.keycodes = {
		{
			/* row 0 */
			-1,		// Unused
			KEY_LEFTSHIFT,	// Left Shift
			-1,		// Unused
			KEY_Q,		// Q
			KEY_W,		// W
			KEY_E,		// E
			KEY_R,	 	// R
		}, {	/* row 1 */
			-1,		// Unused
			-1,		// Unused
			KEY_LEFTALT,	// Red Dot
			KEY_T,		// T
			KEY_Y,		// Y
			KEY_U,		// U
			KEY_I,		// I
		}, {	/* row 2 */
			-1,		// Unused
			KEY_LEFTMETA,	// Windows Key
			-1,		// Unused
			KEY_ENTER,	// Return
			KEY_SPACE,	// Space
			KEY_BACKSPACE,	// Backspace
			KEY_A,		// A
		}, {	/* row 3 */
			-1,		// Unused
			KEY_S,		// S
			KEY_D,		// D
			KEY_F,		// F
			KEY_G,		// G
			KEY_H,		// H
			KEY_J,		// J
		}, {	/* row 4 */
			KEY_LEFTCTRL,	// Left Menu
			KEY_K,		// K
			KEY_Z,		// Z
			KEY_X,		// X
			KEY_C,		// C
			KEY_V,		// V
			KEY_B,		// B
		}, {	/* row 5 */
			KEY_RIGHTCTRL,	// Right Menu
			KEY_N,		// N
			KEY_M,		// M
			KEY_O,		// O
			KEY_L,		// L
			KEY_P,		// P
			KEY_DOT, 	// .
		}, {	/* row 6 */
			-1,		// Unused
			KEY_LEFT,	// Left Arrow
			KEY_DOWN,	// Down Arrow
			KEY_UP,		// Up Arrow
			KEY_ESC,	// OK button
			KEY_TAB,	// Tab
			KEY_RIGHT,	// Right Arrow
		},
	},
	.gpio_modes = {
		 GPIO_NR_HTCAPACHE_KP_MKIN0_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN1_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN2_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN3_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN4_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN5_MD,
		 GPIO_NR_HTCAPACHE_KP_MKIN6_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT0_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT1_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT2_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT3_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT4_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT5_MD,
		 GPIO_NR_HTCAPACHE_KP_MKOUT6_MD,
	 },
};

static struct platform_device htcapache_keyboard = {
        .name   = "pxa27x-keyboard",
        .id     = -1,
	.dev	=  {
		.platform_data	= &htcapache_kbd,
	},
};


/****************************************************************
 * Buttons on side
 ****************************************************************/

static struct gpio_keys_button htcapache_button_list[] = {
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_POWER, .keycode = KEY_POWER},
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_RECORD, .keycode = KEY_RECORD},
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_VOLUP, .keycode = KEY_VOLUMEUP},
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_VOLDOWN, .keycode = KEY_VOLUMEDOWN},
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_BROWSER, .keycode = KEY_WWW},
	{ .gpio = GPIO_NR_HTCAPACHE_BUTTON_CAMERA, .keycode = KEY_CAMERA},
};

static struct gpio_keys_platform_data htcapache_buttons_data = {
	.buttons = htcapache_button_list,
	.nbuttons = ARRAY_SIZE(htcapache_button_list),
};

static struct platform_device htcapache_buttons = {
        .name   = "gpio-keys",
        .id     = -1,
	.dev	=  {
		.platform_data	= &htcapache_buttons_data,
	},
};


/****************************************************************
 * USB client controller
 ****************************************************************/

static void udc_command(int cmd)
{
	switch (cmd)
	{
		case PXA2XX_UDC_CMD_DISCONNECT:
			printk(KERN_NOTICE "USB cmd disconnect\n");
                        GPSR_BIT(GPIO_NR_HTCAPACHE_USB_PUEN);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			printk(KERN_NOTICE "USB cmd connect\n");
                        GPCR_BIT(GPIO_NR_HTCAPACHE_USB_PUEN);
			break;
	}
}

static struct pxa2xx_udc_mach_info htcapache_udc_mach_info = {
	.udc_command      = udc_command,
};


/****************************************************************
 * Mini-SD card
 ****************************************************************/

static int
htcapache_mci_init(struct device *dev
		   , irqreturn_t (*ih)(int, void *, struct pt_regs *)
		   , void *data)
{
	int err = request_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_SD_CARD_DETECT_N)
			      , ih, SA_INTERRUPT
			      , "MMC/SD card detect", data);
	set_irq_type(IRQ_GPIO(GPIO_NR_HTCAPACHE_SD_CARD_DETECT_N)
		     , IRQT_BOTHEDGE);
	if (err) {
		printk(KERN_ERR "htcapache_mci_init: MMC/SD: can't request MMC card detect IRQ\n");
		return -1;
	}

	return 0;
}

static void htcapache_mci_setpower(struct device *dev, unsigned int vdd)
{
	struct pxamci_platform_data* p_d = dev->platform_data;

	// XXX - No idea if this is correct for apache..
	if ((1 << vdd) & p_d->ocr_mask) {
		printk(KERN_NOTICE "MMC power up (vdd=%d mask=%08x)\n"
		       , vdd, p_d->ocr_mask);
		GPSR_BIT(GPIO_NR_HTCAPACHE_SD_POWER_N);
	} else {
		printk(KERN_NOTICE "MMC power down (vdd=%d mask=%08x)\n"
		       , vdd, p_d->ocr_mask);
		GPCR_BIT(GPIO_NR_HTCAPACHE_SD_POWER_N);
	}
}

static void htcapache_mci_exit(struct device *dev, void *data)
{
	free_irq(IRQ_GPIO(GPIO_NR_HTCAPACHE_SD_CARD_DETECT_N), data);
}

static struct pxamci_platform_data htcapache_mci_platform_data = {
	.ocr_mask       = MMC_VDD_32_33 | MMC_VDD_33_34,
	.init           = htcapache_mci_init,
	.setpower       = htcapache_mci_setpower,
	.exit           = htcapache_mci_exit,
};


/****************************************************************
 * Init
 ****************************************************************/

extern struct platform_device htcapache_bl;

static struct platform_device *devices[] __initdata = {
        &htcapache_keyboard,
        &htcapache_buttons,
	&htcapache_ts,
	&htcapache_bl,
	&acx_device,
};

static void __init htcapache_init(void)
{
	htcapache_egpio_init();
	btuart_device.dev.platform_data = &btuart_funcs;
	set_pxa_fb_info(&htcapache_lcd);
	pxa_set_udc_info(&htcapache_udc_mach_info);
	pxa_set_mci_info(&htcapache_mci_platform_data);
	pxa_set_ficp_info(&apache_ficp_platform_data);
	platform_add_devices(devices, ARRAY_SIZE(devices));
	htcapache_power_init();
}

MACHINE_START(HTCAPACHE, "HTC Apache")
	.phys_io	= 0x40000000,
	.io_pg_offst	= io_p2v(0x40000000),
	.boot_params	= 0xa0000100,
	.map_io 	= pxa_map_io,
	.init_irq	= pxa_init_irq,
	.timer  	= &pxa_timer,
	.init_machine	= htcapache_init,
MACHINE_END
