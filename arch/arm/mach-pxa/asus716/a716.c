/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  Copyright (C) 2005 Pawel Kolodziejski
 *  SIR code copied from hx4700 port
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/major.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/delay.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/slab.h>
#include <linux/platform_device.h>

#include <asm/setup.h>
#include <asm/memory.h>
#include <asm/hardware.h>
#include <asm/apm.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irda.h>

#include <asm/arch/serial.h>
#include <asm/arch/udc.h>
#include <asm/arch/pxa-regs.h>
#include <../drivers/pcmcia/soc_common.h>
#include <asm/arch/pxafb.h>
#include <asm/arch/mmc.h>
#include <asm/arch/asus716-gpio.h>
#include <asm/arch/irda.h>

#include "../generic.h"

int battery_power;
volatile static int pca9535_idle;
volatile static int pca9535_after_resume;

void pca9535_init_chip(void);
void a716_ll_pm_init(void);
unsigned long a716_ssp_putget(ulong data);
void a716_gpo_resume(void);

static struct platform_device a716_device = {
	.name           = "a716",
	.id             = 0,
};

static struct platform_device a716_gpo = { .name = "a716-gpo", };
static struct platform_device a716_lcd = { .name = "a716-lcd", };
static struct platform_device a716_buttons = { .name = "a716-buttons", };
static struct platform_device a716_ssp = { .name = "a716-ssp", };

static struct platform_device *devices[] __initdata = {
	&a716_device,
	&a716_gpo,
	&a716_lcd,
	&a716_buttons,
	&a716_ssp,
};

static void a716_irda_transceiver_mode(struct device *dev, int mode)
{
	unsigned long flags;

	local_irq_save(flags);

	if (mode & IR_SIRMODE) {
		a716_gpo_clear(GPO_A716_FIR_MODE);
	} else if (mode & IR_FIRMODE) {
		a716_gpo_set(GPO_A716_FIR_MODE);
	}

	if (mode & IR_OFF) {
		a716_gpo_set(GPO_A716_IRDA_POWER_N);
	} else {
		a716_gpo_clear(GPO_A716_IRDA_POWER_N);
	}

	local_irq_restore(flags);
}

static struct pxaficp_platform_data a716_ficp_platform_data = {
	.transceiver_cap  = IR_SIRMODE | IR_FIRMODE | IR_OFF,
	.transceiver_mode = a716_irda_transceiver_mode,
};

static int a716_udc_is_connected(void)
{
	int ret = GET_A716_GPIO(USB_CABLE_DETECT_N);
	return ret;
}

static void a716_udc_command(int cmd)
{
	switch (cmd) {
		case PXA2XX_UDC_CMD_DISCONNECT:
			a716_gpo_clear(GPO_A716_USB);
			break;
		case PXA2XX_UDC_CMD_CONNECT:
			a716_gpo_set(GPO_A716_USB);
			break;
		default:
			break;
	}
}

static struct pxa2xx_udc_mach_info a716_udc_mach_info __initdata = {
	.udc_is_connected = a716_udc_is_connected,
	.udc_command      = a716_udc_command,
};

static int a716_mci_init(struct device *dev, irqreturn_t (*a716_detect_int)(int, void *, struct pt_regs *), void *data)
{
	int err = request_irq(IRQ_GPIO(GPIO_NR_A716_SD_CARD_DETECT_N), a716_detect_int, SA_INTERRUPT, "MMC/SD card detect", data);
	set_irq_type(IRQ_GPIO(GPIO_NR_A716_SD_CARD_DETECT_N), IRQT_BOTHEDGE);
	if (err) {
		printk(KERN_ERR "a716_mci_init: MMC/SD: can't request MMC card detect IRQ\n");
		return -1;
	}

	return 0;
}

static void a716_mci_setpower(struct device *dev, unsigned int vdd)
{
	struct pxamci_platform_data* p_d = dev->platform_data;

	if ((1 << vdd) & p_d->ocr_mask) {
		a716_gpo_clear(GPO_A716_SD_POWER_N);
	} else {
		a716_gpo_set(GPO_A716_SD_POWER_N);
	}
}

static void a716_mci_exit(struct device *dev, void *data)
{
	free_irq(IRQ_GPIO(GPIO_NR_A716_SD_CARD_DETECT_N), data);
}

static struct pxamci_platform_data a716_mci_platform_data = {
	.ocr_mask       = MMC_VDD_32_33 | MMC_VDD_33_34,
	.init           = a716_mci_init,
	.setpower       = a716_mci_setpower,
	.exit           = a716_mci_exit,
};

int a716_lcd_set_power(struct lcd_device *lm, int level);
int a716_backlight_set_power(struct backlight_device *bl, int level);

static void a716_lcd_power(int level)
{
	a716_lcd_set_power(NULL, level);
}

static void a716_backlight_power(int level)
{
	a716_backlight_set_power(NULL, level);
}

static struct pxafb_mach_info a716_fb_info = {
	.pixclock =     171521,
	.bpp =          16,
	.xres =         240,
	.yres =         320,
	.hsync_len =    64,
	.vsync_len =    6,
	.left_margin =  11,
	.upper_margin = 4,
	.right_margin = 11,
	.lower_margin = 4,
	.sync =         0,
	.lccr0 =        (LCCR0_PAS),
	.pxafb_lcd_power = (void *)a716_lcd_power,
	.pxafb_backlight_power = (void *)a716_backlight_power,
};

#define CTRL_START  0x80
#define CTRL_VBAT   0x24
#define CTRL_PD0    0x01

#define A716_MAIN_BATTERY_MAX 1676 // ~ sometimes it's more or less (> 1700 - AC)
#define A716_MAIN_BATTERY_MIN 1347
#define A716_MAIN_BATTERY_RANGE (A716_MAIN_BATTERY_MAX - A716_MAIN_BATTERY_MIN)

void a716_battery(void)
{
	int sample;

	sample = a716_ssp_putget(CTRL_PD0 | CTRL_START | CTRL_VBAT); // main battery: min - 1347, max - 1676 (1700 AC)
	a716_ssp_putget(CTRL_START | CTRL_VBAT);

	sample = ((sample - A716_MAIN_BATTERY_MIN) * 100) / A716_MAIN_BATTERY_RANGE;
	if (sample > 100)
		battery_power = 100;
	else
		battery_power = sample;

	if (battery_power < 10 && !(GPLR(GPIO_NR_A716_AC_DETECT) & GPIO_bit(GPIO_NR_A716_AC_DETECT)))
		a716_gpo_set(GPO_A716_POWER_LED_RED);
	else
		a716_gpo_clear(GPO_A716_POWER_LED_RED);

	//printk("battery: %d\n", battery_power);
}

typedef void (*apm_get_power_status_t)(struct apm_power_info*);

static void a716_apm_get_power_status(struct apm_power_info *info)
{
	a716_battery();

	info->battery_life = battery_power;

	if (!(GPLR(GPIO_NR_A716_AC_DETECT) & GPIO_bit(GPIO_NR_A716_AC_DETECT)))
		info->ac_line_status = APM_AC_OFFLINE;
	else
		info->ac_line_status = APM_AC_ONLINE;

	if (battery_power > 50)
		info->battery_status = APM_BATTERY_STATUS_HIGH;
	else if (battery_power < 10)
		info->battery_status = APM_BATTERY_STATUS_CRITICAL;
	else
		info->battery_status = APM_BATTERY_STATUS_LOW;

	info->time = 0;
	info->units = 0;
}

int set_apm_get_power_status(apm_get_power_status_t t)
{
	apm_get_power_status = t;

	return 0;
}

int a716_backlight_set_brightness(struct backlight_device *bm, int value);
int a716_backlight_get_brightness(struct backlight_device *bm);

static void a716_btuart_configure(int state)
{
	switch (state) {
	case PXA_UART_CFG_POST_STARTUP:
		pxa_gpio_mode(GPIO42_BTRXD_MD);
		pxa_gpio_mode(GPIO43_BTTXD_MD);
		pxa_gpio_mode(GPIO44_BTCTS_MD);
		pxa_gpio_mode(GPIO45_BTRTS_MD);
		a716_gpo_set(GPO_A716_BLUETOOTH_POWER);
		mdelay(5);
		a716_gpo_set(GPO_A716_BLUETOOTH_RESET);
		mdelay(5);
		a716_gpo_clear(GPO_A716_BLUETOOTH_RESET);
		printk("btuart: post_startup\n");
		break;
	case PXA_UART_CFG_PRE_SHUTDOWN:
		a716_gpo_clear(GPO_A716_BLUETOOTH_POWER);
		printk("btuart: pre_shutdown\n");
		break;
	default:
		break;
	}
}

static struct platform_pxa_serial_funcs a716_btuart_funcs = {
	.configure = a716_btuart_configure,
};

#ifdef CONFIG_PM
static int a716_resume(struct device *dev)
{
	a716_gpo_resume();
	pca9535_init_chip();

	return 0;
}
#endif

static struct device_driver a716_driver = {
	.name           = "a716",
	.bus		= &platform_bus_type,
#ifdef CONFIG_PM
	.resume         = a716_resume,
#endif
};

static void __init a716_init(void)
{
	PGSR0 = 0x00080000;
	PGSR1 = 0x03ff0300;
	PGSR2 = 0x00010400;

	//GPSR0 = 0x00080000; // don't set gpio(19) it hangs pda
	GPSR1 = 0x00BF0000;
	GPSR2 = 0x00000400;

	GPCR0 = 0xD3830040;
	GPCR1 = 0xFC40AB81;
	GPCR2 = 0x00013BFF;

	GPDR0 = 0xD38B0040;
	GPDR1 = 0xFCFFAB81;
	GPDR2 = 0x00013FFF;

	GAFR0_L = 0x00001004;
	GAFR0_U = 0x591A8002;
	GAFR1_L = 0x99908011;
	GAFR1_U = 0xAAA5AAAA;
	GAFR2_L = 0x0A8AAAAA;
	GAFR2_U = 0x00000002;

	PWER = PWER_RTC | PWER_GPIO0;
	PFER = PWER_GPIO0;
	PRER = 0;
	PEDR = 0;
	PCFR = PCFR_OPDE;

	btuart_device.dev.platform_data = &a716_btuart_funcs;

	set_pxa_fb_info(&a716_fb_info);
	pxa_set_udc_info(&a716_udc_mach_info);
	pxa_set_mci_info(&a716_mci_platform_data);
	pxa_set_ficp_info(&a716_ficp_platform_data);
	a716_ll_pm_init();

	driver_register(&a716_driver);
	platform_add_devices(devices, ARRAY_SIZE(devices));

	set_apm_get_power_status(a716_apm_get_power_status);
}

MACHINE_START(A716, "Asus MyPal A716")
	/* Maintainer: Pawel Kolodziejski */
	.phys_io	= 0x40000000,
	.io_pg_offst	= (io_p2v(0x40000000) >> 18) & 0xfffc,
	.boot_params	= 0xa0000100,
	.map_io		= pxa_map_io,
	.init_irq	= pxa_init_irq,
	.timer		= &pxa_timer,
	.init_machine 	= a716_init,
MACHINE_END
