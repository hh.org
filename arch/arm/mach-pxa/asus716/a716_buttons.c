/*
 * Buttons driver for Asus Mypal 716
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * Based on Input driver for Dell Axim X5.
 *
 * Creates input events for the buttons on the device
 *
 * Copyright 2004 Matthew Garrett
 *                Nicolas Pouillon: Adaptation for A620
 * Copyright 2005 (C) Pawel Kolodziejski
 *
 */

#include <linux/input.h>
#include <linux/input_pda.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/arch/asus716-gpio.h>

#define GET_GPIO(gpio) (GPLR(gpio) & GPIO_bit(gpio))

static struct input_dev *button_dev;
static u16 buttons_state = 0;
u32 pca9535_read_input(void);
void a716_audio_earphone_isr(u16 data);

static struct {
	u16 keyno;
	u8 irq;
	u8 gpio;
        char *desc;
} buttontab [] = {
#define KEY_DEF(x) A716_IRQ (x), GPIO_NR_A716_##x
	{ _KEY_POWER,    KEY_DEF(POWER_BUTTON_N),    "Power button"    },
	{ _KEY_CALENDAR, KEY_DEF(CALENDAR_BUTTON_N), "Calendar button" },
	{ _KEY_CONTACTS, KEY_DEF(CONTACTS_BUTTON_N), "Contacts button" },
	{ _KEY_MAIL,     KEY_DEF(TASKS_BUTTON_N),    "Tasks button"    },
	{ _KEY_HOMEPAGE, KEY_DEF(HOME_BUTTON_N),     "Home button"     },
#undef KEY_DEF
};

irqreturn_t a716_button_handle(int irq, void *dev_id, struct pt_regs *regs)
{
	int button, down;

	for (button = 0; button < ARRAY_SIZE(buttontab); button++)
		if (buttontab[button].irq == irq)
			break;

	if (likely(button < ARRAY_SIZE(buttontab))) {
		down = GET_GPIO(buttontab[button].gpio) ? 0 : 1;
		input_report_key(button_dev, buttontab[button].keyno, down);
		input_sync(button_dev);
	}

	return IRQ_HANDLED;
}

static void a716_pca9535_parse(void *dummy)
{
	u32 buttons, button1, button2, data;

	data = pca9535_read_input();
	if (data == (u32)-1) {
		return;
	}
																						
	a716_audio_earphone_isr(data);

	buttons = data & 0xff00;
	button1 = buttons_state & 0x2f00;
	button2 = buttons & 0x2f00;

	if (!(buttons_state & GPIO_I2C_BUTTON_RECORD_N) && (buttons & GPIO_I2C_BUTTON_RECORD_N)) {
		input_report_key(button_dev, _KEY_RECORD, 0);
		input_sync(button_dev);
	}

	if (!(buttons_state & GPIO_I2C_BUTTON_UP_N) && (buttons & GPIO_I2C_BUTTON_UP_N)) {
		input_report_key(button_dev, KEY_PAGEUP, 0);
		input_sync(button_dev);
	}

	if (!(buttons_state & GPIO_I2C_BUTTON_DOWN_N) && (buttons & GPIO_I2C_BUTTON_DOWN_N)) {
		input_report_key(button_dev, KEY_PAGEDOWN, 0);
		input_sync(button_dev);
	}

	if ((buttons_state & GPIO_I2C_BUTTON_RECORD_N) && !(buttons & GPIO_I2C_BUTTON_RECORD_N)) {
		input_report_key(button_dev, _KEY_RECORD, 1);
		input_sync(button_dev);
	}

	if ((buttons_state & GPIO_I2C_BUTTON_UP_N) && !(buttons & GPIO_I2C_BUTTON_UP_N)) {
		input_report_key(button_dev, KEY_PAGEUP, 1);
		input_sync(button_dev);
	}

	if ((buttons_state & GPIO_I2C_BUTTON_DOWN_N) && !(buttons & GPIO_I2C_BUTTON_DOWN_N)) {
		input_report_key(button_dev, KEY_PAGEDOWN, 1);
		input_sync(button_dev);
	}

	if ((button1 == 0x0c00) && (button2 != 0x0c00)) {
		input_report_key(button_dev, KEY_UP, 0);
		input_sync(button_dev);
	}

	if ((button1 == 0x0a00) && (button2 != 0x0a00)) {
		input_report_key(button_dev, KEY_RIGHT, 0);
		input_sync(button_dev);
	}

	if ((button1 == 0x2200) && (button2 != 0x2200)) {
		input_report_key(button_dev, KEY_DOWN, 0);
		input_sync(button_dev);
	}

	if ((button1 == 0x2400) && (button2 != 0x2400)) {
		input_report_key(button_dev, KEY_LEFT, 0);
		input_sync(button_dev);
	}

	if ((button1 == 0x2e00) && (button2 != 0x2e00)) {
		input_report_key(button_dev, KEY_ENTER, 0);
		input_sync(button_dev);
	}

	if ((button1 != 0x0c00) && (button2 == 0x0c00)) {
		input_report_key(button_dev, KEY_UP, 1);
		input_sync(button_dev);
	}

	if ((button1 != 0x0a00) && (button2 == 0x0a00)) {
		input_report_key(button_dev, KEY_RIGHT, 1);
		input_sync(button_dev);
	}

	if ((button1 != 0x2200) && (button2 == 0x2200)) {
		input_report_key(button_dev, KEY_DOWN, 1);
		input_sync(button_dev);
	}

	if ((button1 != 0x2400) && (button2 == 0x2400)) {
		input_report_key(button_dev, KEY_LEFT, 1);
		input_sync(button_dev);
	}

	if ((button1 != 0x2e00) && (button2 == 0x2e00)) {
		input_report_key(button_dev, KEY_ENTER, 1);
		input_sync(button_dev);
	}

	buttons_state = buttons;
}

DECLARE_WORK(pca_workqueue, a716_pca9535_parse, NULL);

static irqreturn_t a716_pca9535_handle(int irq, void *dev_id, struct pt_regs *regs)
{
	schedule_work(&pca_workqueue);

	return IRQ_HANDLED;
}

static int a716_button_probe(struct device *dev)
{
	int i;

	if (!machine_is_a716())
		return -ENODEV;

	button_dev = input_allocate_device();
	if (!button_dev)
		return -ENOMEM;

	set_bit(EV_KEY, button_dev->evbit);

	button_dev->keybit[LONG(KEY_UP)] |= BIT(KEY_UP);
	button_dev->keybit[LONG(KEY_DOWN)] |= BIT(KEY_DOWN);
	button_dev->keybit[LONG(KEY_LEFT)] |= BIT(KEY_LEFT);
	button_dev->keybit[LONG(KEY_RIGHT)] |= BIT(KEY_RIGHT);
	button_dev->keybit[LONG(KEY_ENTER)] |= BIT(KEY_ENTER);
	button_dev->keybit[LONG(_KEY_RECORD)] |= BIT(_KEY_RECORD);
	button_dev->keybit[LONG(KEY_PAGEUP)] |= BIT(KEY_PAGEUP);
	button_dev->keybit[LONG(KEY_PAGEDOWN)] |= BIT(KEY_PAGEDOWN);

	for (i = 0; i < ARRAY_SIZE(buttontab); i++) {
		request_irq(buttontab[i].irq, a716_button_handle, SA_SAMPLE_RANDOM, buttontab[i].desc, NULL);
		set_irq_type(buttontab[i].irq, IRQT_BOTHEDGE);
		button_dev->keybit[LONG(buttontab[i].keyno)] |= BIT(buttontab[i].keyno);
	}

	request_irq(A716_IRQ(PCA9535_IRQ), a716_pca9535_handle, SA_SAMPLE_RANDOM, "pca9535", NULL);
	set_irq_type(A716_IRQ(PCA9535_IRQ), IRQT_FALLING);

	buttons_state = 0xff00;

	button_dev->name = "Asus MyPal 716 buttons driver";
	input_register_device(button_dev);

	return 0;
}

static int a716_button_remove(struct device *dev)
{
	int i;

	input_unregister_device(button_dev);
	input_free_device(button_dev);

	for (i = 0; i < ARRAY_SIZE(buttontab); i++)
		free_irq(buttontab[i].irq, NULL);

	free_irq(A716_IRQ(PCA9535_IRQ), NULL);

	return 0;
}

#ifdef CONFIG_PM
static int a716_button_suspend(struct device *dev, pm_message_t state)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(buttontab); i++) {
		disable_irq(buttontab[i].irq);
	}

	disable_irq(A716_IRQ(PCA9535_IRQ));

	return 0;
}								

static int a716_button_resume(struct device *dev)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(buttontab); i++) {
		enable_irq(buttontab[i].irq);
	}

	enable_irq(A716_IRQ(PCA9535_IRQ));

	return 0;
}								
#endif

static struct device_driver a716_buttons_driver = {
	.name           = "a716-buttons",
	.bus            = &platform_bus_type,
	.probe          = a716_button_probe,
	.remove         = a716_button_remove,
#ifdef CONFIG_PM
	.suspend        = a716_button_suspend,
	.resume         = a716_button_resume,
#endif
};

static int __init a716_button_init(void)
{
	if (!machine_is_a716())
		return -ENODEV;

	return driver_register(&a716_buttons_driver);
}

static void __exit a716_button_exit(void)
{
        driver_unregister(&a716_buttons_driver);
}

module_init(a716_button_init);
module_exit(a716_button_exit);

MODULE_AUTHOR ("Nicolas Pouillon, Pawel Kolodziejski");
MODULE_DESCRIPTION ("Button support for Asus MyPal A716");
MODULE_LICENSE ("GPL");
