/*
 * MyPal 716 power management support for the original IPL in DoC G3
 *
 * Use consistent with the GNU GPL is permitted, provided that this
 * copyright notice is preserved in its entirety in all copies and
 * derived works.
 *
 * Copyright (C) 2005 Pawel Kolodziejski
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/pm.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxa-pm_ll.h>

static u32 *addr_a2000400;
static u32 *addr_a2000404;
static u32 *addr_a2000408;
static u32 *addr_a200040c;
static u32 save_a2000400;
static u32 save_a2000404;
static u32 save_a2000408;
static u32 save_a200040c;

static void a716_pxa_ll_pm_suspend(unsigned long resume_addr)
{
	save_a2000400 = *addr_a2000400;
	save_a2000404 = *addr_a2000404;
	save_a2000408 = *addr_a2000408;
	save_a200040c = *addr_a200040c;

	*addr_a2000400 = 0xe3a00101; // mov r0, #0x40000000
	*addr_a2000404 = 0xe380060f; // orr r0, r0, #0x0f000000
	*addr_a2000408 = 0xe3800008; // orr r0, r0, #8
	*addr_a200040c = 0xe590f000; // ldr pc, [r0]
}

static void a716_pxa_ll_pm_resume(void)
{
	*addr_a2000400 = save_a2000400;
	*addr_a2000404 = save_a2000404;
	*addr_a2000408 = save_a2000408;
	*addr_a200040c = save_a200040c;
}

static struct pxa_ll_pm_ops a716_ll_pm_ops = {
	.suspend = a716_pxa_ll_pm_suspend,
	.resume  = a716_pxa_ll_pm_resume,
};

void a716_ll_pm_init(void) {
	addr_a2000400 = phys_to_virt(0xa2000400);
	addr_a2000404 = phys_to_virt(0xa2000404);
	addr_a2000408 = phys_to_virt(0xa2000408);
	addr_a200040c = phys_to_virt(0xa200040c);

	pxa_pm_set_ll_ops(&a716_ll_pm_ops);
}
