/*
 *  Asus MyPal 716 LCD and Backlight driver
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  Copyright (C) 2005 Pawel Kolodziejski
 *  Copyright (C) 2004 Vitaliy Sardyko
 *  Copyright (c) 2003 Adam Turowski
 *
 *  2003-12-03: Adam Turowski
 *              initial code.
 *  2004-11-07: Vitaliy Sardyko
 *              updated to 2.6.7
 * 
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/notifier.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/delay.h>
#include <linux/platform_device.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>

#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/asus716-gpio.h>
#include <linux/fb.h>
#include <asm/arch/pxafb.h>

static int lcd_power;
static int backlight_power;

int a716_lcd_set_power(struct lcd_device *lm, int level)
{
	switch (level) {
		case FB_BLANK_UNBLANK:
		case FB_BLANK_NORMAL:
			a716_gpo_set(GPO_A716_LCD_ENABLE);
			a716_gpo_set(GPO_A716_LCD_POWER3);
			mdelay(30);
			a716_gpo_set(GPO_A716_LCD_POWER1);
			mdelay(30);
			break;
		case FB_BLANK_VSYNC_SUSPEND:
		case FB_BLANK_HSYNC_SUSPEND:
                        break;
		case FB_BLANK_POWERDOWN:
			a716_gpo_clear(GPO_A716_LCD_POWER1);
			mdelay(65);
			a716_gpo_clear(GPO_A716_LCD_POWER3);
			a716_gpo_clear(GPO_A716_LCD_ENABLE);
			break;
	}

	lcd_power = level;

	return 0;
}

static int a716_lcd_get_power(struct lcd_device *lm)
{
	return lcd_power;
} 

struct lcd_properties a716_lcd_properties =
{
	.owner		= THIS_MODULE,
	.set_power	= a716_lcd_set_power,
	.get_power	= a716_lcd_get_power,
}; 

int a716_backlight_set_power(struct backlight_device *bm, int level)
{
	switch (level) {
		case FB_BLANK_UNBLANK:
		case FB_BLANK_NORMAL:
			CKEN |= CKEN0_PWM0;
			a716_gpo_set(GPO_A716_BACKLIGHT);
			break;
		case FB_BLANK_VSYNC_SUSPEND:
		case FB_BLANK_HSYNC_SUSPEND:
			break;
		case FB_BLANK_POWERDOWN:
			CKEN &= ~CKEN0_PWM0;
			a716_gpo_clear(GPO_A716_BACKLIGHT);
			mdelay(30);
			break;
	}

	backlight_power = level;

	return 0;
}

static int a716_backlight_get_power(struct backlight_device *bm)
{
	return backlight_power;
}

int a716_backlight_set_brightness(struct backlight_device *bm, int value)
{
	if (value == 0) {
		PWM_PWDUTY0 = 0;
		PWM_PERVAL0 = 255;
	} else {
		PWM_PWDUTY0 = ((value * 220) / 255) + 35;
		PWM_PERVAL0 = 255;
	}

	return 0;
}

int a716_backlight_get_brightness(struct backlight_device *bm)
{
	int value;

	value = PWM_PWDUTY0 - 35;
	if (value < 0)
		return 0;

	return (value * 255) / 220;
}

static struct backlight_properties a716_backlight_properties =
{
	.owner		= THIS_MODULE,
	.max_brightness	= 255,
	.set_power	= a716_backlight_set_power,
	.get_power	= a716_backlight_get_power,
	.set_brightness	= a716_backlight_set_brightness,
	.get_brightness	= a716_backlight_get_brightness,
};

static struct lcd_device *pxafb_lcd_device;
static struct backlight_device *pxafb_backlight_device;

int a716_lcd_probe(struct device *dev)
{
	if (!machine_is_a716())
		return -ENODEV;

	pxafb_lcd_device = lcd_device_register("pxafb", NULL, &a716_lcd_properties);

	if (IS_ERR(pxafb_lcd_device))
		return PTR_ERR(pxafb_lcd_device);
	pxafb_backlight_device = backlight_device_register("pxafb", NULL, &a716_backlight_properties);
	if (IS_ERR(pxafb_backlight_device)) {
		lcd_device_unregister(pxafb_lcd_device);
		return PTR_ERR(pxafb_backlight_device);
	}

	a716_backlight_set_brightness(pxafb_backlight_device, 100);

	return 0;
}

static int a716_lcd_remove(struct device *dev)
{
	lcd_device_unregister(pxafb_lcd_device);
	backlight_device_unregister(pxafb_backlight_device);

	return 0;
}

#ifdef CONFIG_PM
struct pm_save_data {
	int brightness;
};

static int a716_lcd_suspend(struct device *dev, pm_message_t state)
{
	struct pm_save_data *save;

        if (!dev->power.saved_state)
		dev->power.saved_state = kmalloc(sizeof(struct pm_save_data), GFP_KERNEL);
	if (!dev->power.saved_state)
		return -ENOMEM;
	save = dev->power.saved_state;
	save->brightness = a716_backlight_get_brightness(NULL);

	return 0;
}

static int a716_lcd_resume(struct device *dev)
{
	if (dev->power.saved_state) {
		struct pm_save_data *save = dev->power.saved_state;
		a716_backlight_set_brightness(NULL, save->brightness);
		kfree(dev->power.saved_state);
		dev->power.saved_state = NULL;
	}
						
	return 0;
}
#endif

static struct device_driver a716_lcd_driver = {
	.name           = "a716-lcd",
	.bus            = &platform_bus_type,
	.probe          = a716_lcd_probe,
	.remove         = a716_lcd_remove,
#ifdef CONFIG_PM
	.suspend        = a716_lcd_suspend,
	.resume         = a716_lcd_resume,
#endif
};

static int a716_lcd_init(void)
{
	if (!machine_is_a716())
		return -ENODEV;

	return driver_register(&a716_lcd_driver);
}

static void a716_lcd_exit(void)
{
	lcd_device_unregister(pxafb_lcd_device);
	backlight_device_unregister(pxafb_backlight_device);
	driver_unregister(&a716_lcd_driver);
}

module_init(a716_lcd_init);
module_exit(a716_lcd_exit);

MODULE_AUTHOR("Adam Turowski, Nicolas Pouillon, Pawel Kolodziejski");
MODULE_DESCRIPTION("LCD driver for Asus MyPal A716");
MODULE_LICENSE("GPL");
