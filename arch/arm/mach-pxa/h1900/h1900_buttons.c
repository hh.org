/*
 * Buttons driver for iPAQ h1910/h1915
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * Copyright (C) 2005 Pawel Kolodziejski
 * Copyright (C) 2003 Joshua Wise
 *
 */

#include <linux/input.h>
#include <linux/input_pda.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <asm/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/hardware/ipaq-asic3.h>
#include <asm/arch/h1900-gpio.h>
#include <asm/arch/h1900-asic.h>
#include <linux/fb.h>
#include <linux/soc/asic3_base.h>
#include <linux/platform_device.h>

extern struct platform_device h1900_asic3;
#define asic3 &h1900_asic3.dev
static struct input_dev *button_dev;

struct keys {
	int keyno;
	int gpio;
	char *desc;
};

static struct keys pxa_button_tab[] = {
	{ _KEY_POWER,	GPIO_NR_H1900_POWER_BUTTON_N,	"Power button"	},
	{ KEY_ENTER,	GPIO_NR_H1900_ACTION_BUTTON_N,	"Action button"	},
	{ KEY_UP,	GPIO_NR_H1900_UP_BUTTON_N,	"Up button"	},
	{ KEY_DOWN,	GPIO_NR_H1900_DOWN_BUTTON_N,	"Down button"	},
	{ KEY_LEFT,	GPIO_NR_H1900_LEFT_BUTTON_N,	"Left button"	},
	{ KEY_RIGHT,	GPIO_NR_H1900_RIGHT_BUTTON_N,	"Right button"	},
};

struct kpad {
	int keybit;
	int keyno;
	int irq;
	char *desc;
};

#define BIT_RECORD	0x04
#define BIT_HOME	0x08
#define BIT_MAIL	0x10
#define BIT_CONTACTS	0x20
#define BIT_CALENDAR	0x40

static struct kpad button_tab[] = {
	{ BIT_RECORD,	_KEY_RECORD,	H1900_RECORD_BTN_IRQ, "Record button" },
	{ BIT_HOME,	_KEY_HOMEPAGE,	H1900_HOME_BTN_IRQ, "Home button" },
	{ BIT_MAIL,	_KEY_MAIL,	H1900_MAIL_BTN_IRQ, "Mail button" },
	{ BIT_CONTACTS, _KEY_CONTACTS,	H1900_CONTACTS_BTN_IRQ, "Contacts button" },
	{ BIT_CALENDAR, _KEY_CALENDAR,	H1900_CALENDAR_BTN_IRQ, "Calendar button" },
};

static irqreturn_t h1900_buttons_asic_handle(int irq, void *data, struct pt_regs *regs)
{
	int i, pressed, base_irq;

	base_irq = asic3_irq_base(asic3);
	for (i = 0; i < ARRAY_SIZE(button_tab); i++) {
		if ((base_irq + button_tab[i].irq) == irq) {
			pressed = !(asic3_get_gpio_status_d(asic3) & button_tab[i].keybit);
			if (pressed == 0)
				set_irq_type(irq, IRQT_FALLING);
			else
				set_irq_type(irq, IRQT_RISING);

			input_report_key(button_dev, button_tab[i].keyno, pressed);
			input_sync(button_dev);
		}
	}

	return IRQ_HANDLED;
}

static irqreturn_t h1900_buttons_handle(int irq, void* data, struct pt_regs *regs)
{
	int button, down;

	for (button = 0; button < ARRAY_SIZE(pxa_button_tab); button++)
		if (IRQ_GPIO(pxa_button_tab[button].gpio) == irq)
			break;

	if (likely(button < ARRAY_SIZE(pxa_button_tab))) {
		down = GPLR(pxa_button_tab[button].gpio) & GPIO_bit(pxa_button_tab[button].gpio) ? 0 : 1;
		input_report_key(button_dev, pxa_button_tab[button].keyno, down);
		input_sync(button_dev);
	}

	return IRQ_HANDLED;
}

static int __init h1900_buttons_probe(struct device *dev)
{
	int i, base_irq;

	if (!machine_is_h1900())
		return -ENODEV;

	button_dev = input_allocate_device();
	if (!button_dev)
		return -ENOMEM;

	button_dev->name = "hp iPAQ h1910/h1915 buttons driver";
	set_bit(EV_KEY, button_dev->evbit);

	base_irq = asic3_irq_base(asic3);

	for (i = 0; i < ARRAY_SIZE(pxa_button_tab); i++) {
		set_bit(pxa_button_tab[i].keyno, button_dev->keybit);
		request_irq(IRQ_GPIO(pxa_button_tab[i].gpio), h1900_buttons_handle, SA_SAMPLE_RANDOM, pxa_button_tab[i].desc, NULL);
		set_irq_type(IRQ_GPIO(pxa_button_tab[i].gpio), IRQT_BOTHEDGE);
	}

	for (i = 0; i < ARRAY_SIZE(button_tab); i++) {
		set_bit(button_tab[i].keyno, button_dev->keybit);
		request_irq(base_irq + button_tab[i].irq, h1900_buttons_asic_handle, SA_SAMPLE_RANDOM, button_tab[i].desc, NULL);
		set_irq_type(base_irq + button_tab[i].irq, IRQT_FALLING);
	}

	input_register_device(button_dev);

	return 0;
}

#ifdef CONFIG_PM
static int h1900_buttons_suspend(struct device *dev, pm_message_t state)
{
	int i, base_irq;

	base_irq = asic3_irq_base(asic3);

	for (i = 0; i < ARRAY_SIZE(pxa_button_tab); i++) {
		disable_irq(IRQ_GPIO(pxa_button_tab[i].gpio));
	}

	for (i = 0; i < ARRAY_SIZE(button_tab); i++) {
		disable_irq(base_irq + button_tab[i].irq);
	}

	return 0;
}

static int h1900_buttons_resume(struct device *dev)
{
	int i, base_irq;

	base_irq = asic3_irq_base(asic3);

	for (i = 0; i < ARRAY_SIZE(pxa_button_tab); i++) {
		enable_irq(IRQ_GPIO(pxa_button_tab[i].gpio));
	}

	for (i = 0; i < ARRAY_SIZE(button_tab); i++) {
		enable_irq(base_irq + button_tab[i].irq);
	}

	return 0;
}
#endif

static struct device_driver h1900_buttons_driver = {
	.name           = "h1900-buttons",
	.bus            = &platform_bus_type,
	.probe          = h1900_buttons_probe,
#ifdef CONFIG_PM
	.suspend        = h1900_buttons_suspend,
	.resume         = h1900_buttons_resume,
#endif
};

static int __init h1900_buttons_init(void)
{
	if (!machine_is_h1900())
		return -ENODEV;

	return driver_register(&h1900_buttons_driver);
}

static void __exit h1900_buttons_exit(void)
{
	int i, base_irq;

	input_unregister_device(button_dev);

	for (i = 0; i < ARRAY_SIZE(pxa_button_tab); i++) {
		free_irq(IRQ_GPIO(pxa_button_tab[i].gpio), NULL);
	}

	base_irq = asic3_irq_base(asic3);
	for (i = 0; i < ARRAY_SIZE(button_tab); i++) {
		free_irq(base_irq + button_tab[i].irq, NULL);
	}

	driver_unregister(&h1900_buttons_driver);
}

module_init(h1900_buttons_init);
module_exit(h1900_buttons_exit);

MODULE_AUTHOR ("Joshua Wise, Pawel Kolodziejski");
MODULE_DESCRIPTION ("Button support for iPAQ h1910/h1915");
MODULE_LICENSE ("GPL");
