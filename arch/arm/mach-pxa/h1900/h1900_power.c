/*
 * Power and APM status driver for iPAQ h1910/h1915
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Copyright (C) 2005-2006 Pawel Kolodziejski
 * Copyright (C) 2003 Joshua Wise
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/bootmem.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/platform_device.h>
#include <linux/soc/asic3_base.h>
#include <linux/soc/tmio_mmc.h>

#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>
#include <asm/io.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irda.h>
#include <asm/arch/h1900-asic.h>
#include <asm/arch/h1900-gpio.h>
#include <asm/arch/ipaq.h>
#include <asm/arch/udc.h>
#include <asm/arch/pxafb.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxa-pm_ll.h>
#include <asm/arch/irq.h>
#include <asm/types.h>
#include <asm/apm.h>

#include "../generic.h"

int battery_power;
extern struct platform_device h1900_asic3;
#define asic3 &h1900_asic3.dev

static u32 *addr_sig;
static u32 save_sig;

unsigned long h1900_ssp_putget(ulong data);
void h1900_set_led(int color, int duty_time, int cycle_time);

static void h1900_pxa_ll_pm_suspend(unsigned long resume_addr)
{
	save_sig = *addr_sig;
	*addr_sig = 0x4C494E42; // LINB signature for bootloader while resume
}

static void h1900_pxa_ll_pm_resume(void)
{
	*addr_sig = save_sig;
}

static struct pxa_ll_pm_ops h1900_ll_pm_ops = {
	.suspend = h1900_pxa_ll_pm_suspend,
	.resume  = h1900_pxa_ll_pm_resume,
};

void h1900_ll_pm_init(void)
{
	addr_sig = phys_to_virt(0xa0a00000);
	pxa_pm_set_ll_ops(&h1900_ll_pm_ops);
}

static irqreturn_t h1900_charge(int irq, void *dev_id, struct pt_regs *regs)
{
	int charge;

	if (!(GPLR(GPIO_NR_H1900_AC_IN_N) & GPIO_bit(GPIO_NR_H1900_AC_IN_N)) &&
			(GPLR(GPIO_NR_H1900_MBAT_IN) & GPIO_bit(GPIO_NR_H1900_MBAT_IN))  )
		charge = 1;
	else
		charge = 0;
		
	if (charge) {
		int dtime;
		
		if (GPLR(GPIO_NR_H1900_CHARGING) & GPIO_bit(GPIO_NR_H1900_CHARGING)) {
			dtime = 0x80;
		} else {
			dtime = 0x101;
		}
		asic3_set_led(asic3, 1, dtime, 0x100);
		asic3_set_led(asic3, 0, dtime, 0x100);
		GPSR(GPIO_NR_H1900_CHARGER_EN) = GPIO_bit(GPIO_NR_H1900_CHARGER_EN);
	} else {
		asic3_set_led(asic3, 1, 0, 0x100);
		asic3_set_led(asic3, 0, 0, 0x100);
		GPCR(GPIO_NR_H1900_CHARGER_EN) = GPIO_bit(GPIO_NR_H1900_CHARGER_EN);
	};

	return IRQ_HANDLED;
};

static struct irqaction h1900_charge_irq = {
	name:     "h1910/h1915 charge",
	handler:  h1900_charge,
	flags:    SA_INTERRUPT
};

#ifdef CONFIG_PM

#define CTRL_START  0x80
#define CTRL_AUX_N  0x64
#define CTRL_PD0    0x01

static void h1900_battery(void)
{
	int sample;

	sample = h1900_ssp_putget(CTRL_PD0 | CTRL_START | CTRL_AUX_N); // main battery: max - 4096, min - 3300
	h1900_ssp_putget(CTRL_START | CTRL_AUX_N);

	battery_power = ((sample - 3300) * 100) / 796;

	if (!!(GPLR(GPIO_NR_H1900_AC_IN_N) & GPIO_bit(GPIO_NR_H1900_AC_IN_N))) {
		if (battery_power > 50) {
			asic3_set_led(asic3, 1, 0, 0x100);
			asic3_set_led(asic3, 0, 0, 0x100);
		} else if ((battery_power < 50) && (battery_power > 10)) {
			h1900_set_led(H1900_GREEN_LED, 0x101, 0x100);
		} else if (battery_power < 10) {
			h1900_set_led(H1900_RED_LED, 0x101, 0x100);
		}
	}

	//printk("bat: %d\n", battery_power);
}

typedef void (*apm_get_power_status_t)(struct apm_power_info*);

static void h1900_apm_get_power_status(struct apm_power_info *info)
{
	h1900_battery();

	info->battery_life = battery_power;

	if (!(GPLR(GPIO_NR_H1900_AC_IN_N) & GPIO_bit(GPIO_NR_H1900_AC_IN_N)))
		info->ac_line_status = APM_AC_ONLINE;
	else
		info->ac_line_status = APM_AC_OFFLINE;

	if (GPLR(GPIO_NR_H1900_CHARGING) & GPIO_bit(GPIO_NR_H1900_CHARGING))
		info->battery_status = APM_BATTERY_STATUS_CHARGING;
	else if (!(GPLR(GPIO_NR_H1900_MBAT_IN) & GPIO_bit(GPIO_NR_H1900_MBAT_IN)))
		info->battery_status = APM_BATTERY_STATUS_NOT_PRESENT;
	else {
		if (battery_power > 50)
			info->battery_status = APM_BATTERY_STATUS_HIGH;
		else if (battery_power < 5)
			info->battery_status = APM_BATTERY_STATUS_CRITICAL;
		else
			info->battery_status = APM_BATTERY_STATUS_LOW;
	}

	info->time = 0;
	info->units = 0;
}

int set_apm_get_power_status(apm_get_power_status_t t)
{
	apm_get_power_status = t;

	return 0;
}
#endif

static int h1900_power_probe(struct device *dev)
{
	h1900_charge(0, NULL, NULL);

	set_irq_type(IRQ_GPIO(GPIO_NR_H1900_AC_IN_N), IRQT_BOTHEDGE);
	set_irq_type(IRQ_GPIO(GPIO_NR_H1900_MBAT_IN), IRQT_BOTHEDGE);
	set_irq_type(IRQ_GPIO(GPIO_NR_H1900_CHARGING), IRQT_BOTHEDGE);
	setup_irq(IRQ_GPIO(GPIO_NR_H1900_AC_IN_N), &h1900_charge_irq);
	setup_irq(IRQ_GPIO(GPIO_NR_H1900_MBAT_IN), &h1900_charge_irq);
	setup_irq(IRQ_GPIO(GPIO_NR_H1900_CHARGING), &h1900_charge_irq);

#ifdef CONFIG_PM
	set_apm_get_power_status(h1900_apm_get_power_status);
#endif

	return 0;
}

#ifdef CONFIG_PM
static int h1900_power_suspend(struct device *dev, pm_message_t state)
{
	asic3_set_led(asic3, 1, 0, 0x100);
	asic3_set_led(asic3, 0, 0, 0x100);

	disable_irq(IRQ_GPIO(GPIO_NR_H1900_AC_IN_N));
	disable_irq(IRQ_GPIO(GPIO_NR_H1900_MBAT_IN));
	disable_irq(IRQ_GPIO(GPIO_NR_H1900_CHARGING));

	return 0;
}

static int h1900_power_resume(struct device *dev)
{
	enable_irq(IRQ_GPIO(GPIO_NR_H1900_AC_IN_N));
	enable_irq(IRQ_GPIO(GPIO_NR_H1900_MBAT_IN));
	enable_irq(IRQ_GPIO(GPIO_NR_H1900_CHARGING));

	return 0;
}
#endif

static struct device_driver h1900_power_driver = {
	.name		= "h1900-power",
	.bus		= &platform_bus_type,
	.probe		= h1900_power_probe,
#ifdef CONFIG_PM
	.suspend	= h1900_power_suspend,
	.resume		= h1900_power_resume,
#endif
};

static int __init h1900_power_init(void)
{
	if (!machine_is_h1900())
		return -ENODEV;

	return driver_register(&h1900_power_driver);
}

static void __exit h1900_power_exit(void)
{
	driver_unregister(&h1900_power_driver);
}

module_init(h1900_power_init);
module_exit(h1900_power_exit);

MODULE_AUTHOR("Joshua Wise, Pawel Kolodziejski");
MODULE_DESCRIPTION("HP iPAQ h1910/h1915 Power Driver");
MODULE_LICENSE("GPL");
