/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Copyright (C) 2003-2004 Joshua Wise
 * Copyright (C) 2005 Pawel Kolodziejski
 *
 * History:
 *
 * 2004-06-01   Joshua Wise        Adapted to new LED API
 * 2004-06-01   Joshua Wise        Re-adapted for hp iPAQ h1900
 * ????-??-??   ?                  Converted to h3900_lcd.c
 * 2003-05-14   Joshua Wise        Adapted for the HP iPAQ H1900
 * 2002-08-23   Jamey Hicks        Adapted for use with PXA250-based iPAQs
 * 2001-10-??   Andrew Christian   Added support for iPAQ H3800
 *                                 and abstracted EGPIO interface.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/lcd.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/hardware.h>
#include <asm/setup.h>

#include <asm/arch/pxafb.h>
#include <asm/mach/arch.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/h1900-asic.h>
#include <asm/arch/h1900-gpio.h>
#include <asm/arch/ipaq.h>

#include <linux/soc/asic3_base.h>

static int lcd_power;
static int backlight_power;

extern struct platform_device pxafb_device;
extern struct platform_device h1900_asic3;
#define asic3 &h1900_asic3.dev

int h1900_lcd_set_power(struct lcd_device *lm, int level)
{
	switch (level) {
		case FB_BLANK_UNBLANK:
		case FB_BLANK_NORMAL:
            		asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_5V, GPIO3_H1900_LCD_5V);
			mdelay(20);
            		asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_PCI, GPIO3_H1900_LCD_PCI);
			mdelay(20);
            		asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_3V, GPIO3_H1900_LCD_3V);
			GPSR(GPIO_NR_H1900_VGL_EN) = GPIO_bit(GPIO_NR_H1900_VGL_EN);
			GPSR(GPIO_NR_H1900_LCD_SOMETHING) = GPIO_bit(GPIO_NR_H1900_LCD_SOMETHING);
			mdelay(10);
			break;
		case FB_BLANK_VSYNC_SUSPEND:
		case FB_BLANK_HSYNC_SUSPEND:
			break;
		case FB_BLANK_POWERDOWN:
			GPCR(GPIO_NR_H1900_LCD_SOMETHING) = GPIO_bit(GPIO_NR_H1900_LCD_SOMETHING);
			GPCR(GPIO_NR_H1900_VGL_EN) = GPIO_bit(GPIO_NR_H1900_VGL_EN);
			mdelay(1);
			asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_3V, 0);
			asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_PCI, 0);
			asic3_set_gpio_out_c(asic3, GPIO3_H1900_LCD_5V, 0);
			break;
	}

	lcd_power = level;

	return 0;
}

static int h1900_lcd_get_power(struct lcd_device *lm)
{
	return lcd_power;
}

int h1900_backlight_set_power(struct backlight_device *bl, int level)
{
	switch (level) {
		case FB_BLANK_UNBLANK:
		case FB_BLANK_NORMAL:
			GPSR(GPIO_NR_H1900_LCD_PWM) = GPIO_bit(GPIO_NR_H1900_LCD_PWM);
			CKEN |= CKEN0_PWM0;
			mdelay(50);
			asic3_set_gpio_out_c(asic3, GPIO3_H1900_BACKLIGHT_POWER, GPIO3_H1900_BACKLIGHT_POWER);
			break;
		case FB_BLANK_VSYNC_SUSPEND:
		case FB_BLANK_HSYNC_SUSPEND:
			break;
		case FB_BLANK_POWERDOWN:
			GPCR(GPIO_NR_H1900_LCD_PWM) = GPIO_bit(GPIO_NR_H1900_LCD_PWM);
			CKEN &= ~CKEN0_PWM0;
			asic3_set_gpio_out_c(asic3, GPIO3_H1900_BACKLIGHT_POWER, 0);
			break;
	}

	backlight_power = level;

	return 0;
}

static int h1900_backlight_get_power(struct backlight_device *bl)
{
	return backlight_power;
}

int h1900_backlight_set_brightness(struct backlight_device *bm, int value)
{
	PWM_CTRL0 = 0;
	PWM_PWDUTY0 = (value * 183) / 255;
	PWM_PERVAL0 = 183;

	return 0;
}

int h1900_backlight_get_brightness(struct backlight_device *bm)
{
	return (PWM_PWDUTY0 * 255) / 183;
}

static struct lcd_properties h1900_lcd_properties = {
	.owner          = THIS_MODULE,
	.set_power      = h1900_lcd_set_power,
	.get_power      = h1900_lcd_get_power,
};

static struct backlight_properties h1900_backlight_properties = {
	.owner          = THIS_MODULE,
	.set_power      = h1900_backlight_set_power,
	.get_power      = h1900_backlight_get_power,
	.max_brightness = 255,
	.set_brightness = h1900_backlight_set_brightness,
	.get_brightness = h1900_backlight_get_brightness,
};

static struct lcd_device *pxafb_lcd_device;
static struct backlight_device *pxafb_backlight_device;

struct pm_save_data {
	int brightness;
};

static int h1900_lcd_probe(struct device *dev)
{
	pxafb_lcd_device = lcd_device_register("pxafb", NULL, &h1900_lcd_properties);

	if (IS_ERR(pxafb_lcd_device))
		return PTR_ERR(pxafb_lcd_device);
	pxafb_backlight_device = backlight_device_register("pxafb", NULL, &h1900_backlight_properties);
	if (IS_ERR(pxafb_backlight_device)) {
		lcd_device_unregister(pxafb_lcd_device);
		return PTR_ERR(pxafb_backlight_device);
	}

	h1900_backlight_set_brightness(pxafb_backlight_device, 100);

	return 0;
}

static int h1900_lcd_remove(struct device *dev)
{
	lcd_device_unregister(pxafb_lcd_device);
	backlight_device_unregister(pxafb_backlight_device);

	return 0;
}

#ifdef CONFIG_PM
static int h1900_lcd_suspend(struct device *dev, pm_message_t state)
{
	struct pm_save_data *save;

	save = kmalloc(sizeof(struct pm_save_data), GFP_KERNEL);
	if (!save)
		return -ENOMEM;

	dev->power.saved_state = save;

	save->brightness = h1900_backlight_get_brightness(NULL);

	return 0;
}

static int h1900_lcd_resume(struct device *dev)
{
	struct pm_save_data *save;

	save = (struct pm_save_data *)dev->power.saved_state;
	if (!save)
		return 0;

	h1900_backlight_set_brightness(NULL, save->brightness);

	return 0;
}
#endif

static struct device_driver h1900_lcd_driver = {
	.name		= "h1900-lcd",
	.bus		= &platform_bus_type,
	.probe		= h1900_lcd_probe,
	.remove		= h1900_lcd_remove,
#ifdef CONFIG_PM
	.suspend	= h1900_lcd_suspend,
	.resume		= h1900_lcd_resume,
#endif
};

static __init int h1900_lcd_init(void)
{
	if (!machine_is_h1900())
		return -ENODEV;

	return driver_register(&h1900_lcd_driver);
}

static __exit void h1900_lcd_exit(void)
{
	driver_unregister(&h1900_lcd_driver);
}

module_init(h1900_lcd_init);
module_exit(h1900_lcd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pawel Kolodziejski");
MODULE_DESCRIPTION("iPAQ h1910/h1915 LCD driver glue");
