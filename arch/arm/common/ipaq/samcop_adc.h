#define ADC_DATA_MASK	0x3ff

enum adc_state {
	ADC_STATE_IDLE,
	ADC_STATE_TOUCHSCREEN,  // Servicing the touchscreen
	ADC_STATE_USER          // Servicing a user-level request
};

struct touchscreen_data;

struct samcop_adc_platform_data {
	int	adc_delay;
};

struct samcop_adc {
	struct clk	   *clk;
	enum adc_state     state;
	struct semaphore   lock;      // Mutex for access from user-level
	wait_queue_head_t  waitq;     // Waitq for user-level access (waits for interrupt service)
	struct samcop_adc_platform_data *plat;
	int		   ts_irq;
	int		   ts_absfuzz_x;
	int		   ts_absfuzz_y;

	struct device	   *parent;
	void		   *map;
	struct resource	    res;

	struct touchscreen_data *ts;
};

int samcop_adc_request_sample (struct samcop_adc *, u32 delay, u32 controlval);
void samcop_adc_read_data (struct samcop_adc *, u16 *data0, u16 *data1);

static inline void
samcop_adc_write_register (struct samcop_adc *adc, u32 reg, u16 val)
{
	__raw_writew (val, reg + adc->map);
}

static inline u16
samcop_adc_read_register (struct samcop_adc *adc, u32 reg)
{
	return __raw_readw (reg + adc->map);
}
