/*
 * linux/arch/arm/common/ipaq/clock.h
 *
 * Copyright (c) 2004-2005 Simtec Electronics
 *	http://www.simtec.co.uk/products/SWLINUX/
 *	Written by Ben Dooks, <ben@simtec.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

struct clk {
	struct list_head      list;
	struct module        *owner;
	struct clk           *parent;
	const char           *name;
	int		      id;
	atomic_t              used;
	unsigned long         rate;
	unsigned long         ctrlbit;
	unsigned long	      priv;
	int		    (*enable)(struct clk *, int enable);
};

/* exports for arch/arm/common/ipaq
 *
 * Please DO NOT use these outside of arch/arm/common/ipaq
*/

extern int ipaq_register_clock(struct clk *clk);
extern int ipaq_unregister_clock(struct clk *clk);
