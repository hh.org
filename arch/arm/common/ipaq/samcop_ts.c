/*
* Driver interface to the ASIC Companion chip on the iPAQ H5400
*
* Copyright © 2003 Compaq Computer Corporation.
*
* Use consistent with the GNU GPL is permitted,
* provided that this copyright notice is
* preserved in its entirety in all copies and derived works.
*
* COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
* AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
* FITNESS FOR ANY PARTICULAR PURPOSE.
*
* Author:  Keith Packard <keith.packard@hp.com>
*          May 2003
*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/config.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/device.h>
#include <linux/clk.h>

#include <asm/irq.h>
#include <asm/io.h>
#include <asm/arch/hardware.h>
#include <asm/hardware/samcop-adc.h>

#include "samcop_adc.h"
#include "samcop_ts.h"

// Define this to see all suspend/resume init/cleanup messages
#define DEBUG_INIT()  \
        if (0) printk(KERN_NOTICE "enter %s\n", __FUNCTION__)
#define DEBUG_FINI() \
        if (0) printk(KERN_NOTICE "leave %s\n", __FUNCTION__)
#define DEBUG_OUT(f,a...) \
	if (0) printk(KERN_NOTICE "%s:" f, __FUNCTION__, ##a)
#define DEBUG_ISR_INIT()  \
        if (0) printk(KERN_NOTICE "enter %s\n", __FUNCTION__)
#define DEBUG_ISR_FINI() \
        if (0) printk(KERN_NOTICE "leave %s\n", __FUNCTION__)
#define DEBUG_ISR_OUT(f,a...) \
	if (0) printk(KERN_NOTICE "%s:" f, __FUNCTION__, ##a)

#define PDEBUG(format,arg...) printk(KERN_DEBUG __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)
#define PALERT(format,arg...) printk(KERN_ALERT __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)
#define PERROR(format,arg...) printk(KERN_ERR __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)
	
enum touchscreen_state {
	TS_STATE_WAIT_PEN_DOWN,   // Waiting for a PEN interrupt
	TS_STATE_ACTIVE_SAMPLING  // Actively sampling ADC
};

struct touchscreen_data {
	enum touchscreen_state state;
	struct timer_list      timer;
	int		       irq;
	struct input_dev       *input;
};

#define ADC_FREQ		2000000	/* target ADC clock frequency */

#define ADC_PEN_DELAY	    500

#define ADC_DEFAULT_DELAY   500

#define ADC_SAMPLE_PERIOD	10  /* Sample every 10 milliseconds */
static unsigned long adc_sample_period   = ADC_SAMPLE_PERIOD;
static unsigned long adc_prescaler       = 0;
static unsigned long adc_delay           = 0;

module_param(adc_sample_period, ulong, S_IRUGO);
module_param(adc_delay, ulong, S_IRUGO);

static void samcop_touchscreen_start_record (struct samcop_adc *);
static void samcop_touchscreen_record (struct samcop_adc *);
static void samcop_touchscreen_next_record (struct samcop_adc *);

/* Jamey says: 
 *
 * initialization:
 * adc control: enable prescaler, use prescaler value 19, sel=3 (ain3?)
 * use adc delay=1000
 * touchscreen control: initialize to 0xd3
 *  SAMCOP_ADC_TSC_XYPST_INTERRUPT, SAMCOP_ADC_TSC_XP_SEN, 
 *  SAMCOP_ADC_TSC_YP_SEN, SAMCOP_ADC_TSC_YM_SEN
 */

#define ASIC_ADC_CONTROL_INIT	(SAMCOP_ADC_CONTROL_PRESCALER_ENABLE | \
				 (adc_prescaler << SAMCOP_ADC_CONTROL_PRESCALER_SHIFT) | \
				 SAMCOP_ADC_CONTROL_SEL_AIN3)

#define ASIC_ADC_TSC_WAIT	(SAMCOP_ADC_TSC_XYPST_INTERRUPT | \
				 SAMCOP_ADC_TSC_XP_SEN | \
				 SAMCOP_ADC_TSC_YP_SEN | \
				 SAMCOP_ADC_TSC_YM_SEN)


/* Jamey says:
 * when getting touch coordinage,
 *  - set adctsc to ((1<<3)|(1<<2)) -- pull up disable, auto mode
 *  - set bit 0 of adc control
 *  - set back to 0xd3 after getting coordinate
 */

#define ASIC_ADC_CONTROL_SAMPLE	(ASIC_ADC_CONTROL_INIT|SAMCOP_ADC_CONTROL_FORCE_START)

#define ASIC_ADC_TSC_SAMPLE	(SAMCOP_ADC_TSC_AUTO_MODE | \
				 SAMCOP_ADC_TSC_PULL_UP | \
				 SAMCOP_ADC_TSC_XP_SEN | \
				 SAMCOP_ADC_TSC_YP_SEN | \
				 SAMCOP_ADC_TSC_YM_SEN)


#define ASIC_ADC_TSC_SUSPEND	(SAMCOP_ADC_TSC_XYPST_NONE | \
				 SAMCOP_ADC_TSC_PULL_UP    | \
				 SAMCOP_ADC_TSC_XP_SEN     | \
				 SAMCOP_ADC_TSC_YP_SEN)

/***********************************************************************************
 *   Touchscreen support (stolen from the H3900 code)
 ***********************************************************************************/
	
static void
report_touchpanel (struct touchscreen_data *ts, int pressure, int x, int y)
{
	if (pressure) {
		input_report_key (ts->input, BTN_TOUCH, 1);
		input_report_abs (ts->input, ABS_X, x);
		input_report_abs (ts->input, ABS_Y, y);
		input_report_abs (ts->input, ABS_PRESSURE, pressure);
	} else {
		input_report_key (ts->input, BTN_TOUCH, 0);
		input_report_abs (ts->input, ABS_PRESSURE, pressure);
	}
	input_sync (ts->input);
}

static int
samcop_pen_isr (int irq, void *dev_id, struct pt_regs *regs)
{
	struct samcop_adc *adc = dev_id;

	samcop_touchscreen_start_record (adc);

	return 1;
}

static inline void
samcop_adc_write_tscontrol (struct samcop_adc *adc, u16 val)
{
	samcop_adc_write_register (adc, _SAMCOP_ADC_TouchScreenControl, val);
}

static void 
samcop_ts_interrupt_mode (struct samcop_adc *adc)
{
	samcop_adc_write_register (adc, _SAMCOP_ADC_Delay, ADC_PEN_DELAY);
	samcop_adc_write_tscontrol (adc, ASIC_ADC_TSC_WAIT);
}

/* Ask for the next sample */
static void 
samcop_adc_start_touchscreen (struct samcop_adc *adc, struct touchscreen_data *touch)
{
	samcop_adc_write_tscontrol (adc, ASIC_ADC_TSC_SAMPLE);

	if (samcop_adc_request_sample (adc, adc_delay, ASIC_ADC_CONTROL_SAMPLE) == 0) {
		samcop_touchscreen_record (adc);
	} else {
		printk ("touchscreen adc timed out\n");
		/* try again */
		samcop_touchscreen_next_record (adc);
	}
}

/* Pen is up, stop sample collection process */
static void 
samcop_touchscreen_stop_record (struct samcop_adc *adc)
{
	del_timer_sync (&adc->ts->timer);
	adc->ts->state = TS_STATE_WAIT_PEN_DOWN;
	samcop_ts_interrupt_mode (adc);
}

/* Pen is down, start sample collection process */
static void 
samcop_touchscreen_start_record (struct samcop_adc *adc)
{
	struct touchscreen_data *touch;

	touch = adc->ts;

	if (touch->state == TS_STATE_WAIT_PEN_DOWN) {
		touch->state = TS_STATE_ACTIVE_SAMPLING;

		samcop_adc_start_touchscreen (adc, touch);
	}
}


static void 
samcop_touchscreen_timer_callback (unsigned long data)
{
	struct samcop_adc *adc = (struct samcop_adc *)data;

	/* ask for another sample */
	samcop_adc_start_touchscreen (adc, adc->ts);
}

static void 
samcop_touchscreen_next_record (struct samcop_adc *adc)
{
	unsigned long inc = (adc_sample_period * HZ) / 1000;
	if (!inc) inc = 1;

	/* 
	 * Set touch screen control back to 0xd3 so that the next
	 * sample will have the PEN_UP bit set correctly
	 */
	samcop_adc_write_tscontrol (adc, ASIC_ADC_TSC_WAIT);

	/* queue the timer to get another sample */
	mod_timer (&adc->ts->timer, jiffies + inc);
}

/* Read a sample, then queue a timer if the pen is still down */
static void 
samcop_touchscreen_record (struct samcop_adc *adc)
{
	u16	Data0, Data1;
	int	x, y;

	samcop_adc_read_data (adc, &Data0, &Data1);

	if (Data0 & SAMCOP_ADC_DATA0_PEN_UP ||
	    Data1 & SAMCOP_ADC_DATA0_PEN_UP) {
		DEBUG_ISR_OUT ("pen up\n");
		report_touchpanel (adc->ts, 0, 0, 0);
		samcop_touchscreen_stop_record (adc);
	} else {
		x = Data0 & ADC_DATA_MASK;	/* flip X axis */
		y = Data1 & ADC_DATA_MASK;	/* flip Y axis */
		DEBUG_ISR_OUT ("pen down %d, %d\n", x, y);
		report_touchpanel (adc->ts, 1, x, y);
		samcop_touchscreen_next_record (adc);
	}
}
	
int 
samcop_touchscreen_attach (struct samcop_adc *adc)
{
	struct touchscreen_data *ts;
	int result;

	ts = kzalloc(sizeof (*ts), GFP_KERNEL);
	if (!ts)
		return -ENOMEM;

	ts->input = input_allocate_device();
	ts->input->evbit[0] = BIT(EV_ABS) | BIT(EV_KEY);
	ts->input->keybit[LONG(BTN_TOUCH)] = BIT(BTN_TOUCH);
	input_set_abs_params(ts->input, ABS_X, 0, 1023, adc->ts_absfuzz_x, 0);
	input_set_abs_params(ts->input, ABS_Y, 0, 1023, adc->ts_absfuzz_y, 0);
	input_set_abs_params(ts->input, ABS_PRESSURE, 0, 1, 0, 0);
	ts->input->name = "samcop ts";
	ts->input->private = ts;

	ts->timer.function = samcop_touchscreen_timer_callback;
	ts->timer.data = (unsigned long)adc;
	ts->state = TS_STATE_WAIT_PEN_DOWN;
	init_timer (&ts->timer);

	input_register_device (ts->input);

	adc->ts = ts;

	if (adc_prescaler == 0)
		adc_prescaler = (clk_get_rate (adc->clk) / ADC_FREQ) - 1;

	/* Set adc_delay, according to this priority:
	   module argument, platform data, default. */
	if (!adc_delay && adc->plat)
		adc_delay = adc->plat->adc_delay;

	if (adc_delay == 0)
		adc_delay = ADC_DEFAULT_DELAY;

	samcop_ts_interrupt_mode (adc);

	result = request_irq (adc->ts_irq, samcop_pen_isr, SA_SAMPLE_RANDOM, "touchscreen", adc);

	if (result) {
		printk(KERN_CRIT "%s: unable to grab ADCTS IRQ %d error=%d\n", __FUNCTION__, 
		       adc->ts_irq, result);
		input_unregister_device (ts->input);
		kfree (ts);
		return result;
	}

	return 0;
}

void 
samcop_touchscreen_detach (struct samcop_adc *adc)
{
	struct touchscreen_data *ts = adc->ts;

	del_timer_sync (&ts->timer);
	free_irq (adc->ts_irq, adc);
	input_unregister_device (ts->input);
	kfree (ts);
}

/* Note: this is called from samcop_adc_suspend. */
int
samcop_touchscreen_suspend (struct samcop_adc *adc, pm_message_t state)
{
	report_touchpanel (adc->ts, 0, 0, 0);
	samcop_touchscreen_stop_record (adc);
	samcop_adc_write_tscontrol (adc, ASIC_ADC_TSC_SUSPEND);

	return 0;
}

/* Note: this is called from samcop_adc_resume. */
int
samcop_touchscreen_resume (struct samcop_adc *adc)
{
        samcop_ts_interrupt_mode (adc);
	return 0;
}
