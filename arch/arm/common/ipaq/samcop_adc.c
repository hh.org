/*
* Driver interface to the ASIC Companion chip on the iPAQ H5400
*
* Copyright © 2003 Compaq Computer Corporation.
*
* Use consistent with the GNU GPL is permitted,
* provided that this copyright notice is
* preserved in its entirety in all copies and derived works.
*
* COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
* AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
* FITNESS FOR ANY PARTICULAR PURPOSE.
*
* Author:  Keith Packard <keith.packard@hp.com>
*          May 2003
*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/config.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

#include <asm/arch/hardware.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/hardware/samcop-adc.h>

#include "samcop_adc.h"
#include "samcop_ts.h"


// Define this to see all suspend/resume init/cleanup messages
#define DEBUG_INIT()  \
        if (0) printk(KERN_NOTICE "enter %s\n", __FUNCTION__)
#define DEBUG_FINI() \
        if (0) printk(KERN_NOTICE "leave %s\n", __FUNCTION__)
#define DEBUG_OUT(f,a...) \
	if (0) printk(KERN_NOTICE "%s:" f, __FUNCTION__, ##a)
#define DEBUG_ISR_INIT()  \
        if (0) printk(KERN_NOTICE "enter %s\n", __FUNCTION__)
#define DEBUG_ISR_FINI() \
        if (0) printk(KERN_NOTICE "leave %s\n", __FUNCTION__)
#define DEBUG_ISR_OUT(f,a...) \
	if (0) printk(KERN_NOTICE "%s:" f, __FUNCTION__, ##a)

#define PDEBUG(format,arg...) printk(KERN_DEBUG __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)
#define PALERT(format,arg...) printk(KERN_ALERT __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)
#define PERROR(format,arg...) printk(KERN_ERR __FILE__ ":%s - " format "\n", __FUNCTION__, ## arg)

static unsigned long adc_spins_max       = 1000;
static int absfuzz_x = 0;
static int absfuzz_y = 0;

module_param(adc_spins_max, ulong, S_IRUGO);
module_param(absfuzz_x, int, S_IRUGO);
module_param(absfuzz_y, int, S_IRUGO);


/***********************************************************************************
 *   ADC support (stolen from the H3900 code)
 ***********************************************************************************/

static void 
samcop_adc_up (struct samcop_adc *adc)
{
	DEBUG_INIT ();
	
	/* Enable the ADC clock */
	clk_enable(adc->clk);

	DEBUG_FINI ();
}

static void 
samcop_adc_down (struct samcop_adc *adc)
{
	u16 val;
	
	/* Place the ADC in standby mode */
	val = samcop_adc_read_register (adc, _SAMCOP_ADC_Control);
	samcop_adc_write_register (adc, _SAMCOP_ADC_Control, val | SAMCOP_ADC_CONTROL_STANDBY);

	/* Disable the ADC clock */
	clk_disable(adc->clk);
	
	adc->state  = ADC_STATE_IDLE;
}

int
samcop_adc_request_sample (struct samcop_adc *adc, u32 delay, u32 controlval)
{
	unsigned long spins = 0;

	samcop_adc_write_register (adc, _SAMCOP_ADC_Delay, delay);
	samcop_adc_write_register (adc, _SAMCOP_ADC_Control, controlval | SAMCOP_ADC_CONTROL_FORCE_START);

	/* XXX Wait for an interrupt instead? */
	while ((samcop_adc_read_register (adc, _SAMCOP_ADC_Control) & SAMCOP_ADC_CONTROL_CONVERSION_END) == 0) {
		if (spins++ > adc_spins_max)
			return -EIO;
	}

	return 0;
}

void
samcop_adc_read_data (struct samcop_adc *adc, u16 *data0, u16 *data1)
{
	if (data0)
		*data0 = samcop_adc_read_register (adc, _SAMCOP_ADC_Data0);

	if (data1)
		*data1 = samcop_adc_read_register (adc, _SAMCOP_ADC_Data1);
}

static int 
samcop_adc_suspend (struct platform_device *pdev, pm_message_t state)
{
	struct samcop_adc *adc = platform_get_drvdata(pdev);

	samcop_touchscreen_suspend(adc, state);
	samcop_adc_down (adc);

	return 0;
}

static int
samcop_adc_resume (struct platform_device *pdev)
{
	struct samcop_adc *adc = platform_get_drvdata(pdev);

	samcop_adc_up (adc);
	samcop_touchscreen_resume(adc);

	return 0;
}

static int 
samcop_adc_probe (struct platform_device *pdev)
{
	int result = 0;
	struct samcop_adc *adc;
	struct resource *res;

	adc = kmalloc (sizeof (*adc), GFP_KERNEL);
	if (!adc)
		return -ENOMEM;
	memset (adc, 0, sizeof (*adc));

	adc->parent = pdev->dev.parent;
	adc->plat = pdev->dev.platform_data;
	platform_set_drvdata(pdev, adc);
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	adc->map = ioremap (res->start, res->end - res->start + 1);
	adc->ts_irq = platform_get_irq(pdev, 0);
	adc->ts_absfuzz_x = absfuzz_x;
	adc->ts_absfuzz_y = absfuzz_y;

	adc->clk = clk_get(&pdev->dev, "adc");
	if (IS_ERR(adc->clk)) {
		printk(KERN_ERR "failed to get adc clock");
		result = -ENOENT;
		goto error0;
	}

	samcop_adc_up (adc);

	result = samcop_touchscreen_attach (adc);
	if (result) {
		printk(KERN_ERR "couldn't attach samcop touchscreen driver: error %d\n", result);
		goto error1;
	}

	return 0;

error1:
	samcop_adc_up (adc);
	clk_put(adc->clk);

error0:
	kfree (adc);

	return result;
}

static int
samcop_adc_remove (struct platform_device *pdev)
{
	struct samcop_adc *adc = platform_get_drvdata(pdev);

	samcop_touchscreen_detach (adc);
	samcop_adc_down (adc);
	clk_disable(adc->clk);
	clk_put(adc->clk);
	iounmap (adc->map);
	kfree (adc);

	return 0;
}

//static platform_device_id samcop_adc_device_ids[] = { { IPAQ_SAMCOP_ADC_DEVICE_ID }, { 0 } };

struct platform_driver samcop_adc_driver = {
	.driver   = {
		.name = "samcop adc",
	},
	.probe    = samcop_adc_probe,
	.remove   = samcop_adc_remove,
	.suspend  = samcop_adc_suspend,
	.resume   = samcop_adc_resume
};

static int
samcop_adc_init (void)
{
	return platform_driver_register (&samcop_adc_driver);
}

static void
samcop_adc_cleanup (void)
{
	platform_driver_unregister (&samcop_adc_driver);
}

module_init(samcop_adc_init);
module_exit(samcop_adc_cleanup);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org>");
MODULE_DESCRIPTION("ADC driver for HAMCOP/SAMCOP");
//MODULE_DEVICE_TABLE(soc, samcop_adc_device_ids);
