/*
 * 1-Wire busmaster lowlevel interface for the SHAMCOP ASIC
 *
 * Copyright (c) 2004-2005, Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Author:  
 *          Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *          September 2004
 *
 *          Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *          January 2005 - W1 implementation
 */

#ifndef __w1_samcop_h__
#define __w1_samcop_h__

struct owm_freq {
	unsigned long freq;
	unsigned long divisor;
};

struct samcop_w1_data {
	struct device *parent;
	void *map;
	int irq;
	struct clk *clk;
	int slave_present;
	void *reset_complete;
	void *read_complete;
	void *write_complete;
	u8 read_byte;
};

#endif /* !__w1_samcop_h__ */

/*
 * Local Variables:
 *  mode:c
 *  c-style:"K&R"
 *  c-basic-offset:8
 * End:
 *
 */
