/*
 * 1-wire busmaster driver for DS1WM, and ASICs with embedded DS1WMs
 * such as HP iPAQs (including h5xxx, h2200, and devices with ASIC3
 * like hx4700).
 *
 * Copyright (c) 2004-2005, Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 * Copyright (c) 2004-2006, Matt Reimer <mreimer@vpop.net>
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 */

#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/pm.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

#include <asm/io.h>

#include "../w1.h"
#include "../w1_int.h"

#include "ds1wm.h"

#define DS1WM_CMD	0x00	/* R/W 4 bits command */
#define DS1WM_DATA	0x01	/* R/W 8 bits, transmit/receive buffer */
#define DS1WM_INT	0x02	/* R/W interrupt status */
#define DS1WM_INT_EN	0x03	/* R/W interrupt enable */
#define DS1WM_CLKDIV	0x04	/* R/W 5 bits of divisor and pre-scale */

#define DS1WM_CMD_1W_RESET  1 << 0	/* force reset on 1-wire bus */
#define DS1WM_CMD_SRA	    1 << 1	/* enable Search ROM accelerator mode */
#define DS1WM_CMD_DQ_OUTPUT 1 << 2	/* write only - forces bus low */
#define DS1WM_CMD_DQ_INPUT  1 << 3	/* read only - reflects state of bus */

#define DS1WM_INT_PD	    1 << 0	/* presence detect */
#define DS1WM_INT_PDR	    1 << 1	/* presence detect result */
#define DS1WM_INT_TBE	    1 << 2	/* tx buffer empty */
#define DS1WM_INT_TSRE	    1 << 3	/* tx shift register empty */
#define DS1WM_INT_RBF	    1 << 4	/* rx buffer full */
#define DS1WM_INT_RSRF	    1 << 5	/* rx shift register full */

#define DS1WM_INTEN_EPD	    1 << 0	/* enable presence detect interrupt */
#define DS1WM_INTEN_IAS	    1 << 1	/* INTR active state */
#define DS1WM_INTEN_ETBE    1 << 2	/* enable tx buffer empty interrupt */
#define DS1WM_INTEN_ETMT    1 << 3	/* enable tx shift register empty interrupt */
#define DS1WM_INTEN_ERBF    1 << 4	/* enable rx buffer full interrupt */
#define DS1WM_INTEN_ERSRF   1 << 5	/* enable rx shift register full interrupt */
#define DS1WM_INTEN_DQO	    1 << 6	/* enable direct bus driving operations (Undocumented), Szabolcs Gyurko */



#define DEBUG(format, args...) if (ds1wm_debug) printk(__FILE__ ":%s - " format "\n", __FUNCTION__, ## args)

#define DS1WM_TIMEOUT (HZ * 5)

static int ds1wm_debug = 0;

module_param_named(debug, ds1wm_debug, int, 0644);
MODULE_PARM_DESC(ds1wm_debug, "Set to enable debugging output");

static struct {
	unsigned long freq;
	unsigned long divisor;
} freq[] = {
	{ 4000000, 0x8 },
	{ 5000000, 0x2 },
	{ 6000000, 0x5 },
	{ 7000000, 0x3 },
	{ 8000000, 0xc },
	{ 10000000, 0x6 },
	{ 12000000, 0x9 },
	{ 14000000, 0x7 },
	{ 16000000, 0x10 },
	{ 20000000, 0xa },
	{ 24000000, 0xd },
	{ 28000000, 0xb },
	{ 32000000, 0x14 },
	{ 40000000, 0xe },
	{ 48000000, 0x11 },
	{ 56000000, 0xf },
	{ 64000000, 0x18 },
	{ 80000000, 0x12 },
	{ 96000000, 0x15 },
	{ 112000000, 0x13 },
	{ 128000000, 0x1c },
};

struct ds1wm_data {
	struct device	*parent;
	void		*map;
	int		bus_shift; /* # of shifts to calculate register offsets */
	int		irq;
	struct clk	*clk;
	int		slave_present;
	void		*reset_complete;
	void		*read_complete;
	void		*write_complete;
	u8		read_byte; /* last byte received */
};

static inline void
ds1wm_write_register(struct ds1wm_data *ds1wm_data, u32 reg, u8 val)
{
        __raw_writeb(val, ds1wm_data->map + (reg << ds1wm_data->bus_shift));
}

static inline u8
ds1wm_read_register(struct ds1wm_data *ds1wm_data, u32 reg)
{
        return __raw_readb(ds1wm_data->map + (reg << ds1wm_data->bus_shift));
}


static irqreturn_t
ds1wm_isr(int isr, void *data, struct pt_regs *regs)
{
	struct ds1wm_data *ds1wm_data = (struct ds1wm_data *)data;
	u8 owintr = ds1wm_read_register(ds1wm_data, DS1WM_INT);

	ds1wm_data->slave_present = owintr & DS1WM_INT_PDR ? 0 : 1;

	if (owintr & DS1WM_INT_PD && ds1wm_data->reset_complete)
		complete(ds1wm_data->reset_complete);

	if (owintr & DS1WM_INT_RBF) {
		ds1wm_data->read_byte = ds1wm_read_register(ds1wm_data,
							    DS1WM_DATA);
		if (ds1wm_data->read_complete)
			complete(ds1wm_data->read_complete);
	}

	if (owintr & DS1WM_INT_TSRE && ds1wm_data->write_complete)
		complete(ds1wm_data->write_complete);

	return IRQ_HANDLED;
}

static int
ds1wm_reset(struct ds1wm_data *ds1wm_data)
{
	DECLARE_COMPLETION(reset_done);

	DEBUG("enter");

	ds1wm_data->reset_complete = &reset_done;

	ds1wm_write_register(ds1wm_data, DS1WM_INT_EN, DS1WM_INTEN_EPD);

	ds1wm_write_register(ds1wm_data, DS1WM_CMD, DS1WM_CMD_1W_RESET);

	wait_for_completion_timeout(&reset_done, DS1WM_TIMEOUT);
	ds1wm_data->reset_complete = NULL;

	ds1wm_write_register(ds1wm_data, DS1WM_INT_EN,
		DS1WM_INTEN_ERBF | DS1WM_INTEN_ETMT | DS1WM_INTEN_EPD);

	if (!ds1wm_data->slave_present) {
                DEBUG("1-Wire Bus reset FAILED: no devices found");
                return 1;
        }

        DEBUG("1-Wire Bus reset successfully completed");

	DEBUG("leave");
        return 0;
}

static int
ds1wm_write(struct ds1wm_data *ds1wm_data, u8 data)
{
	DECLARE_COMPLETION(write_done);
	ds1wm_data->write_complete = &write_done;

	ds1wm_write_register(ds1wm_data, DS1WM_DATA, data);

	wait_for_completion_timeout(&write_done, DS1WM_TIMEOUT);
	ds1wm_data->write_complete = NULL;

	return 0;
}

static int
ds1wm_read(struct ds1wm_data *ds1wm_data, unsigned char data)
{
	DECLARE_COMPLETION(read_done);
	ds1wm_data->read_complete = &read_done;

	ds1wm_write(ds1wm_data, data);
	wait_for_completion_timeout(&read_done, DS1WM_TIMEOUT);
	ds1wm_data->read_complete = NULL;

	return ds1wm_data->read_byte;
}

static int
ds1wm_find_divisor(int gclk)
{
	int i;

	for (i = 0; i < ARRAY_SIZE (freq); i++) {
		if (gclk < freq[i].freq)
			return freq[i].divisor;
	}
	
	printk (KERN_ERR "ds1wm: no suitable divisor for %dHz clock\n", gclk);
	return 0;
}

static void
ds1wm_down(struct ds1wm_data *ds1wm_data)
{
	DEBUG("enter");
	clk_disable(ds1wm_data->clk);
	DEBUG("leave");
}

static void
ds1wm_up(struct ds1wm_data *ds1wm_data)
{
	int gclk;
	
	DEBUG("enter");
	gclk = clk_get_rate(ds1wm_data->clk);
	clk_enable(ds1wm_data->clk);
	ds1wm_write_register(ds1wm_data, DS1WM_CLKDIV,
			     ds1wm_find_divisor(gclk));
	DEBUG("leave");
}

/* --------------------------------------------------------------------- */
/* w1 methods */

static u8
ds1wm_read_byte(void *data)
{
	struct ds1wm_data *ds1wm_data = (struct ds1wm_data *)data;
	int ret;

	DEBUG("enter");

	ret = ds1wm_read(ds1wm_data, 0xff);

	DEBUG("leave");

	return ret;
}

static void
ds1wm_write_byte(void *data, u8 byte)
{
	struct ds1wm_data *ds1wm_data = (struct ds1wm_data *)data;

	DEBUG("enter");

	ds1wm_write(ds1wm_data, byte);

	DEBUG("leave");
}

static u8
ds1wm_reset_bus(void *data)
{
	struct ds1wm_data *ds1wm_data = (struct ds1wm_data *)data;

	DEBUG("enter");

	ds1wm_reset(ds1wm_data);

	DEBUG("leave");
	
	return 0;
}

static void
ds1wm_search(void *data, u8 search_type, w1_slave_found_callback slave_found)
{
	struct ds1wm_data *ds1wm_data = (struct ds1wm_data *)data;
	int i;
	unsigned long long rom_id;

	DEBUG("enter");

	/* XXX We need to iterate for multiple devices per the DS1WM docs.
	 * See http://www.maxim-ic.com/appnotes.cfm/appnote_number/120. */
	if (ds1wm_reset(ds1wm_data))
		return;
	
	ds1wm_write(ds1wm_data, search_type);
	ds1wm_write_register(ds1wm_data, DS1WM_CMD, DS1WM_CMD_SRA);

	for (rom_id = 0, i = 0; i < 16; i++) {

		unsigned char resp, r, d;

		resp = ds1wm_read(ds1wm_data, 0x00);

		r = ((resp & 0x02) >> 1) |
		    ((resp & 0x08) >> 2) |
		    ((resp & 0x20) >> 3) |
		    ((resp & 0x80) >> 4);

		d = ((resp & 0x01) >> 0) |
		    ((resp & 0x04) >> 1) |
		    ((resp & 0x10) >> 2) |
		    ((resp & 0x40) >> 3);

		rom_id |= (unsigned long long) r << (i * 4);

	}
	DEBUG("found 0x%08llX", rom_id);

	ds1wm_write_register(ds1wm_data, DS1WM_CMD, ~DS1WM_CMD_SRA);
	ds1wm_reset(ds1wm_data);

	slave_found(ds1wm_data, rom_id);
}

/* --------------------------------------------------------------------- */

static struct w1_bus_master ds1wm_master = {
	.read_byte  = ds1wm_read_byte,
	.write_byte = ds1wm_write_byte,
	.reset_bus  = ds1wm_reset_bus,
	.search	    = ds1wm_search,
};

/* XXX Fix leaks on error. */
static int 
ds1wm_probe(struct platform_device *pdev)
{
	struct ds1wm_data *ds1wm_data;
	struct ds1wm_platform_data *plat;
	struct resource *res;

	DEBUG("enter");
	if (!pdev)
		return -ENODEV;

	ds1wm_data = kmalloc(sizeof (*ds1wm_data), GFP_KERNEL);
	if (!ds1wm_data)
		return -ENOMEM;
	memset(ds1wm_data, 0, sizeof (*ds1wm_data));

	platform_set_drvdata(pdev, ds1wm_data);

	ds1wm_data->parent = pdev->dev.parent;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	ds1wm_data->map = ioremap(res->start, res->end - res->start + 1);
	plat = pdev->dev.platform_data;
	ds1wm_data->bus_shift = plat->bus_shift;

	ds1wm_data->irq = platform_get_irq(pdev, 0);

	set_irq_type(ds1wm_data->irq, IRQT_RISING);
	request_irq(ds1wm_data->irq, ds1wm_isr, SA_SAMPLE_RANDOM, "ds1wm",
		    ds1wm_data);

	ds1wm_data->clk = clk_get(&pdev->dev, "ds1wm");
	if (!ds1wm_data->clk) {
		printk(KERN_ERR "failed to get ds1wm clock\n");
		kfree(ds1wm_data);
		return -EBUSY;
	}
	ds1wm_up(ds1wm_data);

	ds1wm_master.data = (void *)ds1wm_data;

	if (w1_add_master_device(&ds1wm_master))
		ds1wm_down(ds1wm_data);

	DEBUG("leave");
	return 0;
}

#ifdef CONFIG_PM
static int 
ds1wm_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct ds1wm_data *ds1wm_data = platform_get_drvdata(pdev);

	DEBUG("start");

	ds1wm_down(ds1wm_data);

	DEBUG("finish");

	return 0;
}

static int
ds1wm_resume(struct platform_device *pdev)
{
	struct ds1wm_data *ds1wm_data = platform_get_drvdata(pdev);

	DEBUG("start");

	ds1wm_up(ds1wm_data);

	DEBUG("finish");

	return 0;
}
#endif

static int
ds1wm_remove(struct platform_device *pdev)
{
	struct ds1wm_data *ds1wm_data = platform_get_drvdata(pdev);

	DEBUG("enter");
	w1_remove_master_device(&ds1wm_master);
	ds1wm_reset(ds1wm_data);
	ds1wm_down(ds1wm_data);
	clk_put(ds1wm_data->clk);
	free_irq(ds1wm_data->irq, ds1wm_data);
	iounmap(ds1wm_data->map);
	kfree(ds1wm_data);
	DEBUG("leave");

	return 0;
}

static struct platform_driver ds1wm_driver = {
	.driver   = {
		.name = "ds1wm",
	},
	.probe    = ds1wm_probe,
	.remove   = ds1wm_remove,
#ifdef CONFIG_PM
	.suspend  = ds1wm_suspend,
	.resume   = ds1wm_resume
#endif
};

static int
ds1wm_init(void)
{
	printk("DS1WM 1-wire busmaster driver - (c) 2004 Szabolcs Gyurko\n");
	return platform_driver_register(&ds1wm_driver);
}

static void
ds1wm_exit(void)
{
	platform_driver_unregister(&ds1wm_driver);
}

module_init(ds1wm_init);
module_exit(ds1wm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>, Matt Reimer <mreimer@vpop.net>");
MODULE_DESCRIPTION("1-Wire busmaster interface for DS1WM");
