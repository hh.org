/*
 * 1-Wire busmaster lowlevel interface for the SAMSUNG SAMCOP & HAMCOP ASIC
 *
 * Copyright (c) 2004-2005, Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * Author:  
 *          Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *          September 2004
 *
 *          Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *          January 2005 - W1 Implementation
 *
 *	    Matt Reimer <mreimer@vpop.net>
 *	    April 2005 - interrupt-driven implementation
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <asm/io.h>

#include <asm/arch/hardware.h>

#include <asm/hardware/samcop-owm.h>

#include "w1_samcop.h"
#include "../w1.h"
#include "../w1_int.h"

#define DEBUG(format, args...) if (samcop_owm_debug) printk(__FILE__ ":%s - " format "\n", __FUNCTION__, ## args)

#define W1_SAMCOP_TIMEOUT (HZ * 5)

int samcop_owm_debug = 0;

module_param(samcop_owm_debug, int, S_IRUGO);
MODULE_PARM_DESC(samcop_owm_debug, "If it's non-zero then the module will generate debug output");

static struct owm_freq freq[] = {
	{ 4000000, 0x8 },
	{ 5000000, 0x2 },
	{ 6000000, 0x5 },
	{ 7000000, 0x3 },
	{ 8000000, 0xc },
	{ 10000000, 0x6 },
	{ 12000000, 0x9 },
	{ 14000000, 0x7 },
	{ 16000000, 0x10 },
	{ 20000000, 0xa },
	{ 24000000, 0xd },
	{ 28000000, 0xb },
	{ 32000000, 0x14 },
	{ 40000000, 0xe },
	{ 48000000, 0x11 },
	{ 56000000, 0xf },
	{ 64000000, 0x18 },
	{ 80000000, 0x12 },
	{ 96000000, 0x15 },
	{ 112000000, 0x13 },
	{ 128000000, 0x1c },
};

static inline void
samcop_w1_write_register(struct samcop_w1_data *samcop_w1_data, u32 reg, u8 val)
{
        __raw_writeb(val, reg + samcop_w1_data->map);
}

static inline u8
samcop_w1_read_register(struct samcop_w1_data *samcop_w1_data, u32 reg)
{
        return __raw_readb(reg + samcop_w1_data->map);
}


static irqreturn_t
samcop_w1_isr(int isr, void *data, struct pt_regs *regs)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;
	u8 owintr = samcop_w1_read_register(samcop_w1_data,
					    _SAMCOP_OWM_Interrupt);

	samcop_w1_data->slave_present = owintr & OWM_INT_PDR ? 0 : 1;

	if (owintr & OWM_INT_PD && samcop_w1_data->reset_complete)
		complete(samcop_w1_data->reset_complete);

	if (owintr & OWM_INT_RBF) {
		samcop_w1_data->read_byte = samcop_w1_read_register(samcop_w1_data, _SAMCOP_OWM_Data);
		if (samcop_w1_data->read_complete)
			complete(samcop_w1_data->read_complete);
	}

	if (owintr & OWM_INT_TSRE && samcop_w1_data->write_complete)
		complete(samcop_w1_data->write_complete);

	return IRQ_HANDLED;
}

static int
samcop_owm_reset(struct samcop_w1_data *samcop_w1_data)
{
	DECLARE_COMPLETION(reset_done);

	DEBUG("enter");

	samcop_w1_data->reset_complete = &reset_done;

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_InterruptEnable,
				 OWM_INTEN_EPD);

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Command,
				 OWM_CMD_ONE_WIRE_RESET);

	wait_for_completion_timeout(&reset_done, W1_SAMCOP_TIMEOUT);
	samcop_w1_data->reset_complete = NULL;

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_InterruptEnable,
		OWM_INTEN_ERBF | OWM_INTEN_ETMT | OWM_INTEN_EPD);

	if (!samcop_w1_data->slave_present) {
                DEBUG("1-Wire Bus reset FAILED: no devices found");
                return 1;
        }

        DEBUG("1-Wire Bus reset successfully completed");

	DEBUG("leave");
        return 0;
}

static int
samcop_owm_write(struct samcop_w1_data *samcop_w1_data, u8 data)
{
	DECLARE_COMPLETION(write_done);
	samcop_w1_data->write_complete = &write_done;

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Data, data);

	wait_for_completion_timeout(&write_done, W1_SAMCOP_TIMEOUT);
	samcop_w1_data->write_complete = NULL;

	return 0;
}

static int
samcop_owm_read(struct samcop_w1_data *samcop_w1_data, unsigned char data)
{
	DECLARE_COMPLETION(read_done);
	samcop_w1_data->read_complete = &read_done;

	samcop_owm_write(samcop_w1_data, data);
	wait_for_completion_timeout(&read_done, W1_SAMCOP_TIMEOUT);
	samcop_w1_data->read_complete = NULL;

	return samcop_w1_data->read_byte;
}

static int
samcop_owm_find_divisor(int gclk)
{
	int i;

	for (i = 0; i < ARRAY_SIZE (freq); i++) {
		if (gclk < freq[i].freq)
			return freq[i].divisor;
	}
	
	printk (KERN_ERR "samcop_owm: no suitable divisor for %dHz clock\n", gclk);
	return 0;
}

static void
samcop_owm_down(struct samcop_w1_data *samcop_w1_data)
{
	DEBUG("enter");
	clk_disable(samcop_w1_data->clk);
	DEBUG("leave");
}

static void
samcop_owm_up(struct samcop_w1_data *samcop_w1_data)
{
	int gclk;
	
	DEBUG("enter");
	gclk = clk_get_rate(samcop_w1_data->clk);
	clk_enable(samcop_w1_data->clk);
	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_ClockDivisor,
				 samcop_owm_find_divisor(gclk));
	DEBUG("leave");
}

/* --------------------------------------------------------------------- */
/* w1 methods */

static u8
samcop_w1_read_bit(void *data)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;
	u8 value;

	DEBUG("enter");

	value = (samcop_w1_read_register(samcop_w1_data, _SAMCOP_OWM_Command) & OWM_CMD_DQ_INPUT) ? 1 : 0;

	DEBUG("value: %d", value);
	
	return value;
}

static void
samcop_w1_write_bit(void *data, u8 bit)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;

	DEBUG("enter");

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_InterruptEnable,
		OWM_INTEN_ERBF | OWM_INTEN_EPD | OWM_INTEN_DQO);
	if (bit)
		samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Command,
			samcop_w1_read_register(samcop_w1_data,
				_SAMCOP_OWM_Command) & ~OWM_CMD_DQ_OUTPUT);
	else
		samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Command,
			samcop_w1_read_register(samcop_w1_data,
				_SAMCOP_OWM_Command) | OWM_CMD_DQ_OUTPUT);

	DEBUG("leave");
}

static u8
samcop_w1_read_byte(void *data)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;
	int ret;

	DEBUG("enter");

	ret = samcop_owm_read(samcop_w1_data, 0xff);

	DEBUG("leave");

	return ret;
}

static void
samcop_w1_write_byte(void *data, u8 byte)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;

	DEBUG("enter");

	samcop_owm_write(samcop_w1_data, byte);

	DEBUG("leave");
}

static u8
samcop_w1_reset_bus(void *data)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;

	DEBUG("enter");

	samcop_owm_reset(samcop_w1_data);

	DEBUG("leave");
	
	return 0;
}

static void
samcop_w1_search(void *data, u8 search_type, w1_slave_found_callback slave_found)
{
	struct samcop_w1_data *samcop_w1_data = (struct samcop_w1_data *)data;
	int i;
	unsigned long long rom_id;

	DEBUG("enter");

	/* XXX We need to iterate for multiple devices per the DS1WM docs.
	 * See http://www.maxim-ic.com/appnotes.cfm/appnote_number/120. */
	if (samcop_owm_reset(samcop_w1_data))
		return;
	
	samcop_owm_write(samcop_w1_data, W1_SEARCH);
	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Command,
				 OWM_CMD_SRA);

	for (rom_id = 0, i = 0; i < 16; i++) {

		unsigned char resp, r, d;

		resp = samcop_owm_read(samcop_w1_data, 0x00);

		r = ((resp & 0x02) >> 1) |
		    ((resp & 0x08) >> 2) |
		    ((resp & 0x20) >> 3) |
		    ((resp & 0x80) >> 4);

		d = ((resp & 0x01) >> 0) |
		    ((resp & 0x04) >> 1) |
		    ((resp & 0x10) >> 2) |
		    ((resp & 0x40) >> 3);

		rom_id |= (unsigned long long) r << (i * 4);

	}
	DEBUG("found 0x%08llX", rom_id);

	samcop_w1_write_register(samcop_w1_data, _SAMCOP_OWM_Command,
				 ~OWM_CMD_SRA);
	samcop_owm_reset(samcop_w1_data);

	slave_found(samcop_w1_data, rom_id);
}

/* --------------------------------------------------------------------- */

static struct w1_bus_master samcop_w1_master = {
	.read_bit = &samcop_w1_read_bit,
	.write_bit = &samcop_w1_write_bit,
	.read_byte = &samcop_w1_read_byte,
	.write_byte = &samcop_w1_write_byte,
	.reset_bus = &samcop_w1_reset_bus,
	.search = &samcop_w1_search,
};

/* XXX Fix leaks on error. */
static int 
samcop_owm_probe(struct platform_device *pdev)
{
	struct samcop_w1_data *samcop_w1_data;
	struct resource *res;

	DEBUG("enter");
	if (!pdev)
		return -ENODEV;

	samcop_w1_data = kmalloc(sizeof (*samcop_w1_data), GFP_KERNEL);
	if (!samcop_w1_data)
		return -ENOMEM;
	memset(samcop_w1_data, 0, sizeof (*samcop_w1_data));

	platform_set_drvdata(pdev, samcop_w1_data);

	samcop_w1_data->parent = pdev->dev.parent;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	samcop_w1_data->map = ioremap(res->start, res->end - res->start + 1);
	samcop_w1_data->irq = platform_get_irq(pdev, 0);

	set_irq_type(samcop_w1_data->irq, IRQT_RISING);
	request_irq(samcop_w1_data->irq, samcop_w1_isr, SA_SAMPLE_RANDOM,
		    "1-wire", samcop_w1_data);

	samcop_w1_data->clk = clk_get(&pdev->dev, "w1");
	if (IS_ERR(samcop_w1_data->clk)) {
		printk(KERN_ERR "failed to get w1 clock\n");
		kfree(samcop_w1_data);
		return -EBUSY;
	}
	samcop_owm_up(samcop_w1_data);

	samcop_w1_master.data = (void *)samcop_w1_data;

	if (w1_add_master_device(&samcop_w1_master))
		samcop_owm_down(samcop_w1_data);

	DEBUG("leave");
	return 0;
}

#ifdef CONFIG_PM
static int 
samcop_owm_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct samcop_w1_data *samcop_w1_data = platform_get_drvdata(pdev);

	DEBUG("start");

	samcop_owm_down(samcop_w1_data);

	DEBUG("finish");

	return 0;
}

static int
samcop_owm_resume(struct platform_device *pdev)
{
	struct samcop_w1_data *samcop_w1_data = platform_get_drvdata(pdev);

	DEBUG("start");

	samcop_owm_up(samcop_w1_data);

	DEBUG("finish");

	return 0;
}
#endif

static int
samcop_owm_remove(struct platform_device *pdev)
{
	struct samcop_w1_data *samcop_w1_data = platform_get_drvdata(pdev);

	DEBUG("enter");
	w1_remove_master_device(&samcop_w1_master);
	samcop_owm_reset(samcop_w1_data);
	samcop_owm_down(samcop_w1_data);
	clk_put(samcop_w1_data->clk);
	free_irq(samcop_w1_data->irq, samcop_w1_data);
	iounmap(samcop_w1_data->map);
	kfree(samcop_w1_data);
	DEBUG("leave");

	return 0;
}

static struct platform_driver samcop_owm_driver = {
	.driver   = {
		.name = "samcop owm",
	},
	.probe    = samcop_owm_probe,
	.remove   = samcop_owm_remove,
#ifdef CONFIG_PM
	.suspend  = samcop_owm_suspend,
	.resume   = samcop_owm_resume
#endif
};

static int
samcop_owm_init(void)
{
	printk("SAMCOP 1-Wire busmaster driver - (c) 2004 Szabolcs Gyurko\n");
	return platform_driver_register(&samcop_owm_driver);
}

static void
samcop_owm_exit(void)
{
	platform_driver_unregister(&samcop_owm_driver);
}

module_init(samcop_owm_init);
module_exit(samcop_owm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>");
MODULE_DESCRIPTION("1-Wire busmaster interface for the SAMSUNG SAMCOP & HAMCOP ASIC");

/*
 * Local Variables:
 *  mode:c
 *  c-style:"K&R"
 *  c-basic-offset:8
 * End:
 *
 */
