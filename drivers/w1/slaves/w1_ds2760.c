/*
 * 1-Wire implementation for the ds2760 chip
 *
 * Copyright (c) 2004-2005, Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 */

#include <asm/types.h>

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/types.h>

#include "../w1.h"
#include "../w1_int.h"
#include "../w1_family.h"
#include "w1_ds2760.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szabolcs Gyurko <szabolcs.gyurko@tlt.hu>");
MODULE_DESCRIPTION("1-wire Driver Dallas 2760 battery monitor chip");

static int
w1_ds2760_io(struct device *dev, char *buf, int addr, size_t count, int io)
{
	struct w1_slave *sl = container_of(dev, struct w1_slave, dev);

	if (!dev)
		return 0;

	mutex_lock(&sl->master->mutex);

	if (addr > DS2760_DATA_SIZE || addr < 0) {
		count = 0;
		goto out;
	}
	if (addr + count > DS2760_DATA_SIZE)
		count = DS2760_DATA_SIZE - addr;

	if (!w1_reset_select_slave(sl)) {
		if (!io) {
			w1_write_8(sl->master, W1_DS2760_READ_DATA);
			w1_write_8(sl->master, addr);
			count = w1_read_block(sl->master, buf, count);
		} else {
			w1_write_8(sl->master, W1_DS2760_WRITE_DATA);
			w1_write_8(sl->master, addr);
			w1_write_block(sl->master, buf, count);
			/* XXX w1_write_block returns void, not n_written */
		}
	}

out:
	mutex_unlock(&sl->master->mutex);

	return count;
}

int
w1_ds2760_read(struct device *dev, char *buf, int addr, size_t count)
{
	return w1_ds2760_io(dev, buf, addr, count, 0);
}
EXPORT_SYMBOL(w1_ds2760_read);

int
w1_ds2760_write(struct device *dev, char *buf, int addr, size_t count)
{
	return w1_ds2760_io(dev, buf, addr, count, 1);
}
EXPORT_SYMBOL(w1_ds2760_write);


/* io = 0 means copy from EEPROM to SRAM, 1 means from SRAM to EEPROM */
static int
w1_ds2760_eeprom(struct device *dev, int addr, int io)
{
	struct w1_slave *sl = container_of(dev, struct w1_slave, dev);
	int ret = 0;

	mutex_lock(&sl->master->mutex);

	if (!w1_reset_select_slave(sl)) {
		if (!io)
		    w1_write_8(sl->master, W1_DS2760_RECALL_DATA);
		else
		    w1_write_8(sl->master, W1_DS2760_COPY_DATA);
		w1_write_8(sl->master, addr);
	}

	mutex_unlock(&sl->master->mutex);

	return ret;
}

int
w1_ds2760_recall(struct device *dev, int addr)
{
	return w1_ds2760_eeprom(dev, addr, 0);
}
EXPORT_SYMBOL(w1_ds2760_recall);

int
w1_ds2760_copy(struct device *dev, int addr)
{
	return w1_ds2760_eeprom(dev, addr, 1);
}
EXPORT_SYMBOL(w1_ds2760_copy);


static int
battery_interpolate(int array[], int temp)
{
	int index, dt;

	temp /= 8;
	if (temp <= 0)
		return array[0];
	if (temp >= 40)
		return array[4];

	index = temp / 10;
	dt    = temp % 10;

	return (array[index + 1] * dt + array[index] * (10 - dt)) / 10;
}


int
w1_ds2760_status(struct device *dev, struct ds2760_status *status)
{
	int ret, i, empty[5];

	ret = w1_ds2760_read(dev, status->raw, 0, DS2760_DATA_SIZE);
	if (ret != DS2760_DATA_SIZE)
		return 0;

	status->update_time = jiffies;

        /* DS2760 reports voltage in units of 4.88mV, but the battery class
	 * reports in units of mV, so convert by multiplying by 4.875.
	 * We approximate because integer math is cheap, and close enough. */
	status->voltage_raw = (status->raw[DS2760_VOLTAGE_MSB] << 3) |
			      (status->raw[DS2760_VOLTAGE_LSB] >> 5);
	status->voltage_mV = (status->voltage_raw * 5) -
			     (status->voltage_raw / 8);

	/* DS2760 reports current in signed units of 0.625mA, but the battery
	 * class reports in units of mA, so convert by multiplying by 0.625. */
	status->current_raw =
	    (((signed char)status->raw[DS2760_CURRENT_MSB]) << 5) |
			  (status->raw[DS2760_CURRENT_LSB] >> 3);
	status->current_mA = (status->current_raw / 2) +
			     (status->current_raw / 8);

	/* DS2760 reports accumulated current in signed units of 0.25mAh, but
	 * the battery class reports charge in mJ, so we pretend that
	 * 1 mAh = 1 mJ. */ 
	status->accum_current_raw =
	    (((signed char)status->raw[DS2760_CURRENT_ACCUM_MSB]) << 8) |
			   status->raw[DS2760_CURRENT_ACCUM_LSB];
	status->accum_current_mAh = status->accum_current_raw / 4;

	/* DS2760 reports temperature in signed units of 0.125C, but the
	 * battery class reports in units of 1/10 C, so we convert by
	 * multiplying by .125 * 10 = 1.25. */
	status->temp_raw = (((signed char)status->raw[DS2760_TEMP_MSB]) << 3) |
					 (status->raw[DS2760_TEMP_LSB] >> 5);
	status->temp_C = status->temp_raw + (status->temp_raw / 4);

	/* At least some battery monitors (e.g. HP iPAQ) store the battery's
	 * maximum rated capacity. */
	status->rated_capacity = status->raw[DS2760_RATED_CAPACITY] * 10;

	/* Interpolate the empty level and estimate battery life. */
	empty[4] = status->raw[DS2760_ACTIVE_EMPTY + 4];
	for (i = 3; i >= 0; i--)
		empty[i] = empty[i + 1] + status->raw[DS2760_ACTIVE_EMPTY + i];

	status->empty_mAh = battery_interpolate(empty, status->temp_C);

	if (status->current_mA)
		status->life_min = - (60 * (status->accum_current_mAh -
					    status->empty_mAh)) /
					    status->current_mA;
	else
		status->life_min = 0;

	return 1;
}
EXPORT_SYMBOL(w1_ds2760_status);


static ssize_t
w1_ds2760_read_bin(struct kobject *kobj, char *buf, loff_t off, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	return w1_ds2760_read(dev, buf, off, count);
}

static struct bin_attribute w1_ds2760_bin_attr = {
	.attr = {
		.name = "w1_slave",
		.mode = S_IRUGO,
		.owner = THIS_MODULE,
	},
	.size = DS2760_DATA_SIZE,
	.read = w1_ds2760_read_bin,
};

static int
w1_ds2760_add_slave(struct w1_slave *sl)
{
	return sysfs_create_bin_file(&sl->dev.kobj, &w1_ds2760_bin_attr);
}

static void
w1_ds2760_remove_slave(struct w1_slave *sl)
{
	sysfs_remove_bin_file(&sl->dev.kobj, &w1_ds2760_bin_attr);
}

static struct w1_family_ops w1_ds2760_fops = {
	.add_slave    = w1_ds2760_add_slave,
	.remove_slave = w1_ds2760_remove_slave,
};

static struct w1_family w1_ds2760_family = {
	.fid = W1_FAMILY_DS2760,
	.fops = &w1_ds2760_fops,
};

static int __init
w1_ds2760_init(void)
{
	printk("1-Wire driver for the DS2760 battery monitor chip - (c) 2004-2005, Szabolcs Gyurko\n");

	return w1_register_family(&w1_ds2760_family);
}

static void __exit
w1_ds2760_exit(void)
{
	w1_unregister_family(&w1_ds2760_family);
}

module_init(w1_ds2760_init);
module_exit(w1_ds2760_exit);
