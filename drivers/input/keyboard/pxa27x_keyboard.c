/*
 * linux/arch/arm/mach-pxa/pxa27x_keyboard.c
 *
 * Driver for the pxa27x matrix keyboard controller.
 *
 * Based on code by: Alex Osborne <bobofdoom@gmail.com>
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/device.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/irqs.h>
#include <asm/arch/pxa27x_keyboard.h>

#define DRIVER_NAME	"pxa27x-keyboard"

#define KPASMKP(col)		(col/2==0 ? KPASMKP0 : col/2==1 ? KPASMKP1 : col/2==2 ? KPASMKP2 : KPASMKP3)
#define KPASMKPx_MKC(row, col)	(1 << (row + 16*(col%2)))

static irqreturn_t
pxakbd_irq_handler(int irq, void *dev_id)
{
	struct device *dev = dev_id;
	struct pxa27x_keyboard_platform_data *pdev = dev->platform_data;
	struct input_dev *button_dev = dev->driver_data;
	int mi;
	int row, col;

	/*
	 * notify controller that interrupt was handled, otherwise it'll
	 * constantly send interrupts and lock up the device.
	 */
	mi = KPC & KPC_MI;

	/* report the status of every button */
	for (row=0; row < pdev->nr_rows; row++) {
		for (col=0; col < pdev->nr_cols; col++) {
			int pressed = KPASMKP(col) & KPASMKPx_MKC(row, col);
			input_report_key(button_dev
					 , pdev->keycodes[row][col]
					 , pressed);
		}
	}
	input_sync(button_dev);

	return IRQ_HANDLED;
}

static int pxakbd_probe(struct device *dev)
{
	struct input_dev *button_dev;
	struct pxa27x_keyboard_platform_data *pdev = dev->platform_data;
	int row, col, i, err;

	// Create and register the input driver.
	dev->driver_data = button_dev = input_allocate_device();
	set_bit(EV_KEY, button_dev->evbit);
	set_bit(EV_REP, button_dev->evbit);
	for (row=0; row < pdev->nr_rows; row++) {
		for (col=0; col < pdev->nr_cols; col++) {
			int code = pdev->keycodes[row][col];
			if (code <= 0)
				continue;
			set_bit(code, button_dev->keybit);
		}
	}
	button_dev->name = DRIVER_NAME;
	button_dev->id.bustype = BUS_HOST;
	input_register_device(button_dev);

	//
	// Enable keyboard.
	//

	pxa_set_cken(CKEN19_KEYPAD, 1);

	// Setup GPIOs.
	for (i=0; i < pdev->nr_rows + pdev->nr_cols; i++)
		pxa_gpio_mode(pdev->gpio_modes[i]);

	KPC |= pdev->nr_rows << 26;
	KPC |= pdev->nr_cols << 23;

	KPC |= KPC_ASACT;	/* enable automatic scan on activity */
	KPC &= ~KPC_AS; 	/* disable automatic scan */
	KPC &= ~KPC_IMKP;	/* do not ignore multiple keypresses */

        KPC &= ~KPC_DE;		/* disable direct keypad */
        KPC &= ~KPC_DIE;	/* disable direct keypad interrupt */

        err = request_irq(IRQ_KEYPAD, pxakbd_irq_handler, SA_INTERRUPT,
			  DRIVER_NAME, dev);
	if (err) {
		printk(KERN_ERR "Cannot request keypad IRQ\n");
		input_unregister_device(button_dev);
		pxa_set_cken(CKEN19_KEYPAD, 0);
		return -1;
	}

       	KPC |= KPC_ME;		/* matrix keypad enabled */
	KPC |= KPC_MIE;		/* matrix keypad interrupt enabled */

	return 0;
}

static int pxakbd_remove(struct device *dev)
{
	struct input_dev *button_dev = dev->driver_data;
	input_unregister_device(button_dev);
	free_irq(IRQ_KEYPAD, NULL);
	pxa_set_cken(CKEN19_KEYPAD, 0);
	return 0;
}

static struct device_driver pxakbd_driver = {
	.name   = DRIVER_NAME,
	.bus    = &platform_bus_type,
	.probe  = pxakbd_probe,
	.remove = pxakbd_remove,
};

static int __init pxakbd_init(void)
{
	return driver_register(&pxakbd_driver);
}

static void __exit pxakbd_exit(void)
{
	driver_unregister(&pxakbd_driver);
}

module_init(pxakbd_init);
module_exit(pxakbd_exit);

MODULE_DESCRIPTION("PXA27x Matrix Keyboard Driver");
MODULE_LICENSE("GPL");
