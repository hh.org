/*
 * Driver for keys on GPIO lines capable of generating interrupts.
 *
 * Copyright 2005 Phil Blundell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/version.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/irq.h>

#include <asm/arch/hardware.h>

#ifdef CONFIG_ARCH_SA1100
#include <asm/arch/SA-1100.h>
#endif 

#ifdef CONFIG_ARCH_PXA
#include <asm/arch/pxa-regs.h>
#endif 

#ifdef CONFIG_ARCH_S3C2410
#include <asm/arch/regs-gpio.h>
#endif 

#include <asm/hardware/gpio_keys.h>

static irqreturn_t
gpio_keys_isr(int irq, void *dev_id)
{
	int i;
	struct gpio_keys_platform_data *pdata = dev_id;

	for (i = 0; i < pdata->nbuttons; i++) {
		int gpio = pdata->buttons[i].gpio;
		if (irq == IRQ_GPIO(gpio)) {
			int state;
#if defined(CONFIG_ARCH_SA1100)
			state = GPLR & GPIO_GPIO(gpio);
#elif defined(CONFIG_ARCH_PXA)
			state = GPLR(gpio) & GPIO_bit(gpio);
#elif defined(CONFIG_ARCH_S3C2410)
			state = s3c2410_gpio_getpin(gpio);
#endif
			state = !!state ^ pdata->buttons[i].active_low;
			if (state != pdata->buttons[i].down) {
				input_report_key(pdata->input, pdata->buttons[i].keycode, state);
				input_sync(pdata->input);
				pdata->buttons[i].down = state;
			}
		}
	}

	return IRQ_HANDLED;
}

static int __devinit
gpio_keys_probe(struct platform_device *pdev)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	int i;

	pdata->input = input_allocate_device();
	pdata->input->evbit[0] = BIT(EV_KEY);

	for (i = 0; i < pdata->nbuttons; i++) {
		int code = pdata->buttons[i].keycode;
		int irq = IRQ_GPIO(pdata->buttons[i].gpio);
		int result;

		set_irq_type(irq, IRQ_TYPE_EDGE_BOTH);
		result = request_irq(irq, gpio_keys_isr, SA_SAMPLE_RANDOM,
				     pdata->buttons[i].desc ? pdata->buttons[i].desc : "gpio_keys", 
				     pdata);
		if (result == 0)
			set_bit(code, pdata->input->keybit);
		else
			printk("gpio-keys: unable to claim irq %d; error %d\n", irq, result);
	}

	pdata->input->name = pdev->name;
	pdata->input->private = pdata;

	input_register_device(pdata->input);

	return 0;
}

static int
gpio_keys_remove(struct platform_device *pdev)
{
	struct gpio_keys_platform_data *pdata = pdev->dev.platform_data;
	int i;
	
	for (i = 0; i < pdata->nbuttons; i++) {
		int irq = IRQ_GPIO(pdata->buttons[i].gpio);
		free_irq(irq, pdata);
	}

	input_unregister_device(pdata->input);

	return 0;
}

struct platform_driver gpio_keys_device_driver = {
	.probe    = gpio_keys_probe,
	.remove   = gpio_keys_remove,
	.driver = {
		.name     = "gpio-keys",
	}
};

static int __devinit
gpio_keys_init(void)
{
	return platform_driver_register(&gpio_keys_device_driver);
}

static void __devexit
gpio_keys_cleanup(void)
{
	platform_driver_unregister(&gpio_keys_device_driver);
}

module_init(gpio_keys_init);
module_exit(gpio_keys_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org>");
MODULE_DESCRIPTION("Keyboard driver for CPU GPIOs");
