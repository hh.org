/*
 * Driver interface to HTC "ASIC3"
 * 
 * Copyright 2001 Compaq Computer Corporation.
 * Copyright 2004-2005 Phil Blundell
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author:  Andrew Christian
 *          <Andrew.Christian@compaq.com>
 *          October 2001
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/platform_device.h>
#include <linux/soc-old.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/irq.h>

#include <asm/hardware.h>
#include <asm/irq.h>
#include <asm/io.h>

#include <asm/mach/irq.h>

#include <asm/hardware/ipaq-asic3.h>

#include <linux/soc/asic3_base.h>
#include <linux/soc/tmio_mmc.h>

struct asic3_data
{
	void *mapping;
	unsigned long bus_shift;
	int irq_base;
	int irq_nr;

	struct platform_device *mmc_dev;
};

static spinlock_t asic3_gpio_lock;

static int asic3_remove(struct platform_device *dev);

static inline unsigned long
asic3_address (struct device *dev, unsigned int reg)
{
	struct asic3_data *adata;

	adata = (struct asic3_data *)dev->driver_data;

	return (unsigned long)adata->mapping + (reg >> (2 - adata->bus_shift));
}

void
asic3_write_register (struct device *dev, unsigned int reg, unsigned long value)
{
	__raw_writew (value, asic3_address (dev, reg));
}
EXPORT_SYMBOL(asic3_write_register);

unsigned long
asic3_read_register (struct device *dev, unsigned int reg)
{
	return __raw_readw (asic3_address (dev, reg));
}
EXPORT_SYMBOL(asic3_read_register);

static inline void
__asic3_write_register (struct asic3_data *asic, unsigned int reg, unsigned long value)
{
	__raw_writew (value, (unsigned long)asic->mapping + (reg >> (2 - asic->bus_shift)));
}

static inline unsigned long
__asic3_read_register (struct asic3_data *asic, unsigned int reg)
{
	return __raw_readw ((unsigned long)asic->mapping + (reg >> (2 - asic->bus_shift)));
}

#define ASIC3_GPIO_FN(get_fn_name, set_fn_name, REG)			\
u32                                                                     \
get_fn_name (struct device *dev)					\
{                                                                       \
	return asic3_read_register (dev, REG);				\
}                                                                       \
EXPORT_SYMBOL( get_fn_name );						\
                                                                        \
void                                                                    \
set_fn_name (struct device *dev, u32 bits, u32 val)                     \
{                                                                       \
	unsigned long flags;						\
									\
	spin_lock_irqsave (&asic3_gpio_lock, flags);			\
	val |= (asic3_read_register (dev, REG) & ~bits);		\
	asic3_write_register (dev, REG, val);				\
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);		\
}                                                                       \
EXPORT_SYMBOL(set_fn_name);

#define ASIC3_GPIO_REGISTER(ACTION, action, fn, FN)			\
	ASIC3_GPIO_FN (asic3_get_gpio_ ## action ## _ ## fn ,	\
		       asic3_set_gpio_ ## action ## _ ## fn ,	\
	               _IPAQ_ASIC3_GPIO_ ## FN ## _Base + _IPAQ_ASIC3_GPIO_ ## ACTION )

#define ASIC3_GPIO_FUNCTIONS(fn, FN)					\
	ASIC3_GPIO_REGISTER (Direction, dir, fn, FN)			\
	ASIC3_GPIO_REGISTER (Out, out, fn, FN)				\
	ASIC3_GPIO_REGISTER (SleepMask, sleepmask, fn, FN)		\
	ASIC3_GPIO_REGISTER (SleepOut, sleepout, fn, FN)		\
	ASIC3_GPIO_REGISTER (BattFaultOut, battfaultout, fn, FN)	\
	ASIC3_GPIO_REGISTER (AltFunction, alt_fn, fn, FN)		\
	ASIC3_GPIO_REGISTER (SleepConf, sleepconf, fn, FN)		\
	ASIC3_GPIO_REGISTER (Status, status, fn, FN)

#if 0
	ASIC3_GPIO_REGISTER (Mask, mask, fn, FN)                        
	ASIC3_GPIO_REGISTER (TriggerType, trigtype, fn, FN)             
	ASIC3_GPIO_REGISTER (EdgeTrigger, rising, fn, FN)               
	ASIC3_GPIO_REGISTER (LevelTrigger, triglevel, fn, FN)           
	ASIC3_GPIO_REGISTER (IntStatus, intstatus, fn, FN)              
#endif

ASIC3_GPIO_FUNCTIONS(a, A)
ASIC3_GPIO_FUNCTIONS(b, B)
ASIC3_GPIO_FUNCTIONS(c, C)
ASIC3_GPIO_FUNCTIONS(d, D)

int asic3_get_gpio_bit(struct device *dev, int gpio)
{
	u32 mask = ASIC3_GPIO_bit(gpio);
	switch (gpio >> 4) {
		case _IPAQ_ASIC3_GPIO_BANK_A:
			return asic3_get_gpio_status_a(dev) & mask;
		case _IPAQ_ASIC3_GPIO_BANK_B:
			return asic3_get_gpio_status_b(dev) & mask;
		case _IPAQ_ASIC3_GPIO_BANK_C:
			return asic3_get_gpio_status_c(dev) & mask;
		case _IPAQ_ASIC3_GPIO_BANK_D:
			return asic3_get_gpio_status_d(dev) & mask;
	}

	printk("%s: invalid GPIO value 0x%x", __FUNCTION__, gpio);
	return 0;
}
EXPORT_SYMBOL(asic3_get_gpio_bit);

void asic3_set_gpio_bit(struct device *dev, int gpio, int val)
{
	u32 mask = ASIC3_GPIO_bit(gpio);
	u32 bitval = 0;
	if (val)  bitval = mask;

	switch (gpio >> 4) {
		case _IPAQ_ASIC3_GPIO_BANK_A:
			asic3_set_gpio_out_a(dev, mask, bitval);
			return;
		case _IPAQ_ASIC3_GPIO_BANK_B:
			asic3_set_gpio_out_b(dev, mask, bitval);
			return;
		case _IPAQ_ASIC3_GPIO_BANK_C:
			asic3_set_gpio_out_c(dev, mask, bitval);
			return;
		case _IPAQ_ASIC3_GPIO_BANK_D:
			asic3_set_gpio_out_d(dev, mask, bitval);
			return;
		default:
			printk("%s: invalid GPIO value 0x%x", __FUNCTION__, gpio);
	}
}
EXPORT_SYMBOL(asic3_set_gpio_bit);

int
asic3_irq_base (struct device *dev)
{
	struct asic3_data *asic = dev->driver_data;

	return asic->irq_base;
}

EXPORT_SYMBOL(asic3_irq_base);

void 
asic3_set_led (struct device *dev, int led_num, int duty_time, int cycle_time)
{
	struct asic3_data *asic = dev->driver_data;
	unsigned int led_base;

	/* it's a macro thing: see #define _IPAQ_ASIC_LED_0_Base for why you
	 * can't substitute led_num in the macros below...
	 */
	
	switch (led_num) {
	case 0:
		led_base = _IPAQ_ASIC3_LED_0_Base;
		break;
 	case 1:
		led_base = _IPAQ_ASIC3_LED_1_Base;
		break;
 	case 2:
		led_base = _IPAQ_ASIC3_LED_2_Base;
		break;
	default:
		printk ("%s: invalid led number %d", __FUNCTION__, led_num);
		return;
	}
		
	__asic3_write_register (asic, led_base + _IPAQ_ASIC3_LED_TimeBase, (0x6 | LED_EN));
	__asic3_write_register (asic, led_base + _IPAQ_ASIC3_LED_PeriodTime, cycle_time);
	__asic3_write_register (asic, led_base + _IPAQ_ASIC3_LED_DutyTime, 0);
	udelay(20);     // asic voodoo - possibly need a whole duty cycle?
	__asic3_write_register (asic, led_base + _IPAQ_ASIC3_LED_DutyTime, duty_time);
}

EXPORT_SYMBOL(asic3_set_led);

void
asic3_set_clock_sel (struct device *dev, unsigned long bits, unsigned long val)
{
	struct asic3_data *asic = dev->driver_data;
	unsigned long flags, v;

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	v = __asic3_read_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_SEL);
	v = (v & ~bits) | val;
	__asic3_write_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_SEL, v);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}
EXPORT_SYMBOL (asic3_set_clock_sel);

void
asic3_set_clock_cdex (struct device *dev, unsigned long bits, unsigned long val)
{
	struct asic3_data *asic = dev->driver_data;
	unsigned long flags, v;

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	v = __asic3_read_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_CDEX);
	v = (v & ~bits) | val;
	__asic3_write_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_CDEX, v);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}
EXPORT_SYMBOL (asic3_set_clock_cdex);

void
asic3_set_extcf_select (struct device *dev, unsigned long bits, unsigned long val)
{
	struct asic3_data *asic = dev->driver_data;
	unsigned long flags, v;

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	v = __asic3_read_register (asic, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Select);
	v = (v & ~bits) | val;
	__asic3_write_register (asic, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Select, v);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}
EXPORT_SYMBOL (asic3_set_extcf_select);

void
asic3_set_extcf_reset (struct device *dev, unsigned long bits, unsigned long val)
{
	struct asic3_data *asic = dev->driver_data;
	unsigned long flags, v;

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	v = __asic3_read_register (asic, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Reset);
	v = (v & ~bits) | val;
	__asic3_write_register (asic, _IPAQ_ASIC3_EXTCF_Base + _IPAQ_ASIC3_EXTCF_Reset, v);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}
EXPORT_SYMBOL (asic3_set_extcf_reset);

void
asic3_set_sdhwctrl (struct device *dev, unsigned long bits, unsigned long val)
{
        struct asic3_data *asic = dev->driver_data;
        unsigned long flags, v;

        spin_lock_irqsave (&asic3_gpio_lock, flags);
        v = __asic3_read_register (asic, _IPAQ_ASIC3_SDHWCTRL_Base + _IPAQ_ASIC3_SDHWCTRL_SDConf);
        v = (v & ~bits) | val;
        __asic3_write_register (asic, _IPAQ_ASIC3_SDHWCTRL_Base + _IPAQ_ASIC3_SDHWCTRL_SDConf, v);
        spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}
EXPORT_SYMBOL (asic3_set_sdhwctrl);


#define MAX_ASIC_ISR_LOOPS    20

static void
asic3_irq_demux (unsigned int irq, struct irqdesc *desc)
{
	int i;
	struct asic3_data *asic;

	/* Acknowledge the parrent (i.e. CPU's) IRQ */
	desc->chip->ack(irq);

	asic = desc->handler_data;

	// printk( KERN_NOTICE "asic3_irq_demux: irq=%d\n", irq );
	for (i = 0 ; i < MAX_ASIC_ISR_LOOPS; i++) {
		u32 status;
		int bank;

		status = __asic3_read_register (asic, _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_PIntStat);
		/* Check all ten register bits */
		if ((status & 0x3ff) == 0)
			break;

		for (bank = 0; bank < 4; bank++) {
			if (status & (1 << bank)) {
				unsigned long base, i, istat;

				base = _IPAQ_ASIC3_GPIO_A_Base + (bank * (_IPAQ_ASIC3_GPIO_B_Base - _IPAQ_ASIC3_GPIO_A_Base));
				istat = __asic3_read_register (asic, base + _IPAQ_ASIC3_GPIO_IntStatus);
				/* IntStatus is write 0 to clear */
				/* XXX could miss interrupts! */
				__asic3_write_register (asic, base + _IPAQ_ASIC3_GPIO_IntStatus, 0);

				for (i = 0; i < 16; i++) {
					if (istat & (1 << i)) {
						unsigned int irqnr;

						irqnr = asic->irq_base + (16 * bank) + i;

						desc = irq_desc + irqnr;
						desc->handle_irq (irqnr, desc);
					}
				}
			}
		}

		/* Handle remaining IRQs in the status register */
		{
			int i;

			for (i = ASIC3_LED0_IRQ; i <= ASIC3_OWM_IRQ; i++) {
				/* They start at bit 4 and go up */
				if (status & (1 << (i - ASIC3_LED0_IRQ + 4))) {
					desc = irq_desc + asic->irq_base + i;
					desc->handle_irq (asic->irq_base + i, desc);
				}
			}
		}

	}

	if (i >= MAX_ASIC_ISR_LOOPS)
		printk("%s: interrupt processing overrun\n", __FUNCTION__);
}

static inline int
asic3_irq_to_bank (struct asic3_data *asic, int irq)
{
	int n;

	n = (irq - asic->irq_base) >> 4;

	return (n * (_IPAQ_ASIC3_GPIO_B_Base - _IPAQ_ASIC3_GPIO_A_Base));
}

static inline int
asic3_irq_to_index (struct asic3_data *asic, int irq)
{
	return (irq - asic->irq_base) & 15;
}

static void 
asic3_mask_gpio_irq (unsigned int irq)
{
	struct asic3_data *asic = get_irq_chipdata (irq);
	u32 val, bank, index;
	unsigned long flags;

	bank = asic3_irq_to_bank (asic, irq);
	index = asic3_irq_to_index (asic, irq);

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	val = __asic3_read_register (asic, bank + _IPAQ_ASIC3_GPIO_Mask);
	val |= 1 << index;
	__asic3_write_register (asic, bank + _IPAQ_ASIC3_GPIO_Mask, val);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}

static void
asic3_mask_irq (unsigned int irq)
{
	struct asic3_data *asic = get_irq_chipdata (irq);
	int regval;

	if(irq < ASIC3_NR_GPIO_IRQS) {
		printk(KERN_ERR "asic3_base: gpio mask attempt, irq %d\n", irq);
		return;
	}

	regval = __asic3_read_register(asic,
		_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask);

	switch(irq - asic->irq_base) {
		case ASIC3_LED0_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK0);
			break;
		case ASIC3_LED1_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK1);
			break;
		case ASIC3_LED2_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK2);
			break;
		case ASIC3_SPI_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK3);
			break;
		case ASIC3_SMBUS_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK4);
			break;
		case ASIC3_OWM_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval & ~ASIC3_INTMASK_MASK5);
			break;
		default:
			printk(KERN_ERR "asic3_base: bad non-gpio irq %d\n", irq);
			break;
	}
}

static void 
asic3_unmask_gpio_irq (unsigned int irq)
{
	struct asic3_data *asic = get_irq_chipdata (irq);
	u32 val, bank, index;
	unsigned long flags;

	bank = asic3_irq_to_bank (asic, irq);
	index = asic3_irq_to_index (asic, irq);

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	val = __asic3_read_register (asic, bank + _IPAQ_ASIC3_GPIO_Mask);
	val &= ~(1 << index);
	__asic3_write_register (asic, bank + _IPAQ_ASIC3_GPIO_Mask, val);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
}

static void
asic3_unmask_irq (unsigned int irq)
{
	struct asic3_data *asic = get_irq_chipdata (irq);
	int regval;

	if(irq < ASIC3_NR_GPIO_IRQS) {
		printk(KERN_ERR "asic3_base: gpio unmask attempt, irq %d\n", irq);
		return;
	}

	regval = __asic3_read_register(asic,
		_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask);

	switch(irq - asic->irq_base) {
		case ASIC3_LED0_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK0);
			break;
		case ASIC3_LED1_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK1);
			break;
		case ASIC3_LED2_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK2);
			break;
		case ASIC3_SPI_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK3);
			break;
		case ASIC3_SMBUS_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK4);
			break;
		case ASIC3_OWM_IRQ:
			__asic3_write_register (asic,
				_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
				regval | ASIC3_INTMASK_MASK5);
			break;
		default:
			printk(KERN_ERR "asic3_base: bad non-gpio irq %d\n", irq);
			break;
	}
}

static int
asic3_gpio_irq_type(unsigned int irq, unsigned int type)
{
	struct asic3_data *asic = get_irq_chipdata (irq);
	u32 bank, index;
	unsigned long flags;
	u16 trigger, level, edge, bit;

	bank = asic3_irq_to_bank (asic, irq);
	index = asic3_irq_to_index (asic, irq);
	bit = 1<<index;

	spin_lock_irqsave (&asic3_gpio_lock, flags);
	level = __asic3_read_register (asic, bank + _IPAQ_ASIC3_GPIO_LevelTrigger);
	edge = __asic3_read_register (asic, bank + _IPAQ_ASIC3_GPIO_EdgeTrigger);
	trigger = __asic3_read_register (asic, bank + _IPAQ_ASIC3_GPIO_TriggerType);
	if (type == IRQT_RISING) {
	    trigger |= bit;
	    edge |= bit;
	}
	else if (type == IRQT_FALLING) {
	    trigger |= bit;
	    edge &= ~bit;
	}
	else if (type == IRQT_LOW) {
	    trigger &= ~bit;
	    level &= ~bit;
	}
	else if (type == IRQT_HIGH) {
	    trigger &= ~bit;
	    level |= bit;
	}
	else {
		/*
		 * if type == IRQT_NOEDGE, we should mask interrupts, but
		 * be careful to not unmask them if mask was also called.
		 * Probably need internal state for mask.
		 */
		printk( KERN_NOTICE "asic3: irq type not changed.\n" );
	}
	__asic3_write_register (asic, bank + _IPAQ_ASIC3_GPIO_LevelTrigger, level);
	__asic3_write_register (asic, bank + _IPAQ_ASIC3_GPIO_EdgeTrigger, edge);
	__asic3_write_register (asic, bank + _IPAQ_ASIC3_GPIO_TriggerType, trigger);
	spin_unlock_irqrestore (&asic3_gpio_lock, flags);
	return 0;
}

static struct irqchip asic3_gpio_irq_chip = {
	.name		= "ASIC3-GPIO",
	.ack		= asic3_mask_gpio_irq,
	.mask		= asic3_mask_gpio_irq,
	.unmask		= asic3_unmask_gpio_irq,
	.set_type	= asic3_gpio_irq_type,
};

static struct irqchip asic3_irq_chip = {
	.name		= "ASIC3",
	.ack		= asic3_mask_irq,
	.mask		= asic3_mask_irq,
	.unmask		= asic3_unmask_irq,
};

static void
asic3_release (struct device *dev)
{
	struct platform_device *sdev = to_platform_device (dev);

	kfree (sdev->resource);
	kfree (sdev);
}

int
asic3_register_mmc (struct device *dev)
{
	struct platform_device *sdev = kmalloc (sizeof (*sdev), GFP_KERNEL);
	struct tmio_mmc_hwconfig *mmc_config = kmalloc (sizeof (*mmc_config), GFP_KERNEL);
	struct platform_device *pdev = to_platform_device (dev);
	struct asic3_data *asic = dev->driver_data;
	struct resource *res;
	int rc;

	if (sdev == NULL)
		return -ENOMEM;

	memset (sdev, 0, sizeof (*sdev));
	memset (mmc_config, 0, sizeof (*mmc_config));
	mmc_config->address_shift = asic->bus_shift;

	//sdev->id = TMIO_MMC_DEVICE_ID;
	sdev->name = "asic3_mmc";
	sdev->dev.parent = dev;
	sdev->num_resources = 2;
	sdev->dev.platform_data = mmc_config;
	sdev->dev.release = asic3_release;

	res = kmalloc (sdev->num_resources * sizeof (struct resource), GFP_KERNEL);
	if (res == NULL) {
		kfree (sdev);
		return -ENOMEM;
	}
	sdev->resource = res;
	memset (res, 0, sdev->num_resources * sizeof (struct resource));

	res[0].start = pdev->resource[2].start;
	res[0].end   = pdev->resource[2].end;
	res[0].flags = IORESOURCE_MEM;
	res[1].start = res[1].end = pdev->resource[3].start;
	res[1].flags = IORESOURCE_IRQ;
	
	rc = platform_device_register (sdev); 
	if (rc) {
	        printk("asic3_base: Could not register virtual asic3_mmc device\n");
		kfree (res);
		kfree (sdev);
		return rc;
	}

	asic->mmc_dev = sdev;

	return 0;
}
EXPORT_SYMBOL(asic3_register_mmc);

int
asic3_unregister_mmc (struct device *dev)
{
	struct asic3_data *asic = dev->driver_data;
	platform_device_unregister (asic->mmc_dev);
	asic->mmc_dev = 0;

	return 0;
}
EXPORT_SYMBOL(asic3_unregister_mmc);

static int 
asic3_probe(struct platform_device *pdev)
{
	struct asic3_platform_data *pdata = pdev->dev.platform_data;
	struct asic3_data *asic;
	struct device *dev = &pdev->dev;
	unsigned long clksel;

	asic = kmalloc (sizeof (struct asic3_data), GFP_KERNEL);
	if (!asic)
		return -ENOMEM;
	memset (asic, 0, sizeof (*asic));

	spin_lock_init (&asic3_gpio_lock);
	platform_set_drvdata(pdev, asic);

	asic->mapping = ioremap ((unsigned long)pdev->resource[0].start, IPAQ_ASIC3_MAP_SIZE);
	if (!asic->mapping) {
		printk ("asic3: couldn't ioremap\n");
		kfree (asic);
		return -ENOMEM;
	}

	if (pdata && pdata->bus_shift)
		asic->bus_shift = pdata->bus_shift;
	else
		asic->bus_shift = 2;

	/* XXX: should get correct SD clock values from pdata struct  */
	clksel = 0;
	__asic3_write_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_SEL, clksel);

	__asic3_write_register (asic, _IPAQ_ASIC3_GPIO_A_Base + _IPAQ_ASIC3_GPIO_Mask, 0xffff);
	__asic3_write_register (asic, _IPAQ_ASIC3_GPIO_B_Base + _IPAQ_ASIC3_GPIO_Mask, 0xffff);
	__asic3_write_register (asic, _IPAQ_ASIC3_GPIO_C_Base + _IPAQ_ASIC3_GPIO_Mask, 0xffff);
	__asic3_write_register (asic, _IPAQ_ASIC3_GPIO_D_Base + _IPAQ_ASIC3_GPIO_Mask, 0xffff);

	asic3_set_gpio_sleepmask_a (dev, 0xffff, 0xffff);
	asic3_set_gpio_sleepmask_b (dev, 0xffff, 0xffff);
	asic3_set_gpio_sleepmask_c (dev, 0xffff, 0xffff);
	asic3_set_gpio_sleepmask_d (dev, 0xffff, 0xffff);

	if (pdata) {
		asic3_set_gpio_out_a(dev, 0xffff, pdata->gpio_a.init);
		asic3_set_gpio_out_b(dev, 0xffff, pdata->gpio_b.init);
		asic3_set_gpio_out_c(dev, 0xffff, pdata->gpio_c.init);
		asic3_set_gpio_out_d(dev, 0xffff, pdata->gpio_d.init);

		asic3_set_gpio_dir_a(dev, 0xffff, pdata->gpio_a.dir);
		asic3_set_gpio_dir_b(dev, 0xffff, pdata->gpio_b.dir);
		asic3_set_gpio_dir_c(dev, 0xffff, pdata->gpio_c.dir);
		asic3_set_gpio_dir_d(dev, 0xffff, pdata->gpio_d.dir);

		asic3_set_gpio_sleepmask_a(dev, 0xffff, pdata->gpio_a.sleep_mask);
		asic3_set_gpio_sleepmask_b(dev, 0xffff, pdata->gpio_b.sleep_mask);
		asic3_set_gpio_sleepmask_c(dev, 0xffff, pdata->gpio_c.sleep_mask);
		asic3_set_gpio_sleepmask_d(dev, 0xffff, pdata->gpio_d.sleep_mask);

		asic3_set_gpio_sleepout_a(dev, 0xffff, pdata->gpio_a.sleep_out);
		asic3_set_gpio_sleepout_b(dev, 0xffff, pdata->gpio_b.sleep_out);
		asic3_set_gpio_sleepout_c(dev, 0xffff, pdata->gpio_c.sleep_out);
		asic3_set_gpio_sleepout_d(dev, 0xffff, pdata->gpio_d.sleep_out);

		asic3_set_gpio_battfaultout_a(dev, 0xffff, pdata->gpio_a.batt_fault_out);
		asic3_set_gpio_battfaultout_b(dev, 0xffff, pdata->gpio_b.batt_fault_out);
		asic3_set_gpio_battfaultout_c(dev, 0xffff, pdata->gpio_c.batt_fault_out);
		asic3_set_gpio_battfaultout_d(dev, 0xffff, pdata->gpio_d.batt_fault_out);

		asic3_set_gpio_sleepconf_a(dev, 0xffff, pdata->gpio_a.sleep_conf);
		asic3_set_gpio_sleepconf_b(dev, 0xffff, pdata->gpio_b.sleep_conf);
		asic3_set_gpio_sleepconf_c(dev, 0xffff, pdata->gpio_c.sleep_conf);
		asic3_set_gpio_sleepconf_d(dev, 0xffff, pdata->gpio_d.sleep_conf);

		asic3_set_gpio_alt_fn_a(dev, 0xffff, pdata->gpio_a.alt_function);
		asic3_set_gpio_alt_fn_b(dev, 0xffff, pdata->gpio_b.alt_function);
		asic3_set_gpio_alt_fn_c(dev, 0xffff, pdata->gpio_c.alt_function);
		asic3_set_gpio_alt_fn_d(dev, 0xffff, pdata->gpio_d.alt_function);
	}

	asic->irq_nr = -1;
	asic->irq_base = -1;

	if (pdev->num_resources > 1)
		asic->irq_nr = pdev->resource[1].start;

	if (asic->irq_nr != -1) {
		unsigned int i;

		asic->irq_base = alloc_irq_space (ASIC3_NR_IRQS);
		if (asic->irq_base == -1) {
			printk("asic3: unable to allocate %d IRQs\n", ASIC3_NR_IRQS);
			asic3_remove(pdev);
			return -ENOMEM;
		}

		/* turn on clock to IRQ controller */
		clksel |= CLOCK_SEL_CX;
		__asic3_write_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_SEL, clksel);

		printk ("asic3: using irq %d-%d on irq %d\n", 
			asic->irq_base, asic->irq_base + ASIC3_NR_IRQS - 1, asic->irq_nr);

		for (i = 0 ; i < ASIC3_NR_IRQS ; i++) {
			int irq = i + asic->irq_base;
			if(i < ASIC3_NR_GPIO_IRQS) {
				set_irq_chip (irq, &asic3_gpio_irq_chip);
				set_irq_chipdata (irq, asic);
				set_irq_handler (irq, do_level_IRQ);
				set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
			} else {
				/* The remaining IRQs are not GPIO */
				set_irq_chip (irq, &asic3_irq_chip);
				set_irq_chipdata (irq, asic);
				set_irq_handler (irq, do_level_IRQ);
				set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
			}
		}

		__asic3_write_register (asic, _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
					ASIC3_INTMASK_GINTMASK);

		set_irq_chained_handler (asic->irq_nr, asic3_irq_demux);
		set_irq_type (asic->irq_nr, IRQT_RISING);
		set_irq_data (asic->irq_nr, asic);
	}

	if (pdev->num_resources > 2) {
		int rc;
		rc = asic3_register_mmc (dev);
		if (rc) {
			asic3_remove(pdev);
			return rc;
		}
	}

	if (pdata && pdata->child_platform_devs && pdata->num_child_platform_devs != 0)
		platform_add_devices (pdata->child_platform_devs, pdata->num_child_platform_devs);

	return 0;
}

static int
asic3_remove(struct platform_device *pdev)
{
	struct asic3_platform_data *pdata = pdev->dev.platform_data;
	struct asic3_data *asic = platform_get_drvdata(pdev);

	if (pdata && pdata->child_platform_devs && pdata->num_child_platform_devs != 0) {
		int i;
		for (i = 0; i < pdata->num_child_platform_devs; i++) {
			platform_device_unregister (pdata->child_platform_devs[i]);
		}
	}

	if (asic->irq_nr != -1) {
		unsigned int i;

		for (i = 0 ; i < ASIC3_NR_IRQS ; i++) {
			int irq = i + asic->irq_base;
			set_irq_flags(irq, 0);
			set_irq_handler (irq, NULL);
			set_irq_chip (irq, NULL);
			set_irq_chipdata (irq, NULL);
		}

		set_irq_chained_handler (asic->irq_nr, NULL);
		free_irq_space (asic->irq_base, ASIC3_NR_IRQS);
	}

	if (asic->mmc_dev)
		asic3_unregister_mmc(&pdev->dev);

	__asic3_write_register (asic, _IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_SEL, 0);
	__asic3_write_register (asic, _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask, 0);

	iounmap (asic->mapping);

	kfree (asic);
	
	return 0;
}

static void
asic3_shutdown(struct platform_device *pdev)
{
}

static unsigned short suspend_cdex;

static int
asic3_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct asic3_data *asic = platform_get_drvdata(pdev);
	suspend_cdex = __asic3_read_register (asic,
		_IPAQ_ASIC3_CLOCK_Base + _IPAQ_ASIC3_CLOCK_CDEX);
	/* The LEDs are still active during suspend */
	__asic3_write_register (asic,
		_IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_CLOCK_CDEX,
		suspend_cdex & (CLOCK_CDEX_LED0 | CLOCK_CDEX_LED1 | CLOCK_CDEX_LED2) );
	return 0;
}

static int
asic3_resume(struct platform_device *pdev)
{
	struct asic3_data *asic = platform_get_drvdata(pdev);

	__asic3_write_register (asic, _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_CLOCK_CDEX,
		suspend_cdex );

	if (asic->irq_nr != -1) {
                __asic3_write_register (asic,
                                _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
                                0 );
                mdelay(1);
                __asic3_write_register (asic,
                                _IPAQ_ASIC3_INTR_Base + _IPAQ_ASIC3_INTR_IntMask,
                                ASIC3_INTMASK_GINTMASK);
	}
	return 0;
}

static struct platform_driver asic3_device_driver = {
	.driver		= {
		.name	= "asic3",
	},
	.probe		= asic3_probe,
	.remove		= asic3_remove,
	.suspend	= asic3_suspend,
	.resume		= asic3_resume,
	.shutdown	= asic3_shutdown,
};

static int __init 
asic3_base_init (void)
{
	int retval = 0;
	retval = platform_driver_register(&asic3_device_driver);
	return retval;
}

static void __exit 
asic3_base_exit (void)
{
	platform_driver_unregister(&asic3_device_driver);
}

#ifdef MODULE
module_init (asic3_base_init);
#else	/* start early for dependencies */
subsys_initcall (asic3_base_init);
#endif
module_exit (asic3_base_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phil Blundell <pb@handhelds.org>");
MODULE_DESCRIPTION("Core driver for HTC ASIC3");
MODULE_SUPPORTED_DEVICE("asic3");
