/*
 * Hardware definitions for HP iPAQ Handheld Computers
 *
 * Copyright 2000-2003 Hewlett-Packard Company.
 * Copyright 2005 Phil Blundell
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 *
 * COMPAQ COMPUTER CORPORATION MAKES NO WARRANTIES, EXPRESSED OR IMPLIED,
 * AS TO THE USEFULNESS OR CORRECTNESS OF THIS CODE OR ITS
 * FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * Author: Jamey Hicks.
 *
 * History:
 *
 * 2002-08-23   Jamey Hicks        GPIO and IRQ support for iPAQ H5400
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/pm.h>
#include <linux/bootmem.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/soc-old.h>
#include <linux/dma-mapping.h>

#include <asm/irq.h>
#include <asm/hardware.h>
#include <asm/setup.h>
#include <asm/io.h>
#include <asm/mach-types.h>

#include <asm/mach/irq.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#include <linux/clk.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/h5400-asic.h>
#include <asm/arch/h5400-gpio.h>
#include <asm/hardware/ipaq-samcop.h>
#include <asm/hardware/samcop_base.h>
#include <asm/hardware/samcop-sdi.h>
#include <asm/hardware/samcop-dma.h>
#include <asm/arch/pxa-dmabounce.h>
#include "../mmc/samcop_sdi.h"

#include <asm/arch/irq.h>
#include <asm/arch/clock.h>
#include <asm/types.h>

#include "../../arch/arm/common/ipaq/clock.h"

struct samcop_data
{
	int irq_base, irq_nr;
	void *mapping;
	spinlock_t gpio_lock;
	unsigned long irqmask;
	struct platform_device **devices;
	int ndevices;
};

static int samcop_remove (struct device *dev);
static int samcop_gclk (void *ptr);
static void samcop_set_gpio_a_pullup (struct device *dev, u32 mask, u32 bits);
static void
samcop_set_gpio_a_con (struct device *dev, unsigned int idx, u32 mask, u32 bits);
void
samcop_set_gpio_int (struct device *dev, unsigned int idx, u32 mask, u32 bits);
void
samcop_set_gpio_int_enable (struct device *dev, unsigned int idx, u32 mask, u32 bits);
void
samcop_set_gpio_filter_config (struct device *dev, unsigned int idx, u32 mask,
			       u32 bits);

/***********************************************************************************/
/*      Register access                                                            */
/***********************************************************************************/

static inline void
samcop_write_register (struct samcop_data *samcop, unsigned long addr, unsigned long value)
{
	__raw_writel (value, (unsigned long)samcop->mapping + addr);
}

static inline unsigned long
samcop_read_register (struct samcop_data *samcop, unsigned long addr)
{
	return __raw_readl ((unsigned long)samcop->mapping + addr);
}

/***********************************************************************************/
/*      ASIC IRQ demux                                                             */
/***********************************************************************************/

#define MAX_ASIC_ISR_LOOPS    1000

static void
samcop_irq_demux (unsigned int irq, struct irqdesc *desc, struct pt_regs *regs)
{
	int i;
	struct samcop_data *samcop;
	u32 pending;

	// Tell processor to acknowledge its interrupt
	desc->chip->ack(irq);
	samcop = desc->handler_data;
	
	if (0) printk("%s: interrupt received\n", __FUNCTION__);

	for ( i = 0 ; (i < MAX_ASIC_ISR_LOOPS); i++ ) {
		int asic_irq;
		
		pending = samcop_read_register (samcop, SAMCOP_IC_INTPND);
		if (!pending)
			break;

		asic_irq = samcop->irq_base + (31 - __builtin_clz (pending));
		desc = irq_desc + asic_irq;
		desc->handle_irq(asic_irq, desc, regs);
	}

	if (i >= MAX_ASIC_ISR_LOOPS && pending) {
		u32 val;

		printk(KERN_WARNING
		       "%s: interrupt processing overrun, pending=0x%08x. "
		       "Masking interrupt\n", __FUNCTION__, pending);
		val = samcop_read_register (samcop, SAMCOP_IC_INTMSK);
		samcop_write_register (samcop, SAMCOP_IC_INTMSK, val | pending);
		samcop_write_register (samcop, SAMCOP_IC_SRCPND, pending);
		samcop_write_register (samcop, SAMCOP_IC_INTPND, pending);
	}
}

/***********************************************************************************/
/*      ASIC EPS IRQ demux                                                         */
/***********************************************************************************/

static u32 eps_irq_mask[] = {
	SAMCOP_PCMCIA_EPS_CD0_N, /* IRQ_GPIO_SAMCOP_EPS_CD0 */
	SAMCOP_PCMCIA_EPS_CD1_N, /* IRQ_GPIO_SAMCOP_EPS_CD1 */
	SAMCOP_PCMCIA_EPS_IRQ0_N, /* IRQ_GPIO_SAMCOP_EPS_IRQ0 */
	SAMCOP_PCMCIA_EPS_IRQ1_N, /* IRQ_GPIO_SAMCOP_EPS_IRQ1 */
	(SAMCOP_PCMCIA_EPS_ODET0_N|SAMCOP_PCMCIA_EPS_ODET1_N),/* IRQ_GPIO_SAMCOP_EPS_ODET */
	SAMCOP_PCMCIA_EPS_BATT_FLT/* IRQ_GPIO_SAMCOP_EPS_BATT_FAULT */
};

static void
samcop_asic_eps_irq_demux (unsigned int irq, struct irqdesc *desc, struct pt_regs *regs)
{
	int i;
	struct samcop_data *samcop;
	u32 eps_pending;

	samcop = desc->handler_data;

	if (0) printk("%s: interrupt received irq=%d\n", __FUNCTION__, irq);

	for ( i = 0 ; (i < MAX_ASIC_ISR_LOOPS) ; i++ ) {
		int j;
		eps_pending = samcop_read_register (samcop, SAMCOP_PCMCIA_IP);
		if (!eps_pending)
			break;
		if (0) printk("%s: eps_pending=0x%08x\n", __FUNCTION__, eps_pending);
		for ( j = 0 ; j < SAMCOP_EPS_IRQ_COUNT ; j++ )
			if ( eps_pending & eps_irq_mask[j] ) {
				int eps_irq = samcop->irq_base + j + SAMCOP_EPS_IRQ_START;
				desc = irq_desc + eps_irq;
				desc->handle_irq(eps_irq, desc, regs);
			}
	}

	if (eps_pending)
		printk("%s: interrupt processing overrun pending=0x%08x\n", __FUNCTION__, eps_pending);
}

/***********************************************************************************/
/*      ASIC GPIO IRQ demux                                                        */
/***********************************************************************************/

static void
samcop_asic_gpio_irq_demux (unsigned int irq, struct irqdesc *desc, struct pt_regs *regs)
{
	u32 pending;
	struct samcop_data *samcop;
	int loop;

	samcop = desc->handler_data;

	for (loop = 0; loop < MAX_ASIC_ISR_LOOPS; loop++)	
	{
		int i;
		pending = samcop_read_register (samcop, SAMCOP_GPIO_INTPND);

		if (!pending)
			break;
		
		for (i = 0; i < SAMCOP_GPIO_IRQ_COUNT; i++) {
			if (pending & (1 << i)) {
				int gpio_irq = samcop->irq_base + i + SAMCOP_GPIO_IRQ_START;
				desc = irq_desc + gpio_irq;
				desc->handle_irq(gpio_irq, desc, regs);
			}
		}
	}

	if (pending)
		printk ("%s: demux overrun, pending=%08x\n", __FUNCTION__, pending);
}

/***********************************************************************************/
/*      IRQ handling                                                               */
/***********************************************************************************/

/* ack <- IRQ is first serviced.
       mask <- IRQ is disabled.  
     unmask <- IRQ is enabled */

static void 
samcop_asic_ack_ic_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = 1 << (irq - SAMCOP_IC_IRQ_START - samcop->irq_base);

	samcop_write_register (samcop, SAMCOP_IC_SRCPND, mask);
	samcop_write_register (samcop, SAMCOP_IC_INTPND, mask);
}

static void 
samcop_asic_mask_ic_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = 1 << (irq - SAMCOP_IC_IRQ_START - samcop->irq_base);
	u32 val;

	val = samcop_read_register (samcop, SAMCOP_IC_INTMSK);
	val |= mask;
	samcop_write_register (samcop, SAMCOP_IC_INTMSK, val);
}

static void 
samcop_asic_unmask_ic_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = 1 << (irq - SAMCOP_IC_IRQ_START - samcop->irq_base);
	u32 val;

	val = samcop_read_register (samcop, SAMCOP_IC_INTMSK);
	val &= ~mask;
	samcop_write_register (samcop, SAMCOP_IC_INTMSK, val);
}

static void 
samcop_asic_ack_eps_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = eps_irq_mask[irq - SAMCOP_EPS_IRQ_START - samcop->irq_base];

	samcop_write_register (samcop, SAMCOP_PCMCIA_IP, mask);
}

static void 
samcop_asic_mask_eps_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = eps_irq_mask[irq - SAMCOP_EPS_IRQ_START - samcop->irq_base];
	u32 val;

	val = samcop_read_register (samcop, SAMCOP_PCMCIA_IC);
	val &= ~mask;
	samcop_write_register (samcop, SAMCOP_PCMCIA_IC, val);
}

static void 
samcop_asic_unmask_eps_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	int mask = eps_irq_mask[irq - SAMCOP_EPS_IRQ_START - samcop->irq_base];
	u32 val;

	val = samcop_read_register (samcop, SAMCOP_PCMCIA_IC);
	val |= mask;
	samcop_write_register (samcop, SAMCOP_PCMCIA_IC, val);
}

static void 
samcop_asic_ack_gpio_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);

	samcop_write_register (samcop, SAMCOP_GPIO_INTPND, 
			       1 << (irq - SAMCOP_GPIO_IRQ_START - samcop->irq_base));
}

static void 
samcop_asic_mask_gpio_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	u32 mask = 1 << (irq - SAMCOP_GPIO_IRQ_START - samcop->irq_base);
	u32 val;

	val = samcop_read_register (samcop, SAMCOP_GPIO_ENINT2);
	val &= ~mask;
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT2, val);
}

static void 
samcop_asic_unmask_gpio_irq (unsigned int irq)
{
	struct samcop_data *samcop = get_irq_chipdata (irq);
	u32 mask = 1 << (irq - SAMCOP_GPIO_IRQ_START - samcop->irq_base);
	u32 val;
	
	val = samcop_read_register (samcop, SAMCOP_GPIO_ENINT2);
	val |= mask;
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT2, val);
}

static struct irqchip samcop_ic_irq_chip = {
	.ack		= samcop_asic_ack_ic_irq,
	.mask		= samcop_asic_mask_ic_irq,
	.unmask		= samcop_asic_unmask_ic_irq,
};

static struct irqchip samcop_eps_irq_chip = {
	.ack		= samcop_asic_ack_eps_irq,
	.mask		= samcop_asic_mask_eps_irq,
	.unmask		= samcop_asic_unmask_eps_irq,
};

static struct irqchip samcop_gpio_irq_chip = {
	.ack		= samcop_asic_ack_gpio_irq,
	.mask		= samcop_asic_mask_gpio_irq,
	.unmask		= samcop_asic_unmask_gpio_irq,
};

static void __init 
samcop_irq_init (struct samcop_data *samcop)
{
	int i;

	/* mask all interrupts, this will cause the processor to ignore all interrupts from SAMCOP */
	samcop_write_register (samcop, SAMCOP_IC_INTMSK, 0xffffffff);

	/* clear out any pending irqs */
	for (i = 0; i < 32; i++) {
		if (samcop_read_register (samcop, SAMCOP_IC_INTPND) == 0) {
			printk(KERN_INFO
			       "%s: no interrupts pending, looped %d %s. "
			       "Continuing\n", __FUNCTION__,
			       i + 1, ((i + 1) ? "time" : "times"));
			break;
		}
		samcop_write_register (samcop, SAMCOP_IC_SRCPND, 0xffffffff);
		samcop_write_register (samcop, SAMCOP_IC_INTPND, 0xffffffff);
	}

	samcop_write_register (samcop, SAMCOP_PCMCIA_IC, 0);	/* nothing enabled */
	samcop_write_register (samcop, SAMCOP_PCMCIA_IP, 0xff);	/* clear anything here now */
	samcop_write_register (samcop, SAMCOP_PCMCIA_IM, 0x2555);

	samcop_write_register (samcop, SAMCOP_GPIO_ENINT1, 0);
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT2, 0);
	samcop_write_register (samcop, SAMCOP_GPIO_INTPND, 0x3fff);

	for (i = 0; i < SAMCOP_IC_IRQ_COUNT; i++) {
		int irq = samcop->irq_base + i + SAMCOP_IC_IRQ_START;
		set_irq_chipdata (irq, samcop);
		set_irq_chip (irq, &samcop_ic_irq_chip);
		set_irq_handler (irq, do_edge_IRQ);
		set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
	}

	for (i = 0; i < SAMCOP_EPS_IRQ_COUNT; i++) {
		int irq = samcop->irq_base + i + SAMCOP_EPS_IRQ_START;
		set_irq_chipdata (irq, samcop);
		set_irq_chip (irq, &samcop_eps_irq_chip);
		set_irq_handler (irq, do_edge_IRQ);
		set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
	}

	for (i = 0; i < SAMCOP_GPIO_IRQ_COUNT; i++) {
		int irq = samcop->irq_base + i + SAMCOP_GPIO_IRQ_START;
		set_irq_chipdata (irq, samcop);
		set_irq_chip (irq, &samcop_gpio_irq_chip);
		set_irq_handler (irq, do_edge_IRQ);
		set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
	}

	set_irq_data (samcop->irq_base + _IRQ_SAMCOP_PCMCIA, samcop);
	set_irq_chained_handler (samcop->irq_base + _IRQ_SAMCOP_PCMCIA, 
				 samcop_asic_eps_irq_demux);
	set_irq_data (samcop->irq_base + _IRQ_SAMCOP_GPIO, samcop);
	set_irq_chained_handler (samcop->irq_base + _IRQ_SAMCOP_GPIO, 
				 samcop_asic_gpio_irq_demux);

	set_irq_data (samcop->irq_nr, samcop);
	set_irq_type (samcop->irq_nr, IRQT_FALLING);
	set_irq_chained_handler (samcop->irq_nr, samcop_irq_demux);
}

/*************************************************************
 * SAMCOP SD interface hooks and initialization
 *************************************************************/
int
samcop_dma_needs_bounce (struct device *dev, dma_addr_t addr, size_t size)
{
	return (addr + size >= H5400_SAMCOP_BASE + _SAMCOP_SRAM_Base + SAMCOP_SRAM_SIZE);
}

static u64 samcop_sdi_dmamask = 0xffffffffUL;

static void
samcop_sdi_init(struct device *dev)
{
	struct samcop_data *samcop = dev->parent->driver_data;
	struct samcop_sdi_data *plat = dev->platform_data;

	/* Init run-time values */
	plat->f_min = samcop_gclk(samcop) / 256; /* Divisor is SDIPRE max + 1 */
	plat->f_max = samcop_gclk(samcop) / 2;   /* Divisor is SDIPRE min + 1 */
	plat->dma_devaddr = (void *) _SAMCOP_SDI_Base + SAMCOP_SDIDATA;

	/* Set up card detect input on gpio port a and set up the irq
	 * trigger
	 */
	samcop_set_gpio_int(dev->parent, 2, SAMCOP_GPIO_IRQT_MASK(0),
			SAMCOP_GPIO_IRQT_BOTHEDGE << SAMCOP_SD_WAKEUP_IRQT_SHIFT);
	samcop_set_gpio_int(dev->parent, 2, SAMCOP_GPIO_IRQF_MASK(0), 0);
	samcop_set_gpio_int_enable(dev->parent, 0, SAMCOP_GPIO_ENINT_MASK(8),
			           1 << SAMCOP_SD_WAKEUP_ENINT_SHIFT);
	samcop_set_gpio_a_con(dev->parent, 1, SAMCOP_GPIO_GPx_CON_MASK(0),
			      SAMCOP_GPIO_GPx_CON_MODE(0, EXTIN));

	/* provide the right card detect irq to the mmc subsystem by
	 * applying the samcop irq_base offset
	 */
	plat->irq_cd = plat->irq_cd + samcop_irq_base(dev->parent);

	/* setup DMA (including dma_bounce) */
	dev->dma_mask = &samcop_sdi_dmamask; /* spread the address range wide
				              * so we can DMA bounce wherever
					      * we want.
					      */
	dev->coherent_dma_mask = samcop_sdi_dmamask;
	dmabounce_register_dev(dev, 512, 4096);
	pxa_set_dma_needs_bounce(samcop_dma_needs_bounce);
}

static void
samcop_sdi_exit(struct device *dev)
{
	dmabounce_unregister_dev(dev);
}

static u32
samcop_sdi_read_register(struct device *dev, u32 reg)
{
	struct samcop_data *samcop = dev->parent->driver_data;

	return samcop_read_register(samcop, _SAMCOP_SDI_Base + reg);
}

static void
samcop_sdi_write_register(struct device *dev, u32 reg, u32 val)
{
	struct samcop_data *samcop = dev->parent->driver_data;
	
	samcop_write_register(samcop, _SAMCOP_SDI_Base + reg, val);
}

static void
samcop_sdi_card_power(struct device *dev, int on, int clock_req)
{
	struct samcop_data *samcop = dev->parent->driver_data;
	u32 sdi_psc = 1, sdi_con;
	int clock_rate;

	/* Set power */
	sdi_con = samcop_sdi_read_register(dev, SAMCOP_SDICON);
#ifdef CONFIG_ARCH_H5400
	/* apply proper power setting to the sd slot */
	if (machine_is_h5400())
		SET_H5400_GPIO(POWER_SD_N, on != 0);
#endif

	if (on) {
		/* enable pullups */
		samcop_set_spcr(dev->parent, SAMCOP_GPIO_SPCR_SDPUCR, 0);
		samcop_set_gpio_a_pullup(dev->parent, SAMCOP_GPIO_GPA_SD_DETECT_N, 0);

		/* enable the SD clock prescaler and clear the fifo */
		sdi_con |= SAMCOP_SDICON_FIFORESET | SAMCOP_SDICON_CLOCKTYPE;
	} else {
		/* remove power from the sd slot */
#ifdef CONFIG_ARCH_H5400
		if (machine_is_h5400())
			SET_H5400_GPIO(POWER_SD_N, 0);
#endif

		/* disable pullups */
		samcop_set_spcr(dev->parent, SAMCOP_GPIO_SPCR_SDPUCR, 
				SAMCOP_GPIO_SPCR_SDPUCR);
		samcop_set_gpio_a_pullup(dev->parent,
					 SAMCOP_GPIO_GPA_SD_DETECT_N,
					 SAMCOP_GPIO_GPA_SD_DETECT_N);

		sdi_con &= ~SAMCOP_SDICON_CLOCKTYPE;
	}

	clock_rate = samcop_gclk(samcop);

	if (clock_req == 0) {
		/* Don't set the prescaler or enable the card clock if the mmc
		 * subsystem is sending a 0 clock rate.
		 */
		sdi_con &= ~SAMCOP_SDICON_CLOCKTYPE;
	} else if (clock_req < (clock_rate / 256)) {
		printk(KERN_WARNING
		       "%s: MMC subsystem requesting bogus clock rate %d. "
		       "Hardcoding prescaler to 255\n",
		       __FUNCTION__, clock_req);
		sdi_psc = 255;
	} else {
		sdi_psc = clock_rate / clock_req - 1;
		if (sdi_psc > 255) {
			printk(KERN_WARNING
			       "%s: calculated prescaler %d is too high. "
			       "Hardcoding prescaler to 255\n",
			       __FUNCTION__, sdi_psc);
			sdi_psc = 255;
		} else if (sdi_psc < 1) {
			printk(KERN_WARNING
			       "%s: calculated prescaler %d is too low. "
			       "Hardcoding prescaler to 1\n",
			      __FUNCTION__, sdi_psc);
			sdi_psc = 1;
		}
	}

	samcop_sdi_write_register(dev, SAMCOP_SDICON, sdi_con);

	if (clock_req != 0 && on) {
		if (clock_rate / (sdi_psc + 1) > clock_req && sdi_psc < 255)
			sdi_psc++; 
		samcop_sdi_write_register(dev, SAMCOP_SDIPRE, sdi_psc);
		/* wait for the prescribed time that the card requires to init */
		if ((clock_rate / sdi_psc) > 1000000UL)
			ndelay(sdi_psc * 1000000000UL / clock_rate * SAMCOP_CARD_INIT_CYCLES);
		else
			udelay(sdi_psc * 1000000UL / clock_rate * SAMCOP_CARD_INIT_CYCLES);
	}
}

static struct samcop_sdi_data samcop_sdi_data = {
	.init		  = samcop_sdi_init,
	.exit		  = samcop_sdi_exit,
	.read_reg	  = samcop_sdi_read_register,
	.write_reg	  = samcop_sdi_write_register,
	.card_power	  = samcop_sdi_card_power,
	/* card detect IRQ. Macro is for the samcop number,
	 * use samcop->irq_base + irq_cd to get the irq system number
	 */
	.irq_cd		  = _IRQ_SAMCOP_SD_WAKEUP,
	.f_min		  = 0, /* this will be set by sdi_init */
	.f_max		  = 0, /* this will be set by sdi_init */
	/* SAMCOP has 32KB of SRAM, 32KB / 512B (per sector) = 64 sectors max.*/
	.max_sectors  = 64,
	.timeout	  = SAMCOP_SDI_TIMER_MAX,

	/* DMA specific settings */
	.dma_chan	  = 0, /* hardcoded for now */
	.dma_devaddr  = (void *)_SAMCOP_SDI_Base + SAMCOP_SDIDATA,
	.hwsrcsel	  = SAMCOP_DCON_CH0_SD,
	/* SAMCOP supports 32bit transfers, 32/8 = 4 bytes per transfer unit */
	.xfer_unit	  = 4, 
};
	
/*************************************************************
 * SAMCOP DMA interface hooks and initialization
 *************************************************************/

static struct samcop_dma_plat_data samcop_dma_plat_data = {
	.n_channels = 2,
};

/*************************************************************/

int
samcop_clock_enable (struct clk* clk, int enable)
{
	unsigned long flags, val;
	struct samcop_data *samcop = (struct samcop_data *)clk->priv;

	local_irq_save (flags);
	val = samcop_read_register (samcop, SAMCOP_CPM_ClockControl);

	/* UCLK is different from the rest (0 = on, 1 = off), so we
	 * special-case it here. */
	if (strcmp(clk->name, "uclk") == 0)
		enable = enable ? 0 : 1;

	if (enable)
		val |= clk->ctrlbit;
	else
		val &= ~clk->ctrlbit;
	samcop_write_register (samcop, SAMCOP_CPM_ClockControl, val);
	local_irq_restore (flags);

	return 0;
}
EXPORT_SYMBOL(samcop_clock_enable);

static int 
samcop_gclk (void *ptr)
{
	struct samcop_data *samcop = ptr;
	int half_clk;
	unsigned int memory_clock = get_memclk_frequency_10khz() * 10000;

	half_clk = (samcop_read_register (samcop, SAMCOP_CPM_ClockSleep) & SAMCOP_CPM_CLKSLEEP_HALF_CLK);

	return half_clk ? memory_clock : memory_clock / 2;
}

void
samcop_set_gpio_a (struct device *dev, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_GPA_DAT) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_GPA_DAT, val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}
EXPORT_SYMBOL(samcop_set_gpio_a);

static void
samcop_set_gpio_a_pullup (struct device *dev, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_GPA_PUP) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_GPA_PUP, val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);
}

static void
samcop_set_gpio_a_con (struct device *dev, unsigned int idx, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags;
	u32 val;

	if (idx > 1) {
		printk(KERN_WARNING "%s idx %u is invalid, must be {0-1}\n",
		       __FUNCTION__, idx);
		return;
	}

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_GPA_CON(idx)) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_GPA_CON(idx), val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}

u32
samcop_get_gpio_a (struct device *dev)
{
	struct samcop_data *samcop = dev->driver_data;

	return samcop_read_register (samcop, SAMCOP_GPIO_GPA_DAT);
}
EXPORT_SYMBOL(samcop_get_gpio_a);

void
samcop_set_gpio_b (struct device *dev, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_GPB_DAT) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_GPB_DAT, val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}
EXPORT_SYMBOL(samcop_set_gpio_b);

u32
samcop_get_gpio_b (struct device *dev)
{
	struct samcop_data *samcop = dev->driver_data;

	return samcop_read_register (samcop, SAMCOP_GPIO_GPB_DAT);
}
EXPORT_SYMBOL(samcop_get_gpio_b);

void
samcop_set_gpio_int (struct device *dev, unsigned int idx, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	if (idx > 2) {
		printk(KERN_WARNING "%s idx %u is invalid, must be {0-2}\n",
		       __FUNCTION__, idx);
		return;
	}

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_INT(idx)) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_INT(idx), val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}

void
samcop_set_gpio_filter_config (struct device *dev, unsigned int idx, u32 mask,
			       u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	if (idx > 6) {
		printk(KERN_WARNING "%s idx %u is invalid, must be {0-6}\n",
		       __FUNCTION__, idx);
		return;
	}

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_FLTCONFIG(idx)) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_FLTCONFIG(idx), val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}

void
samcop_set_gpio_int_enable (struct device *dev, unsigned int idx, u32 mask,
			       u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags, val;

	if (idx > 1) {
		printk(KERN_WARNING "%s idx %u is invalid, must be {0,1}\n",
		       __FUNCTION__, idx);
		return;
	}

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_ENINT(idx)) & ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT(idx), val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}

void
samcop_reset_fcd (struct device *dev)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags;

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	samcop_write_register (samcop, SAMCOP_GPIO_GPD_CON, SAMCOP_GPIO_GPD_CON_RESET);
	samcop_write_register(samcop, SAMCOP_GPIO_GPE_CON, SAMCOP_GPIO_GPE_CON_RESET);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);	
}
EXPORT_SYMBOL(samcop_reset_fcd);

int
samcop_irq_base (struct device *dev)
{
	struct samcop_data *samcop = dev->driver_data;

	return samcop->irq_base;
}
EXPORT_SYMBOL(samcop_irq_base);

void
samcop_set_spcr (struct device *dev, u32 mask, u32 bits)
{
	struct samcop_data *samcop = dev->driver_data;
	unsigned long flags;
	u32 val;

	spin_lock_irqsave (&samcop->gpio_lock, flags);
	val = samcop_read_register (samcop, SAMCOP_GPIO_SPCR);
	val &= ~mask;
	val |= bits;
	samcop_write_register (samcop, SAMCOP_GPIO_SPCR, val);
	spin_unlock_irqrestore (&samcop->gpio_lock, flags);     
}
EXPORT_SYMBOL(samcop_set_spcr);


/*************************************************************
 SAMCOP clocks
 *************************************************************/

/* base clocks */

static struct clk clk_g = {
	.name		= "gclk",
	.id		= -1,
	.rate		= 0,
	.parent		= NULL,
	.ctrlbit	= 0,
};

/* clock definitions */

static struct clk samcop_clocks[] = {
	{ .name    = "uclk",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  /* note that the sense of this uclk bit is reversed */
	  .ctrlbit = SAMCOP_CPM_CLKCON_UCLK_EN
	},
	{ .name    = "usb",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_USBHOST_CLKEN
	},
	{ .name    = "dma",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_DMAC_CLKEN
	},
	{ .name    = "gpio",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_GPIO_CLKEN
	},
	{ .name    = "fsi",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_FCD_CLKEN
	},
	{ .name    = "sdi",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_SD_CLKEN
	},
	{ .name    = "adc",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_ADC_CLKEN
	},
	{ .name    = "uart2",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_UART2_CLKEN
	},
	{ .name    = "uart1",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_UART1_CLKEN
	},
	{ .name    = "led",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_LED_CLKEN
	},
	{ .name    = "misc",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_MISC_CLKEN
	},
	{ .name    = "w1",
	  .id	   = -1,
	  .parent  = &clk_g,
	  .enable  = samcop_clock_enable,
	  .ctrlbit = SAMCOP_CPM_CLKCON_1WIRE_CLKEN
	},
};

/*************************************************************/

struct samcop_block
{
	platform_device_id id;
	char *name;
	unsigned long start, end;
	unsigned long irq;
	void *platform_data;
};

static struct samcop_block samcop_blocks[] = {
	{
		.id = { -1 },
		.name = "samcop adc",
		.start = _SAMCOP_ADC_Base,
		.end = _SAMCOP_ADC_Base + 0x1f,
		.irq = _IRQ_SAMCOP_ADCTS,
	},
	{
		.id = { -1 },
		.name = "samcop owm",
		.start = _SAMCOP_OWM_Base,
		.end = _SAMCOP_OWM_Base + 0x1f, /* 0x47 */
		.irq = _IRQ_SAMCOP_ONEWIRE,
	},
	{
		.id = { -1 },
		.name = "samcop fsi",
		.start = _SAMCOP_FSI_Base,
		.end = _SAMCOP_FSI_Base + 0x1f,
		.irq = _IRQ_SAMCOP_FCD,
	},
	{
		.id = { -1 },
		.name = "samcop sleeve",
		.start = _SAMCOP_PCMCIA_Base,
		.end = _SAMCOP_PCMCIA_Base + 0x1f,
		.irq = SAMCOP_EPS_IRQ_START,
	},
	{
		.id = { -1 },
		.name = "samcop dma",
		.start = _SAMCOP_DMAC_Base,
		.end = _SAMCOP_DMAC_Base + 0x3f,
		.irq = _IRQ_SAMCOP_DMA0,
		.platform_data = &samcop_dma_plat_data,
	},
	{
		.id = { -1 },
		.name = "samcop sdi",
		.start = _SAMCOP_SDI_Base,
		.end = _SAMCOP_SDI_Base + 0x43,
		.irq = _IRQ_SAMCOP_SD,
		.platform_data = &samcop_sdi_data,
	},
	{
		.id = { -1 },
		.name = "samcop usb host",
		.start = _SAMCOP_USBHOST_Base,
		.end = _SAMCOP_USBHOST_Base + 0xffff,
		.irq = _IRQ_SAMCOP_USBH,
	},	
};

static void
samcop_release (struct device *dev)
{
	struct platform_device *sdev = to_platform_device (dev);
	kfree (sdev->resource);
	kfree (sdev);
}

static int 
samcop_probe (struct device *dev)
{
	int i, rc;
	struct platform_device *pdev = to_platform_device (dev);
	struct samcop_platform_data *platform_data = dev->platform_data;
	struct samcop_data *samcop;

	samcop = kmalloc (sizeof (struct samcop_data), GFP_KERNEL);
	if (!samcop)
		goto enomem3;
	memset (samcop, 0, sizeof (*samcop));
	dev->driver_data = samcop;

	samcop->irq_nr = platform_get_irq(pdev, 0);
	samcop->irq_base = alloc_irq_space (SAMCOP_NR_IRQS);
	if (samcop->irq_base == -1) {
		printk("samcop: unable to allocate %d irqs\n", SAMCOP_NR_IRQS);
		goto enomem2;
	}

	samcop->mapping = ioremap ((unsigned long)pdev->resource[0].start, SAMCOP_MAP_SIZE);
	if (!samcop->mapping) {
		printk ("samcop: couldn't ioremap\n");
		goto enomem1;
	}

	if (dma_declare_coherent_memory (dev,
					 pdev->resource[0].start + _SAMCOP_SRAM_Base,
					 _SAMCOP_SRAM_Base, SAMCOP_SRAM_SIZE,
					 DMA_MEMORY_MAP | DMA_MEMORY_INCLUDES_CHILDREN |
					 DMA_MEMORY_EXCLUSIVE) != DMA_MEMORY_MAP) {
		printk ("samcop: couldn't declare coherent dma memory\n");
		goto enomem0;
	}

	printk ("%s: using irq %d-%d on irq %d\n", pdev->name, samcop->irq_base, 
		samcop->irq_base + SAMCOP_NR_IRQS - 1, samcop->irq_nr);

	samcop_write_register (samcop, SAMCOP_CPM_ClockControl, SAMCOP_CPM_CLKCON_GPIO_CLKEN);
	samcop_write_register (samcop, SAMCOP_CPM_ClockSleep, platform_data->clocksleep);
	samcop_write_register (samcop, SAMCOP_CPM_PllControl, platform_data->pllcontrol);

	samcop_irq_init (samcop);

#ifdef CONFIG_ARCH_H5400
	h5400_set_samcop_gpio_b = samcop_set_gpio_b;
#endif

	/* Register SAMCOP's clocks. */
	clk_g.rate = samcop_gclk(samcop);

	if (clk_register(&clk_g) < 0)
		printk(KERN_ERR "failed to register SAMCOP gclk\n");

	for (i = 0; i < ARRAY_SIZE(samcop_clocks); i++) {
		samcop_clocks[i].priv = (unsigned long)samcop;
		rc = clk_register(&samcop_clocks[i]);
		if (rc < 0)
			printk(KERN_ERR "Failed to register clock %s (%d)\n",
			       samcop_clocks[i].name, rc);
	}

	/* Register SAMCOP's platform devices. */
	samcop->ndevices = ARRAY_SIZE (samcop_blocks);
	samcop->devices = kmalloc (samcop->ndevices * sizeof (struct platform_device *), GFP_KERNEL);
	if (unlikely (!samcop->devices))
		goto enomem;
	memset (samcop->devices, 0, samcop->ndevices * sizeof (struct platform_device *));
	
	for (i = 0; i < samcop->ndevices; i++) {
		struct platform_device *sdev;
		struct samcop_block *blk = &samcop_blocks[i];
		struct resource *res;

		sdev = kmalloc (sizeof (*sdev), GFP_KERNEL);
		if (unlikely (!sdev))
			goto enomem;
		memset (sdev, 0, sizeof (*sdev));
		sdev->id = samcop_blocks[i].id.id;
		sdev->dev.parent = dev;
		sdev->dev.platform_data = samcop_blocks[i].platform_data;
		sdev->dev.release = samcop_release;
		sdev->num_resources = (blk->irq == -1) ? 1 : 2;
		sdev->name = blk->name;
		res = kmalloc (sdev->num_resources * sizeof (struct resource), GFP_KERNEL);
		if (unlikely (!res))
			goto enomem;
		sdev->resource = res;
		memset (res, 0, sdev->num_resources * sizeof (struct resource));
		res[0].start = blk->start + pdev->resource[0].start;
		res[0].end = blk->end + pdev->resource[0].start;
		res[0].flags = IORESOURCE_MEM;
		res[0].parent = &pdev->resource[0];
		if (blk->irq != -1) {
			res[1].start = blk->irq + samcop->irq_base;
			res[1].end = res[1].start;
			res[1].flags = IORESOURCE_IRQ;
		}
		sdev->dev.dma_mem = dev->dma_mem;
		rc = platform_device_register (sdev);
		if (unlikely (rc != 0)) {
			printk ("samcop: could not register %s\n", blk->name);
			kfree (sdev->resource);
			kfree (sdev);
			goto error;
		}
		samcop->devices[i] = sdev;
	}

	return 0;

 enomem:
	rc = -ENOMEM;
 error:
	samcop_remove (dev);
	return rc;

 enomem0:
	iounmap (samcop->mapping);
 enomem1:
	free_irq_space (samcop->irq_base, SAMCOP_NR_IRQS);
 enomem2:
	kfree (samcop);
 enomem3:
	return -ENOMEM;
}

static int
samcop_remove (struct device *dev)
{
	int i;
	struct samcop_data *samcop;

	samcop = dev->driver_data;

	samcop_write_register (samcop, SAMCOP_PCMCIA_IC, 0);	/* nothing enabled */
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT1, 0);
	samcop_write_register (samcop, SAMCOP_GPIO_ENINT2, 0);
	samcop_write_register (samcop, SAMCOP_IC_INTMSK, 0xffffffff);

	for (i = 0; i < SAMCOP_NR_IRQS; i++) {
		int irq = i + samcop->irq_base;
		set_irq_flags(irq, 0);
		set_irq_handler (irq, NULL);
		set_irq_chip (irq, NULL);
		set_irq_chipdata (irq, NULL);
	}

	set_irq_chained_handler (samcop->irq_nr, NULL);

	if (samcop->devices) {
		for (i = 0; i < samcop->ndevices; i++) {
			if (samcop->devices[i])
				platform_device_unregister (samcop->devices[i]);
		}
		kfree (samcop->devices);
	}

	for (i = 0; i < ARRAY_SIZE(samcop_clocks); i++)
		clk_unregister(&samcop_clocks[i]);
	clk_unregister(&clk_g);

	samcop_write_register (samcop, SAMCOP_CPM_ClockControl, 0);

	dma_release_declared_memory (dev);
	iounmap (samcop->mapping);
	free_irq_space (samcop->irq_base, SAMCOP_NR_IRQS);

	kfree (samcop);

	return 0;
}

static void
samcop_shutdown (struct device *dev)
{
}

static int
samcop_suspend (struct device *dev, pm_message_t state)
{
	struct samcop_data *samcop;

	samcop = dev->driver_data;

	samcop->irqmask = samcop_read_register (samcop, SAMCOP_IC_INTMSK);
	if (samcop->irqmask != 0xffffffff) {
		samcop_write_register (samcop, SAMCOP_IC_INTMSK, 0xffffffff);
		printk (KERN_WARNING "irqs %08lx still enabled\n", ~samcop->irqmask);
	}

	return 0;
}

static int
samcop_resume (struct device *dev)
{
	struct samcop_data *samcop;

	samcop = dev->driver_data;

	if (samcop->irqmask != 0xffffffff)
		samcop_write_register (samcop, SAMCOP_IC_INTMSK, samcop->irqmask);

	return 0;
}

static struct device_driver samcop_device_driver = {
	.name		= "samcop",
	.bus		= &platform_bus_type,

	.probe		= samcop_probe,
	.remove		= samcop_remove,
	.suspend	= samcop_suspend,
	.resume		= samcop_resume,
	.shutdown	= samcop_shutdown,
};

static int __init 
samcop_base_init (void)
{
	int retval = 0;
	retval = driver_register (&samcop_device_driver);
	return retval;
}

static void __exit 
samcop_base_exit (void)
{
	driver_unregister (&samcop_device_driver);
}

module_init (samcop_base_init)
module_exit (samcop_base_exit)

MODULE_AUTHOR("Jamey Hicks <jamey@handhelds.org>");
MODULE_DESCRIPTION("Base platform_device driver for the SAMCOP chip");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_SUPPORTED_DEVICE("samcop");
