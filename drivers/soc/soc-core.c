/*
 * drivers/soc/soc-core.c
 *
 * core SoC support
 * Copyright (c) 2006 Ian Molton
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This file contains functionality used by many SoC type devices.
 *
 * Created: 2006-11-28
 *
 */

#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include "soc-core.h"

void soc_free_devices (struct platform_device *devices, int nr_devs) {
	struct platform_device *dev = devices;
	int i;
	
	for (i = 0; i < nr_devs; i++) {
		struct resource *res = dev->resource;
		platform_device_unregister(dev++);
		kfree(res);
	}
	kfree(devices);
}

struct platform_device *soc_add_devices(struct platform_device *dev, struct soc_device_data *soc, int nr_devs, struct resource *mem, int irq_base) {
	struct platform_device *devices;
	int i, r, offset;

        devices = kzalloc(nr_devs * sizeof(struct platform_device), GFP_KERNEL);
        if(!devices)
		return NULL;

	for (i = 0; i < nr_devs; i++) {
                struct platform_device *sdev = &devices[i];
                struct soc_device_data *blk = &soc[i];
                struct resource *res;

		sdev->id = -1;
                sdev->name = blk->name;

                sdev->dev.parent = &dev->dev;
                sdev->dev.platform_data = (void *)blk->hwconfig;
                sdev->num_resources = blk->num_resources;

                /* Allocate space for the subdevice resources */
                res = kzalloc (blk->num_resources * sizeof (struct resource), GFP_KERNEL);
		if(!res)
			goto fail;
		
		for (r = 0 ; r < blk->num_resources ; r++ ){
			res[r].name = blk->res[r].name; // Fixme - should copy
			if(blk->res[r].flags & IORESOURCE_MEM) {
				offset = mem->start;
				res[r].parent = mem;
			}
			else if((blk->res[r].flags & IORESOURCE_IRQ) &&
			        (blk->res[r].flags & IORESOURCE_IRQ_SOC_SUBDEVICE))
				offset = irq_base;
			else
				offset = 0;
			res[r].start = blk->res[r].start + offset;
			res[r].end   = blk->res[r].end + offset;
			res[r].flags = blk->res[r].flags;
		}

                sdev->resource = res;
		if(platform_device_register(sdev)) {
			kfree(res);	
			goto fail;
		}

		printk("SoC: registering %s\n", blk->name);
        }
	return devices;

fail:
	soc_free_devices(devices, i+1);
	return NULL;
}

