/*
 * OHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2002 David Brownell <dbrownell@users.sourceforge.net>
 * (C) Copyright 2002 Hewlett-Packard Company
 * 
 * HP iPAQ H5400 Bus Glue
 *
 * Written by Catalin Drula <catalin@cs.utoronto.ca>
 * Largely based on ohci-sa1111.c and tmio_ohci.c
 *
 * This file is licenced under the GPL.
 */
 
#include <asm/hardware.h>
#include <asm/mach-types.h>
#include <asm/hardware/shamcop-clocks.h>
#include <asm/hardware/ipaq-samcop.h>

#include <linux/kernel.h>
#include <linux/soc-old.h>

#ifndef CONFIG_ARCH_H5400
#error "This file is iPAQ H5xxx bus glue.  CONFIG_ARCH_H5400 must be defined."
#endif

static u64 ohci_dmamask = 0xffffffffUL;

extern int usb_disabled(void);

/*-------------------------------------------------------------------------*/

void __ohci_h5400_remove(struct usb_hcd *, struct platform_device *);


/**
 * __ohci_h5400_probe - initialize Samcop-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int __ohci_h5400_probe(const struct hc_driver *driver,
		       struct usb_hcd **hcd_out,
		       struct platform_device *sdev)
{
	int retval;
	struct usb_hcd *hcd = NULL;
	struct ohci_hcd *ohci;

	if (!request_mem_region(sdev->resource[0].start,
				sdev->resource[0].end - sdev->resource[0].start + 1,
				hcd_name)) {
		dbg("request_mem_region failed");
		return -EBUSY;
	}

	dmabounce_register_dev(&sdev->dev, 512, 4096);

	hcd = usb_create_hcd(driver);
	if (hcd == NULL) {
		dbg("usb_create_hcd failed");
		retval = -ENOMEM;
		goto err1;
	}

	ohci = hcd_to_ohci(hcd);
	ohci_hcd_init(ohci);

	hcd->irq = sdev->resource[2].start; 
	hcd->self.controller = &sdev->dev;
	hcd->regs = ioremap(sdev->resource[0].start, 
			    sdev->resource[0].end - sdev->resource[0].start + 1);

	shamcop_clock_enable(sdev->dev.parent, SHAMCOP_USBHOST_CLKEN, 1);
	shamcop_clock_enable(sdev->dev.parent, SHAMCOP_UCLK_EN, 0);

	mdelay(250);

	/* taken from tmio_ohci, have to see if they are needed */
	sdev->dev.dma_mask = &ohci_dmamask;
	sdev->dev.coherent_dma_mask = 0xffffffffUL;

	retval = hcd_buffer_create(hcd);
	if (retval != 0) {
		dbg("hcd_buffer_create fail");
		goto err1;
	}

	retval = request_irq(hcd->irq, usb_hcd_irq, SA_INTERRUPT,
			     hcd->driver->description, hcd);
	if (retval != 0) {
		dbg("request_irq failed");
		retval = -EBUSY;
		goto err2;
	}

	info("(Samcop) at 0x%p, irq %d\n", hcd->regs, hcd->irq);

	hcd->self.bus_name = "h5400";

	usb_register_bus(&hcd->self);

	if ((retval = driver->start(hcd)) < 0) 
	{
		dbg("Could not start OHCI driver!!!\n");
		__ohci_h5400_remove(hcd, sdev);
		return retval;
	}

	*hcd_out = hcd;
	return 0;

 err2:
	hcd_buffer_destroy (hcd);
 err1:
	release_mem_region(sdev->resource[0].start, 
			   sdev->resource[0].end - sdev->resource[0].start + 1);
	return retval;
}


/**
 * __ohci_h5400_remove - shutdown processing for Samcop-based HCDs
 * @hcd: USB Host Controller being removed
 * @sdev: Samcop USB device
 * Context: !in_interrupt()
 *
 * Reverses the effect of __ohci_h5400_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void __ohci_h5400_remove(struct usb_hcd *hcd, struct platform_device *sdev)
{
	if (hcd == NULL) {
		dbg("This should not happen\n");
		return;
	}

	info ("remove: %s, state %x", hcd->self.bus_name, hcd->state);

	if (in_interrupt ())
		BUG ();

	hcd->state = USB_STATE_QUIESCING;
	usb_disconnect(&hcd->self.root_hub);

	hcd->driver->stop(hcd);
	hcd->state = USB_STATE_HALT;

	free_irq(hcd->irq, hcd);
	hcd_buffer_destroy (hcd);

	usb_deregister_bus(&hcd->self);

	dmabounce_unregister_dev(&sdev->dev);

	release_mem_region(sdev->resource[0].start, 
			   sdev->resource[0].end - sdev->resource[0].start + 1);
}


/*-------------------------------------------------------------------------*/

static int __devinit ohci_h5400_start(struct usb_hcd *hcd)
{
	struct ohci_hcd	*ohci = hcd_to_ohci(hcd);
	int ret;

	if ((ret = ohci_init(ohci)) < 0)
		return ret;

	dbg("ohci-h5400: 0x%08x 0x%08x\n", (unsigned int)ohci->hcca, ohci->hcca_dma);

	if ((ret = ohci_run(ohci)) < 0) {
		ohci_err(ohci, "can't start %s\n", hcd->self.bus_name);
		ohci_stop(hcd);
		return ret;
        }

#ifdef DEBUG
	ohci_dump(ohci, 1);
#endif

	return 0;
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ohci_h5400_hc_driver = {
	.description =		hcd_name,
        .product_desc =         "h5400 OHCI",
        .hcd_priv_size =        sizeof(struct ohci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq =			ohci_irq,
	.flags =		HCD_USB11,

	/*
	 * basic lifecycle operations
	 */
	.start =		ohci_h5400_start,
#ifdef	CONFIG_PM
	/* suspend:		ohci_h5400_suspend,  -- tbd */
	/* resume:		ohci_h5400_resume,   -- tbd */
#endif
	.stop =			ohci_stop,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue =		ohci_urb_enqueue,
	.urb_dequeue =		ohci_urb_dequeue,
	.endpoint_disable =	ohci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number =	ohci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data =	ohci_hub_status_data,
	.hub_control =		ohci_hub_control,

        .start_port_reset =     ohci_start_port_reset,
};

/*-------------------------------------------------------------------------*/

static int ohci_h5400_probe(struct device *dev)
{
	struct usb_hcd *hcd = NULL;
	int ret;

	if (usb_disabled())
		return -ENODEV;

	ret = __ohci_h5400_probe(&ohci_h5400_hc_driver, &hcd, to_platform_device(dev));

	dev->driver_data = (void *)hcd;

	return ret;
}

static int ohci_h5400_remove(struct device *dev)
{
	struct usb_hcd *hcd = (struct usb_hcd *)dev->driver_data;

	__ohci_h5400_remove(hcd, to_platform_device(dev));

	hcd = NULL;

	return 0;
}

static platform_device_id h5400_usb_device_ids[] = { {IPAQ_SAMCOP_USBH_DEVICE_ID}, {0} };

static struct device_driver ohci_h5400_driver = {
	.name = "h5400-ohci",
	.probe = ohci_h5400_probe,
	.remove = ohci_h5400_remove
};

static int __init ohci_h5400_init(void)
{
	return driver_register(&ohci_h5400_driver);
}

static void __exit ohci_h5400_exit(void)
{
	driver_unregister(&ohci_h5400_driver);
}

module_init(ohci_h5400_init);
module_exit(ohci_h5400_exit);
