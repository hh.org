/*
 * HTC ASIC3 LED Timer Trigger
 * 
 * Copyright 2006 Anton Vorontsov <cbou@mail.ru>
 * Copyright 2005-2006 Openedhand Ltd.
 *
 * Original author: Richard Purdie <rpurdie@openedhand.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/device.h>
#include <linux/leds.h>
#include <linux/ctype.h>
#include <asm/hardware/asic3_leds.h>
#include "leds.h"

static ssize_t led_delay_on_show(struct class_device *dev, char *buf)
{
	struct led_classdev *led_cdev = class_get_devdata(dev);
	struct asic3_timer_data *timer_data = led_cdev->trigger_data;

	sprintf(buf, "%lu\n", timer_data->delay_on);

	return strlen(buf) + 1;
}

static ssize_t led_delay_on_store(struct class_device *dev, const char *buf,
				size_t size)
{
	struct led_classdev *led_cdev = class_get_devdata(dev);
	struct asic3_timer_data *timer_data = led_cdev->trigger_data;
	int ret = -EINVAL;
	char *after;
	unsigned long state = simple_strtoul(buf, &after, 10);
	size_t count = after - buf;

	if (*after && isspace(*after))
		count++;

	if (count == size) {
		timer_data->delay_on = state;
		if (!timer_data->delay_on || !timer_data->delay_off)
			led_set_brightness(led_cdev, LED_OFF);
		else
			led_set_brightness(led_cdev, LED_FULL);
		ret = count;
	}

	return ret;
}

static ssize_t led_delay_off_show(struct class_device *dev, char *buf)
{
	struct led_classdev *led_cdev = class_get_devdata(dev);
	struct asic3_timer_data *timer_data = led_cdev->trigger_data;

	sprintf(buf, "%lu\n", timer_data->delay_off);

	return strlen(buf) + 1;
}

static ssize_t led_delay_off_store(struct class_device *dev, const char *buf,
				size_t size)
{
	struct led_classdev *led_cdev = class_get_devdata(dev);
	struct asic3_timer_data *timer_data = led_cdev->trigger_data;
	int ret = -EINVAL;
	char *after;
	unsigned long state = simple_strtoul(buf, &after, 10);
	size_t count = after - buf;

	if (*after && isspace(*after))
		count++;

	if (count == size) {
		timer_data->delay_off = state;
		if (!timer_data->delay_on || !timer_data->delay_off)
			led_set_brightness(led_cdev, LED_OFF);
		else
			led_set_brightness(led_cdev, LED_FULL);
		ret = count;
	}

	return ret;
}

static CLASS_DEVICE_ATTR(delay_on, 0644, led_delay_on_show,
			led_delay_on_store);
static CLASS_DEVICE_ATTR(delay_off, 0644, led_delay_off_show,
			led_delay_off_store);

static void timer_trig_activate(struct led_classdev *led_cdev)
{
	struct asic3_timer_data *timer_data;

	timer_data = kzalloc(sizeof(struct asic3_timer_data), GFP_KERNEL);
	if (!timer_data) {
		led_cdev->trigger_data = NULL;
		return;
	}

	timer_data->delay_on = 1000;
	timer_data->delay_off = 1000;

	led_cdev->trigger_data = timer_data;

	class_device_create_file(led_cdev->class_dev,
				&class_device_attr_delay_on);
	class_device_create_file(led_cdev->class_dev,
				&class_device_attr_delay_off);
	return;
}

static void timer_trig_deactivate(struct led_classdev *led_cdev)
{
	struct asic3_timer_data *timer_data = led_cdev->trigger_data;

	if (timer_data) {
		class_device_remove_file(led_cdev->class_dev,
					&class_device_attr_delay_on);
		class_device_remove_file(led_cdev->class_dev,
					&class_device_attr_delay_off);
		kfree(timer_data);
	}

	led_cdev->trigger_data = NULL;
	return;
}

static int timer_trig_led_supported(struct led_classdev *led_cdev)
{
	if (!strcmp(led_cdev->class_dev->dev->driver->name, "asic3-leds"))
		return 1;
	return 0;
}

void led_trigger_register_asic3_timer(const char *name,
                                      struct led_trigger **trig)
{
	struct led_trigger *_trig;

	_trig = kzalloc(sizeof(struct led_trigger), GFP_KERNEL);
	if (_trig) {
		_trig->name = name;
		_trig->activate = timer_trig_activate;
		_trig->deactivate = timer_trig_deactivate;
		_trig->is_led_supported = timer_trig_led_supported;
		led_trigger_register(_trig);
	}
	*trig = _trig;

	return;
}

void led_trigger_unregister_asic3_timer(struct led_trigger *trig)
{
	led_trigger_unregister(trig);
	kfree(trig);
	return;
}

EXPORT_SYMBOL_GPL(led_trigger_register_asic3_timer);
EXPORT_SYMBOL_GPL(led_trigger_unregister_asic3_timer);

static struct led_trigger timer_led_trigger = {
	.name     = "timer-asic3",
	.activate = timer_trig_activate,
	.deactivate = timer_trig_deactivate,
	.is_led_supported = timer_trig_led_supported,
};

static int __init timer_trig_init(void)
{
	return led_trigger_register(&timer_led_trigger);
}

static void __exit timer_trig_exit(void)
{
	led_trigger_unregister(&timer_led_trigger);
}

module_init(timer_trig_init);
module_exit(timer_trig_exit);

MODULE_AUTHOR("Anton Vorontsov <cbou@mail.ru>");
MODULE_DESCRIPTION("HTC ASIC3 LED Timer Trigger");
MODULE_LICENSE("GPL");
