/*
 * HTC Magician LED driver
 *
 * Copyright (C) 2006 Philipp Zabel <philipp.zabel@gmail.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/leds.h>

#include <asm/io.h>
#include <asm/mach-types.h>
#include <asm/arch/magician.h>
#include <asm/arch/magician_cpld.h>

/*
 * mask for register 0x20,0x21,0x22
 */
#define LED_NUM0           0x04	/* mask red */
#define LED_NUM1           0x08	/* mask green */
#define LED_NUM0_NUM1      0x0c
#define LED_NUM2           0x40	/* mask blue */
#define LED_NUM0_NUM1_NUM2 0x4c

/*
 * bits in register 0x06
 */
#define LED_RED    0x01
#define LED_GREEN  0x02
#define LED_BLUE   0x04
#define LED_RED2   0x08		/* higher voltage --> brighter */
#define LED_GREEN2 0x10		/* brighter */
#define LED_BLUE2  0x20		/* same brightness as 0x04 */

/* wince uses 0x18 for the "charging" orange */

extern struct platform_device magician_cpld;

static void *iobase;

static int read_register(int reg)
{
	u16 *addr=((u16 *)(iobase+0x14));
	u8 *data=((u8 *)(iobase+0x18));

	*addr |= 0x80; /* read mode */
	*addr = (*addr & 0xff80) | (reg & 0x7f);

	barrier();
	return *data;
}

static void write_register(int reg, int val)
{
	u16 *addr=((u16 *)(iobase+0x14));
	u16 *data=((u16 *)(iobase+0x18));

	*addr = (*addr & 0xff80) | (reg & 0x7f);
	*addr &= 0xff7f; /* write mode */

	barrier();
	*data = (*data & 0xff00) | (val & 0xff);

}

static void magician_enable_led(int bits)
{
	int r20,r21,r22;

	r20 = read_register(0x20);
	r21 = read_register(0x21);
	r22 = read_register(0x22);

	r20 &= ~bits;

	write_register(0x20, r20);
	write_register(0x21, r21);
	write_register(0x22, r22);
}

static void magician_disable_led(int bits)
{
	int r20,r21,r22;

	r20 = read_register(0x20);
	r21 = read_register(0x21);
	r22 = read_register(0x22);

	r20 |= bits;
	r21 |= bits;	/* what are 0x21 and 0x22 good for? */
	r22 &= ~bits;	/* the blueangel driver doesn't seem to need them */

	write_register(0x20, r20);
	write_register(0x21, r21);
	write_register(0x22, r22);
}

static void magician_led_red_set(struct led_classdev *led_cdev,
					enum led_brightness value)
{
	int r6;

	r6 = read_register(0x06);
	if (value) {
		magician_enable_led(LED_NUM0);
		r6 |= LED_RED;
	} else {
		magician_disable_led(LED_NUM0);
		r6 &= ~LED_RED;
	}
	write_register(0x06, r6);
}

static void magician_led_green_set(struct led_classdev *led_cdev,
					enum led_brightness value)
{
	int r6;

	r6 = read_register(0x06);
	if (value) {
		magician_enable_led(LED_NUM1);
		r6 |= LED_GREEN2;
	} else {
		magician_disable_led(LED_NUM1);
		r6 &= ~LED_GREEN2;
	}
	write_register(0x06, r6);
}

static void magician_led_blue_set(struct led_classdev *led_cdev,
					enum led_brightness value)
{
	int r6;

	r6 = read_register(0x06);
	if (value) {
		magician_enable_led(LED_NUM2);
		r6 |= LED_BLUE;
	} else {
		magician_disable_led(LED_NUM2);
		r6 &= ~LED_BLUE;
	}
	write_register(0x06, r6);
}

static struct led_classdev magician_red_led = {
	.name			= "magician:red",
	.default_trigger	= "magician-charge",
	.brightness_set		= magician_led_red_set,
};

static struct led_classdev magician_green_led = {
	.name			= "magician:green",
/*	.default_trigger	= "magician-charge,radio", */
	.brightness_set		= magician_led_green_set,
};

static struct led_classdev magician_blue_led = {
	.name			= "magician:blue",
	.default_trigger	= "magician-bluetooth",
	.brightness_set		= magician_led_blue_set,
};

/*
 * red + green = amber/orange
 * red + blue = pink
 * blue + green = cyan
 * red + green + blue = pink
 */

#ifdef CONFIG_PM
static int magician_led_suspend(struct platform_device *dev, pm_message_t state)
{
#if 0 /* #ifdef CONFIG_LEDS_TRIGGERS */
	if (magician_amber_led.trigger && strcmp(magician_amber_led.trigger->name,
						"magician-charge"))
#endif
/*		led_classdev_suspend(&magician_amber_led); */

	led_classdev_suspend(&magician_red_led);
	led_classdev_suspend(&magician_green_led);
	led_classdev_suspend(&magician_blue_led);

	/* TODO: leave the blue led blinking if bt is enabled */
	/* TODO: leave the green led blinking if gsm is enabled */

	return 0;
}

static int magician_led_resume(struct platform_device *dev)
{
	led_classdev_resume(&magician_red_led);
	led_classdev_resume(&magician_green_led);
	led_classdev_resume(&magician_blue_led);
/*	led_classdev_resume(&magician_amber_led); */
	return 0;
}
#else
#define magician_led_suspend NULL
#define magician_led_resume NULL
#endif

static int magician_led_probe(struct platform_device *pdev)
{
	int ret;

	ret = led_classdev_register(&pdev->dev, &magician_red_led);
	if (ret < 0)
		goto red_err;

	ret = led_classdev_register(&pdev->dev, &magician_green_led);
	if (ret < 0)
		goto green_err;

	ret = led_classdev_register(&pdev->dev, &magician_blue_led);
	if (ret < 0)
		goto blue_err;

	magician_egpio_enable(&magician_cpld.dev, EGPIO_NR_MAGICIAN_LED_POWER);

	iobase = ioremap_nocache(0x08000000, 0x1000); // 'PASIC3'?

	write_register(0x00,0);	/* force on, these are duty time / cycle time registers */
	write_register(0x01,0);
	write_register(0x02,0); /* green on time, 0x10 = ~1s ((ms*16+500)/1000) */
	write_register(0x03,0); /* green off time, */
	write_register(0x04,0); /* blue duty time, (cycle = duty+off) */
	write_register(0x05,0); /* blue off time, */

	magician_disable_led(LED_NUM0_NUM1_NUM2);
	write_register(0x06, read_register(0x06) & ~0x3f);

	return ret;

blue_err:
	led_classdev_unregister(&magician_green_led);
green_err:
	led_classdev_unregister(&magician_red_led);
red_err:
	return ret;
}

static int magician_led_remove(struct platform_device *pdev)
{
	iounmap((void *)iobase);

	magician_egpio_disable(&magician_cpld.dev, EGPIO_NR_MAGICIAN_LED_POWER);

	led_classdev_unregister(&magician_blue_led);
	led_classdev_unregister(&magician_green_led);
	led_classdev_unregister(&magician_red_led);

	return 0;
}

static struct platform_driver magician_led_driver = {
	.probe		= magician_led_probe,
	.remove		= magician_led_remove,
	.suspend	= magician_led_suspend,
	.resume		= magician_led_resume,
	.driver		= {
		.name		= "magician-led",
	},
};

static int __init magician_led_init (void)
{
	return platform_driver_register(&magician_led_driver);
}

static void __exit magician_led_exit (void)
{
 	platform_driver_unregister(&magician_led_driver);
}

module_init (magician_led_init);
module_exit (magician_led_exit);

MODULE_AUTHOR("Philipp Zabel <philipp.zabel@gmail.com>");
MODULE_DESCRIPTION("HTC Magician LED driver");
MODULE_LICENSE("GPL");
