/*
 * WLAN (TI TNETW1100B) support in the Blueangel.
 *
 * Copyright (c) 2006 SDG Systems, LLC
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * 28-March-2006          Todd Blumer <todd@sdgsystems.com>
 */


#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/delay.h>

#include <asm/hardware.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/htcblueangel-gpio.h>
#include <asm/arch/htcblueangel-asic.h>
#include <asm/io.h>

#include "acx_hw.h"

#define WLAN_OFFSET     0x1000000
#define WLAN_BASE       (PXA_CS5_PHYS+WLAN_OFFSET)
//#define WLAN_BASE	0x2c000000


extern struct platform_device blueangel_asic3;

static int
htcblueangel_wlan_start( void )
{
	disable_irq(IRQ_GPIO(17));
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOA_WLAN_RESET, 1);
	mdelay(5);
	asic3_set_gpio_out_a (&blueangel_asic3.dev, GPIOA_WLAN_PWR1, GPIOA_WLAN_PWR1);
	mdelay(100);
	asic3_set_gpio_out_a (&blueangel_asic3.dev, GPIOA_WLAN_PWR2, GPIOA_WLAN_PWR2);
	mdelay(150);
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOB_WLAN_PWR3, GPIOB_WLAN_PWR3);
	mdelay(100);
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOA_WLAN_RESET, 0);
	mdelay(100);
	enable_irq(IRQ_GPIO(17));
	return 0;
}

static int
htcblueangel_wlan_stop( void )
{
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOA_WLAN_RESET, 0);
	mdelay(50);
	asic3_set_gpio_out_a (&blueangel_asic3.dev, GPIOA_WLAN_PWR1, 0);
	asic3_set_gpio_out_a (&blueangel_asic3.dev, GPIOA_WLAN_PWR2, 0);
	asic3_set_gpio_out_b (&blueangel_asic3.dev, GPIOB_WLAN_PWR3, 0);
	disable_irq(IRQ_GPIO(17));
	return 0;
}

static struct resource acx_resources[] = {
        [0] = {
                .start  = WLAN_BASE,
                .end    = WLAN_BASE + 0x20,
                .flags  = IORESOURCE_MEM,
        },
        [1] = {
                .start  = IRQ_GPIO(17),
                .end    = IRQ_GPIO(17),
                .flags  = IORESOURCE_IRQ,
        },
};

static struct acx_hardware_data acx_data = {
	.start_hw	= htcblueangel_wlan_start,
	.stop_hw	= htcblueangel_wlan_stop,
};

static struct platform_device acx_device = {
	.name	= "acx-mem",
	.dev	= {
		.platform_data = &acx_data,
	},
	.num_resources	= ARRAY_SIZE( acx_resources ),
	.resource	= acx_resources,
};

static int __init
htcblueangel_wlan_init( void )
{
	printk( "htcblueangel_wlan_init: acx-mem platform_device_register\n" );
	return platform_device_register( &acx_device );
}


static void __exit
htcblueangel_wlan_exit( void )
{
	platform_device_unregister( &acx_device );
}

module_init( htcblueangel_wlan_init );
module_exit( htcblueangel_wlan_exit );

MODULE_AUTHOR( "Todd Blumer <todd@sdgsystems.com>" );
MODULE_DESCRIPTION( "WLAN driver for HTC Blueangel" );
MODULE_LICENSE( "GPL" );

