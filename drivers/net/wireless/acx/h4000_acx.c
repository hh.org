/*
 * WLAN (TI TNETW1100B) support in the h4000.
 *
 * Copyright (c) 2006 SDG Systems, LLC
 * Copyright (c) 2006 Paul Sokolovsky
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * Based on hx4700_acx.c
 */


#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/delay.h>

#include <asm/hardware.h>

#include <asm/arch/pxa-regs.h>
#include <asm/arch/h4000-gpio.h>
#include <asm/arch/h4000-asic.h>
#include <linux/soc/asic3_base.h>
//#include <asm/arch/ipaq-asic3.h>
#include <asm/io.h>
#include "acx_hw.h"

// Mem area of PCMCIA socket #0
#define WLAN_BASE	0x2c000000

extern struct platform_device h4000_asic3;

static int
h4000_wlan_start( void )
{
	printk("h4000_wlan_start\n");
        asic3_set_gpio_out_c(&h4000_asic3.dev, GPIOC_WLAN_POWER_ON, GPIOC_WLAN_POWER_ON);
        mdelay(100);

        asic3_set_gpio_out_d(&h4000_asic3.dev, GPIOD_WLAN_MAC_RESET, GPIOD_WLAN_MAC_RESET);
        mdelay(100);
        asic3_set_gpio_out_d(&h4000_asic3.dev, GPIOD_WLAN_MAC_RESET, 0);
        mdelay(100);

	return 0;
}

static int
h4000_wlan_stop( void )
{
	printk("h4000_wlan_stop\n");
        asic3_set_gpio_out_c(&h4000_asic3.dev, GPIOC_WLAN_POWER_ON, 0);
	return 0;
}

static struct resource acx_resources[] = {
	[0] = {
		.start	= WLAN_BASE,
		.end	= WLAN_BASE + 0x20,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= H4000_IRQ(WLAN_MAC_IRQ_N),
		.end	= H4000_IRQ(WLAN_MAC_IRQ_N),
		.flags	= IORESOURCE_IRQ,
	},
};

static struct acx_hardware_data acx_data = {
	.start_hw	= h4000_wlan_start,
	.stop_hw	= h4000_wlan_stop,
};

static struct platform_device acx_device = {
	.name	= "acx-mem",
	.dev	= {
		.platform_data = &acx_data,
	},
	.num_resources	= ARRAY_SIZE( acx_resources ),
	.resource	= acx_resources,
};

static int __init
h4000_wlan_init( void )
{
	printk( "h4000_wlan_init: acx-mem platform_device_register\n" );
	return platform_device_register( &acx_device );
}


static void __exit
h4000_wlan_exit( void )
{
	platform_device_unregister( &acx_device );
}

module_init( h4000_wlan_init );
module_exit( h4000_wlan_exit );

MODULE_AUTHOR( "Paul Sokolovsky <pfalcon@handhelds.org>" );
MODULE_DESCRIPTION( "WLAN driver for iPAQ h4000" );
MODULE_LICENSE( "GPL" );
